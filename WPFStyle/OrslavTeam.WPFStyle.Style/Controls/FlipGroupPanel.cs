﻿using System;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace OrslavTeam.WPFStyle.Style.Controls
{
    [TemplateVisualState(Name = "Normal", GroupName = "ViewState")]
    [TemplateVisualState(Name = "Flipped", GroupName = "ViewState")]
    [TemplatePart(Name = "PART_FrontContent", Type = typeof(object))]
    [TemplatePart(Name = "PART_BackContent", Type = typeof(object))]
    public class FlipGroupPanel : Control
    {
        private FrameworkElement _backControl = null;
        
        static FlipGroupPanel()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(FlipGroupPanel),
                new FrameworkPropertyMetadata(typeof(FlipGroupPanel)));
        }
        
        public static readonly DependencyProperty FrontContentProperty = DependencyProperty.Register(
            "FrontContent", typeof(object), typeof(FlipGroupPanel), null);

        public object FrontContent
        {
            get => GetValue(FrontContentProperty);
            set => SetValue(FrontContentProperty, value);
        }

        public static readonly DependencyProperty BackContentsProperty = DependencyProperty.Register(
            "BackContents", typeof(object), typeof(FlipGroupPanel), null);

        public object BackContents
        {
            get => GetValue(BackContentsProperty);
            set => SetValue(BackContentsProperty, value);
        }

        public static readonly DependencyProperty IsFlippedProperty = DependencyProperty.Register(
            "IsFlipped", typeof(bool), typeof(FlipGroupPanel), null);

        public bool IsFlipped
        {
            get => (bool) GetValue(IsFlippedProperty);
            set
            {
                SetValue(IsFlippedProperty, value);
                ChangeVisualState(false);
            }
        }

        private void ChangeVisualState(bool useTransition)
        {
            VisualStateManager.GoToState(this, IsFlipped ? "Flipped" : "Normal", useTransition);
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            FrameworkElement frontControl = GetTemplateChild("PART_FrontContent") as FrameworkElement;
            _backControl = GetTemplateChild("PART_BackContent") as FrameworkElement;

            frontControl?.AddHandler(ButtonBase.ClickEvent, new RoutedEventHandler(FlipToBack_Click));
            _backControl?.AddHandler(ButtonBase.ClickEvent, new RoutedEventHandler(FlipToTop_Click));
            IsFlipped = false;
        }

        private void FlipToTop_Click(object sender, RoutedEventArgs e)
        {
            if (!(e.OriginalSource is Button)) return;
            IsFlipped = false;
            ChangeVisualState(true);
        }

        private void FlipToBack_Click(object sender, RoutedEventArgs e)
        {
            if (!(e.OriginalSource is Button || e.OriginalSource is ButtonWithCheck)) return;
            //_backControl.DataContext = ((Button) e.OriginalSource).DataContext;
            IsFlipped = true;
            ChangeVisualState(true);
        }
    }
}