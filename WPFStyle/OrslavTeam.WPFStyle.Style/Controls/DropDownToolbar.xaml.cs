﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace OrslavTeam.WPFStyle.Style.Controls
{
    [TemplateVisualState(Name = "Normal", GroupName = "CommonStates")]
    [TemplateVisualState(Name = "Open", GroupName = "CommonStates")]
    [TemplatePart(Name = "PART_Header", Type = typeof(object))]
    [TemplatePart(Name = "PART_Toggle", Type = typeof(ToggleButton))]
    [TemplatePart(Name = "PART_Content", Type = typeof(object))]
    public class DropDownToolbar : Control
    {
        #region Properties

        public static readonly DependencyProperty HeaderProperty =
            DependencyProperty.Register(
                "Header",
                typeof(string),
                typeof(DropDownToolbar),
                null);

        public static readonly DependencyProperty ContentProperty =
            DependencyProperty.Register(
                "Content",
                typeof(FrameworkElement),
                typeof(DropDownToolbar),
                null);

        public static readonly DependencyProperty IsOpenProperty =
            DependencyProperty.Register(
                "IsOpen",
                typeof(bool),
                typeof(DropDownToolbar),
                null);

        #endregion

        #region PropertyAccess

        public string Header
        {
            get => (string) GetValue(HeaderProperty);
            set => SetValue(HeaderProperty, value);
        }

        public FrameworkElement Content
        {
            get => (FrameworkElement) GetValue(ContentProperty);
            set => SetValue(ContentProperty, value);
        }

        public bool IsOpen
        {
            get => (bool) GetValue(IsOpenProperty);
            set => SetValue(IsOpenProperty, value);
        }

        #endregion

        #region Constructor

        static DropDownToolbar()
        {
            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(DropDownToolbar),
                new FrameworkPropertyMetadata(typeof(DropDownToolbar)));
        }

        #endregion

        private void ChangeVisualState(bool useTransition)
        {
            VisualStateManager.GoToState(this, IsOpen ? "Open" : "Normal", useTransition);
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            if (GetTemplateChild("PART_Toggle") is ToggleButton checker)
                checker.Click += (sender, args) =>
                {

                    IsOpen = checker.IsChecked.Value;
                    ChangeVisualState(true);
                };
        }
    }
}