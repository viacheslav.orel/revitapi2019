﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace OrslavTeam.WPFStyle.Style.Controls
{
    [TemplateVisualState(Name = "Normal", GroupName = "CommonStates")]
    [TemplateVisualState(Name = "MouseOver", GroupName = "CommonStates")]
    [TemplateVisualState(Name = "Pressed", GroupName = "CommonStates")]
    [TemplateVisualState(Name = "Checked", GroupName = "CheckStates")]
    [TemplateVisualState(Name = "Unchecked", GroupName = "CheckStates")]
    [TemplatePart(Name = "BtnPart", Type = typeof(Border))]
    [TemplatePart(Name = "CheckPart", Type = typeof(Border))]
    public class ButtonWithCheck : Control
    {
        #region Propertis

        public static readonly DependencyProperty ButtonContentProperty =
            DependencyProperty.Register(
                "ButtonContent",
                typeof(string),
                typeof(ButtonWithCheck),
                null
            );

        public static readonly DependencyProperty ButtonCommandProperty =
            DependencyProperty.Register(
                "ButtonCommand",
                typeof(ICommand),
                typeof(ButtonWithCheck),
                null
            );

        public static readonly DependencyProperty IsCheckedProperty =
            DependencyProperty.Register(
                "IsChecked",
                typeof(bool),
                typeof(ButtonWithCheck),
                null
            );

        public static readonly DependencyProperty CommandParameterProperty =
            DependencyProperty.Register(
                "CommandParameter",
                typeof(object),
                typeof(ButtonWithCheck),
                null
            );

        #endregion

        #region PropertisAccess

        public string ButtonContent
        {
            get => (string)GetValue(ButtonContentProperty);
            set => SetValue(ButtonContentProperty, value);
        }

        public ICommand ButtonCommand
        {
            get => (ICommand)GetValue(ButtonCommandProperty);
            set => SetValue(ButtonCommandProperty, value);
        }

        public bool IsChecked
        {
            get => (bool)GetValue(IsCheckedProperty);
            set
            {
                SetValue(IsCheckedProperty, value);
                VisualStateManager.GoToState(this, value ? "Checked" : "Unchecked", false);
            }
        }

        public object CommandParameter
        {
            get => GetValue(CommandParameterProperty);
            set => SetValue(CommandParameterProperty, value);
        }

        #endregion

        static ButtonWithCheck()
        {
            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(ButtonWithCheck),
                new FrameworkPropertyMetadata(typeof(ButtonWithCheck))
            );
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            MouseEnter += (sender, args) =>
            {
                VisualStateManager.GoToState(this, "MouseOver", true);
            };
            MouseLeave += (sender, args) =>
            {
                VisualStateManager.GoToState(this, "Normal", true);
            };


            MouseLeftButtonDown += (sender, args) =>
            {
                if (!IsChecked || ((FrameworkElement)args.OriginalSource).Name != "ClickEffect") return;

                VisualStateManager.GoToState(this, "Pressed", true);
                RaiseEvent(new RoutedEventArgs(ButtonBase.ClickEvent));
                ButtonCommand?.Execute(CommandParameter);
            };

            if (GetTemplateChild("ClickStoryboard") is Storyboard clickAnimation)
                clickAnimation.Completed += (sender, args) => VisualStateManager.GoToState(this, "Normal", false);

            if ((GetTemplateChild("CheckPart") is Border checkControl))
            {
                bool mouseDown = false;

                checkControl.MouseLeftButtonDown += (sender, args) => mouseDown = true;
                checkControl.MouseLeftButtonUp += (sender, args) =>
                {
                    if (!mouseDown) return;
                    IsChecked = !IsChecked;
                    mouseDown = false;

                    VisualStateManager.GoToState(this, IsChecked ? "Checked" : "Unchecked", true);
                };
                checkControl.MouseLeave += (sender, args) => mouseDown = false;
            }
        }
    }
}
