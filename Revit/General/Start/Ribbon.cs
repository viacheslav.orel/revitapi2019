﻿using System;
using System.Windows.Media.Imaging;
using Autodesk.Revit.UI;
using OrslavTeam.Revit.Axillary.PublicInformation;

namespace OrslavTeam.Revit.General.Start
{
    public class Ribbon: IExternalApplication
    {
        public Result OnStartup(UIControlledApplication application)
        {
            if (System.Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties().DomainName.ToLower() ==
                "adept-group.biz") return Result.Succeeded;
            
            #region inputParameter
            
            const string generalTabName = "otGeneral";
            
            #endregion

            //Create tab
            application.CreateRibbonTab(generalTabName);
            
            //Create panels
            RibbonPanel openingPanel = application.CreateRibbonPanel(generalTabName, "Прорізи");
            RibbonPanel servicePanel = application.CreateRibbonPanel(generalTabName, "Службові");

            #region Create EmbrasureElevation button

            //ButtonData
            var embrasureElevationOneByOne = new PushButtonData("setElevationFromEmbrasure",
                "Відмітка прорізу\n(по одній)",
                PublicParameter.FilePath + "OrslavTeam.Revit.General.Embrasure.dll",
                "OrslavTeam.Revit.General.Embrasure.ElevationOneByOne")
            {
                LargeImage = new BitmapImage(new Uri(PublicParameter.FilePath + "res\\General\\btn_ico\\ico_ElevationEmbrasure_OneByOne.png")),
                ToolTip = "Після запуску оберіть проріз",
                LongDescription = "Для прямокутних прорізів вноситься відмітка низа прорізу.\nДля круглих прорізів вноситься відмітка центру прорізу"
            };
            
            var embrasureElevationVisibleInView = new PushButtonData("setElevationFromEmbrasureInView",
                "Відмітка прорізу\n(на виді)",
                PublicParameter.FilePath + "OrslavTeam.Revit.General.Embrasure.dll",
                "OrslavTeam.Revit.General.Embrasure.ElevationInView")
            {
                LargeImage = new BitmapImage(new Uri(PublicParameter.FilePath + "res\\General\\btn_ico\\ico_ElevationEmbrasure_InView.png")),
                ToolTip = "Тільки для прорізів на поточному виді",
                LongDescription = "Для прямокутних прорізів вноситься відмітка низа прорізу."
                                  + "\nДля круглих прорізів вноситься відмітка центру прорізу"
                                  + "\nЗверніть увагу що усі прорізи мають бути доступні для редагування"
            };
            
            var embrasureElevationInProject = new PushButtonData("setElevationFromEmbrasureInProject",
                "Відмітка прорізу\n(у моделі)",
                PublicParameter.FilePath + "OrslavTeam.Revit.General.Embrasure.dll",
                "OrslavTeam.Revit.General.Embrasure.ElevationInProject")
            {
                LargeImage = new BitmapImage(new Uri(PublicParameter.FilePath + "res\\General\\btn_ico\\ico_ElevationEmbrasure_All.png")),
                ToolTip = "Для прорізів у всій моделі",
                LongDescription = "Для прямокутних прорізів вноситься відмітка низа прорізу."
                                  + "\nДля круглих прорізів вноситься відмітка центру прорізу"
                                  + "\nЗверніть увагу що усі прорізи мають бути доступні для редагування"
            };
            
            //Create button
            var embrasureElevation =
                openingPanel.AddItem(new SplitButtonData("embrasureElevation", "embrasureElevationSplit")) as
                    SplitButton;
            // ReSharper disable once PossibleNullReferenceException
            embrasureElevation.AddPushButton(embrasureElevationOneByOne);
            embrasureElevation.AddPushButton(embrasureElevationVisibleInView);
            embrasureElevation.AddPushButton(embrasureElevationInProject);

            #endregion

            #region Opening mark

            //Create button data
            var openingMarkInViewData = new PushButtonData("setMarkForOpeningByOneLeyer",
                "Марка прорізу\n(на виді)",
                PublicParameter.FilePath + "OrslavTeam.Revit.General.Embrasure.dll",
                "OrslavTeam.Revit.General.Embrasure.MarkEmbrasureByView")
            {
                ToolTip = "Для прорізів на одному виді",
                LongDescription = "Використовувати для маркування всіх прорізів на виді.",
                LargeImage = new BitmapImage(new Uri(PublicParameter.FilePath + "res\\General\\btn_ico\\ico_EmbrasureMark.png"))
            };
            
            //Create button
            openingPanel.AddItem(openingMarkInViewData);

            #endregion

            #region Mountion

            //Create button data
            var mountingElevationData = new PushButtonData("mountingElevation",
                "Монтажний\nрівень",
                PublicParameter.FilePath + "OrslavTeam.Revit.General.MountingElevation.dll",
                "OrslavTeam.Revit.General.MountingElevation.SetMountingElevation")
            {
                ToolTip = "Призначення монтажного рівня для елементів на виді",
                LargeImage = new BitmapImage(new Uri(PublicParameter.FilePath + "res\\General\\btn_ico\\ico_MountingLevel.png"))
            };
            
            //Create button
            servicePanel.AddItem(mountingElevationData);

            #endregion

            return Result.Succeeded;
        }

        public Result OnShutdown(UIControlledApplication application)
        {
            return Result.Succeeded;
        }
    }
}