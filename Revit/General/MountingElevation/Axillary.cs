﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autodesk.Revit.DB;
using OrslavTeam.Revit.Axillary.PublicInformation;

namespace OrslavTeam.Revit.General.MountingElevation
{
    public class Axillary
    {
        public static void SetMountingElevationHostOrLevel(Element element, List<ElementId> levels,
            ref List<string> changeList, ref List<string> notChangeList, ref List<string> errorList)
        {
            ElementId hostLevel = element.LevelId ?? ((FamilyInstance) element).Host.Id;
            int levelsLength = levels.Count();

            for (int i = 0; i < levelsLength; i++)
            {
                if (hostLevel != levels[i]) continue;
                
                if (PublicParameter.MountingLevel(element) == (i + 1) * 100)
                {
                    notChangeList.Add(element.Id.ToString());
                }
                else
                {
                    try
                    {
                        PublicParameter.MountingLevel(element, (i + 1) * 100);
                        changeList.Add(element.Id.ToString());
                    }
                    catch (Exception)
                    {
                        errorList.Add(element.Id.ToString());
                    }
                }

                return;
            }
        }

        public static void SetMountingElevationByReferenceLevel(Element element, List<ElementId> levels,
            ref List<string> changeList, ref List<string> notChangeList, ref List<string> errorList)
        {
            ElementId hostLevel = element.get_Parameter(BuiltInParameter.INSTANCE_REFERENCE_LEVEL_PARAM).AsElementId();
            for (int i = 0; i < levels.Count(); i++)
            {
                if (hostLevel != levels[i]) continue;
                
                if (PublicParameter.MountingLevel(element) == (i + 1) * 100)
                {
                    notChangeList.Add(element.Id.ToString());
                }
                else
                {
                    try
                    {
                        PublicParameter.MountingLevel(element, (i + 1) * 100);
                        changeList.Add(element.Id.ToString());
                    }
                    catch (Exception)
                    {
                        errorList.Add(element.Id.ToString());
                    }
                }

                return;
            }
        }
    }
}