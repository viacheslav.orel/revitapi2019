﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using OrslavTeam.Revit.Axillary.PublicInformation;
using OrslavTeam.Revit.Axillary.ViewTemplate.ReportWindow;

namespace OrslavTeam.Revit.General.MountingElevation
{
    [Transaction(mode: TransactionMode.Manual)]
    public class SetMountingElevation : IExternalCommand
    {
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            List<string> changedElements = new List<string>() {"Змінені елементи:"};
            List<string> dontChangedElements = new List<string>() {"Пропущені елементи:"};
            List<string> errorChangedElements = new List<string>() {"Помилки запису:"};
            int countElement = 0;

            Document doc = commandData.Application.ActiveUIDocument.Document;
            FilteredElementCollector levels = new FilteredElementCollector(doc, doc.ActiveView.Id)
                .OfCategory(BuiltInCategory.OST_Levels).OfClass(typeof(Level));
            if (!levels.Any())
            {
                MessageView messageView =
                    new MessageView(
                        "На поточному виді рівнів не виявлено.\nСпробуйте змінити налаштування виду або виберіть інший вид.");
                messageView.ShowDialog();
            }
            else
            {
                List<ElementId> levelsList =
                    (from lev in levels orderby lev.get_Parameter(BuiltInParameter.LEVEL_ELEV).AsDouble() select lev.Id)
                    .ToList();
                int levelsCount = levelsList.Count();

                //Select all element in view
                FilteredElementCollector structuralFoundationInView =
                    new FilteredElementCollector(doc, doc.ActiveView.Id).OfCategory(BuiltInCategory
                        .OST_StructuralFoundation);
                FilteredElementCollector floorsInView =
                    new FilteredElementCollector(doc, doc.ActiveView.Id).OfCategory(BuiltInCategory.OST_Floors);
                FilteredElementCollector wallInView =
                    new FilteredElementCollector(doc, doc.ActiveView.Id).OfCategory(BuiltInCategory.OST_Walls);
                FilteredElementCollector structuralColumnsInView =
                    new FilteredElementCollector(doc, doc.ActiveView.Id).OfCategory(BuiltInCategory
                        .OST_StructuralColumns);
                FilteredElementCollector structuralFramingInView =
                    new FilteredElementCollector(doc, doc.ActiveView.Id).OfCategory(BuiltInCategory
                        .OST_StructuralFraming);
                FilteredElementCollector stairsInView =
                    new FilteredElementCollector(doc, doc.ActiveView.Id).OfCategory(BuiltInCategory.OST_Stairs);
                FilteredElementCollector windowsInView =
                    new FilteredElementCollector(doc, doc.ActiveView.Id).OfCategory(BuiltInCategory.OST_Windows);
                FilteredElementCollector doorsInView =
                    new FilteredElementCollector(doc, doc.ActiveView.Id).OfCategory(BuiltInCategory.OST_Doors);
                FilteredElementCollector genericModelInView =
                    new FilteredElementCollector(doc, doc.ActiveView.Id).OfCategory(BuiltInCategory.OST_GenericModel);
                FilteredElementCollector assemblyInView =
                    new FilteredElementCollector(doc, doc.ActiveView.Id).OfCategory(BuiltInCategory.OST_Assemblies);

                using (Transaction tr = new Transaction(doc, "SetMountingElevation"))
                {
                    tr.Start();

                    //Structural foundation
                    foreach (Element element in structuralFoundationInView)
                    {
                        Axillary.SetMountingElevationHostOrLevel(element, levelsList, ref changedElements,
                            ref dontChangedElements, ref errorChangedElements);
                        countElement++;
                    }

                    //Floors
                    foreach (Element element in floorsInView)
                    {
                        Axillary.SetMountingElevationHostOrLevel(element, levelsList, ref changedElements,
                            ref dontChangedElements, ref errorChangedElements);
                        countElement++;
                    }

                    //Walls
                    foreach (Element element in wallInView)
                    {
                        Axillary.SetMountingElevationHostOrLevel(element, levelsList, ref changedElements,
                            ref dontChangedElements, ref errorChangedElements);
                        countElement++;
                    }

                    //Structural columns
                    foreach (Element element in structuralColumnsInView)
                    {
                        Axillary.SetMountingElevationHostOrLevel(element, levelsList, ref changedElements,
                            ref dontChangedElements, ref errorChangedElements);
                        countElement++;
                    }

                    //Structural framing
                    foreach (Element element in structuralFramingInView)
                    {
                        Axillary.SetMountingElevationByReferenceLevel(element, levelsList, ref changedElements,
                            ref dontChangedElements, ref errorChangedElements);
                        countElement++;
                    }

                    //Stairs
                    foreach (Element element in stairsInView)
                    {
                        ElementId hostLevel = element.get_Parameter(BuiltInParameter.STAIRS_BASE_LEVEL_PARAM)
                            .AsElementId();
                        for (int i = 0; i < levelsCount; i++)
                        {
                            if (hostLevel != levelsList[i]) continue;
                            
                            if (PublicParameter.MountingLevel(element) == (i + 1) * 100)
                            {
                                dontChangedElements.Add(element.Id.ToString());
                            }
                            else
                            {
                                try
                                {
                                    PublicParameter.MountingLevel(element, (i + 1) * 100);
                                    changedElements.Add(element.Id.ToString());
                                }
                                catch (Exception)
                                {
                                    errorChangedElements.Add(element.Id.ToString());
                                }
                            }

                            countElement++;
                            break;
                        }
                    }

                    //Windows
                    foreach (Element element in windowsInView)
                    {
                        Axillary.SetMountingElevationHostOrLevel(element, levelsList, ref changedElements,
                            ref dontChangedElements, ref errorChangedElements);
                        countElement++;
                    }

                    //Doors
                    foreach (Element element in doorsInView)
                    {
                        Axillary.SetMountingElevationHostOrLevel(element, levelsList, ref changedElements,
                            ref dontChangedElements, ref errorChangedElements);
                        countElement++;
                    }

                    //Generic models
                    foreach (Element element in genericModelInView)
                    {
                        Axillary.SetMountingElevationHostOrLevel(element, levelsList, ref changedElements,
                            ref dontChangedElements, ref errorChangedElements);
                        countElement++;
                    }

                    //Assemblies
                    foreach (Element element in assemblyInView)
                    {
                        int assemblyMounting = PublicParameter.MountingLevel(element);
                        int elementMounting =
                            PublicParameter.MountingLevel(
                                doc.GetElement(((AssemblyInstance) element).GetMemberIds().First()));
                        if (assemblyMounting == elementMounting)
                        {
                            dontChangedElements.Add(element.Id.ToString());
                        }
                        else
                        {
                            PublicParameter.MountingLevel(element, elementMounting);
                        }

                        countElement++;
                    }

                    tr.Commit();
                }

                ReportView reportView = new ReportView(countElement, changedElements, dontChangedElements,
                    errorChangedElements);
                reportView.Show();
            }

            return Result.Succeeded;
        }
    }
}