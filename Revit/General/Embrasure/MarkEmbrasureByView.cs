﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using OrslavTeam.Revit.Axillary.PublicInformation;
using OrslavTeam.Revit.Axillary.ViewTemplate.ReportWindow;

namespace OrslavTeam.Revit.General.Embrasure
{
    [Transaction(TransactionMode.Manual)]
    public class MarkEmbrasureByView : IExternalCommand
    {
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            List<string> changedElements = new List<string>() { "Змінені елементи:" };
            List<string> dontChangedElements = new List<string>() { "Пропущені елементи:" };
            List<string> errorChangedElements = new List<string>() { "Помилки запису:" };
            int embrasureCount = 0;

            int numOfOpening = 1;

            Document doc = commandData.Application.ActiveUIDocument.Document;

            IEnumerable<Element> openingInFloor = Axillary.GetOpeningInFloor(doc, doc.ActiveView.Id);
            IEnumerable<Element> inFloor = openingInFloor as Element[] ?? openingInFloor.ToArray();
            int countOpeningInFloor = inFloor.Count();
            IEnumerable<Element> openingInWall = Axillary.GetOpeningInWall(doc, doc.ActiveView.Id);
            IEnumerable<Element> inWall = openingInWall as Element[] ?? openingInWall.ToArray();
            int countOpeningInWall = inWall.Count();
            IEnumerable<Element> openingByDoor = Axillary.GetDoorsOpening(doc, doc.ActiveView.Id);
            IEnumerable<Element> byDoor = openingByDoor as Element[] ?? openingByDoor.ToArray();
            int countOpeningByDoor = byDoor.Count();

            if (countOpeningInFloor == 0 && countOpeningInWall == 0 && countOpeningByDoor == 0)
            {
                MessageView messageView = new MessageView("На виді прорізів не виявлено");
                messageView.ShowDialog();
                return Result.Cancelled;
            }
            else
            {
                Transaction tr = new Transaction(doc, "setOpeningMarkInView");
                tr.Start();

                if (countOpeningByDoor != 0)
                {
                    var sortedOpeningByDoor = from element in byDoor
                                              let dimensionsHeight = PublicParameter.DimensionsHeight(element)
                                              let dimensionsWidth = PublicParameter.DimensionsWidth(element)
                                              group element by new { dimensionsHeight, dimensionsWidth };
                    foreach (var group in sortedOpeningByDoor)
                    {
                        string mark = $"От{numOfOpening}";
                        foreach (var groupItem in group)
                        {
                            try
                            {
                                PublicParameter.MarkText(groupItem, mark);
                                changedElements.Add(groupItem.Id.ToString());
                            }
                            catch (Exception)
                            {
                                errorChangedElements.Add(groupItem.Id.ToString());
                            }
                            embrasureCount++;
                        }
                        numOfOpening++;
                    }
                }
                if (countOpeningInWall != 0)
                {
                    var sortedOpeningInWall = from element in inWall
                                              let designDiscipline = PublicParameter.DisignDiscipline(((FamilyInstance) element).Symbol)
                                              let dimensionsH = PublicParameter.DimensionsH(element)
                                              let dimensionsA = PublicParameter.DimensionsA(element)
                                              let dimensionsD = PublicParameter.DimensionsD(element)
                                              let lvl = PublicParameter.ElevationTextValue(element)
                                              orderby designDiscipline
                                              group element by new { designDiscipilene = designDiscipline, dimensionsH, dimensionsA, dimensionsD, lvl };

                    foreach (var group in sortedOpeningInWall)
                    {
                        string mark = $"От{numOfOpening}";
                        foreach (var groupItem in group)
                        {
                            try
                            {
                                PublicParameter.MarkText(groupItem, mark);
                                changedElements.Add(groupItem.Id.ToString());
                            }
                            catch (Exception)
                            {
                                errorChangedElements.Add(groupItem.Id.ToString());
                            }
                            embrasureCount++;
                        }
                        numOfOpening++;
                    }
                }
                if (countOpeningInFloor != 0)
                {
                    var sortedOpeningInFloor = from element in inFloor
                                               let designDiscipilene = PublicParameter.DisignDiscipline(element)
                                               let dimensionsB = PublicParameter.DimensionsB(element)
                                               let dimensionsA = PublicParameter.DimensionsA(element)
                                               let dimensionsD = PublicParameter.DimensionsD(element)
                                               orderby designDiscipilene
                                               group element by new { designDiscipilene, dimensionsB, dimensionsA, dimensionsD };

                    foreach (var group in sortedOpeningInFloor)
                    {
                        string mark = $"От{numOfOpening}";
                        foreach (var groupItem in group)
                        {
                            try
                            {
                                PublicParameter.MarkText(groupItem, mark);
                                changedElements.Add(groupItem.Id.ToString());
                            }
                            catch (Exception)
                            {
                                errorChangedElements.Add(groupItem.Id.ToString());
                            }
                            embrasureCount++;
                        }
                        numOfOpening++;
                    }
                }

                tr.Commit();
            }

            ReportView reportView = new ReportView(embrasureCount, changedElements, dontChangedElements, errorChangedElements);
            reportView.Show();

            return Result.Succeeded;
        }
    }
}