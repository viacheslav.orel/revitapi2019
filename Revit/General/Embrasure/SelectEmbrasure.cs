﻿using Autodesk.Revit.DB;
using Autodesk.Revit.UI.Selection;

namespace OrslavTeam.Revit.General.Embrasure
{
    public class SelectEmbrasure : ISelectionFilter
    {
        public bool AllowElement(Element elem)
        {
            return Axillary.IsEmbrasure(elem);
        }

        public bool AllowReference(Reference reference, XYZ position)
        {
            return false;
        }
    }
}