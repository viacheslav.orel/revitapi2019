﻿using System;
using System.Collections.Generic;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using Autodesk.Revit.UI.Selection;

namespace OrslavTeam.Revit.General.Embrasure
{
    [Transaction(TransactionMode.Manual)]
    public class ElevationOneByOne : IExternalCommand
    {
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            UIApplication uiApp = commandData.Application;
            Document doc = uiApp.ActiveUIDocument.Document;
            Selection sel = uiApp.ActiveUIDocument.Selection;
            SelectEmbrasure embrasureSel = new SelectEmbrasure();
            ICollection<ElementId> selEmbrasure = sel.GetElementIds();
            if (selEmbrasure.Count > 0)
            {
                using (Transaction tr = new Transaction(doc, "EmbrasureElevation"))
                {
                    tr.Start();
                    foreach (ElementId elId in selEmbrasure)
                    {
                        Element el = doc.GetElement(elId);
                        if (Axillary.IsEmbrasure(el))
                        {
                            Axillary.EmbrasureThis(el);
                        }
                    }
                    tr.Commit();
                }
            }
            else
            {
                do
                {
                    Reference pickedEmbrasure;
                    try
                    {
                        pickedEmbrasure = sel.PickObject(ObjectType.Element, embrasureSel, "Оберіть проріз у стіні");
                    }
                    catch (Exception)
                    {
                        return Result.Succeeded;
                    }
                    Element el = doc.GetElement(pickedEmbrasure);
                    using (Transaction tr = new Transaction(doc, "EmbrasureElevation"))
                    {
                        tr.Start();
                        Axillary.EmbrasureThis(el);
                        tr.Commit();
                    }
                } while (true);
            }
            return Result.Succeeded;
        }
    }
}