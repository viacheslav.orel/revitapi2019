﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Autodesk.Revit.DB;
using OrslavTeam.Revit.Axillary.PublicInformation;

namespace OrslavTeam.Revit.General.Embrasure
{
    public class Axillary
    {
        public static int EmbrasureThis(Element el)
        {
            int sillHeight = PublicParameter.SillHeight(el);
            int sillHeightAnn = PublicParameter.HeightUnderLevel(el);
            
            if (sillHeight == sillHeightAnn) return -1;
            
            try
            {
                PublicParameter.HeightUnderLevel(el, sillHeight);
                int newZLocation = (int) UnitUtils.ConvertFromInternalUnits(((LocationPoint) el.Location).Point.Z,
                    DisplayUnitType.DUT_MILLIMETERS);
                PublicParameter.ElevationTextValue(el,
                    newZLocation > 0
                        ? ("+" + newZLocation.ToString("F3", CultureInfo.CreateSpecificCulture("es-ES")))
                        : newZLocation.ToString("F3", CultureInfo.CreateSpecificCulture("es-ES")));
                return 0;
            }
            catch (Exception)
            {
                return 1;
            }
        }
        
        public static bool IsEmbrasure(Element el)
        {
            try
            {
                string uniKey = PublicParameter.GetUniformatKode(((FamilyInstance) el).Symbol).Substring(0, 10);
                return uniKey == "Z.01.02.01";
            }
            catch (Exception)
            {
                return false;
            }
        }
        
        public static bool IsFloorOpening(Element el)
        {
            try
            {
                string uniKey = PublicParameter.GetUniformatKode(((FamilyInstance) el).Symbol).Substring(0, 10);
                return uniKey == "Z.01.01.01";
            }
            catch (Exception)
            {
                return false;
            }
        }
        
        public static bool IsDoorOpening(Element el)
        {
            try
            {
                string uniKey = PublicParameter.GetUniformatKode(((FamilyInstance) el).Symbol);
                return uniKey == "Z.01.02.01.10" || uniKey == "Z.01.02.01.11";
            }
            catch (Exception)
            {
                return false;
            }
        }
        
        public static IEnumerable<Element> GetOpeningInWall(Document doc, ElementId viewId)
        {
            try
            {
                return new FilteredElementCollector(doc, viewId).OfCategory(BuiltInCategory.OST_Windows)
                    .Where(IsEmbrasure);
            }
            catch (Exception)
            {
                return Enumerable.Empty<Element>();
            }
        }
        
        public static IEnumerable<Element> GetOpeningInWall(Document doc)
        {
            try
            {
                return new FilteredElementCollector(doc).OfCategory(BuiltInCategory.OST_Windows).Where(IsEmbrasure);
            }
            catch (Exception)
            {
                return Enumerable.Empty<Element>();
            }
        }
        
        public static IEnumerable<Element> GetOpeningInFloor(Document doc, ElementId viewId)
        {
            try
            {
                return new FilteredElementCollector(doc, viewId).OfCategory(BuiltInCategory.OST_GenericModel).Where(IsFloorOpening);
            }
            catch (Exception)
            {
                return Enumerable.Empty<Element>();
            }
        }
        
        public static IEnumerable<Element> GetDoorsOpening(Document doc, ElementId viewId)
        {
            try
            {
                return new FilteredElementCollector(doc, viewId).OfCategory(BuiltInCategory.OST_Doors)
                    .Where(IsDoorOpening);
            }
            catch (Exception)
            {
                return Enumerable.Empty<Element>();
            }
        }
    }
}