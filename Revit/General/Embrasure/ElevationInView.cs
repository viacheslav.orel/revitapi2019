﻿using System.Collections.Generic;
using System.Linq;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using OrslavTeam.Revit.Axillary.ViewTemplate.ReportWindow;

namespace OrslavTeam.Revit.General.Embrasure
{
    [Transaction(TransactionMode.Manual)]
    public class ElevationInView : IExternalCommand
    {
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            Document doc = commandData.Application.ActiveUIDocument.Document;
            ElementId viewId = commandData.Application.ActiveUIDocument.ActiveView.Id;
            IEnumerable<Element> embrasure = Axillary.GetOpeningInWall(doc, viewId);
            IEnumerable<Element> enumerable = embrasure as Element[] ?? embrasure.ToArray();
            int embrasureCount = enumerable.Count();

            List<string> changedElements = new List<string>() { "Змінені елементи:" };
            List<string> dontChangedElements = new List<string>() { "Пропущені елементи:" };
            List<string> errorChangedElements = new List<string>() { "Помилки запису:" };

            if (embrasureCount > 0)
            {
                using (Transaction tr = new Transaction(doc, "EmbrasureElevation"))
                {
                    tr.Start();
                    foreach (Element el in enumerable)
                    {
                        int reportKey = Axillary.EmbrasureThis(el);
                        // ReSharper disable once SwitchStatementMissingSomeCases
                        switch (reportKey)
                        {
                            case 0:
                                changedElements.Add(el.Id.ToString());
                                break;
                            case 1:
                                errorChangedElements.Add(el.Id.ToString());
                                break;
                            case -1:
                                dontChangedElements.Add(el.Id.ToString());
                                break;
                        }
                    }
                    tr.Commit();
                }
                ReportView reportView = new ReportView(embrasureCount, changedElements, dontChangedElements,
                    errorChangedElements);
                reportView.Show();
            }
            else
            {
                MessageView messageView =
                    new MessageView(
                        "На поточному виді прорізів у стіні не виявлено.\nСпробуйте змінити налаштування виду або виберіть інший вид.\nМожливо для прорізів втановлено невірний код OmniClass");
                messageView.ShowDialog();
            }
            return Result.Succeeded;
        }
    }
}