﻿using System;
using System.Globalization;
using Autodesk.Revit.DB;

namespace OrslavTeam.Revit.Axillary.PublicInformation
{
    public static class PublicParameter
    {
        #region Path

        public static string XmlPath => @"C:\ProgramData\Autodesk\Revit\Addins\2019\OTeam\xmlFile\";
        public static string FilePath => @"C:\ProgramData\Autodesk\Revit\Addins\2019\OTeam\";

        public static string DBPath =
            $"Data Source={FilePath}res\\revit_element.db; Version=3;UseUTF16Encoding=True; Read Only=True;";

        #endregion

        //00_Загальні

        #region OTeam_Марка_Префікс

        private static readonly Guid _markTextPrefix = new Guid("3f5ba89e-428e-4366-a6b6-b777843b54ce");

        public static string MarkTextPrefix(Element el)
        {
            return el?.get_Parameter(_markTextPrefix)?.AsString() ?? "";
        }

        public static string MarkTextPrefix(Element el, string value)
        {
            el.get_Parameter(_markTextPrefix).Set(value);
            return value;
        }

        #endregion

        #region OTeam_Марка_Текст

        private static readonly Guid _markText = new Guid("a141e7d6-fe80-4e5a-be8a-feaa97a42fef");

        public static string MarkText(Element el)
        {
            if (el == null) throw new ArgumentNullException(nameof(el));
            return el.get_Parameter(_markText)?.AsString() ?? "";
        }

        public static string MarkText(Element el, string value)
        {
            try
            {
                el.get_Parameter(_markText).Set(value);
            }
            catch (Exception)
            {
                throw new Exception($"Error set mark to #{el.Id}");
            }
            
            return value;
        }

        #endregion

        #region OTeam_Марка_Число

        private static readonly Guid _markNumber = new Guid("46c9bd60-564f-4a7f-9bed-fe6d2518fe53");

        public static int MarkNumber(Element el)
        {
            return el.get_Parameter(_markNumber).AsInteger();
        }

        public static int MarkNumber(Element el, int value)
        {
            el.get_Parameter(_markNumber).Set(value);
            return value;
        }

        #endregion

        #region OTeam_Марка_Суфікс

        private static readonly Guid _markSufix = new Guid("0ba0dd73-8d02-4b04-884e-ea6d0fdc15a1");

        public static string MarkSufix(Element el)
        {
            return el.get_Parameter(_markSufix)?.AsString() ?? "";
        }

        public static string MarkSufix(Element el, string value)
        {
            Parameter param = el.get_Parameter(_markSufix);
            if (param == null) return value;

            param.Set(value);
            return value;
        }

        #endregion

        #region OTeam_ПозиціяКомпонента_Текс

        private static readonly Guid _position = new Guid("8f0212b7-6872-44ea-a408-9fe4504333d3");

        public static string Position(Element elem) => elem.get_Parameter(_position)?.AsString() ?? "";

        public static string Position(Element elem, string value)
        {
            try
            {
                elem.get_Parameter(_position).Set(value);
            }
            catch (Exception)
            {
                throw new Exception($"Error set mark to #{elem.Id}");
            }

            return value;
        }

        #endregion

        #region OTeam_КодТипу

        private static readonly Guid _productMixKey = new Guid("5e83b8e6-692c-4e6e-a7a4-b4f61b02be93");

        public static int ProductMixKey(Element el)
        {
            return el.get_Parameter(_productMixKey)?.AsInteger() ?? 0;
        }

        public static int ProductMixKey(Element el, int value)
        {
            Parameter param = el.get_Parameter(_productMixKey);
            if (param == null) return value;

            param.Set(value);
            return value;
        }

        #endregion

        #region OTeam_КодВиробу

        private static readonly Guid _elementKey = new Guid("9dac54fb-77bc-470e-84ea-69ac2f4e7358");

        public static int ElementKey(Element el)
        {
            return el.get_Parameter(_elementKey)?.AsInteger() ?? 0;
        }

        public static int ElementKey(Element el, int value)
        {
            Parameter param = el.get_Parameter(_elementKey);
            if (param == null) return value;

            param.Set(value);
            return value;
        }

        #endregion

        #region OTeam_Номер НД

        private static readonly Guid _docNumber = new Guid("4275899b-a06f-4c1b-8988-5e55114a5803");

        public static string DocNumber(Element el)
        {
            return el.get_Parameter(_docNumber)?.AsString() ?? "";
        }

        public static string DocNumber(Element el, string value)
        {
            Parameter param = el.get_Parameter(_docNumber);
            if (param == null) return value;

            param.Set(value);
            return value;
        }

        #endregion

        #region OTeam_Назва елементу_Префікс

        private static readonly Guid _profileNamePrefix = new Guid("63771982-85b2-487b-939d-d496c3c24923");

        public static string ProfileNamePrefix(Element el)
        {
            return el.get_Parameter(_profileNamePrefix)?.AsString() ?? "";
        }

        public static string ProfileNamePrefix(Element el, string value)
        {
            Parameter param = el.get_Parameter(_profileNamePrefix);
            if (param == null) return value;

            param.Set(value);
            return value;
        }

        #endregion

        #region OTeam_Назва елементу_Основа

        private static readonly Guid _profileName = new Guid("4f0d8af4-73a5-4cb7-af12-1cbb513a68b8");

        public static string ProfileName(Element el)
        {
            return el.get_Parameter(_profileName)?.AsString() ?? "";
        }

        public static string ProfileName(Element el, string value)
        {
            Parameter param = el.get_Parameter(_profileName);
            if (param == null) return value;

            param.Set(value);
            return value;
        }

        #endregion

        #region OTeam_Назва елементу_Суфікс

        private static readonly Guid _profileNameSuffix = new Guid("925418f4-5532-4c30-b4ec-6bc919acfd40");

        public static string ProfileNameSuffix(Element el)
        {
            return el?.get_Parameter(_profileNameSuffix)?.AsString() ?? "";
        }

        public static string ProfileNameSuffix(Element el, string value)
        {
            el.get_Parameter(_profileNameSuffix).Set(value);
            return value;
        }

        #endregion

        #region OTeam_Робоча позначка_Текст

        private static readonly Guid _elevationTextValue = new Guid("8fec81bb-5350-486b-827a-050eb4a696ec");

        public static string ElevationTextValue(Element el)
        {
            return el.get_Parameter(_elevationTextValue).AsString();
        }

        public static string ElevationTextValue(Element el, string value)
        {
            el.get_Parameter(_elevationTextValue).Set(value);
            return value;
        }

        #endregion

        #region OTeam_Монтажний рівень

        private static readonly Guid _mountigLevel = new Guid("06f1e56c-25a5-4425-b51d-4b23f8472daf");

        public static int MountingLevel(Element el)
        {
            return (int)(el.get_Parameter(_mountigLevel).AsDouble() * 100);
        }

        public static int MountingLevel(Element el, int value)
        {
            el.get_Parameter(_mountigLevel).Set(value / 100.0);
            return value;
        }

        #endregion

        #region OTeam_ПозиціяКомпонента_Текс

        private static readonly Guid _componentIndex = new Guid("8f0212b7-6872-44ea-a408-9fe4504333d3");

        public static string ComponentIndex(Element elem)
        {
            return elem.get_Parameter(_componentIndex)?.AsString() ?? "";
        }

        public static string ComponentIndex(Element elem, string value)
        {
            elem.get_Parameter(_componentIndex)?.Set(value);
            return value;
        }

        #endregion

        #region OTeam_Груповий параметр_Текст

        private static readonly Guid _groupParameterText = new Guid("cd863f95-cbdf-41c7-be34-3780daebcb5f");

        public static string GroupParametersText(Element el)
        {
            return el.get_Parameter(_groupParameterText).AsString();
        }

        public static string GroupParametersText(Element el, string value)
        {
            el.get_Parameter(_groupParameterText).Set(value);
            return value;
        }

        #endregion

        #region OTeam_Матеріал

        private static readonly Guid _material = new Guid("0b019559-8c68-4601-acc9-fcc4d4b0afb8");

        public static ElementId Material(Element elem)
        {
            return elem?.get_Parameter(_material)?.AsElementId() ?? ElementId.InvalidElementId;
        }

        public static ElementId Material(Element elem, ElementId materialId)
        {
            elem.get_Parameter(_material)?.Set(materialId);

            return materialId;
        }

        #endregion

        #region OTeam_Специфікація_Фільтр_Число

        private static readonly Guid _specificationFilter = new Guid("09e52d2f-eaa4-40a1-8bf6-17e93f4829f4");

        public static int SpecificationFilter(Element elem)
        {
            return elem.get_Parameter(_specificationFilter)?.AsInteger() ?? 0;
        }

        #endregion

        #region OTeam_Повна назва

        private static readonly Guid _fullName = new Guid("6d7b60e5-dfb6-47a1-b16b-599a96bf840f");

        public static string FullName(Element el)
        {
            return el?.get_Parameter(_fullName)?.AsString() ?? "";
        }

        public static string FullName(Element el, string value)
        {
            el.get_Parameter(_fullName).Set(value);
            return value;
        }

        #endregion

        //01_Розміри

        #region OTeam_Розмір_Довжина

        private static readonly Guid _dimensionLength = new Guid("556d2177-8c07-4526-a09f-ad36cfb8234c");

        public static int DimensionLength(Element el)
        {
            Parameter param = el.get_Parameter(_dimensionLength);

            if (param == null) return 0;

            return (int)Math.Round(UnitUtils.ConvertFromInternalUnits(param.AsDouble(),
                DisplayUnitType.DUT_MILLIMETERS));
        }

        public static int DimensionLength(Element el, int value)
        {
            el.get_Parameter(_dimensionLength)
                ?.Set(UnitUtils.ConvertToInternalUnits(value, DisplayUnitType.DUT_MILLIMETERS));
            return value;
        }

        #endregion

        #region OTeam_Розмір_Довжина1

        private static readonly Guid _dimensionsLength1 = new Guid("5d9f25ca-9efd-4053-9bf1-05d02202dece");

        public static int DimensionsLength1(Element el)
        {
            return (int)UnitUtils.ConvertFromInternalUnits(el.get_Parameter(_dimensionsLength1).AsDouble(),
                DisplayUnitType.DUT_MILLIMETERS);
        }

        public static int DimensionsLength1(Element el, int value)
        {
            el.get_Parameter(_dimensionsLength1)
                .Set(UnitUtils.ConvertToInternalUnits(value, DisplayUnitType.DUT_MILLIMETERS));
            return value;
        }

        #endregion

        #region OTeam_Розмір_Висота

        private static readonly Guid _dimensionsHeight = new Guid("95197d85-d94d-4fbf-9e89-78cad91028e0");

        public static int DimensionsHeight(Element el)
        {
            return (int)UnitUtils.ConvertFromInternalUnits(el.get_Parameter(_dimensionsHeight).AsDouble(),
                DisplayUnitType.DUT_MILLIMETERS);
        }

        public static int DimensionsHeight(Element el, int value)
        {
            el.get_Parameter(_dimensionsHeight)
                .Set(UnitUtils.ConvertToInternalUnits(value, DisplayUnitType.DUT_MILLIMETERS));
            return value;
        }

        #endregion

        #region OTeam_Розмір_Ширина

        private static readonly Guid _dimensionsWidth = new Guid("ac4485b9-1e0d-41db-bcd6-8d60d2db8a2e");

        public static int DimensionsWidth(Element el)
        {
            return (int)UnitUtils.ConvertFromInternalUnits(el.get_Parameter(_dimensionsWidth).AsDouble(),
                DisplayUnitType.DUT_MILLIMETERS);
        }

        public static int DimensionsWidth(Element el, int value)
        {
            el.get_Parameter(_dimensionsWidth)
                .Set(UnitUtils.ConvertToInternalUnits(value, DisplayUnitType.DUT_MILLIMETERS));
            return value;
        }

        #endregion

        #region OTeam_Розмір_A

        private static readonly Guid _dimensionsA = new Guid("745e6f01-869c-4718-8a9e-009e049b2854");

        public static int DimensionsA(Element el)
        {
            return (int)UnitUtils.ConvertFromInternalUnits(el.get_Parameter(_dimensionsA)?.AsDouble() ?? 0,
                DisplayUnitType.DUT_MILLIMETERS);
        }

        public static int DimensionsA(Element el, int value)
        {
            Parameter param = el.get_Parameter(_dimensionsA);
            if (param == null) return value;

            param.Set(value);
            return value;
        }

        #endregion

        #region OTeam_Розмір_B

        private static readonly Guid _dimensionsB = new Guid("d1f813e6-42a6-4b84-af35-a671b1186d17");

        public static int DimensionsB(Element el)
        {
            return (int)UnitUtils.ConvertFromInternalUnits(el.get_Parameter(_dimensionsB)?.AsDouble() ?? 0,
                DisplayUnitType.DUT_MILLIMETERS);
        }

        public static int DimensionsB(Element el, int value)
        {
            Parameter param = el.get_Parameter(_dimensionsB);
            if (param == null) return value;

            param.Set(value);
            return value;
        }

        #endregion

        #region OTeam_Розмір_C

        private static readonly Guid _dimensionsC = new Guid("34a0585f-b933-4ede-8d8b-b8b6a0f8962e");

        public static int DimensionsC(Element el)
        {
            return (int)UnitUtils.ConvertFromInternalUnits(el.get_Parameter(_dimensionsC)?.AsDouble() ?? 0,
                DisplayUnitType.DUT_MILLIMETERS);
        }

        public static int DimensionsC(Element el, int value)
        {
            Parameter param = el.get_Parameter(_dimensionsC);
            if (param == null) return value;

            param.Set(value);
            return value;
        }

        #endregion

        #region OTeam_Розмір_D

        private static readonly Guid _dimensionsD = new Guid("2c9c56ac-0401-410d-8fbf-67d391ac9018");

        public static int DimensionsD(Element el)
        {
            return (int)UnitUtils.ConvertFromInternalUnits(el.get_Parameter(_dimensionsD)?.AsDouble() ?? 0,
                DisplayUnitType.DUT_MILLIMETERS);
        }

        public static int DimensionsD(Element el, int value)
        {
            Parameter param = el.get_Parameter(_dimensionsD);
            if (param == null) return value;

            param.Set(value);
            return value;
        }

        #endregion

        #region OTeam_Розмір_E

        private static readonly Guid _dimensionsE = new Guid("2241e033-496a-4e83-872d-3b1de874a698");

        public static int DimensionsE(Element el)
        {
            return (int)UnitUtils.ConvertFromInternalUnits(el.get_Parameter(_dimensionsE)?.AsDouble() ?? 0,
                DisplayUnitType.DUT_MILLIMETERS);
        }

        public static int DimensionsE(Element el, int value)
        {
            Parameter param = el.get_Parameter(_dimensionsE);
            if (param == null) return value;

            param.Set(value);
            return value;
        }

        #endregion

        #region OTeam_Розмір_F

        private static readonly Guid _dimensionsF = new Guid("4c0a0594-0941-4574-9666-0081be540a81");

        public static int DimensionsF(Element el)
        {
            return (int)UnitUtils.ConvertFromInternalUnits(el.get_Parameter(_dimensionsF)?.AsDouble() ?? 0,
                DisplayUnitType.DUT_MILLIMETERS);
        }

        public static int DimensionsF(Element el, int value)
        {
            Parameter param = el.get_Parameter(_dimensionsF);
            if (param == null) return value;

            param.Set(value);
            return value;
        }

        #endregion

        #region OTeam_Розмір_G

        private static readonly Guid _dimensionsG = new Guid("2cf85c76-18f8-493f-92f7-164f75cd9f9c");

        public static int DimensionsG(Element el)
        {
            return (int)UnitUtils.ConvertFromInternalUnits(el.get_Parameter(_dimensionsG)?.AsDouble() ?? 0,
                DisplayUnitType.DUT_MILLIMETERS);
        }

        public static int DimensionsG(Element el, int value)
        {
            Parameter param = el.get_Parameter(_dimensionsG);
            if (param == null) return value;

            param.Set(value);
            return value;
        }

        #endregion

        #region OTeam_Розмір_H

        private static readonly Guid _dimensionsH = new Guid("0074661b-2387-4c02-a042-66a5f81badc9");

        public static int DimensionsH(Element el)
        {
            return (int)UnitUtils.ConvertFromInternalUnits(el.get_Parameter(_dimensionsH)?.AsDouble() ?? 0,
                DisplayUnitType.DUT_MILLIMETERS);
        }

        public static int DimensionsH(Element el, int value)
        {
            Parameter param = el.get_Parameter(_dimensionsH);
            if (param == null) return value;

            param.Set(value);
            return value;
        }

        #endregion

        #region OTeam_Розмір_I

        private static readonly Guid _dimensionsI = new Guid("0bbc6b1d-9841-4d73-abe4-3b53545de142");

        public static int DimensionsI(Element el)
        {
            return (int)UnitUtils.ConvertFromInternalUnits(el.get_Parameter(_dimensionsI)?.AsDouble() ?? 0,
                DisplayUnitType.DUT_MILLIMETERS);
        }

        public static int DimensionsI(Element el, int value)
        {
            Parameter param = el.get_Parameter(_dimensionsI);
            if (param == null) return value;

            param.Set(value);
            return value;
        }

        #endregion

        #region OTeam_Розмір_J

        private static readonly Guid _dimensionsJ = new Guid("7aadc093-8f08-4f8e-b1c2-b410409a9bd6");

        public static int DimensionsJ(Element el)
        {
            return (int)UnitUtils.ConvertFromInternalUnits(el.get_Parameter(_dimensionsJ)?.AsDouble() ?? 0,
                DisplayUnitType.DUT_MILLIMETERS);
        }

        public static int DimensionsJ(Element el, int value)
        {
            Parameter param = el.get_Parameter(_dimensionsJ);
            if (param == null) return value;

            param.Set(value);
            return value;
        }

        #endregion

        #region OTeam_Розмір_K

        private static readonly Guid _dimensionsK = new Guid("35eec545-0744-4133-860a-3976301ec181");

        public static int DimensionsK(Element el)
        {
            return (int)UnitUtils.ConvertFromInternalUnits(el.get_Parameter(_dimensionsK)?.AsDouble() ?? 0,
                DisplayUnitType.DUT_MILLIMETERS);
        }

        public static int DimensionsK(Element el, int value)
        {
            Parameter param = el.get_Parameter(_dimensionsK);
            if (param == null) return value;

            param.Set(value);
            return value;
        }

        #endregion

        #region OTeam_Розмір_L

        private static readonly Guid _dimensionsL = new Guid("7d7ef3ef-7156-4160-aaaa-bdd950b1ab95");

        public static int DimensionsL(Element el)
        {
            return (int)UnitUtils.ConvertFromInternalUnits(el.get_Parameter(_dimensionsL)?.AsDouble() ?? 0,
                DisplayUnitType.DUT_MILLIMETERS);
        }

        public static int DimensionsL(Element el, int value)
        {
            Parameter param = el.get_Parameter(_dimensionsL);
            if (param == null) return value;

            param.Set(value);
            return value;
        }

        #endregion

        #region OTeam_Розмір_M

        private static readonly Guid _dimensionsM = new Guid("ab01efe9-6503-4c5f-99c4-1e5afc75fdfe");

        public static int DimensionsM(Element el)
        {
            return (int)UnitUtils.ConvertFromInternalUnits(el.get_Parameter(_dimensionsM)?.AsDouble() ?? 0,
                DisplayUnitType.DUT_MILLIMETERS);
        }

        public static int DimensionsM(Element el, int value)
        {
            Parameter param = el.get_Parameter(_dimensionsM);
            if (param == null) return value;

            param.Set(value);
            return value;
        }

        #endregion

        #region OTeam_Розмір_N

        private static readonly Guid _dimensionsN = new Guid("fad61449-0784-4f67-b94b-9d62dd7269f2");

        public static int DimensionsN(Element el)
        {
            return (int)UnitUtils.ConvertFromInternalUnits(el.get_Parameter(_dimensionsN)?.AsDouble() ?? 0,
                DisplayUnitType.DUT_MILLIMETERS);
        }

        public static int DimensionsN(Element el, int value)
        {
            Parameter param = el.get_Parameter(_dimensionsN);
            if (param == null) return value;

            param.Set(value);
            return value;
        }

        #endregion

        #region OTeam_Розмір_O

        private static readonly Guid _dimensionsO = new Guid("ab6c4c53-e433-477c-a0cd-b2e6d2890d50");

        public static int DimensionsO(Element el)
        {
            return (int)UnitUtils.ConvertFromInternalUnits(el.get_Parameter(_dimensionsO)?.AsDouble() ?? 0,
                DisplayUnitType.DUT_MILLIMETERS);
        }

        public static int DimensionsO(Element el, int value)
        {
            Parameter param = el.get_Parameter(_dimensionsO);
            if (param == null) return value;

            param.Set(value);
            return value;
        }

        #endregion

        #region OTeam_Розмір_P

        private static readonly Guid _dimensionsP = new Guid("abeb94bc-9a7b-4b8a-9eb0-1b2e44346296");

        public static int DimensionsP(Element el)
        {
            return (int)UnitUtils.ConvertFromInternalUnits(el.get_Parameter(_dimensionsP)?.AsDouble() ?? 0,
                DisplayUnitType.DUT_MILLIMETERS);
        }

        public static int DimensionsP(Element el, int value)
        {
            Parameter param = el.get_Parameter(_dimensionsO);
            if (param == null) return value;

            param.Set(value);
            return value;
        }

        #endregion

        #region OTeam_Розмір_Q

        private static readonly Guid _dimensionsQ = new Guid("c30be4b0-1bd9-4523-b40d-0610aaf5dde6");

        public static int DimensionsQ(Element el)
        {
            return (int)UnitUtils.ConvertFromInternalUnits(el.get_Parameter(_dimensionsQ)?.AsDouble() ?? 0,
                DisplayUnitType.DUT_MILLIMETERS);
        }

        public static int DimensionsQ(Element el, int value)
        {
            Parameter param = el.get_Parameter(_dimensionsQ);
            if (param == null) return value;

            param.Set(value);
            return value;
        }

        #endregion

        #region OTeam_Розмір_R

        private static readonly Guid _dimensionsR = new Guid("31656a42-9f96-42f2-b070-94a9a457377a");

        public static int DimensionsR(Element el)
        {
            return (int)UnitUtils.ConvertFromInternalUnits(el.get_Parameter(_dimensionsR)?.AsDouble() ?? 0,
                DisplayUnitType.DUT_MILLIMETERS);
        }

        public static int DimensionsR(Element el, int value)
        {
            Parameter param = el.get_Parameter(_dimensionsR);
            if (param == null) return value;

            param.Set(value);
            return value;
        }

        #endregion

        #region OTeam_Розмір_r

        private static readonly Guid _dimensionsSmallR = new Guid("a823270e-536a-48e9-9c6e-3c543c857857");

        public static int DimensionsSmallR(Element el)
        {
            return (int)UnitUtils.ConvertFromInternalUnits(el.get_Parameter(_dimensionsSmallR)?.AsDouble() ?? 0,
                DisplayUnitType.DUT_MILLIMETERS);
        }

        public static int DimensionsSmallR(Element el, int value)
        {
            Parameter param = el.get_Parameter(_dimensionsSmallR);
            if (param == null) return value;

            param.Set(value);
            return value;
        }

        #endregion

        #region OTeam_Розмір_S

        private static readonly Guid _dimensionsS = new Guid("eb1c909d-0d02-484f-98af-703cb5926f86");

        public static int DimensionsS(Element el)
        {
            return (int)UnitUtils.ConvertFromInternalUnits(el.get_Parameter(_dimensionsS)?.AsDouble() ?? 0,
                DisplayUnitType.DUT_MILLIMETERS);
        }

        public static int DimensionsS(Element el, int value)
        {
            Parameter param = el.get_Parameter(_dimensionsS);
            if (param == null) return value;

            param.Set(value);
            return value;
        }

        #endregion

        #region OTeam_Розмір_T

        private static readonly Guid _dimensionsT = new Guid("374301cd-f4ee-49f9-b57f-46f9b8014581");

        public static int DimensionsT(Element el)
        {
            return (int)UnitUtils.ConvertFromInternalUnits(el.get_Parameter(_dimensionsT)?.AsDouble() ?? 0,
                DisplayUnitType.DUT_MILLIMETERS);
        }

        public static int DimensionsT(Element el, int value)
        {
            Parameter param = el.get_Parameter(_dimensionsT);
            if (param == null) return value;

            param.Set(value);
            return value;
        }

        #endregion

        #region OTeam_Розмір_U

        private static readonly Guid _dimensionsU = new Guid("a9e5a2b2-b9fe-47c0-92d5-bcd2cb925213");

        public static int DimensionsU(Element el)
        {
            return (int)UnitUtils.ConvertFromInternalUnits(el.get_Parameter(_dimensionsU)?.AsDouble() ?? 0,
                DisplayUnitType.DUT_MILLIMETERS);
        }

        public static int DimensionsU(Element el, int value)
        {
            Parameter param = el.get_Parameter(_dimensionsU);
            if (param == null) return value;

            param.Set(value);
            return value;
        }

        #endregion

        #region OTeam_Розмір_V

        private static readonly Guid _dimensionsV = new Guid("ada6d9a9-1908-479a-8177-a9332c0be10c");

        public static int DimensionsV(Element el)
        {
            return (int)UnitUtils.ConvertFromInternalUnits(el.get_Parameter(_dimensionsV)?.AsDouble() ?? 0,
                DisplayUnitType.DUT_MILLIMETERS);
        }

        public static int DimensionsV(Element el, int value)
        {
            Parameter param = el.get_Parameter(_dimensionsV);
            if (param == null) return value;

            param.Set(value);
            return value;
        }

        #endregion

        #region OTeam_Розмір_W

        private static readonly Guid _dimensionsW = new Guid("4e3788e2-d6ee-478e-9a71-c08508a9e4bb");

        public static int DimensionsW(Element el)
        {
            return (int)UnitUtils.ConvertFromInternalUnits(el.get_Parameter(_dimensionsW)?.AsDouble() ?? 0,
                DisplayUnitType.DUT_MILLIMETERS);
        }

        public static int DimensionsW(Element el, int value)
        {
            Parameter param = el.get_Parameter(_dimensionsW);
            if (param == null) return value;

            param.Set(value);
            return value;
        }

        #endregion

        #region OTeam_Розмір_X

        private static readonly Guid _dimensionsX = new Guid("9077757b-2f09-4980-8175-0fc0980ff4d2");

        public static int DimensionsX(Element el)
        {
            return (int)UnitUtils.ConvertFromInternalUnits(el.get_Parameter(_dimensionsX)?.AsDouble() ?? 0,
                DisplayUnitType.DUT_MILLIMETERS);
        }

        public static int DimensionsX(Element el, int value)
        {
            Parameter param = el.get_Parameter(_dimensionsX);
            if (param == null) return value;

            param.Set(value);
            return value;
        }

        #endregion

        #region OTeam_Розмір_Y

        private static readonly Guid _dimensionsY = new Guid("35d3f3bd-fe32-4be9-bdd0-208797802ee6");

        public static int DimensionsY(Element el)
        {
            return (int)UnitUtils.ConvertFromInternalUnits(el.get_Parameter(_dimensionsY)?.AsDouble() ?? 0,
                DisplayUnitType.DUT_MILLIMETERS);
        }

        public static int DimensionsY(Element el, int value)
        {
            Parameter param = el.get_Parameter(_dimensionsY);
            if (param == null) return value;

            param.Set(value);
            return value;
        }

        #endregion


        // 02_Конструкції

        #region OTeam_Тип конструкції_Текст

        private static readonly Guid _structuralType = new Guid("d79a3082-e57c-425f-a7a0-eb6a63531de6");

        public static string StructuralType(Element el)
        {
            return el?.get_Parameter(_structuralType)?.AsString() ?? "";
        }

        public static string StructuralType(Element el, string value)
        {
            try
            {
                el.get_Parameter(_structuralType).Set(value);
            }
            catch (Exception)
            {
                throw new Exception($"Елемент {el.Id} не містить параметру \"OTeam_Тип конструкції_Текст\"");
            }

            return value;
        }

        #endregion


        //04_Виміри

        #region OTeam_КількістьЕлементів

        private static readonly Guid _elementQuantity = new Guid("ea89d52c-a5c6-4b9d-84ca-3be0ae6899e6");

        public static int ElementQuantity(Element el)
        {
            return el.get_Parameter(_elementQuantity)?.AsInteger() ?? 0;
        }

        public static int ElementQuantity(Element el, int value)
        {
            Parameter param = el.get_Parameter(_elementQuantity);
            if (param == null) return value;

            param.Set(value);
            return value;
        }

        #endregion

        #region OTeam_Маса 1 м.п.

        private static readonly Guid _massPerUnitLength = new Guid("7433130d-4066-4dec-b79f-ec30f11a833b");

        public static double MassPerUnitLength(Element el)
        {
            Parameter param = el.get_Parameter(_massPerUnitLength);
            if (param == null) return -1;

            return UnitUtils.ConvertFromInternalUnits(param.AsDouble(),
                DisplayUnitType.DUT_KILOGRAMS_MASS_PER_METER);
        }

        public static double MassPerUnitLength(Element el, double value)
        {
            Parameter param = el.get_Parameter(_massPerUnitLength);
            if (param == null) return value;

            el.get_Parameter(_massPerUnitLength)
                .Set(UnitUtils.ConvertToInternalUnits(value, DisplayUnitType.DUT_KILOGRAMS_MASS_PER_METER));

            return value;
        }

        #endregion

        #region OTeam_Маса елементу

        private static readonly Guid _elementMass = new Guid("5aff62c3-743e-4778-b7ae-7ec355934a02");

        public static double ElementMass(Element el)
        {
            Parameter param = el.get_Parameter(_elementMass);
            if (param == null) return 0;

            return UnitUtils.ConvertFromInternalUnits(param.AsDouble(), DisplayUnitType.DUT_KILOGRAMS_MASS);
        }

        public static double ElementMass(Element el, double value)
        {
            Parameter param = el.get_Parameter(_elementMass);
            if (param == null) return value;

            param.Set(UnitUtils.ConvertToInternalUnits(value, DisplayUnitType.DUT_KILOGRAMS_MASS));

            return value;
        }

        #endregion

        #region OTeam_Площа перетину

        private static readonly Guid _sectionArea = new Guid("ce084797-406f-4871-9f66-e653e23810bb");

        public static double SectionArea(Element el)
        {
            Parameter param = el.get_Parameter(_sectionArea);
            if (param == null) return 0;

            return UnitUtils.ConvertFromInternalUnits(param.AsDouble(), DisplayUnitType.DUT_SQUARE_METERS);
        }

        public static double SectionArea(Element el, double value)
        {
            Parameter param = el.get_Parameter(_sectionArea);
            if (param == null) return value;

            param.Set(UnitUtils.ConvertToInternalUnits(value, DisplayUnitType.DUT_SQUARE_METERS));
            return value;
        }

        #endregion

        #region OTeam_Зусилля_A

        private static readonly Guid _stressA = new Guid("e1d81295-c647-46a3-b40a-41d15858119a");

        public static string StressA(Element el)
        {
            Parameter param = el.get_Parameter(_stressA);
            return param == null ? "0" : param.AsString();
        }

        public static string StressA(Element el, string value)
        {
            Parameter param = el.get_Parameter(_stressA);
            if (param == null) return value;

            param.Set(value);
            return value;
        }

        #endregion

        #region OTeam_Зусилля_A.max

        private static readonly Guid _stressAMax = new Guid("4ae70e5a-d827-4985-bff0-45d828a4a0ab");

        public static double StressAMax(Element el)
        {
            Parameter param = el.get_Parameter(_stressAMax);
            if (param == null) return 0;

            return UnitUtils.ConvertFromInternalUnits(param.AsDouble(), DisplayUnitType.DUT_KILONEWTONS);
        }

        public static double StressAMax(Element el, double value)
        {
            Parameter param = el.get_Parameter(_stressAMax);
            if (param == null) return value;

            param.Set(UnitUtils.ConvertToInternalUnits(value, DisplayUnitType.DUT_KILONEWTONS));
            return value;
        }

        #endregion

        #region OTeam_Зусилля_A.min

        private static readonly Guid _stressAMin = new Guid("649db91e-0173-4432-8672-b19b31a05c14");

        public static double StressAMin(Element el)
        {
            Parameter param = el.get_Parameter(_stressAMin);
            if (param == null) return 0;

            return UnitUtils.ConvertFromInternalUnits(param.AsDouble(), DisplayUnitType.DUT_KILONEWTONS);
        }

        public static double StressAMin(Element el, double value)
        {
            Parameter param = el.get_Parameter(_stressAMin);
            if (param == null) return value;

            param.Set(UnitUtils.ConvertToInternalUnits(value, DisplayUnitType.DUT_KILONEWTONS));
            return value;
        }

        #endregion

        #region OTeam_Зусилля_N

        private static readonly Guid _stressN = new Guid("a8c0575b-04e3-4528-b450-1e8e6ba286e9");

        public static string StressN(Element el)
        {
            Parameter param = el.get_Parameter(_stressN);
            return param == null ? "0" : param.AsString();
        }

        public static string StressN(Element el, string value)
        {
            Parameter param = el.get_Parameter(_stressN);
            if (param == null) return value;

            param.Set(value);
            return value;
        }

        #endregion

        #region OTeam_Зусилля_N.max

        private static readonly Guid _stressNMax = new Guid("9f60be22-8d10-422d-a7e9-549e5b3ba352");

        public static double StressNMax(Element el)
        {
            Parameter param = el.get_Parameter(_stressNMax);
            if (param == null) return 0;

            return UnitUtils.ConvertFromInternalUnits(param.AsDouble(), DisplayUnitType.DUT_KILONEWTONS);
        }

        public static double StressNMax(Element el, double value)
        {
            Parameter param = el.get_Parameter(_stressNMax);
            if (param == null) return value;

            param.Set(UnitUtils.ConvertToInternalUnits(value, DisplayUnitType.DUT_KILONEWTONS));
            return value;
        }

        #endregion

        #region OTeam_Зусилля_N.min

        private static readonly Guid _stressNMin = new Guid("451a0d61-65ff-4db0-b869-f51a3281499e");

        public static double StressNMin(Element el)
        {
            Parameter param = el.get_Parameter(_stressNMin);
            if (param == null) return 0;

            return UnitUtils.ConvertFromInternalUnits(param.AsDouble(), DisplayUnitType.DUT_KILONEWTONS);
        }

        public static double StressNMin(Element el, double value)
        {
            Parameter param = el.get_Parameter(_stressNMin);
            if (param == null) return value;

            param.Set(UnitUtils.ConvertToInternalUnits(value, DisplayUnitType.DUT_KILONEWTONS));
            return value;
        }

        #endregion

        #region OTeam_Зусилля_M

        private static readonly Guid _stressM = new Guid("2e2cfa70-ee78-45fe-a8d0-dc600eb856c6");

        public static string StressM(Element el)
        {
            Parameter param = el.get_Parameter(_stressM);
            return param == null ? "0" : param.AsString();
        }

        public static string StressM(Element el, string value)
        {
            Parameter param = el.get_Parameter(_stressM);
            if (param == null) return value;

            param.Set(value);
            return value;
        }

        #endregion

        #region OTeam_Зусилля_M.max

        private static readonly Guid _stressMMax = new Guid("61ac5680-165a-49e7-91a0-cfd356065357");

        public static double StressMMax(Element el)
        {
            Parameter param = el.get_Parameter(_stressMMax);
            if (param == null) return 0;

            return UnitUtils.ConvertFromInternalUnits(param.AsDouble(), DisplayUnitType.DUT_KILONEWTON_METERS);
        }

        public static double StressMMax(Element el, double value)
        {
            Parameter param = el.get_Parameter(_stressMMax);
            if (param == null) return value;

            param.Set(UnitUtils.ConvertToInternalUnits(value, DisplayUnitType.DUT_KILONEWTON_METERS));
            return value;
        }

        #endregion

        #region OTeam_Зусилля_M.min

        private static readonly Guid _stressMMin = new Guid("d8306f8d-62e3-4f0c-890f-cfc43fd640f0");

        public static double StressMMin(Element el)
        {
            Parameter param = el.get_Parameter(_stressMMin);
            if (param == null) return 0;

            return UnitUtils.ConvertFromInternalUnits(param.AsDouble(), DisplayUnitType.DUT_KILONEWTON_METERS);
        }

        public static double StressMMin(Element el, double value)
        {
            Parameter param = el.get_Parameter(_stressMMin);
            if (param == null) return value;

            param.Set(UnitUtils.ConvertToInternalUnits(value, DisplayUnitType.DUT_KILONEWTON_METERS));
            return value;
        }

        #endregion

        #region OTeam_ГрупаКонструкцій

        private static readonly Guid _structuralGroup = new Guid("0fc9de30-953d-4e9d-9bb0-ec1a34563dae");

        public static string StructuralGroup(Element el)
        {
            return el.get_Parameter(_structuralGroup)?.AsString() ?? "";
        }

        public static string StructuralGroup(Element el, string value)
        {
            Parameter param = el.get_Parameter(_structuralGroup);
            if (param == null) return value;

            param.Set(value);
            return value;
        }

        #endregion

        #region OTeam_ПідрахунокМаси

        private static readonly Guid _massCountingType = new Guid("2c4f0be8-93df-480c-876f-fb61d4a88123");

        public static int MassCountingType(Element elem)
        {
            return elem.get_Parameter(_massCountingType)?.AsInteger() ?? 0;
        }

        #endregion

        #region OTeam_Об'єм конструкції

        private static readonly Guid _elementValue = new Guid("af7db4e5-5aa2-4f58-af9b-928e9114be16");

        public static double ElementValue(Element element)
        {
            Parameter param = element.get_Parameter(_elementValue);

            if (param == null) return 0;

            return UnitUtils.ConvertFromInternalUnits(param.AsDouble(), DisplayUnitType.DUT_CUBIC_METERS);
        }

        #endregion

        #region OTeam_Кількість_Currency

        private static readonly Guid _technicalQuantity = new Guid("a5dd2b2e-2e98-4908-8f25-ee459801bcd5");

        public static double TechnicalQuantity(Element el)
        {
            return el?.get_Parameter(_technicalQuantity)?.AsDouble() ?? 0;
        }

        public static double TechnicalQuantity(Element el, double value)
        {
            el.get_Parameter(_technicalQuantity).Set(value);
            return value;
        }

        #endregion

        #region OTeam_МасаОд_Currency

        private static readonly Guid _technicalMassOne = new Guid("0907f79d-861e-4906-9d99-4d4361214bbc");

        public static double TechnicalMassOne(Element el)
        {
            return el?.get_Parameter(_technicalMassOne)?.AsDouble() ?? 0;
        }

        public static double TechnicalMassOne(Element el, double value)
        {
            el.get_Parameter(_technicalMassOne).Set(value);
            return value;
        }

        #endregion

        //05_Матеріали

        #region OTeam_Матеріал_Номер НД

        private static readonly Guid _materialProductMixNumber = new Guid("83c404ab-6a98-4b1e-8d23-49bde93402be");

        public static string MaterialProductMixNumber(Element el)
        {
            return el.get_Parameter(_materialProductMixNumber)?.AsString() ?? "";
        }

        public static string MaterialProductMixNumber(Element el, string value)
        {
            Parameter param = el.get_Parameter(_materialProductMixNumber);
            if (param == null) return value;

            param.Set(value);
            return value;
        }

        #endregion

        #region OTeam_Матеріал_Назва НД

        private static readonly Guid _materialProductMixName = new Guid("771f0984-40ab-4f47-9884-4634e8f5e484");

        public static string MaterialProductMixName(Element el)
        {
            return el.get_Parameter(_materialProductMixName)?.AsString() ?? "";
        }

        public static string MaterialProductMixName(Element el, string value)
        {
            Parameter param = el.get_Parameter(_materialProductMixName);
            if (param == null) return value;

            param.Set(value);
            return value;
        }

        #endregion

        #region OTeam_Матеріал_Суфікс_1

        private static readonly Guid _materialNameSufix1 = new Guid("94c8db5a-c6fa-4272-93a0-2bd712ce92a2");

        public static string MaterialNameSufix1(Element el)
        {
            return el.get_Parameter(_materialNameSufix1)?.AsString() ?? "";
        }

        public static string MaterialNameSufix1(Element el, string value)
        {
            Parameter param = el.get_Parameter(_materialNameSufix1);
            if (param == null) return value;

            param.Set(value);
            return value;
        }

        #endregion

        #region OTeam_Матеріал_Суфікс_2

        private static readonly Guid _materialNameSufix2 = new Guid("925d4323-e6aa-42b6-8a97-0823429a4b35");

        public static string MaterialNameSufix2(Element el)
        {
            return el.get_Parameter(_materialNameSufix2)?.AsString() ?? "";
        }

        public static string MaterialNameSufix2(Element el, string value)
        {
            Parameter param = el.get_Parameter(_materialNameSufix2);
            if (param == null) return value;

            el.get_Parameter(_materialNameSufix2).Set(value);
            return value;
        }

        #endregion

        #region OTeam_Матеріал_Назва

        private static readonly Guid _materialName = new Guid("be390a73-d0dd-4072-96cc-a7049a9fc239");

        public static string MaterialName(Element el)
        {
            return el.get_Parameter(_materialName)?.AsString() ?? "";
        }

        public static string MaterialName(Element el, string value)
        {
            Parameter param = el.get_Parameter(_materialName);
            if (param == null) return value;

            param.Set(value);
            return value;
        }

        #endregion

        #region OTeam_Матеріал_Код

        private static readonly Guid _materialKod = new Guid("bacbf8e3-ae27-4666-a88f-ea57c30c02e5");

        public static int MaterialKod(Element el)
        {
            return (int)Math.Round(el.get_Parameter(_materialKod)?.AsDouble() ?? -1);
        }

        public static int MaterialKod(Element el, int value)
        {
            Parameter param = el.get_Parameter(_materialKod);
            param?.Set(value);
            return value;
        }

        #endregion

        //06_Арматура

        #region OTeam_H_Тип арматури

        private static readonly Guid _hiddenRebarType = new Guid("48274aa2-1927-46f9-9a3c-e6f1a61e8d13");

        public static int HiddenRebarType(Element el)
        {
            return el.get_Parameter(_hiddenRebarType)?.AsInteger() ?? 0;
        }

        public static int HiddenRebarType(Element el, int value)
        {
            Parameter param = el.get_Parameter(_hiddenRebarType);
            if (param == null) return value;

            param.Set(value);
            return value;
        }

        #endregion

        #region OTeam_Арм_Специфікація_Фільтр

        private static readonly Guid _rebarScheduleFilter = new Guid("4d2c4130-cc39-4fc5-8a73-15016083d03e");

        public static int RebarScheduleFilter(Element el)
        {
            return el.get_Parameter(_rebarScheduleFilter)?.AsInteger() ?? -1;
        }

        #endregion

        #region OTeam_Арм_ДовжНапуску

        private static readonly Guid _rebarScarfLength = new Guid("40f4be34-7ba7-476a-98ce-93895db70ad1");

        public static int RebarScraftLength(Element el)
        {
            Parameter param = el.get_Parameter(_rebarScarfLength);
            if (param == null) return 0;

            return (int)UnitUtils.ConvertFromInternalUnits(param.AsDouble(), DisplayUnitType.DUT_MILLIMETERS);
        }

        public static int RebarScraftLength(Element el, int value)
        {
            Parameter param = el.get_Parameter(_rebarScarfLength);
            if (param == null) return value;

            param.Set(UnitUtils.ConvertToInternalUnits(value, DisplayUnitType.DUT_MILLIMETERS));
            return value;
        }

        #endregion

        #region OTeam_Арм_Форма.Номер

        private static readonly Guid _rebarForm = new Guid("ce40bba5-9896-4e24-8aad-7e0c8dcda334");

        public static string RebarForm(Element el)
        {
            return el.get_Parameter(_rebarForm)?.AsString() ?? "";
        }

        public static string RebarForm(Element el, string value)
        {
            Parameter param = el.get_Parameter(_rebarForm);
            if (param == null) return value;

            param.Set(value);
            return value;
        }

        #endregion

        #region ProjectRebarDimensions

        private static readonly Guid _rebarA = new Guid("dd553991-27ea-4c4f-b097-1d4316dad1e8");
        private static readonly Guid _rebarB = new Guid("f834358e-9e43-404b-83d1-d4e0edb823f1");
        private static readonly Guid _rebarC = new Guid("3aec5c98-26a1-4d40-86a6-f2c3c6e4b76c");
        private static readonly Guid _rebarD = new Guid("70f581ed-487b-43df-a683-0ba9e456493c");
        private static readonly Guid _rebarE = new Guid("9f2c282b-8372-4b52-a7c1-b91949e39a53");
        private static readonly Guid _rebarF = new Guid("be96cbcd-f511-4085-b546-27c0713401aa");
        private static readonly Guid _rebarG = new Guid("a125dad2-94cf-414c-838b-9135b14ae30a");
        private static readonly Guid _rebarH = new Guid("0b595f9b-8b52-4e19-aba6-f04f745850d0");
        private static readonly Guid _rebarJ = new Guid("3892d826-9d37-4576-b69d-08a6f3c258ce");
        private static readonly Guid _rebarK = new Guid("1941bdda-f6f3-4908-a1f2-25d0f968cb02");
        private static readonly Guid _rebarO = new Guid("650670d7-6558-48dd-9a02-46e3688893cf");
        private static readonly Guid _rebarR = new Guid("a71a6b0a-3427-489f-ae27-ac38f59d6bca");

        public static int RebarA(Element el)
        {
            Parameter param = el.get_Parameter(_rebarA);
            if (param == null) return 0;

            return (int)(Math.Round(UnitUtils.ConvertFromInternalUnits(param.AsDouble(),
                                         DisplayUnitType.DUT_MILLIMETERS) / 5) * 5);
        }

        public static int RebarB(Element el)
        {
            Parameter param = el.get_Parameter(_rebarB);
            if (param == null) return 0;

            return (int)(Math.Round(UnitUtils.ConvertFromInternalUnits(param.AsDouble(),
                                         DisplayUnitType.DUT_MILLIMETERS) / 5) * 5);
        }

        public static int RebarC(Element el)
        {
            Parameter param = el.get_Parameter(_rebarC);
            if (param == null) return 0;

            return (int)(Math.Round(UnitUtils.ConvertFromInternalUnits(param.AsDouble(),
                                         DisplayUnitType.DUT_MILLIMETERS) / 5) * 5);
        }

        public static int RebarD(Element el)
        {
            Parameter param = el.get_Parameter(_rebarD);
            if (param == null) return 0;

            return (int)(Math.Round(UnitUtils.ConvertFromInternalUnits(param.AsDouble(),
                                         DisplayUnitType.DUT_MILLIMETERS) / 5) * 5);
        }

        public static int RebarE(Element el)
        {
            Parameter param = el.get_Parameter(_rebarE);
            if (param == null) return 0;

            return (int)(Math.Round(UnitUtils.ConvertFromInternalUnits(param.AsDouble(),
                                         DisplayUnitType.DUT_MILLIMETERS) / 5) * 5);
        }

        public static int RebarF(Element el)
        {
            Parameter param = el.get_Parameter(_rebarF);
            if (param == null) return 0;

            return (int)(Math.Round(UnitUtils.ConvertFromInternalUnits(param.AsDouble(),
                                         DisplayUnitType.DUT_MILLIMETERS) / 5) * 5);
        }

        public static int RebarG(Element el)
        {
            Parameter param = el.get_Parameter(_rebarG);
            if (param == null) return 0;

            return (int)(Math.Round(UnitUtils.ConvertFromInternalUnits(param.AsDouble(),
                                         DisplayUnitType.DUT_MILLIMETERS) / 5) * 5);
        }

        public static int RebarH(Element el)
        {
            Parameter param = el.get_Parameter(_rebarH);
            if (param == null) return 0;

            return (int)(Math.Round(UnitUtils.ConvertFromInternalUnits(param.AsDouble(),
                                         DisplayUnitType.DUT_MILLIMETERS) / 5) * 5);
        }

        public static int RebarJ(Element el)
        {
            Parameter param = el.get_Parameter(_rebarJ);
            if (param == null) return 0;

            return (int)(Math.Round(UnitUtils.ConvertFromInternalUnits(param.AsDouble(),
                                         DisplayUnitType.DUT_MILLIMETERS) / 5) * 5);
        }

        public static int RebarK(Element el)
        {
            Parameter param = el.get_Parameter(_rebarK);
            if (param == null) return 0;

            return (int)(Math.Round(UnitUtils.ConvertFromInternalUnits(param.AsDouble(),
                                         DisplayUnitType.DUT_MILLIMETERS) / 5) * 5);
        }

        public static int RebarO(Element el)
        {
            Parameter param = el.get_Parameter(_rebarO);
            if (param == null) return 0;

            return (int)(Math.Round(UnitUtils.ConvertFromInternalUnits(param.AsDouble(),
                                         DisplayUnitType.DUT_MILLIMETERS) / 5) * 5);
        }

        public static int RebarR(Element el)
        {
            Parameter param = el.get_Parameter(_rebarR);
            if (param == null) return 0;

            return (int)(Math.Round(UnitUtils.ConvertFromInternalUnits(param.AsDouble(),
                                         DisplayUnitType.DUT_MILLIMETERS) / 5) * 5);
        }

        #endregion

        //08_ПрихованіТехнічніПараметри

        #region OTeam_Hide_Guid

        private static readonly Guid _usingGuid = new Guid("cc0d0fc9-e2a1-4f97-819c-a9ec442e2e1c");

        public static string ElementGuid(Element el)
        {
            return el.get_Parameter(_usingGuid)?.AsString() ?? "";
        }

        public static string ElementGuid(Element el, string value)
        {
            Parameter param = el.get_Parameter(_usingGuid);
            if (param == null) return value;

            param.Set(value);
            return value;
        }

        #endregion

        #region OTeam_GetIt

        private static readonly Guid _getIt = new Guid("87f1100f-1df6-4fca-b852-5ade6ec5360c");

        public static string GetIt(Element el)
        {
            return el.get_Parameter(_getIt)?.AsString() ?? "";
        }

        public static string GetIt(Element el, string value)
        {
            Parameter param = el.get_Parameter(_getIt);
            if (param == null) return value;

            param.Set(value);
            return value;
        }

        #endregion

        #region OTeam_Dim1

        private static readonly Guid _dim1 = new Guid("7befddca-9c9a-4917-a7d8-ee961c131271");

        public static int Dim1(Element elem)
        {
            return (int)(elem.get_Parameter(_dim1)?.AsDouble() ?? 0);
        }

        #endregion

        #region OTeam_Dim2

        private static readonly Guid _dim2 = new Guid("9360fdb8-6beb-4753-9d23-6d87d3ce7ebf");

        public static int Dim2(Element elem)
        {
            return (int)(elem.get_Parameter(_dim2)?.AsDouble() ?? 0);
        }

        #endregion

        #region OTeam_Mark.Prefix

        private static readonly Guid _markPrefix = new Guid("76a30ebf-c285-4608-8912-7ebb40fcf32f");

        public static string MarkPrefix(Element el)
        {
            return el.get_Parameter(_markPrefix)?.AsString() ?? "";
        }

        public static string MarkPrefix(Element el, string value)
        {
            el.get_Parameter(_markPrefix).Set(value);
            return value;
        }

        #endregion

        #region OTeam_Mark.First

        private static readonly Guid _markFirst = new Guid("95e73a8e-2cfa-49ae-8aa7-44aece56c219");

        public static string MarkFirst(Element el)
        {
            return el.get_Parameter(_markFirst)?.AsString() ?? "";
        }

        public static string MarkFirst(Element el, string value)
        {
            el.get_Parameter(_markFirst).Set(value);

            return value;
        }

        #endregion

        #region OTeam_Mark.Second

        private static readonly Guid _markSecond = new Guid("c21f7ac0-00c3-4c65-bac6-05bba59c958e");

        public static string MarkSecond(Element el)
        {
            return el.get_Parameter(_markSecond)?.AsString() ?? "";
        }

        public static string MarkSecond(Element el, string value)
        {
            el.get_Parameter(_markSecond).Set(value);

            return value;
        }

        #endregion

        #region OTeam_Mark.Third

        private static readonly Guid _markThird = new Guid("03c96287-6ff9-4ee6-9173-83f163e012c4");

        public static string MarkThird(Element el)
        {
            return el.get_Parameter(_markThird)?.AsString() ?? "";
        }

        public static string MarkThird(Element el, string value)
        {
            el.get_Parameter(_markThird).Set(value);

            return value;
        }

        #endregion

        #region OTeam_Mark.Furs

        private static readonly Guid _markFours = new Guid("47e0654c-8351-46a6-8c1e-9b4d5644ecad");

        public static string MarkFours(Element el)
        {
            return el.get_Parameter(_markFours)?.AsString() ?? "";
        }

        public static string MarkFours(Element el, string value)
        {
            el.get_Parameter(_markFours).Set(value);

            return value;
        }

        #endregion
        
        #region OTeam_rebar.host

        private static readonly Guid _rebarHost = new Guid("890134f3-1ba1-462d-ab9d-738bf6540753");

        public static ElementId RebarHost(Element el)
        {
            string value = el?.get_Parameter(_rebarHost)?.AsString() ?? "-1";
            return value == "-1" ? ElementId.InvalidElementId : new ElementId(int.Parse(value));
        }

        public static int HostNumber(Element el)
        {
            string value = el?.get_Parameter(_rebarHost)?.AsString() ?? "-1";
            return int.Parse(value);
        }

        public static string HostString(Element el)
        {
            return el?.get_Parameter(_rebarHost)?.AsString() ?? string.Empty;
        }

        public static string HostString(Element el, string value)
        {
            try
            {
                el.get_Parameter(_rebarHost).Set(value);
            }
            catch (Exception)
            {
                throw new Exception($"Can not set rebar host to element - {el.Id}");
            }
            
            return value;
        }

        public static ElementId RebarHost(Element el, ElementId value)
        {
            el.get_Parameter(_rebarHost)?.Set(value.IntegerValue.ToString());
            return value;
        }

        #endregion

        #region OTeam_technical_rebarDimensions

        private static readonly Guid _technicalRebarDimensions = new Guid("b5dc8d0f-a20a-4e86-b3e9-11266609db70");

        public static string TechnicalRebarDimensions(Element el)
        {
            return el?.get_Parameter(_technicalRebarDimensions)?.AsString() ?? "";
        }

        public static string TechnicalRebarDimensions(Element el, string value)
        {
            el.get_Parameter(_technicalRebarDimensions).Set(value);
            return value;
        }

        #endregion

        //09_Інформаційні

        #region OTeam_Розділ

        private static readonly Guid _designDiscipline = new Guid("a4a8e6ae-1746-434a-8b5e-9982abe77fcd");

        public static string DisignDiscipline(Element el)
        {
            return el.get_Parameter(_designDiscipline)?.AsString() ?? "";
        }

        public static string DisignDiscipline(Element el, string value)
        {
            Parameter param = el.get_Parameter(_designDiscipline);
            if (param == null) return value;

            param.Set(value);
            return value;
        }

        #endregion

        #region OTeam_Тип виду

        private static readonly Guid _viewType = new Guid("566b27b6-7e95-4146-8003-74e5b33e837e");

        public static string ViewType(Element el)
        {
            return el.get_Parameter(_viewType)?.AsString() ?? "";
        }

        public static string ViewType(Element el, string value)
        {
            Parameter param = el.get_Parameter(_viewType);
            if (param == null) return value;

            param.Set(value);
            return value;
        }

        #endregion

        #region OTeam_Підтип виду

        private static readonly Guid _viewSubType = new Guid("f47446b6-b4b3-4e88-86eb-a97f82eb08b0");

        public static string ViewSubType(Element el)
        {
            return el.get_Parameter(_viewSubType)?.AsString() ?? "";
        }

        public static string ViewSubType(Element el, string value)
        {
            Parameter param = el.get_Parameter(_viewSubType);
            if (param == null) return value;

            param.Set(value);
            return value;
        }

        #endregion

        #region OTeam_НазваШаблонуВиду

        private static readonly Guid _viewTemplateName = new Guid("63e0c5e0-2641-4da8-bae7-c0ac2e7f960b");

        public static string ViewTemplateName(Element el)
        {
            return el.get_Parameter(_viewTemplateName)?.AsString() ?? "";
        }

        public static string ViewTemplateName(Element el, string value)
        {
            Parameter param = el.get_Parameter(_viewTemplateName);
            if (param == null) return value;

            param.Set(value);
            return value;
        }

        #endregion

        #region OTeam_Клас відповідальності

        private static readonly Guid _importanceClass = new Guid("7b896779-da01-4a02-8467-b05891e2c0b3");

        public static string ImportanceClass(Element projectInfo)
        {
            return projectInfo.get_Parameter(_importanceClass)?.AsString() ?? "";
        }

        public static string ImportanceClass(Element projectInfo, string value)
        {
            Parameter param = projectInfo.get_Parameter(_importanceClass);
            if (param == null) return value;

            param.Set(value);
            return value;
        }

        #endregion

        //Project

        #region _Основний тип арматури

        private const string GENERAL_REBAR_TYPE = "_Основний тип арматури";

        public static ElementId GeneralRebarType(Element el)
        {
            return el?.LookupParameter(GENERAL_REBAR_TYPE)?.AsElementId() ?? ElementId.InvalidElementId;
        }

        public static ElementId GeneralRebarType(Element el, ElementId value)
        {
            el.LookupParameter(GENERAL_REBAR_TYPE)?.Set(value);
            return value;
        }

        #endregion

        #region _Розміщення арматури

        private const string REBAR_LOCATION = "_Розміщення арматури";

        public static ElementId RebarLocation(Element el)
        {
            return el?.LookupParameter(REBAR_LOCATION)?.AsElementId() ?? ElementId.InvalidElementId;
        }

        public static ElementId RebarLocation(Element el, ElementId value)
        {
            el.LookupParameter(REBAR_LOCATION)?.Set(value);
            return value;
        }

        #endregion
        
        #region ТРА

        public static int TRA(Element el)
        {
            return (int)((el.LookupParameter("ТРА")?.AsDouble() ?? 0) * 100);
        }

        public static int TRA(Element el, int value)
        {
            Parameter param = el.LookupParameter("ТРА");
            if (param == null) return value;

            param.Set(value / 100.0);
            return value;
        }

        #endregion

        #region OTA

        public static int OTA(Element el)
        {
            return (int)((el.LookupParameter("ОТА")?.AsDouble() ?? 0) * 100);
        }

        public static int OTA(Element el, int value)
        {
            el.LookupParameter("ОТА")?.Set(value / 100.0);
            return value;
        }

        #endregion

        #region Rebar length

        public static int RebarLength(Element el)
        {
            Parameter param = el.get_Parameter(BuiltInParameter.REBAR_ELEM_LENGTH);
            if (param == null) return 0;

            return (int)(Math.Ceiling(UnitUtils.ConvertFromInternalUnits(
                                         el.get_Parameter(BuiltInParameter.REBAR_ELEM_LENGTH).AsDouble(),
                                         DisplayUnitType.DUT_MILLIMETERS) / 5) * 5);
        }

        #endregion

        #region Total Bar Length

        public static int TotalBarLength(Element el)
        {
            double totalLengthInRevitUnit = el.get_Parameter(BuiltInParameter.REBAR_ELEM_TOTAL_LENGTH)?.AsDouble() ?? 0;
            if (totalLengthInRevitUnit < 0.0001) return 0;

            return (int) (Math.Ceiling(UnitUtils.ConvertFromInternalUnits(totalLengthInRevitUnit, DisplayUnitType.DUT_MILLIMETERS) / 5) * 5);
        }

        #endregion

        #region Rebar quantity

        public static int RebarQuantity(Element el)
        {
            return el.get_Parameter(BuiltInParameter.REBAR_ELEM_QUANTITY_OF_BARS)?.AsInteger() ?? 0;
        }

        #endregion

        #region Sill Height

        public static int SillHeight(Element el)
        {
            return (int)UnitUtils.ConvertFromInternalUnits(
                el.get_Parameter(BuiltInParameter.INSTANCE_SILL_HEIGHT_PARAM).AsDouble(),
                DisplayUnitType.DUT_MILLIMETERS);
        }

        public static int SillHeight(Element el, int value)
        {
            el.get_Parameter(BuiltInParameter.INSTANCE_SILL_HEIGHT_PARAM)
                .Set(UnitUtils.ConvertToInternalUnits(value, DisplayUnitType.DUT_MILLIMETERS));
            return value;
        }

        #endregion

        #region Висота над рівнем

        public static int HeightUnderLevel(Element el)
        {
            return (int)UnitUtils.ConvertFromInternalUnits(el.LookupParameter("Висота над рівнем").AsDouble(),
                DisplayUnitType.DUT_MILLIMETERS);
        }

        public static int HeightUnderLevel(Element el, int value)
        {
            el.LookupParameter("Висота над рівнем")
                .Set(UnitUtils.ConvertToInternalUnits(value, DisplayUnitType.DUT_MILLIMETERS));
            return value;
        }

        #endregion

        #region UniformatKode

        public static string GetUniformatKode(Element el)
        {
            return el?.get_Parameter(BuiltInParameter.UNIFORMAT_CODE)?.AsString() ?? "";
        }

        #endregion

        #region GeneralUsingType

        private const string GENERAL_USING_TYPE = "_1. Основний тип використання";

        public static ElementId GeneralUsingType(Element el)
        {
            return el.LookupParameter(GENERAL_USING_TYPE)?.AsElementId() ?? ElementId.InvalidElementId;
        }

        public static ElementId GeneralUsingType(Element el, ElementId val)
        {

            if (val != ElementId.InvalidElementId)
                el.LookupParameter(GENERAL_USING_TYPE).Set(val);
            return val;
        }

        #endregion

        #region SubUsingType

        private const string SUB_USING_TYPE = "_2. Додатковий тип використання";

        public static ElementId SubUsingType(Element el)
        {
            return el.LookupParameter(SUB_USING_TYPE)?.AsElementId() ?? ElementId.InvalidElementId;
        }

        public static ElementId SubUsingType(Element el, ElementId val)
        {
            if (val != ElementId.InvalidElementId)
                el.LookupParameter(SUB_USING_TYPE).Set(val);
            return val;
        }

        #endregion

        #region DocumentPath

        public static string DocumentPath(Document doc)
        {
            string path;

            if (doc.IsWorkshared)
            {
                var modelPath = doc.GetWorksharingCentralModelPath();
                path = ModelPathUtils.ConvertModelPathToUserVisiblePath(modelPath);
            }
            else
            {
                path = doc.PathName;
            }

            string[] pathItems = path.Split('\\');
            Array.Resize(ref pathItems, pathItems.Length - 1);

            return string.Join("\\", pathItems);
        }

        #endregion

        #region volume

        public static double GetComponentVolume(Element el)
        {
            double volume = el.get_Parameter(BuiltInParameter.HOST_VOLUME_COMPUTED)?.AsDouble()
                            ?? throw new Exception($"Елемент {el.Id} не містить параметер \"об'єм\"");

            return Math.Ceiling(UnitUtils.ConvertFromInternalUnits(volume, DisplayUnitType.DUT_CUBIC_METERS) * 100) / 100;
        }

        #endregion

        #region OTeam_technical_comments

        private static readonly Guid _technicalComments = new Guid("1d29cd57-05f6-4e7a-af38-654a38cf0fa8");

        public static string TechnicalComments(Element el)
        {
            return el.get_Parameter(_technicalComments)?.AsString() ?? "";
        }

        public static string TechnicalComments(Element el, string value)
        {
            try
            {
                el.get_Parameter(_technicalComments).Set(value);
                return value;
            }
            catch (Exception)
            {
                throw new Exception($"Елемент {el.Id} не містить параметру \"OTeam_technical_comments\"");
            }
            
        }

        public static string TechnicalComments(Element el, double value)
        {
            string volume = value.ToString(TwoDecimal);

            try
            {
                el.get_Parameter(_technicalComments).Set(volume);
                return volume;
            }
            catch (Exception)
            {
                throw new Exception($"Елемент {el.Id} не містить параметру \"OTeam_technical_comments\"");
            }
        }

        #endregion


        #region Culuture

        public static CultureInfo OtCulture = new CultureInfo(0x0422)
        {
            NumberFormat = new NumberFormatInfo
            {
                NumberDecimalSeparator = ",",
                NumberGroupSeparator = " "
            }
        };

        public static CultureInfo TwoDecimal = new CultureInfo(0x0422)
        {
            NumberFormat = new NumberFormatInfo
            {
                NumberDecimalSeparator = ",",
                NumberGroupSeparator = " ",
                NumberDecimalDigits = 2,
            }
        };


        #endregion


        // Base

        #region RevitMark

        public static string RevitMark(Element el)
        {
            return el.get_Parameter(BuiltInParameter.ALL_MODEL_MARK)?.AsString() ?? "";
        }

        public static string RevitMark(Element el, string value)
        {
            el.get_Parameter(BuiltInParameter.ALL_MODEL_MARK)?.Set(value);

            return value;
        }

        #endregion
    }
}