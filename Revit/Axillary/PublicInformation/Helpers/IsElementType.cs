﻿namespace OrslavTeam.Revit.Axillary.PublicInformation.Helpers
{
    public static class IsElementType
    {
        #region const --------------------------------------------------------------------------------------------------

        private const string CONCRETE_KOD_START = "B.01";
        private const string CONCRETE_PILE_KOD_START = "B.01.01.02";
        private const string REBAR_KOD_START = "B.03";

        #endregion

        public static bool IsConcreteKod(string assemblyKod) => assemblyKod.StartsWith(CONCRETE_KOD_START);

        public static bool IsRebarKod(string assemblyKod) => assemblyKod.StartsWith(REBAR_KOD_START);

        public static bool IsPileKod(string assemblyKod) => assemblyKod.StartsWith(CONCRETE_PILE_KOD_START);
    }
}