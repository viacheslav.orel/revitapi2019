﻿using System;
using Autodesk.Revit.DB;

namespace OrslavTeam.Revit.Axillary.PublicInformation.Helpers
{
    public static class GetRvtData
    {
        public static Element GetFamily(Element el, Document doc)
        {
            ElementId familyId = el.get_Parameter(BuiltInParameter.ELEM_FAMILY_PARAM)?.AsElementId()
                                 ?? throw new Exception(
                                     $"An error occurred while trying to retrieve the family from the #{el.Id}");

            return doc.GetElement(familyId);
        }

        public static int GetHostId(Element el)
        {
            int hostKod = PublicParameter.HostNumber(el);

            return hostKod != -1 ? hostKod : el.Id.IntegerValue;
        }
    }
}