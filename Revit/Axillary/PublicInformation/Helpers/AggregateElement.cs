﻿using System.Collections.Generic;
using Autodesk.Revit.DB;

namespace OrslavTeam.Revit.Axillary.PublicInformation.Helpers
{
    public static class AggregateElement
    {
        public static Dictionary<string, List<Element>> Agg(string key, Element el,
            Dictionary<string, List<Element>> acc)
        {
            if (acc.ContainsKey(key)) acc[key].Add(el);
            else acc.Add(key, new List<Element> { el });

            return acc;
        }

        public static Dictionary<int, List<Element>> Agg(int key, Element el,
            Dictionary<int, List<Element>> acc)
        {
            if (acc.ContainsKey(key)) acc[key].Add(el);
            else acc.Add(key, new List<Element> { el });

            return acc;
        }
    }
}