﻿using System.Collections.Generic;
using Autodesk.Revit.DB;

namespace OrslavTeam.Revit.Axillary.PublicInformation.Helpers
{
    public static class GroupElement
    {
        public static void AddToDictionary(string key, Element el, ref Dictionary<string, List<Element>> dict)
        {
            if (dict.ContainsKey(key)) dict[key].Add(el);
            else dict.Add(key, new List<Element>{ el });
        }

        public static void AddToDictionary(int key, Element el, ref Dictionary<int, List<Element>> dict)
        {
            if (dict.ContainsKey(key)) dict[key].Add(el);
            else dict.Add(key, new List<Element>{ el });
        }
    }
}