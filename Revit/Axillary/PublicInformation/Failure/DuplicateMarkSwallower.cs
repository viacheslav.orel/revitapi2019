﻿using System.Collections.Generic;
using Autodesk.Revit.DB;

namespace OrslavTeam.Revit.Axillary.PublicInformation.Failure
{
    public class DuplicateMarkSwallower : IFailuresPreprocessor
    {
        public FailureProcessingResult PreprocessFailures(FailuresAccessor failuresAccessor)
        {
            IList<FailureMessageAccessor> failures = failuresAccessor.GetFailureMessages();
            foreach (var f in failures)
            {
                var id = f.GetFailureDefinitionId();
                if (BuiltInFailures.GeneralFailures.DuplicateValue == id) failuresAccessor.DeleteWarning(f);
            }

            return FailureProcessingResult.Continue;
        }
    }
}