﻿using System.Collections.Generic;
using Autodesk.Revit.DB;

namespace OrslavTeam.Revit.Axillary.PublicInformation.Failure
{
    public class ClearAllWarning : IFailuresPreprocessor
    {
        public FailureProcessingResult PreprocessFailures(FailuresAccessor failuresAccessor)
        {
            IList<FailureMessageAccessor> failures = failuresAccessor.GetFailureMessages();
            foreach (FailureMessageAccessor f in failures)
            {
                failuresAccessor.DeleteWarning(f);
            }
            return FailureProcessingResult.Continue;
        }
    }
}