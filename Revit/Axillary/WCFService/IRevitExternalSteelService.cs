﻿using System.ServiceModel;

namespace OrslavTeam.Revit.Axillary.WCFService
{
    [ServiceContract]
    public interface IRevitExternalSteelService
    {
        [OperationContract]
        void GetRevitInfo();
    }
}