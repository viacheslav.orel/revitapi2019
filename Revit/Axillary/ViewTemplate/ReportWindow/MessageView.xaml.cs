﻿using System.Windows;

namespace OrslavTeam.Revit.Axillary.ViewTemplate.ReportWindow
{
    public partial class MessageView : Window
    {
        public MessageView()
        {
            InitializeComponent();
        }
        
        public MessageView(string content)
        {
            InitializeComponent();

            Message.Text = content;
        }
    }
}