﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace OrslavTeam.Revit.Axillary.ViewTemplate.ReportWindow
{
    public partial class ReportView : Window
    {
        public ReportView()
        {
            InitializeComponent();
        }
        
        public ReportView(int allElementQuantity, IReadOnlyList<string> firstColumn, IReadOnlyList<string> secondColumn,
            IReadOnlyList<string> thirdColumn)
        {
            InitializeComponent();

            this.AllElementCount.Text = "Перевірено елементів: " + allElementQuantity.ToString();

            //FirstColumn
            FirstColumn.Header = $"{firstColumn[0]}: {firstColumn.Count - 1}";
            FirstColumnData.Text = string.Join(";\n", firstColumn.Skip(1));

            //SecondColumn
            SecondColumn.Header = $"{secondColumn[0]}: {secondColumn.Count - 1}";
            SecondColumnData.Text = string.Join(";\n", secondColumn.Skip(1));

            //ThirdColumn
            ThirdColumn.Header = $"{thirdColumn[0]}: {thirdColumn.Count - 1}";
            ThirdColumnData.Text = string.Join(";\n", thirdColumn.Skip(1));
        }
    }
}