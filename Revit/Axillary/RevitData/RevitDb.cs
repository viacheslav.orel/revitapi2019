﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using OrslavTeam.Revit.Axillary.PublicInformation;
using OrslavTeam.Revit.Axillary.RevitData.Model;

namespace OrslavTeam.Revit.Axillary.RevitData
{
    public static class RevitDb
    {
        public static ProfileInfo GetComponentInfo(int productId, int elementId)
        {
            var result = new ProfileInfo
            {
                Id = elementId,
                Name = "Error!"
            };

            using (var connection = new SQLiteConnection(PublicParameter.DBPath))
            {
                connection.Open();
                var parameterNames = new List<string> {"element_name", "profile_name"};

                using (SQLiteCommand getParameterNames = connection.CreateCommand())
                {
                    getParameterNames.CommandText =
                        @"SELECT name FROM product_mix_for_element_steel_parameters pmfesp 
                    JOIN element_steel_parameters ON pmfesp.element_steel_parameters_id = element_steel_parameters.id " +
                        $"WHERE product_mix_id = {productId}";
                    getParameterNames.CommandType = CommandType.Text;

                    using (SQLiteDataReader reader = getParameterNames.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            parameterNames.Add(reader.GetString(0));
                        }

                        reader.Close();
                    }
                }

                using (SQLiteCommand getComponentInfo = connection.CreateCommand())
                {
                    getComponentInfo.CommandText =
                        $"SELECT {string.Join(", ", parameterNames)} FROM element_steel " +
                        $"WHERE product_mix_id = {productId} AND element_id = {elementId}";
                    getComponentInfo.CommandType = CommandType.Text;

                    using (SQLiteDataReader reader = getComponentInfo.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            result.Name = reader.GetString(0);
                            result.ProfileName = reader.GetString(1);

                            var resultDimensions = new List<KeyValuePair<string, double>>();
                            for (int i = 2, max = reader.FieldCount; i < max; i++)
                            {
                                resultDimensions.Add(
                                    new KeyValuePair<string, double>(reader.GetName(i), reader.GetDouble(i)));
                            }

                            result.Parameters = resultDimensions.ToArray();
                        }

                        reader.Close();
                    }
                }

                connection.Close();
            }

            return result;
        }

        public static ProductMixInfo GetProductMixInfo(int productMixId)
        {
            var result = new ProductMixInfo();
            
            try
            {
                using (var connection = new SQLiteConnection(PublicParameter.DBPath))
                {
                    connection.Open();

                    using (var command = new SQLiteCommand(connection))
                    {
                        command.CommandText = "SELECT product_name, product_number " +
                                              "FROM product_mix " +
                                              $"WHERE id = {productMixId}";
                        command.CommandType = CommandType.Text;

                        SQLiteDataReader reader = command.ExecuteReader();
                        reader.Read();

                        result.ProductMixName = reader.GetString(0);
                        result.ProductMixNumber = reader.GetString(1);

                        reader.Close();
                    }

                    connection.Close();
                }
            }
            catch (Exception)
            {
                result.ProductMixNumber = result.ProductMixName = "Error";
            }
            
            return result;
        }

        public static MaterialInfo GetMaterialInfo(int materialId)
        {
            var result = new MaterialInfo();

            try
            {
                using (var connection = new SQLiteConnection(PublicParameter.DBPath))
                {
                    connection.Open();

                    using (var command = new SQLiteCommand(connection))
                    {
                        command.CommandText =
                            "SELECT product_mix.product_number product_mix.product_name material_steel.element_name " +
                            "FROM product_mix JOIN material_steel ON product_mix.id = material_steel.product_mix_id " +
                            $"WHERE material_steel.id = {materialId}";
                        command.CommandType = CommandType.Text;
                        
                        SQLiteDataReader reader = command.ExecuteReader();
                        reader.Read();

                        result.ProductMixNumber = reader.GetString(0);
                        result.ProductMixName = reader.GetString(1);
                        result.Name = reader.GetString(2);
                        
                        reader.Close();
                    }
                    
                    connection.Close();
                }
            }
            catch (Exception)
            {
                result.Name = result.ProductMixNumber = result.ProductMixName = "Error";
            }

            return result;
        }

        public static string GetMaterialName(int materialId)
        {
            var result = "";

            try
            {
                using (var connection = new SQLiteConnection(PublicParameter.DBPath))
                {
                    connection.Open();

                    using (var command = new SQLiteCommand(connection))
                    {
                        command.CommandText =
                            $"SELECT element_name FROM material_steel WHERE id = {materialId}";
                        command.CommandType = CommandType.Text;
                        
                        SQLiteDataReader reader = command.ExecuteReader();
                        reader.Read();

                        result = reader.GetString(0);
                        
                        reader.Close();
                    }
                    
                    connection.Close();
                }
            }
            catch (Exception)
            {
                result = $"Error #{materialId}";
            }

            return result;
        }
    }
}