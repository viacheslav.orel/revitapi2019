﻿﻿using System.Collections.Generic;

 namespace OrslavTeam.Revit.Axillary.RevitData.Model
{
    public struct ProfileInfo
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string ProfileName { get; set; }

        public KeyValuePair<string, double>[] Parameters { get; set; }
    }
}