﻿namespace OrslavTeam.Revit.Axillary.RevitData.Model
{
    public struct ProductMixInfo
    {
        public string ProductMixNumber { get; set; }
        public string ProductMixName { get; set; }
        public string ProductMixFullName => $"{ProductMixNumber}. {ProductMixName}";
    }
}