﻿namespace OrslavTeam.Revit.Axillary.RevitData.Model
{
    public struct MaterialInfo
    {
        public string Name { get; set; }
        public string ProductMixNumber { get; set; }
        public string ProductMixName { get; set; }
    }
}