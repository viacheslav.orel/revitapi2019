﻿using System;
using System.Globalization;
using System.Xml;

namespace OrslavTeam.Revit.Axillary.RevitData
{
    public static class GetData
    {
        #region Attribute
        
        private static XmlElement _xProductMix;

#pragma warning disable 414
        private static XmlElement _xRebarInfo = null;
#pragma warning restore 414


        private const string ProductMixPath = @"C:\ProgramData\Autodesk\Revit\Addins\2019\OrslavTeam\xmlFile\ProductMix.xml";
        private const string RebarInfoPath = @"C:\ProgramData\Autodesk\Revit\Addins\2019\OrslavTeam\xmlFile\Rebar.xml";
        
        #endregion
        
        #region ProductMixMethod
        
        public static string GetProductMixNumber(int productMixKey)
        {
            XmlAttributeCollection xmlAttributeCollection =
                _xProductMix?.SelectSingleNode($"ProductMix[@id='{productMixKey}']")?.Attributes;
            if (xmlAttributeCollection != null)
                return xmlAttributeCollection["product_number"]?.Value ?? "Error product mix kod";

            XmlDocument xDoc = new XmlDocument();
            xDoc.Load(ProductMixPath);
            _xProductMix = xDoc.DocumentElement;
            XmlAttributeCollection attributeCollection = _xProductMix
                ?.SelectSingleNode($"ProductMix[@id='{productMixKey}']")
                ?.Attributes;
            if (attributeCollection != null)
                return attributeCollection["product_number"]?.Value ?? "Error product mix kod";

            return null;
        }
        public static void GetProductMixInfo(int productMixKey, out string productMixNumber, out string productMixPath)
        {
            if (_xProductMix == null)
            {
                XmlDocument xDoc = new XmlDocument();
                xDoc.Load(ProductMixPath);
                _xProductMix = xDoc.DocumentElement;
            }
            XmlNode node = _xProductMix?.SelectSingleNode($"ProductMix[@id='{productMixKey}']");
            productMixNumber = node?.Attributes?["product_number"]?.Value ?? "Error product mix kod";
            productMixPath = node?.Attributes?["scheet_path"]?.Value ?? string.Empty;
        }
        
        #endregion
        
        #region Element
        
        public static string GetElementName(int elementKod, XmlElement xmlElement)
        {
            XmlNodeList nodeList = xmlElement.ChildNodes;
            foreach (XmlNode node in nodeList)
            {
                if (node.Attributes == null || node.Attributes["id"].Value != elementKod.ToString()) continue;
                if (((XmlElement) node).LocalName == "RebarInfo")
                    return "⌀" + node.Attributes["diametr"].Value.Replace('.', ',');
                return node.Attributes["element_name"].Value;
            }
            return "Error element kod";
        }
        
        #endregion

        #region RebarMethod
        
        public static string GetRebarName(int rebarNumber)
        {
            if (_xProductMix != null)
                return _xProductMix.SelectSingleNode($"RebarInfo[@id='{rebarNumber}']")
                           ?.Attributes?["element_name"]?.Value ?? "Error component number";
            XmlDocument xDoc = new XmlDocument();
            xDoc.Load(RebarInfoPath);
            _xProductMix = xDoc.DocumentElement;

            return _xProductMix?.SelectSingleNode($"RebarInfo[@id='{rebarNumber}']")?.Attributes?["element_name"]
                       ?.Value ?? "Error component number";
        }
        public static void GetRebarInfo(int rebarKey, ref string rebarName, ref double massPerUnitLength)
        {
            if (rebarName == null) throw new ArgumentNullException(nameof(rebarName));
            if (_xProductMix == null)
            {
                XmlDocument xDoc = new XmlDocument();
                xDoc.Load(RebarInfoPath);
                _xProductMix = xDoc.DocumentElement;
            }

            XmlNode node = _xProductMix?.SelectSingleNode($"RebarInfo[@id='{rebarKey}']");
            if (node?.Attributes == null) return;
            rebarName = node.Attributes["element_name"]?.Value ?? "Error rebar kod";
            massPerUnitLength = Convert.ToDouble(node.Attributes["mass_per_unit_length"]?.Value ?? "0",
                CultureInfo.InvariantCulture);
        }
        public static double GetRebarDiamete(int rebarKey)
        {
            if (_xProductMix != null)
                return Convert.ToDouble(
                    _xProductMix.SelectSingleNode($"RebarInfo[@id='{rebarKey}']")
                        ?.Attributes?["diametr"]?.Value ?? "0", CultureInfo.InvariantCulture);
            XmlDocument xDoc = new XmlDocument();
            xDoc.Load(RebarInfoPath);
            _xProductMix = xDoc.DocumentElement;
            if (_xProductMix != null)
                return Convert.ToDouble(
                    _xProductMix.SelectSingleNode($"RebarInfo[@id='{rebarKey}']")?.Attributes?["diametr"]?.Value ?? "0",
                    CultureInfo.InvariantCulture);
            return 0;
        }
        public static double GetDiameterOfBand (int rebarKey)
        {
            if (_xProductMix != null)
                return Convert.ToDouble(
                    _xProductMix.SelectSingleNode($"RebarInfo[@id='{rebarKey}']")
                        ?.Attributes?["minimum_diametr_of_band"]?.Value ?? "100", CultureInfo.InvariantCulture);
            XmlDocument xDoc = new XmlDocument();
            xDoc.Load(RebarInfoPath);
            _xProductMix = xDoc.DocumentElement;
            return Convert.ToDouble(
                _xProductMix?.SelectSingleNode($"RebarInfo[@id='{rebarKey}']")?.Attributes?["minimum_diametr_of_band"]
                    ?.Value ?? "100", CultureInfo.InvariantCulture);
        }
        
        #endregion

        public static void CloseConnection()
        {
            _xProductMix = null;
            _xProductMix = null;
        }
    }
}