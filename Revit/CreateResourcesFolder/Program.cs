﻿using System;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Principal;

namespace OrslavTeam.Revit.CreateResourcesFolder
{
    internal class Program
    {
        private const string LIBRARY_RVT = @"C:\ProgramData\Autodesk\RVT 2019\Libraries\PromAspect";

        private const string ADDIN_RVT = @"C:\ProgramData\Autodesk\Revit\Addins\2019\OTeam";
        private const string ADDIN_MANIFEST = @"C:\ProgramData\Autodesk\Revit\Addins\2019\OrslavTeam.addin";
        private const string ADDIN_RESOURCES_RVT = @"C:\Program Files\Autodesk\Revit 2019\";
        private const string PROJECT_PATH = @"C:\_bim_project";

        private const string TEXTURE_RVT = @"C:\Program Files (x86)\Common Files\Autodesk Shared\Materials\Textures\OT";

        private const string MATERIAL_RVT_PATH =
            @"C:\Program Files (x86)\Common Files\Autodesk Shared\Materials\2019\OTeam_Material.adsklib";


        [DllImport("kernel32.dll")]
        private static extern bool CreateSymbolicLink(string lpSymlinkFileName, string lpTargetFileName, int dwFlags);

        private const int SYMBOLIC_LINK_FLAG_FILE = 0x0;
        private const int SYMBOLIC_LINK_FLAG_DIRECTORY = 0x1;

        public static void Main(string[] args)
        {
            if (!IsRunningAsAdmin())
            {
                Console.WriteLine(
                    "You don't have administrator permissions. Run application with administrator permissions");
                Console.WriteLine("Pres any key to exit");
                Console.ReadKey();
                return;
            }

            Console.WriteLine("Current path to project folder:");
            string projectPath = Console.ReadLine();

            string localPat = Directory.GetCurrentDirectory();
            if (Directory.Exists(LIBRARY_RVT)) Directory.Delete(LIBRARY_RVT, true);

            if (Directory.Exists(ADDIN_RVT)) Directory.Delete(ADDIN_RVT, true);
            if (File.Exists(ADDIN_MANIFEST)) File.Delete(ADDIN_MANIFEST);

            if (File.Exists(MATERIAL_RVT_PATH)) File.Delete(MATERIAL_RVT_PATH);
            if (Directory.Exists(TEXTURE_RVT)) Directory.Delete(TEXTURE_RVT, true);

            if (Directory.Exists(PROJECT_PATH)) Directory.Delete(PROJECT_PATH, true);

            Console.WriteLine(
                CreateSymbolicLink(LIBRARY_RVT, localPat + @"\Resources\Revit\2019\rvt_lib",
                    SYMBOLIC_LINK_FLAG_DIRECTORY)
                    ? "Family library integrate - Successfully completed"
                    : "Family library integrate - Execute failed!");

            Console.WriteLine(
                CreateSymbolicLink(ADDIN_RVT,
                    localPat + @"\Resources\Revit\2019\rvt_res\Ресурси\addInFile\OrslavTeam",
                    SYMBOLIC_LINK_FLAG_DIRECTORY)
                && CreateSymbolicLink(ADDIN_MANIFEST,
                    localPat + @"\Resources\Revit\2019\rvt_res\Ресурси\addInFile\OrslavTeam.addin",
                    SYMBOLIC_LINK_FLAG_FILE)
                    ? "Addin files integrate - Successfully completed"
                    : "Addin files integrate - Execute failed!"
            );

            if (Directory.Exists(localPat + @"\Resources\Revit\2019\rvt_res\Ресурси\addInFile\OTResources"))
            {
                string[] resourceNames =
                    Directory.GetFiles(localPat + @"\Resources\Revit\2019\rvt_res\Ресурси\addInFile\OTResources");

                foreach (string filePath in resourceNames)
                {
                    string linkPath = ADDIN_RESOURCES_RVT + filePath.Split('\\').Last();
                    if (File.Exists(linkPath))
                    {
                        File.Delete(linkPath);
                    }

                    CreateSymbolicLink(linkPath, filePath, SYMBOLIC_LINK_FLAG_FILE);
                }
            }

            Console.WriteLine(
                CreateSymbolicLink(PROJECT_PATH, projectPath, SYMBOLIC_LINK_FLAG_DIRECTORY)
                ? "Project folder integrate - Successfully completed"
                : "Project folder integrate - Execute failed!"
                );

            Console.WriteLine(
                CreateSymbolicLink(TEXTURE_RVT,
                    localPat + @"\Resources\Revit\2019\rvt_res\Матеріали\Texture",
                    SYMBOLIC_LINK_FLAG_DIRECTORY)
                    ? "Materials texture integrate - Successfully completed"
                    : "Materials texture integrate - Execute failed!"
            );

            Console.WriteLine(
                CreateSymbolicLink(MATERIAL_RVT_PATH,
                    localPat + @"/Resources\Revit\2019\rvt_res\Матеріали\OTeam_Material.adsklib",
                    SYMBOLIC_LINK_FLAG_FILE)
                    ? "Materials integrate - Successfully completed"
                    : "Materials integrate - Execute failed!"
            );

            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("All integrates successfully completed");
            Console.WriteLine("Pres any key to exit");
            Console.ReadKey();
        }

        private static bool IsRunningAsAdmin()
        {
            //Получим пользователя
            WindowsIdentity windowsIdentity = WindowsIdentity.GetCurrent();
            //Получим роль
            WindowsPrincipal windowsPrincipal = new WindowsPrincipal(windowsIdentity);
            //Вернём ТРУ, если приложение имеет права администратора
            return windowsPrincipal.IsInRole(WindowsBuiltInRole.Administrator);
        }
    }
}