﻿using Autodesk.Revit.DB;
using Autodesk.Revit.UI.Selection;

namespace OrslavTeam.Revit.Concrete.Helper.Selection
{
    public class SelectingScheduleAnnotation : ISelectionFilter
    {
        public bool AllowElement(Element elem)
        {
            return elem.get_Parameter(BuiltInParameter.ELEM_FAMILY_PARAM).AsValueString()
                .Equals("GEN_OTeam_Специфікація-2D_Заголовок");
        }

        public bool AllowReference(Reference reference, XYZ position)
        {
            return false;
        }
    }
}