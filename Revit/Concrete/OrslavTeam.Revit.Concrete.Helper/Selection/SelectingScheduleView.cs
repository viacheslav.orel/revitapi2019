﻿using Autodesk.Revit.DB;
using Autodesk.Revit.UI.Selection;

namespace OrslavTeam.Revit.Concrete.Helper.Selection
{
    public class SelectingScheduleView : ISelectionFilter
    {
        public bool AllowElement(Element elem)
        {
            return elem.Category.Id.IntegerValue == -2000570;
        }

        public bool AllowReference(Reference reference, XYZ position)
        {
            return false;
        }
    }
}