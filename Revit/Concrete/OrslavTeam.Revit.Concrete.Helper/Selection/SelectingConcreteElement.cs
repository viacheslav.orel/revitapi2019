﻿using Autodesk.Revit.DB;
using Autodesk.Revit.UI.Selection;

namespace OrslavTeam.Revit.Concrete.Helper.Selection
{
    public class SelectingConcreteElement : ISelectionFilter
    {
        private Document _doc { get; }

        public SelectingConcreteElement(Document doc)
        {
            _doc = doc;
        }

        public bool AllowElement(Element elem)
        {
            return IsCurrentComponent.IsConcreteComponent(elem, _doc);
        }

        public bool AllowReference(Reference reference, XYZ position)
        {
            return false;
        }
    }
}
