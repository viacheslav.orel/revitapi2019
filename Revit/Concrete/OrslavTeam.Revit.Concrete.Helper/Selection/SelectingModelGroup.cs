﻿using Autodesk.Revit.DB;
using Autodesk.Revit.UI.Selection;

namespace OrslavTeam.Revit.Concrete.Helper.Selection
{
    public class SelectingModelGroup : ISelectionFilter
    {
        public bool AllowElement(Element elem)
        {
            return elem.Name.EndsWith("_!");
        }

        public bool AllowReference(Reference reference, XYZ position)
        {
            return false;
        }
    }
}