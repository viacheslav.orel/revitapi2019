﻿using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Architecture;
using OrslavTeam.Revit.Axillary.PublicInformation;
using OrslavTeam.Revit.Axillary.PublicInformation.Helpers;
using OrslavTeam.Revit.Axillary.ViewTemplate.ReportWindow;

namespace OrslavTeam.Revit.Concrete.Helper
{
    public static class IsCurrentComponent
    {
        #region is concrete element ------------------------------------------------------------------------------------

        private static Element GetFamilyType(Element elem, Document doc)
        {
            switch (elem)
            {
                case FamilyInstance familyInstance:
                    return familyInstance.Symbol;
                case Floor floor:
                    return floor.FloorType;
                case Wall wall:
                    return wall.WallType;
                case Stairs _:
                    return doc.GetElement(elem.GetTypeId());
                default:
                    return null;
            }
        }

        public static bool IsConcreteComponent(Element elem, Document doc)
        {
            Element familyType = GetRvtData.GetFamily(elem, doc);
            return familyType != null && PublicParameter.GetUniformatKode(familyType).StartsWith("B.01");
        }

        public static bool IsCastInPlaceElement(Element elem, Document doc)
        {
            Element familyType = GetFamilyType(elem, doc);
            if (familyType == null) return false;

            return PublicParameter.GetUniformatKode(familyType).StartsWith("B.01")
                   && PublicParameter.StructuralType(familyType) == "Конструкції монолітні";
        }

        public static bool IsConcreteGroup(Element el)
        {
            return !el.Name.StartsWith("&_");
        }

        //public static bool IsConcreteOrRebar(Element el, Document doc)
        //{
        //    Element familyType = GetRvtData.GetFamily(el, doc);
        //    string assemblyKod = PublicParameter.GetUniformatKode(familyType);

        //    return assemblyKod.StartsWith("B.01") 
        //           || (assemblyKod.StartsWith("B.03") && PublicParameter.RebarScheduleFilter(familyType) != 3);
        //}

        //public static bool IsPile(Element el, Document doc)
        //{
        //    Element familyType = GetRvtData.GetFamily(el, doc);
        //    string assemblyKod = PublicParameter.GetUniformatKode(familyType);

        //    return assemblyKod == "B.02.01.01";
        //}

        #endregion


        #region is view ------------------------------------------------------------------------------------------------

        public static bool IsScheduleView(Element element)
        {
            if (element.Category.Id.IntegerValue == -2000573) return true;

            new MessageView("Поточний вид не є специфікацією").ShowDialog();
            return false;
        }

        #endregion
    }
}