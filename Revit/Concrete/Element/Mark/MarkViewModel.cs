﻿using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using OrslavTeam.Revit.Axillary.PublicInformation;
using OrslavTeam.Revit.Axillary.PublicInformation.Failure;
using OrslavTeam.Revit.Axillary.ViewTemplate.Infrastructure;
using OrslavTeam.Revit.Axillary.ViewTemplate.ReportWindow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Input;
using Group = Autodesk.Revit.DB.Group;

namespace OrslavTeam.Revit.Concrete.Element.Mark
{
    public class MarkViewModel : ObservableObject
    {
        #region Fields

        //Precast

        #region PrcFoundation

        private static string _prcFoundation = "Ф";

        public string PrcFoundation
        {
            get => _prcFoundation;
            set
            {
                _prcFoundation = value;
                RaisePropertyChangedEvent("PrcFoundation");
            }
        }

        #endregion

        #region PrcSolidBlock

        private static string _prcSolidBlock = "ФБС";

        public string PrcSolidBlock
        {
            get => _prcSolidBlock;
            set
            {
                _prcSolidBlock = value;
                RaisePropertyChangedEvent("PrcSolidBlock");
            }
        }

        #endregion

        #region PrcBaseSlab

        private static string _prcBaseSlab = "ФП";

        public string PrcBaseSlab
        {
            get => _prcBaseSlab;
            set
            {
                _prcBaseSlab = value;
                RaisePropertyChangedEvent("PrcBaseSlab");
            }
        }

        #endregion

        #region PrcColumn

        private static string _prcColumn = "К";

        public string PrcColumn
        {
            get => _prcColumn;
            set
            {
                _prcColumn = value;
                RaisePropertyChangedEvent("PrcColumn");
            }
        }

        #endregion

        #region PrcBeam

        private static string _prcBeam = "Б";

        public string PrcBeam
        {
            get => _prcBeam;
            set
            {
                _prcBeam = value;
                RaisePropertyChangedEvent("PrcBeam");
            }
        }

        #endregion

        #region PrcFloor

        private static string _prcFloor = "П";

        public string PrcFloor
        {
            get => _prcFloor;
            set
            {
                _prcFloor = value;
                RaisePropertyChangedEvent("PrcFloor");
            }
        }

        #endregion

        #region PrcWall

        private static string _prcWall = "ПС";

        public string PrcWall
        {
            get => _prcWall;
            set
            {
                _prcWall = value;
                RaisePropertyChangedEvent("PrcWall");
            }
        }

        #endregion

        #region PrcStairs

        private static string _prcStairs = "С";

        public string PrcStairs
        {
            get => _prcStairs;
            set
            {
                _prcStairs = value;
                RaisePropertyChangedEvent("PrcStairs");
            }
        }

        #endregion

        //SuitReinforced

        #region SrcFoundation

        private static string _srcFoundation = "Фм";

        public string SrcFoundation
        {
            get => _srcFoundation;
            set
            {
                _srcFoundation = value;
                RaisePropertyChangedEvent("SrcFoundation");
            }
        }

        #endregion

        #region SrcBaseSlab

        private static string _srcBaseSlab = "ФПм";

        public string SrcBaseSlab
        {
            get => _srcBaseSlab;
            set
            {
                _srcBaseSlab = value;
                RaisePropertyChangedEvent("SrcBaseSlab");
            }
        }

        #endregion

        #region SrcColumn

        private static string _srcColumn = "Км";

        public string SrcColumn
        {
            get => _srcColumn;
            set
            {
                _srcColumn = value;
                RaisePropertyChangedEvent("SrcColumn");
            }
        }

        #endregion

        #region SrcBeam

        private static string _srcBeam = "Бм";

        public string SrcBeam
        {
            get => _srcBeam;
            set
            {
                _srcBeam = value;
                RaisePropertyChangedEvent("SrcBeam");
            }
        }

        #endregion

        #region SrcFloor

        private static string _srcFloor = "Пм";

        public string SrcFloor
        {
            get => _srcFloor;
            set
            {
                _srcFloor = value;
                RaisePropertyChangedEvent("SrcFloor");
            }
        }

        #endregion

        #region SrcWall

        private static string _srcWall = "Стм";

        public string SrcWall
        {
            get => _srcWall;
            set
            {
                _srcWall = value;
                RaisePropertyChangedEvent("SrcWall");
            }
        }

        #endregion

        #region SrcStairs

        private static string _srcStairs = "См";

        public string SrcStairs
        {
            get => _srcStairs;
            set
            {
                _srcStairs = value;
                RaisePropertyChangedEvent("SrcStairs");
            }
        }

        #endregion

        #region SrcButtress

        private static string _srcButtress = "КФм";

        public string SrcButtress
        {
            get => _srcButtress;
            set
            {
                _srcButtress = value;
                RaisePropertyChangedEvent("SrcButtress");
            }
        }

        #endregion

        #endregion

        #region Command

        private DelegateCommand _markAll;
        public ICommand MarkAll => _markAll ?? (_markAll = new DelegateCommand(ExecuteMarkAll, null));

        private void ExecuteMarkAll(object obj)
        {
            MarkEventHandler.RevitCommand = RemarkElement;
            MarkEventHandler.ExEvent.Raise();
        }

        #endregion

        #region RegexOptions

        private readonly Regex _dontMarkGroup = new Regex(@"^&", RegexOptions.Compiled);
        private readonly Regex _handmadeMark = new Regex(@"^!", RegexOptions.Compiled);
        private readonly Regex _onlyLetters = new Regex(@"\d", RegexOptions.Compiled);
        private readonly Regex _onlyNumber = new Regex(@"\D", RegexOptions.Compiled);
        private readonly Regex _isSrcElement = new Regex(@"SRC", RegexOptions.Compiled);
        private readonly Regex _isStairsSrc = new Regex(@"^Stair", RegexOptions.Compiled);
        private readonly Regex _floorFoundation = new Regex(@"^SRCPlate", RegexOptions.Compiled);
        private readonly Regex _concreteElement = new Regex(@"^SRC|^PRC", RegexOptions.Compiled);

        #endregion

        #region Dictionary

        private Document _document;

        private readonly List<string> _errorChangedElementsList = new List<string>() { "Помилок запису" };
        private readonly List<string> _dontChangedElementList = new List<string>() { "Пропущено" };
        private readonly List<string> _changedElementList = new List<string>() { "Змінено" };

        private int _countElement = 0;

        private readonly Dictionary<string, Dictionary<string, Dictionary<string, Dictionary<string, int>>>> _markDict =
            new Dictionary<string, Dictionary<string, Dictionary<string, Dictionary<string, int>>>>();

        private Dictionary<string, List<AssemblyInstance>> _assemblyToChange =
            new Dictionary<string, List<AssemblyInstance>>();

        private List<Autodesk.Revit.DB.Element> _precastDict = new List<Autodesk.Revit.DB.Element>();

        #endregion

        #region MarkMethod

        private void RemarkElement(UIApplication uiApplication)
        {
            Dictionary<string, string> prefixMarkFromAssemblyCode = new Dictionary<string, string>()
            {
                //Foundation
                {"B.01.01.03.01", PrcBaseSlab},
                {"B.01.01.03.02", SrcBaseSlab},
                {"B.01.01.04.02", PrcSolidBlock},
                {"B.01.01.05.01", PrcFoundation},
                {"B.01.01.05.02", SrcFoundation},
                //Framework
                {"B.01.02.01.01", PrcColumn},
                {"B.01.02.01.02", SrcColumn},
                {"B.01.02.02.01", PrcBeam},
                {"B.01.02.02.02", SrcBeam},
                //Stairs
                {"B.01.03.01.01", PrcStairs},
                {"B.01.03.02.01", PrcStairs},
                {"B.01.03.01.02", PrcStairs},
                {"B.01.03.02.02", PrcStairs},
                //Floor
                {"B.01.04.01", PrcFloor},
                {"B.01.04.02", SrcFloor},
                //Wall
                {"B.01.05.01", PrcWall},
                {"B.01.05.02", SrcWall}
            };

            Document doc = uiApplication.ActiveUIDocument.Document;
            _document = doc;

            //Set mark for group
            FilteredElementCollector groupInView =
                new FilteredElementCollector(doc, doc.ActiveView.Id).OfCategory(BuiltInCategory.OST_IOSModelGroups);

            Dictionary<string, List<Group>> handmadeGroupDict = new Dictionary<string, List<Group>>();
            Dictionary<string, List<Group>> stairsGroupDict = new Dictionary<string, List<Group>>();
            Dictionary<string, List<AssemblyInstance>> automaticsGroupDict =
                new Dictionary<string, List<AssemblyInstance>>();

            foreach (Autodesk.Revit.DB.Element elementGroup in groupInView)
            {
                string groupName = elementGroup.Name;

                if (_dontMarkGroup.IsMatch(groupName)) continue;

                if (_handmadeMark.IsMatch(groupName))
                {
                    try
                    {
                        handmadeGroupDict.Add(groupName.Split('_')[1], new List<Group> { elementGroup as Group });
                    }
                    catch (ArgumentException)
                    {
                        handmadeGroupDict[groupName.Split('_')[1]].Add(elementGroup as Group);
                    }
                }
                else if (_isStairsSrc.IsMatch(groupName))
                {
                    try
                    {
                        stairsGroupDict.Add(groupName.Split('_')[1], new List<Group> { elementGroup as Group });
                    }
                    catch (ArgumentException)
                    {
                        stairsGroupDict[groupName.Split('_')[1]].Add(elementGroup as Group);
                    }
                }
                else
                {
                    List<ElementId> subElement = (elementGroup as Group).GetMemberIds().Where(elId =>
                        _isSrcElement.IsMatch(doc.GetElement(elId)
                            .get_Parameter(BuiltInParameter.ELEM_FAMILY_AND_TYPE_PARAM).AsValueString())).ToList();
                    using (Transaction tr = new Transaction(doc, "CreateAssembly"))
                    {
                        var failureOptions = tr.GetFailureHandlingOptions();
                        failureOptions.SetFailuresPreprocessor(new DuplicateMarkSwallower());
                        tr.SetFailureHandlingOptions(failureOptions);

                        tr.Start();
                        AssemblyInstance assembly = AssemblyInstance.Create(doc, subElement,
                            doc.GetElement(subElement.First()).Category.Id);
                        tr.Commit();

                        try
                        {
                            automaticsGroupDict.Add(groupName.Split('_')[0], new List<AssemblyInstance> { assembly });
                        }
                        catch (Exception)
                        {
                            automaticsGroupDict[groupName.Split('_')[0]].Add(assembly);
                        }

                        _changedElementList.Add(elementGroup.Id.ToString());
                        _countElement++;
                    }
                }
            }

            if (handmadeGroupDict.Count != 0)
            {
                foreach (KeyValuePair<string, List<Group>> gr in handmadeGroupDict)
                {
                    string markPrefix = _onlyLetters.Replace(gr.Key, "");
                    string markNumber = _onlyNumber.Replace(gr.Key, "");

                    IOrderedEnumerable<Group> sortedGrList =
                        gr.Value.OrderBy(g => ((LocationPoint)g.Location).Point.Z);

                    foreach (Group grEl in sortedGrList)
                    {
                        SetMarkToGroupElement(grEl.GetMemberIds(), markPrefix, markNumber, "0",
                            GetUniqueMarkNumber(markPrefix, markNumber));

                        _changedElementList.Add(grEl.Id.ToString());
                        _countElement++;
                    }
                }
            }

            if (stairsGroupDict.Count != 0)
            {
                foreach (KeyValuePair<string, List<Group>> gr in stairsGroupDict)
                {
                    string markPrefix = _onlyLetters.Replace(gr.Key, "");

                    IEnumerable<IGrouping<string, Group>> sortAndGroupGrValue = from grEl in gr.Value
                                                                                let groupName = grEl.Name
                                                                                orderby groupName, ((LocationPoint)grEl.Location).Point.Z
                                                                                group grEl by groupName;

                    foreach (IGrouping<string, Group> grValue in sortAndGroupGrValue)
                    {
                        string markFirst = GetUniqueMarkNumber(markPrefix);


                        using (Transaction tr = new Transaction(doc, "SetMarkToStairsElement"))
                        {
                            var failureOptions = tr.GetFailureHandlingOptions();
                            failureOptions.SetFailuresPreprocessor(new DuplicateMarkSwallower());
                            tr.SetFailureHandlingOptions(failureOptions);

                            tr.Start();
                            foreach (Group grEl in grValue)
                            {
                                IList<ElementId> subElInGr = grEl.GetMemberIds();
                                string markThird = GetUniqueMarkNumber(markPrefix, markFirst, "0");

                                int markElement = 0;
                                foreach (ElementId elId in subElInGr)
                                {
                                    Autodesk.Revit.DB.Element el = doc.GetElement(elId);
                                    try
                                    {
                                        if (el.Category.GetHashCode() == -2000120)
                                        {
                                            el.get_Parameter(BuiltInParameter.ALL_MODEL_MARK)
                                                .Set(
                                                    $"{markPrefix}{markFirst}-0-{markThird}-{markElement++.ToString()}");
                                            PublicParameter.MarkText(el, $"{markPrefix}{markFirst}");

                                            _changedElementList.Add(grEl.Id.ToString());
                                            _countElement++;
                                        }
                                    }
                                    catch (Exception)
                                    {
                                        // ignored
                                    }
                                }
                            }

                            tr.Commit();
                        }
                    }
                }
            }

            if (automaticsGroupDict.Count != 0)
            {
                foreach (KeyValuePair<string, List<AssemblyInstance>> keyValuePair in automaticsGroupDict)
                {
                    IEnumerable<IGrouping<string, AssemblyInstance>> groupAssemblyInstance = keyValuePair
                        .Value.OrderBy(assembly => assembly.GetTypeId().ToString())
                        .ThenBy(assembly => ((LocationPoint)assembly.Location).Point.Z)
                        .GroupBy(assembly => assembly.GetTypeId().ToString());
                    foreach (IGrouping<string, AssemblyInstance> assemblyInstances in groupAssemblyInstance)
                    {
                        string markFirstPart = GetUniqueMarkNumber(keyValuePair.Key);

                        foreach (AssemblyInstance instance in assemblyInstances)
                        {
                            SetMarkToGroupElement(instance.GetMemberIds().ToList(), keyValuePair.Key, markFirstPart,
                                "0", GetUniqueMarkNumber(keyValuePair.Key, markFirstPart, "0"));
                        }
                    }

                    using (Transaction tr = new Transaction(doc, "disassemble"))
                    {
                        FailureHandlingOptions failureOptions = tr.GetFailureHandlingOptions();
                        failureOptions.SetFailuresPreprocessor(new ClearAllWarning());
                        tr.SetFailureHandlingOptions(failureOptions);

                        tr.Start();
                        foreach (AssemblyInstance instance in keyValuePair.Value)
                        {
                            instance.Disassemble();
                        }

                        tr.Commit();
                    }
                }
            }

            //Set mark for element
            IEnumerable<IGrouping<string, Autodesk.Revit.DB.Element>> structuralFoundationsCollector =
                new FilteredElementCollector(doc, doc.ActiveView.Id)
                    .OfCategory(BuiltInCategory.OST_StructuralFoundation)
                    .Where(ConcreteSingleElement)
                    .OrderBy(PublicParameter.MountingLevel)
                    .ThenBy(el => el.get_Parameter(BuiltInParameter.ELEM_FAMILY_AND_TYPE_PARAM).AsValueString())
                    .GroupBy(el => el.get_Parameter(BuiltInParameter.ELEM_FAMILY_AND_TYPE_PARAM).AsValueString());

            IEnumerable<IGrouping<string, Autodesk.Revit.DB.Element>> genericModelsCollector =
                new FilteredElementCollector(doc, doc.ActiveView.Id)
                    .OfCategory(BuiltInCategory.OST_GenericModel)
                    .Where(ConcreteSingleElement)
                    .OrderBy(el => ((LocationPoint)el.Location).Point.Z)
                    .ThenBy(el => el.get_Parameter(BuiltInParameter.ELEM_FAMILY_AND_TYPE_PARAM).AsValueString())
                    .GroupBy(el => el.get_Parameter(BuiltInParameter.ELEM_FAMILY_AND_TYPE_PARAM).AsValueString());

            IEnumerable<IGrouping<string, Autodesk.Revit.DB.Element>> structuralColumnsCollector =
                new FilteredElementCollector(doc, doc.ActiveView.Id)
                    .OfCategory(BuiltInCategory.OST_StructuralColumns)
                    .Where(ConcreteSingleElement)
                    .OrderBy(el => ((LocationPoint)el.Location).Point.Z)
                    .ThenBy(el => el.get_Parameter(BuiltInParameter.ELEM_FAMILY_AND_TYPE_PARAM).AsValueString())
                    .GroupBy(el => el.get_Parameter(BuiltInParameter.ELEM_FAMILY_AND_TYPE_PARAM).AsValueString());

            IEnumerable<IGrouping<string, Autodesk.Revit.DB.Element>> structuralFramingsCollector =
                new FilteredElementCollector(doc, doc.ActiveView.Id)
                    .OfCategory(BuiltInCategory.OST_StructuralFraming)
                    .Where(ConcreteSingleElement)
                    .OrderBy(el => ((Line)((LocationCurve)el.Location).Curve).Origin.Z)
                    .ThenBy(el => el.get_Parameter(BuiltInParameter.ELEM_FAMILY_AND_TYPE_PARAM).AsValueString())
                    .GroupBy(el => el.get_Parameter(BuiltInParameter.ELEM_FAMILY_AND_TYPE_PARAM).AsValueString());

            IEnumerable<IGrouping<string, Autodesk.Revit.DB.Element>> wallsCollector =
                new FilteredElementCollector(doc, doc.ActiveView.Id)
                    .OfCategory(BuiltInCategory.OST_Walls)
                    .Where(ConcreteSingleElement)
                    .OrderBy(el => ((Line)((LocationCurve)el.Location).Curve).Origin.Z)
                    .ThenBy(el => el.get_Parameter(BuiltInParameter.ELEM_FAMILY_AND_TYPE_PARAM).AsValueString())
                    .GroupBy(el => el.get_Parameter(BuiltInParameter.ELEM_FAMILY_AND_TYPE_PARAM).AsValueString());

            IEnumerable<IGrouping<string, Autodesk.Revit.DB.Element>> floorsCollectors =
                new FilteredElementCollector(doc, doc.ActiveView.Id)
                    .OfCategory(BuiltInCategory.OST_Floors)
                    .Where(ConcreteSingleElement)
                    .OrderBy(PublicParameter.MountingLevel)
                    .ThenBy(el => el.get_Parameter(BuiltInParameter.ELEM_FAMILY_AND_TYPE_PARAM).AsValueString())
                    .GroupBy(el => el.get_Parameter(BuiltInParameter.ELEM_FAMILY_AND_TYPE_PARAM).AsValueString());

            #region structuralFundationsCollector

            foreach (IGrouping<string, Autodesk.Revit.DB.Element> elements in structuralFoundationsCollector)
            {
                string elementName = elements.First().get_Parameter(BuiltInParameter.ELEM_FAMILY_AND_TYPE_PARAM)
                    .AsValueString();
                if (_floorFoundation.IsMatch(elementName))
                {
                    string uni = (elements.First() as Floor).FloorType.get_Parameter(BuiltInParameter.UNIFORMAT_CODE)
                        .AsString();

                    if (prefixMarkFromAssemblyCode.ContainsKey(uni))
                    {
                        string markPrefix = prefixMarkFromAssemblyCode[uni];

                        MarkGroupElement(elements, markPrefix);
                    }
                    else
                    {
                        foreach (Autodesk.Revit.DB.Element el in elements)
                        {
                            _errorChangedElementsList.Add(el.Id.ToString());
                            _countElement++;
                        }
                    }
                }
                else if (_isSrcElement.IsMatch(elementName))
                {
                    string uni = PublicParameter.GetUniformatKode((elements.First() as FamilyInstance).Symbol);

                    if (prefixMarkFromAssemblyCode.ContainsKey(uni))
                    {
                        string markPrefix = prefixMarkFromAssemblyCode[uni];
                        string markFirst = GetUniqueMarkNumber(markPrefix);
                        int markThird = 1;
                        foreach (Autodesk.Revit.DB.Element el in elements)
                        {
                            MarkElement(el, markPrefix, markFirst, markThird.ToString());
                            markThird++;
                        }
                    }
                    else
                    {
                        foreach (Autodesk.Revit.DB.Element el in elements)
                        {
                            _errorChangedElementsList.Add(el.Id.ToString());
                            _countElement++;
                        }
                    }
                }
                else
                {
                    string uni = PublicParameter.GetUniformatKode((elements.First() as FamilyInstance).Symbol);
                    if (prefixMarkFromAssemblyCode.ContainsKey(uni))
                    {
                        string markPrefix = prefixMarkFromAssemblyCode[uni];
                        string markFirst = GetUniqueMarkNumber(markPrefix);

                        using (Transaction tr = new Transaction(doc, "setMark"))
                        {
                            tr.Start();
                            PublicParameter.MarkText((elements.First() as FamilyInstance).Symbol,
                                $"{markPrefix}{markFirst}");
                            tr.Commit();
                        }

                        foreach (Autodesk.Revit.DB.Element el in elements)
                        {
                            _changedElementList.Add(el.Id.ToString());
                            _countElement++;
                        }
                    }
                    else
                    {
                        foreach (Autodesk.Revit.DB.Element el in elements)
                        {
                            _errorChangedElementsList.Add(el.Id.ToString());
                            _countElement++;
                        }
                    }
                }
            }

            #endregion

            #region genericModelsCollector

            foreach (IGrouping<string, Autodesk.Revit.DB.Element> elements in genericModelsCollector)
            {
                string uni = PublicParameter.GetUniformatKode((elements.First() as FamilyInstance).Symbol);
                if (prefixMarkFromAssemblyCode.ContainsKey(uni))
                {
                    string markPrefix = prefixMarkFromAssemblyCode[uni];
                    string markFirst = GetUniqueMarkNumber(markPrefix);

                    if (_isSrcElement.IsMatch(elements.First()
                        .get_Parameter(BuiltInParameter.ELEM_FAMILY_AND_TYPE_PARAM).AsValueString()))
                    {
                        int markThird = 1;
                        foreach (Autodesk.Revit.DB.Element el in elements)
                        {
                            MarkElement(el, markPrefix, markFirst, markThird.ToString());
                            markThird++;
                        }
                    }
                    else
                    {
                        using (Transaction tr = new Transaction(doc, "setMark"))
                        {
                            tr.Start();
                            PublicParameter.MarkText((elements.First() as FamilyInstance).Symbol,
                                String.Format("{0}{1}", markPrefix, markFirst));
                            tr.Commit();
                        }

                        foreach (Autodesk.Revit.DB.Element el in elements)
                        {
                            _changedElementList.Add(el.Id.ToString());
                            _countElement++;
                        }
                    }
                }
                else
                {
                    foreach (Autodesk.Revit.DB.Element el in elements)
                    {
                        _errorChangedElementsList.Add(el.Id.ToString());
                        _countElement++;
                    }
                }
            }

            #endregion

            #region structuralColumnsCollector

            foreach (IGrouping<string, Autodesk.Revit.DB.Element> elements in structuralColumnsCollector)
            {
                string uni = PublicParameter.GetUniformatKode((elements.First() as FamilyInstance).Symbol);
                string markPrefix = prefixMarkFromAssemblyCode[uni];
                string markFirst = GetUniqueMarkNumber(markPrefix);

                if (prefixMarkFromAssemblyCode.ContainsKey(uni))
                {
                    int markFirstSubNumber = 1;

                    if (_isSrcElement.IsMatch(elements.First()
                        .get_Parameter(BuiltInParameter.ELEM_FAMILY_AND_TYPE_PARAM).AsValueString()))
                    {
                        IEnumerable<IGrouping<int, Autodesk.Revit.DB.Element>> elementGroup =
                            elements.GroupBy(PublicParameter.DimensionLength);
                        foreach (IGrouping<int, Autodesk.Revit.DB.Element> element in elementGroup)
                        {
                            string markFirstFull = $"{markFirst}.{markFirstSubNumber++}";
                            int markThird = 1;
                            foreach (Autodesk.Revit.DB.Element el in element)
                            {
                                MarkElement(el, markPrefix, markFirstFull, markThird.ToString());
                                markThird++;
                            }
                        }
                    }
                    else
                    {
                        using (Transaction tr = new Transaction(doc, "setColumnMark"))
                        {
                            tr.Start();
                            PublicParameter.MarkText((elements.First() as FamilyInstance).Symbol,
                                $"{markPrefix}{markFirst}");
                            tr.Commit();
                        }

                        foreach (Autodesk.Revit.DB.Element el in elements)
                        {
                            _changedElementList.Add(el.Id.ToString());
                            _countElement++;
                        }
                    }
                }
                else
                {
                    foreach (Autodesk.Revit.DB.Element el in elements)
                    {
                        _errorChangedElementsList.Add(el.Id.ToString());
                        _countElement++;
                    }
                }
            }

            #endregion

            #region StructuralFramingsCollector

            foreach (IGrouping<string, Autodesk.Revit.DB.Element> elements in structuralFramingsCollector)
            {
                if (_isSrcElement.IsMatch(elements.First().get_Parameter(BuiltInParameter.ELEM_FAMILY_AND_TYPE_PARAM)
                    .AsValueString()))
                {
                    string uni = PublicParameter.GetUniformatKode((elements.First() as FamilyInstance).Symbol);
                    string markPrefix = prefixMarkFromAssemblyCode[uni];
                    string markFirst = GetUniqueMarkNumber(markPrefix);

                    if (prefixMarkFromAssemblyCode.ContainsKey(uni))
                    {
                        int markFirstSubNumber = 1;

                        if (_isSrcElement.IsMatch(elements.First()
                            .get_Parameter(BuiltInParameter.ELEM_FAMILY_AND_TYPE_PARAM).AsValueString()))
                        {
                            IEnumerable<IGrouping<int, Autodesk.Revit.DB.Element>> elementGroup =
                                elements.GroupBy(PublicParameter.DimensionLength);
                            foreach (IGrouping<int, Autodesk.Revit.DB.Element> element in elementGroup)
                            {
                                string markFirstFull = $"{markFirst}.{markFirstSubNumber++}";
                                int markThird = 1;
                                foreach (Autodesk.Revit.DB.Element el in element)
                                {
                                    MarkElement(el, markPrefix, markFirstFull, markThird.ToString());
                                    markThird++;
                                }
                            }
                        }
                        else
                        {
                            using (Transaction tr = new Transaction(doc, "setColumnMark"))
                            {
                                tr.Start();
                                PublicParameter.MarkText((elements.First() as FamilyInstance).Symbol,
                                    $"{markPrefix}{markFirst}");
                                tr.Commit();
                            }

                            foreach (Autodesk.Revit.DB.Element el in elements)
                            {
                                _changedElementList.Add(el.Id.ToString());
                                _countElement++;
                            }
                        }
                    }
                }
                else
                {
                    foreach (Autodesk.Revit.DB.Element el in elements)
                    {
                        _errorChangedElementsList.Add(el.Id.ToString());
                        _countElement++;
                    }
                }
            }

            #endregion

            #region wallsCollector

            foreach (IGrouping<string, Autodesk.Revit.DB.Element> elements in wallsCollector)
            {
                string uni = (elements.First() as Wall).WallType.get_Parameter(BuiltInParameter.UNIFORMAT_CODE)
                    .AsString();
                if (prefixMarkFromAssemblyCode.ContainsKey(uni))
                {
                    string markPrefix = prefixMarkFromAssemblyCode[uni];

                    MarkGroupElement(elements, markPrefix);
                }
                else
                {
                    foreach (Autodesk.Revit.DB.Element el in elements)
                    {
                        _errorChangedElementsList.Add(el.Id.ToString());
                        _countElement++;
                    }
                }
            }

            #endregion

            #region floorsCollectors

            foreach (IGrouping<string, Autodesk.Revit.DB.Element> elements in floorsCollectors)
            {
                string uni = (elements.First() as Floor).FloorType.get_Parameter(BuiltInParameter.UNIFORMAT_CODE)
                    .AsString();
                if (prefixMarkFromAssemblyCode.ContainsKey(uni))
                {
                    string markPrefix = prefixMarkFromAssemblyCode[uni];

                    MarkGroupElement(elements, markPrefix);
                }
                else
                {
                    foreach (Autodesk.Revit.DB.Element el in elements)
                    {
                        _errorChangedElementsList.Add(el.Id.ToString());
                        _countElement++;
                    }
                }
            }

            #endregion

            ReportView reportView = new ReportView(_countElement, _changedElementList, _dontChangedElementList,
                _errorChangedElementsList);
            reportView.Show();
        }

        #endregion

        #region MarkMethodAxillary

        //MarkElement
        private void MarkElement(Autodesk.Revit.DB.Element el, string markPrefix, string markFirst, string markThird)
        {
            using (Transaction tr = new Transaction(_document, "SetMarkToSimpleElement"))
            {
                var failureOptions = tr.GetFailureHandlingOptions();
                failureOptions.SetFailuresPreprocessor(new DuplicateMarkSwallower());
                tr.SetFailureHandlingOptions(failureOptions);

                tr.Start();
                PublicParameter.MarkText(el, $"{markPrefix}{markFirst}");
                el.get_Parameter(BuiltInParameter.ALL_MODEL_MARK)
                    .Set($"{markPrefix}{markFirst}-0-{markThird}-0");

                _changedElementList.Add(el.Id.ToString());
                _countElement++;
                tr.Commit();
            }
        }

        private void MarkGroupElement(IGrouping<string, Autodesk.Revit.DB.Element> elements, string markPrefix)
        {
            if (elements == null) throw new ArgumentNullException(nameof(elements));
            Dictionary<string, List<AssemblyInstance>> assemblyDict = new Dictionary<string, List<AssemblyInstance>>();


            using (Transaction tr = new Transaction(_document, "CreateAssemblyFromSingleElement"))
            {
                foreach (Autodesk.Revit.DB.Element el in elements)
                {
                    tr.Start();
                    AssemblyInstance assembly =
                        AssemblyInstance.Create(_document, new List<ElementId> { el.Id }, el.Category.Id);
                    tr.Commit();
                    if (assemblyDict.ContainsKey(markPrefix)) assemblyDict[markPrefix].Add(assembly);
                    else assemblyDict.Add(markPrefix, new List<AssemblyInstance> { assembly });

                    _changedElementList.Add(el.Id.ToString());
                    _countElement++;
                }
            }

            foreach (var keyValuePair in assemblyDict)
            {
                IEnumerable<IGrouping<ElementId, AssemblyInstance>> assemblyGroup =
                    keyValuePair.Value.GroupBy(instance => instance.GetTypeId());
                foreach (IGrouping<ElementId, AssemblyInstance> assemblies in assemblyGroup)
                {
                    string markFirst = GetUniqueMarkNumber(markPrefix);
                    using (Transaction tr = new Transaction(_document, "MarkElement"))
                    {
                        var failureOptions = tr.GetFailureHandlingOptions();
                        failureOptions.SetFailuresPreprocessor(new Disassembling());
                        tr.SetFailureHandlingOptions(failureOptions);

                        tr.Start();
                        foreach (AssemblyInstance instance in assemblies)
                        {
                            SetMarkToGroupElement(instance.Disassemble().ToList(), markPrefix, markFirst, "0",
                                GetUniqueMarkNumber(markPrefix, markFirst, "0"));
                        }

                        tr.Commit();
                    }
                }
            }
        }

        //AddMarkToDictionary
        private void AddMarkToDictionary(string markPrefix, string markType)
        {
            if (_markDict.ContainsKey(markPrefix))
                _markDict[markPrefix].Add(markType,
                    new Dictionary<string, Dictionary<string, int>> { { "0", new Dictionary<string, int> { { "0", 0 } } } });
            else
                _markDict.Add(markPrefix,
                    new Dictionary<string, Dictionary<string, Dictionary<string, int>>>
                    {
                        {
                            markType,
                            new Dictionary<string, Dictionary<string, int>>
                                {{"0", new Dictionary<string, int> {{"0", 0}}}}
                        }
                    });
        }

        //SetMarkToGroupElement
        private void SetMarkToGroupElement(IEnumerable<ElementId> elementIdList, string markPrefix,
            string markFirst = "0",
            string markSecond = "0", string markThird = "0")
        {
            int markFours = 0;
            foreach (ElementId elementId in elementIdList)
            {
                Autodesk.Revit.DB.Element element = _document.GetElement(elementId);
                if (!_isSrcElement.IsMatch(element.get_Parameter(BuiltInParameter.ELEM_FAMILY_AND_TYPE_PARAM)
                    .AsValueString())) continue;

                using (Transaction tr = new Transaction(_document, "SetParameterToElement"))
                {
                    var failureOptions = tr.GetFailureHandlingOptions();
                    failureOptions.SetFailuresPreprocessor(new DuplicateMarkSwallower());
                    tr.SetFailureHandlingOptions(failureOptions);

                    tr.Start();
                    PublicParameter.MarkText(element, $"{markPrefix}{markFirst}");
                    element.get_Parameter(BuiltInParameter.ALL_MODEL_MARK).Set(
                        $"{markPrefix}{markFirst}-{markSecond}-{markThird}-{markFours++}");
                    tr.Commit();
                }
            }
        }

        //GetUniqueMarkNumber
        private string GetUniqueMarkNumber(string markPrefix)
        {
            int markType = 1;

            if (_markDict.ContainsKey(markPrefix))
            {
                do
                {
                    if (_markDict[markPrefix].ContainsKey(markType.ToString())) markType++;
                    else
                    {
                        _markDict[markPrefix].Add(markType.ToString(),
                            new Dictionary<string, Dictionary<string, int>>
                                {{"0", new Dictionary<string, int> {{"0", 0}}}});
                        return markType.ToString();
                    }
                } while (true);
            }

            _markDict.Add(markPrefix,
                new Dictionary<string, Dictionary<string, Dictionary<string, int>>>
                {
                    {
                        "1",
                        new Dictionary<string, Dictionary<string, int>>
                            {{"0", new Dictionary<string, int> {{"0", 0}}}}
                    }
                });

            return "1";
        }

        private string GetUniqueMarkNumber(string markPrefix, string markType)
        {
            int markElement = 1;

            if (_markDict.ContainsKey(markPrefix))
            {
                if (!_markDict[markPrefix].ContainsKey(markType)) return "1";

                do
                {
                    if (_markDict[markPrefix][markType].ContainsKey(markElement.ToString())) markElement++;
                    else
                    {
                        _markDict[markPrefix][markType].Add(markElement.ToString(),
                            new Dictionary<string, int> { { "0", 1 } });
                        return markElement.ToString();
                    }
                } while (true);
            }
            else
                _markDict.Add(markPrefix,
                    new Dictionary<string, Dictionary<string, Dictionary<string, int>>>
                    {
                        {
                            markType,
                            new Dictionary<string, Dictionary<string, int>>
                                {{"1", new Dictionary<string, int> {{"0", 0}}}}
                        }
                    });

            return "1";
        }

        private string GetUniqueMarkNumber(string markPrefix, string markType, string markElement)
        {
            int markStage = 1;

            if (_markDict.ContainsKey(markPrefix))
            {
                if (_markDict[markPrefix].ContainsKey(markType))
                {
                    if (_markDict[markPrefix][markType].ContainsKey(markElement))
                    {
                        do
                        {
                            if (_markDict[markPrefix][markType][markElement].ContainsKey(markStage.ToString()))
                                markStage++;
                            else
                            {
                                _markDict[markPrefix][markType][markElement].Add(markStage.ToString(), 0);
                                return markStage.ToString();
                            }
                        } while (true);
                    }

                    _markDict[markPrefix][markType].Add(markElement, new Dictionary<string, int> { { "1", 0 } });
                }
                else
                    _markDict[markPrefix].Add(markType,
                        new Dictionary<string, Dictionary<string, int>>
                            {{markElement, new Dictionary<string, int> {{"1", 0}}}});
            }
            else
                _markDict.Add(markPrefix,
                    new Dictionary<string, Dictionary<string, Dictionary<string, int>>>
                    {
                        {
                            markType,
                            new Dictionary<string, Dictionary<string, int>>
                                {{markElement, new Dictionary<string, int> {{"1", 0}}}}
                        }
                    });

            return "1";
        }

        private bool ConcreteSingleElement(Autodesk.Revit.DB.Element el)
        {
            if (_concreteElement.IsMatch(el.get_Parameter(BuiltInParameter.ELEM_FAMILY_AND_TYPE_PARAM).AsValueString()))
            {
                return el.GroupId == new ElementId(-1) || _dontMarkGroup.IsMatch(_document.GetElement(el.GroupId).Name);
            }

            return false;
        }

        #endregion
    }
}