﻿using Autodesk.Revit.UI;

namespace OrslavTeam.Revit.Concrete.Element.Mark
{
    public class MarkEventHandler : IExternalEventHandler
    {
        #region Attribute
        
        public static MarkEventHandler Hadler = null;
        public static ExternalEvent ExEvent = null;

        public delegate void _revitCommand(UIApplication app);
        public static _revitCommand RevitCommand { get; set; }
        
        #endregion
        
        public void Execute(UIApplication app)
        {
            RevitCommand(app);
        }

        public string GetName()
        {
            return nameof(RevitCommand);
        }
    }
}