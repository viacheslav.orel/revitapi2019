﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using Autodesk.Revit.UI.Selection;
using OrslavTeam.Revit.Axillary.PublicInformation;
using OrslavTeam.Revit.Axillary.ViewTemplate.Infrastructure;
using OrslavTeam.Revit.Axillary.ViewTemplate.ReportWindow;

namespace OrslavTeam.Revit.Concrete.Element.Pile
{
    public class PileMarkViewModel : ObservableObject
    {
        #region Fields

        //Numeration

        #region PileNumber

        private static int _pileNumber = 1;

        public int PileNumber
        {
            get => _pileNumber;
            set
            {
                _pileNumber = value;
                RaisePropertyChangedEvent("PileNumber");
            }
        }

        #endregion

        public static bool LeftToRightAndUpToDown { get; set; } = true;

        public static bool RightToLeftAndUpToDown { get; set; } = false;

        public static bool LeftToRightAndDownToUp { get; set; } = false;

        public static bool RightToLeftAndDownToUp { get; set; } = false;

        public static bool UpToDownAndLeftToRight { get; set; } = false;

        public static bool UpToDownAndRightToLeft { get; set; } = false;

        public static bool DownToUpAndLeftToRight { get; set; } = false;

        public static bool DownToUpAndRightToLeft { get; set; } = false;

        //Mark

        #region SrcPrefixMark

        private static string _srcPrefixMark = "Пм";

        public string SrcPrefixMark
        {
            get => _srcPrefixMark;
            set
            {
                _srcPrefixMark = value;
                RaisePropertyChangedEvent("SrcPrefixMark");
            }
        }

        #endregion

        #region PrcPrefixMark

        private static string _prcPrefixMark = "П";

        public string PrcPrefixMark
        {
            get => _prcPrefixMark;
            set
            {
                _prcPrefixMark = value;
                RaisePropertyChangedEvent("PrcPrefixMark");
            }
        }

        #endregion

        #region SteelPrefixMark

        private static string _steelPrefixMark = "Пст";

        public string SteelPrefixMark
        {
            get => _steelPrefixMark;
            set
            {
                _steelPrefixMark = value;
                RaisePropertyChangedEvent("SteelPrefixMark");
            }
        }

        #endregion

        #endregion

        #region MarkMethod

        private void GetPileActualNumber(UIApplication uiApp)
        {
            Document doc = uiApp.ActiveUIDocument.Document;
            IEnumerable<Autodesk.Revit.DB.Element> pileInView = GetPileInView(doc);
            IEnumerable<Autodesk.Revit.DB.Element> inView = pileInView as Autodesk.Revit.DB.Element[] ?? pileInView.ToArray();
            if (inView.Any())
            {
                int maxNumberOfPile = (from pile in inView select PublicParameter.MarkNumber(pile)).Max();
                PileNumber = maxNumberOfPile + 1;
            }
            else
            {
                MessageView messageView =
                    new MessageView(
                        "На поточному виді паль не виявлено.\nМожливо необхідно змінити налаштування виду або обрати інший вид");
                messageView.ShowDialog();
            }
        }

        private void NumberOneByOne(UIApplication uiApp)
        {
            Document doc = uiApp.ActiveUIDocument.Document;
            Selection sel = uiApp.ActiveUIDocument.Selection;
            SelectPile getPile = new SelectPile();

            do
            {
                Reference pickedPile = null;
                try
                {
                    pickedPile = sel.PickObject(ObjectType.Element, getPile, "Оберіть палю");
                }
                catch (Exception)
                {
                    return;
                }

                Autodesk.Revit.DB.Element el = doc.GetElement(pickedPile);
                using (Transaction tr = new Transaction(doc, "numPile"))
                {
                    tr.Start();
                    try
                    {
                        PublicParameter.MarkNumber(el, _pileNumber);
                        PileNumber++;
                    }
                    catch (Exception)
                    {
                        MessageView messageView =
                            new MessageView(
                                "Неможливо призначити номер палі.\nМожливо паля розташована у групі або параметер заблоковано");
                        messageView.ShowDialog();
                    }

                    tr.Commit();
                }
            } while (true);
        }

        private void NumberSelection(UIApplication uiApp)
        {
            Selection sel = uiApp.ActiveUIDocument.Selection;
            Document doc = uiApp.ActiveUIDocument.Document;
            SelectPile isPile = new SelectPile();

            IEnumerable<Autodesk.Revit.DB.Element> pileInSelection;
            ICollection<ElementId> selPile = sel.GetElementIds();
            if (selPile.Any())
            {
                pileInSelection = from sp in selPile
                    where SelectPile.IsPile(doc.GetElement(sp))
                    select doc.GetElement(sp);
                SetPileNumber(pileInSelection, doc);
            }
            else
            {
                do
                {
                    try
                    {
                        pileInSelection = sel.PickElementsByRectangle(isPile, "Оберіть групу паль");
                        SetPileNumber(pileInSelection, doc);
                    }
                    catch (Exception)
                    {
                        return;
                    }
                } while (true);
            }
        }

        private void NumberInView(UIApplication uiApp)
        {
            Document doc = uiApp.ActiveUIDocument.Document;
            IEnumerable<Autodesk.Revit.DB.Element> pileInView = GetPileInView(doc);
            SetPileNumber(pileInView, doc);
        }

        private void CheckPileNumber(UIApplication uiApp)
        {
            Document doc = uiApp.ActiveUIDocument.Document;
            IEnumerable<Autodesk.Revit.DB.Element> pileInView = GetPileInView(doc);
            Dictionary<ElementId, int> pileIdAndNumber =
                (from el in pileInView orderby PublicParameter.MarkNumber(el) select el).ToDictionary(el => el.Id,
                    PublicParameter.MarkNumber);
            List<ElementId> pileKey = pileIdAndNumber.Keys.ToList();
            List<int> pileValue = pileIdAndNumber.Values.ToList();

            #region OutAttribute

            List<string> firstColumn = new List<string> {"Палі мають однаковий номер"};
            List<string> secondColumn = new List<string> {"Пропущенні номера"};
            List<string> thirdColumn = new List<string> {"Палі не мають номеру"};
            int pileQuantity = pileValue.Count;

            #endregion

            int i = 0;
            //GetAllNullElement
            while (pileValue[i] == 0)
            {
                thirdColumn.Add(pileKey[i].ToString());
                i++;
            }

            int counter = pileValue[i];
            //GetDuplicatePile
            int maxListLength = pileQuantity - 1;
            while (i < maxListLength)
            {
                if (pileValue[i] > counter)
                {
                    do
                    {
                        secondColumn.Add(counter.ToString());
                        counter++;
                    } while (pileValue[i] > counter);
                }
                else if (pileValue[i] == pileValue[i + 1])
                {
                    do
                    {
                        firstColumn.Add($"Id = {pileKey[i]}: {pileValue[i]}");
                        i++;
                    } while (pileValue[i] == pileValue[i + 1]);
                }
                else
                {
                    counter++;
                    i++;
                }
            }

            if (pileValue[i] > counter)
            {
                do
                {
                    secondColumn.Add(counter.ToString());
                    counter++;
                } while (pileValue[i] > counter);
            }
            else
            {
                if (pileValue[i] == pileValue[i - 1])
                    firstColumn.Add($"Id = {pileKey[i]}: {pileValue[i]}");
            }

            if (firstColumn.Count == 0 && secondColumn.Count == 0 && thirdColumn.Count == 0)
            {
                MessageView messageView = new MessageView("Помилок при нумерації не виявлено");
                messageView.ShowDialog();
            }
            else
            {
                ReportView reportView = new ReportView(pileQuantity, firstColumn, secondColumn, thirdColumn);
                reportView.Show();
            }
        }

        private void MarkPile(UIApplication uiApp)
        {
            Document doc = uiApp.ActiveUIDocument.Document;

            IEnumerable<Autodesk.Revit.DB.Element> pileInView = GetPileInView(doc);
            List<Autodesk.Revit.DB.Element> srcPileList = new List<Autodesk.Revit.DB.Element>();
            List<Autodesk.Revit.DB.Element> prcPileList = new List<Autodesk.Revit.DB.Element>();
            List<Autodesk.Revit.DB.Element> steelPile = new List<Autodesk.Revit.DB.Element>();

            //SortPile
            foreach (Autodesk.Revit.DB.Element element in pileInView)
            {
                string uniformatValue = ((FamilyInstance) element).Symbol.get_Parameter(BuiltInParameter.UNIFORMAT_CODE)
                    .AsString();
                // ReSharper disable once SwitchStatementMissingSomeCases
                switch (uniformatValue)
                {
                    case "B.01.01.02.02":
                        srcPileList.Add(element);
                        break;
                    case "B.01.01.02.01":
                        prcPileList.Add(element);
                        break;
                    case "B.02.01.01":
                        steelPile.Add(element);
                        break;
                }
            }

            //group pile by mark
            List<Autodesk.Revit.DB.Element> allPileList = new List<Autodesk.Revit.DB.Element>();
            if (srcPileList.Count > 0)
            {
                IOrderedEnumerable<IGrouping<string, Autodesk.Revit.DB.Element>> reinforcedPileMark =
                    from el in srcPileList
                    group el by PublicParameter.ProfileName(el)
                    into elem
                    orderby elem.Key
                    select elem;
                using (Transaction tr = new Transaction(doc, "setMarkReinforced"))
                {
                    tr.Start();
                    int numMark = 1;
                    foreach (IGrouping<string, Autodesk.Revit.DB.Element> elKey in reinforcedPileMark)
                    {
                        foreach (var el in elKey)
                        {
                            el.get_Parameter(BuiltInParameter.ALL_MODEL_MARK)
                                .Set($"{SrcPrefixMark}-{numMark}");
                            allPileList.Add(el);
                        }

                        numMark++;
                    }

                    tr.Commit();
                }
            }

            if (prcPileList.Count > 0)
            {
                IOrderedEnumerable<IGrouping<string, Autodesk.Revit.DB.Element>> concretePileMark =
                    from el in prcPileList
                    group el by PublicParameter.ProfileName(el)
                    into elem
                    orderby elem.Key
                    select elem;
                using (Transaction tr = new Transaction(doc, "setMarcConcrete"))
                {
                    tr.Start();
                    int numMark = 1;
                    foreach (IGrouping<string, Autodesk.Revit.DB.Element> elKey in concretePileMark)
                    {
                        foreach (var el in elKey)
                        {
                            el.get_Parameter(BuiltInParameter.ALL_MODEL_MARK)
                                .Set($"{PrcPrefixMark}-{numMark}");
                            allPileList.Add(el);
                        }

                        numMark++;
                    }

                    tr.Commit();
                }
            }

            if (steelPile.Count > 0)
            {
                var steelPileMark =
                    from el in steelPile
                    group el by new
                        {name = PublicParameter.DocNumber(el) + PublicParameter.ProfileName(el)}
                    into elem
                    orderby elem.Key.name
                    select elem;
                using (Transaction tr = new Transaction(doc, "setMarkSteel"))
                {
                    tr.Start();
                    int numMark = 1;
                    foreach (var elKey in steelPileMark)
                    {
                        foreach (var el in elKey)
                        {
                            el.get_Parameter(BuiltInParameter.ALL_MODEL_MARK)
                                .Set($"{steelPileMark}-{numMark}");
                            allPileList.Add(el);
                        }

                        numMark++;
                    }

                    tr.Commit();
                }
            }

            //mark pile
            List<Autodesk.Revit.DB.Element> sortList = (from el in allPileList orderby PublicParameter.MarkNumber(el) select el).ToList();
            using (Transaction tr = new Transaction(doc, "setGroup"))
            {
                tr.Start();
                int numGroupPile = 1;
                int lenList = sortList.Count;
                PublicParameter.GroupParametersText(sortList[0], numGroupPile.ToString());
                for (int i = 1; i < lenList; i++)
                {
                    if (sortList[i].get_Parameter(BuiltInParameter.ALL_MODEL_MARK).AsString() !=
                        sortList[i - 1].get_Parameter(BuiltInParameter.ALL_MODEL_MARK).AsString() ||
                        // ReSharper disable once CompareOfFloatsByEqualityOperator
                        ((LocationPoint) sortList[i].Location).Point.Z !=
                        ((LocationPoint) sortList[i - 1].Location).Point.Z ||
                        PublicParameter.DimensionsLength1(sortList[i]) !=
                        PublicParameter.DimensionsLength1(sortList[i - 1]))
                    {
                        numGroupPile++;
                        PublicParameter.GroupParametersText(sortList[i], numGroupPile.ToString());
                    }
                    else
                    {
                        PublicParameter.GroupParametersText(sortList[i], numGroupPile.ToString());
                    }
                }

                tr.Commit();
            }
        }

        #endregion

        #region SupportMethod

        private IEnumerable<Autodesk.Revit.DB.Element> GetPileInView(Document doc)
        {
            try
            {
                return new FilteredElementCollector(doc, doc.ActiveView.Id)
                    .OfCategory(BuiltInCategory.OST_StructuralFoundation).Where(el => SelectPile.IsPile(el));
            }
            catch (Exception)
            {
                return Enumerable.Empty<Autodesk.Revit.DB.Element>();
            }
        }

        private static IEnumerable<Autodesk.Revit.DB.Element> SortPile(IEnumerable<Autodesk.Revit.DB.Element> elements)
        {
            if (LeftToRightAndUpToDown)
                return (from el in elements
                    orderby (int) ((LocationPoint) el.Location).Point.Y descending,
                        (int) ((LocationPoint) el.Location).Point.X
                    select el).ToList();
            
            if (RightToLeftAndUpToDown)
                return (from el in elements
                    orderby (int) ((LocationPoint) el.Location).Point.Y descending,
                        (int) ((LocationPoint) el.Location).Point.X descending
                    select el).ToList();
            
            if (LeftToRightAndDownToUp)
                return (from el in elements
                    orderby (int) ((LocationPoint) el.Location).Point.Y,
                        (int) ((LocationPoint) el.Location).Point.X
                    select el).ToList();
            
            if (RightToLeftAndDownToUp)
                return (from el in elements
                    orderby (int) ((LocationPoint) el.Location).Point.Y,
                        (int) ((LocationPoint) el.Location).Point.X descending
                    select el).ToList();
            
            if (UpToDownAndLeftToRight)
                return (from el in elements
                    orderby (int) ((LocationPoint) el.Location).Point.X,
                        (int) ((LocationPoint) el.Location).Point.Y descending
                    select el).ToList();
            
            if (UpToDownAndRightToLeft)
                return (from el in elements
                    orderby (int) ((LocationPoint) el.Location).Point.X descending,
                        (int) ((LocationPoint) el.Location).Point.Y descending
                    select el).ToList();
            
            if (DownToUpAndLeftToRight)
                return (from el in elements
                    orderby (int) ((LocationPoint) el.Location).Point.X,
                        (int) ((LocationPoint) el.Location).Point.Y
                    select el).ToList();
            
            if (DownToUpAndRightToLeft)
                return (from el in elements
                    orderby (int) ((LocationPoint) el.Location).Point.X descending,
                        (int) ((LocationPoint) el.Location).Point.Y
                    select el).ToList();
            return elements.ToList();
        }

        private void SetPileNumber(IEnumerable<Autodesk.Revit.DB.Element> elements, Document doc)
        {
            IEnumerable<Autodesk.Revit.DB.Element> sortedElement = SortPile(elements);
            using (Transaction tr = new Transaction(doc, "setPileNumber"))
            {
                tr.Start();
                foreach (Autodesk.Revit.DB.Element el in sortedElement)
                {
                    PublicParameter.MarkNumber(el, PileNumber++);
                }

                tr.Commit();
            }
        }

        #endregion

        #region ViewCommand

        //GetActualNumber
        DelegateCommand _getActualNumber;

        public ICommand GetActualNumber =>
            _getActualNumber ?? (_getActualNumber = new DelegateCommand(ExecuteGetActualNumber, null));

        private void ExecuteGetActualNumber(object obj)
        {
            PileMarkEventHandler.RevitCommand = GetPileActualNumber;
            PileMarkEventHandler.ExEvent.Raise();
        }

        //NumerationOneByOne
        private DelegateCommand _numerationOneByOne;

        public ICommand NumerationOneByOne =>
            _numerationOneByOne ??
            (_numerationOneByOne = new DelegateCommand(ExecuteNumerationOneByOne, null));

        private void ExecuteNumerationOneByOne(object obj)
        {
            PileMarkEventHandler.RevitCommand = NumberOneByOne;
            PileMarkEventHandler.ExEvent.Raise();
        }

        //NumerationSelection
        private DelegateCommand _numerationSelection;

        public ICommand NumerationSelection =>
            _numerationSelection ??
            (_numerationSelection = new DelegateCommand(ExecuteNumerationSelection, null));

        private void ExecuteNumerationSelection(object obj)
        {
            PileMarkEventHandler.RevitCommand = NumberSelection;
            PileMarkEventHandler.ExEvent.Raise();
        }

        //NumerationInView
        private DelegateCommand _numerationInView;

        public ICommand NumerationInView => _numerationInView ?? (_numerationInView = new DelegateCommand(ExecuteNumerationInView, null));

        private void ExecuteNumerationInView(object obj)
        {
            PileMarkEventHandler.RevitCommand = NumberInView;
            PileMarkEventHandler.ExEvent.Raise();
        }

        //CheckNumeration
        private DelegateCommand _checkNumeration;

        public ICommand CheckNumeration => _checkNumeration ?? (_checkNumeration = new DelegateCommand(ExecuteCheckNumeration, null));

        private void ExecuteCheckNumeration(object obj)
        {
            PileMarkEventHandler.RevitCommand = CheckPileNumber;
            PileMarkEventHandler.ExEvent.Raise();
        }

        //MarkingPile
        private DelegateCommand _markingPile;

        public ICommand MarkingPile => _markingPile ?? (_markingPile = new DelegateCommand(ExecuteMarkingPile, null));

        private void ExecuteMarkingPile(object obj)
        {
            PileMarkEventHandler.RevitCommand = MarkPile;
            PileMarkEventHandler.ExEvent.Raise();
        }

        #endregion

        #region ViewEvent

        //WindowsClosed
        public static void WindowsClosing()
        {
            PileMarkEventHandler.ExEvent.Dispose();
            PileMarkEventHandler.ExEvent = null;
            PileMarkEventHandler.Hadler = null;
        }

        #endregion
    }
}