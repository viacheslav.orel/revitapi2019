﻿using System.ComponentModel;

namespace OrslavTeam.Revit.Concrete.Element.Pile
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            PileMarkViewModel.WindowsClosing();
        }
    }
}