﻿using System;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI.Selection;
using OrslavTeam.Revit.Axillary.PublicInformation;

namespace OrslavTeam.Revit.Concrete.Element.Pile
{
    public class SelectPile : ISelectionFilter
    {
        public bool AllowElement(Autodesk.Revit.DB.Element elem)
        {
            return IsPile(elem);
        }

        public bool AllowReference(Reference reference, XYZ position)
        {
            return false;
        }

        public static bool IsPile(Autodesk.Revit.DB.Element el)
        {
            try
            {
                string uniKey = PublicParameter.GetUniformatKode(((FamilyInstance) el).Symbol);
                return (uniKey == "B.01.01.02.01" || uniKey == "B.01.01.02.02" || uniKey == "B.02.01.01");
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}