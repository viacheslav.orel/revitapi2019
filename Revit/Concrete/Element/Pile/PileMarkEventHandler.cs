﻿using Autodesk.Revit.UI;

namespace OrslavTeam.Revit.Concrete.Element.Pile
{
    public class PileMarkEventHandler : IExternalEventHandler
    {
        #region Attribute
        
        public static PileMarkEventHandler Hadler = null;
        public static ExternalEvent ExEvent = null;

        public delegate void _revitCommand(UIApplication uiApp);
        public static _revitCommand RevitCommand { get; set; }
        
        #endregion
        
        public void Execute(UIApplication app)
        {
            RevitCommand(app);
        }

        public string GetName()
        {
            return nameof(RevitCommand);
        }
    }
}