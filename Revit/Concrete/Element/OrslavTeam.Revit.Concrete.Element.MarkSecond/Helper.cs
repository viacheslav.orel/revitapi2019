﻿using Autodesk.Revit.UI;
using Autodesk.Revit.DB;

namespace OrslavTeam.Revit.Concrete.Element.MarkSecond
{
    public class Helper
    {
        #region fields technical ---------------------------------------------------------------------------------------

        public Document Doc { get; }
        
        public ElementId ActiveViewId { get; }

        #endregion


        #region fields collection --------------------------------------------------------------------------------------

        

        #endregion


        #region fields repport -----------------------------------------------------------------------------------------

        

        #endregion


        #region constructors -------------------------------------------------------------------------------------------

        public Helper(ExternalCommandData commandData)
        {
            Doc = commandData.Application.ActiveUIDocument.Document;
            ActiveViewId = Doc.ActiveView.Id;
        }

        #endregion


        #region methods get --------------------------------------------------------------------------------------------

        public void MarkGroup()
        {
            FilteredElementCollector groupsInView = new FilteredElementCollector(Doc, ActiveViewId)
                .OfCategory(BuiltInCategory.OST_IOSModelGroups);

            foreach (Autodesk.Revit.DB.Element group in groupsInView)
            {
                
            }
        }

        #endregion
    }
}