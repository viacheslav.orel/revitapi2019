﻿using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;

namespace OrslavTeam.Revit.Concrete.Element.MarkSecond
{
    [Transaction(TransactionMode.Manual)]
    public class MarkedElement : IExternalCommand
    {
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            var helper = new Helper(commandData);

            return Result.Succeeded;
        }
    }
}
