﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using Autodesk.Revit.UI.Selection;
using OrslavTeam.Revit.Axillary.PublicInformation;
using OrslavTeam.Revit.Axillary.PublicInformation.Failure;
using OrslavTeam.Revit.Axillary.PublicInformation.Helpers;
using OrslavTeam.Revit.Concrete.Component.Worker.Models;
using OrslavTeam.Revit.Concrete.Helper;

namespace OrslavTeam.Revit.Concrete.Component.Worker.AddIns
{
    [Transaction(TransactionMode.Manual)]
    public class SetConcreteMark : IExternalCommand
    {
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            UIDocument uiDoc = commandData.Application.ActiveUIDocument;
            Document doc = uiDoc.Document;
            ElementId currentPhaseId = doc.ActiveView.get_Parameter(BuiltInParameter.VIEW_PHASE)?.AsElementId()
                ?? throw new Exception("The current view hasn't phase parameter!");
            var phaseFilter = new ElementPhaseStatusFilter(currentPhaseId, ElementOnPhaseStatus.New);

            var tr = new Transaction(doc);
            

            FilteredElementCollector elementsInPhase = new FilteredElementCollector(doc)
                .WhereElementIsNotElementType()
                .WherePasses(phaseFilter);

            IEnumerable<IGrouping<string, StructuralComponent>> prefixMarkGroups = new FilteredElementCollector(doc)
                    .WhereElementIsNotElementType()
                    .WherePasses(phaseFilter)
                    .Where(el =>
                    {
                        Element familyType = GetRvtData.GetFamily(el, doc);
                        string assemblyKod = PublicParameter.GetUniformatKode(familyType);

                        if (IsElementType.IsPileKod(assemblyKod)) return false;
                        if (IsElementType.IsConcreteKod(assemblyKod)) return true;
                        if (IsElementType.IsRebarKod(assemblyKod)
                            && PublicParameter.RebarScheduleFilter(familyType) != 3) return true;

                        return false;
                    })
                    .Aggregate(new Dictionary<int, List<Element>>(),
                        (acc, component) => AggregateElement.Agg(GetRvtData.GetHostId(component), component, acc))
                    .Select(pair => new StructuralComponent(pair.Value, pair.Key, doc, tr))
                    .GroupBy(el => el.MarkPrefix);

            foreach (IGrouping<string, StructuralComponent> prefixMarkGroup in prefixMarkGroups)
            {
                IEnumerable<IGrouping<string, StructuralComponent>> nameGroups =
                    prefixMarkGroup.GroupBy(el => $"{el.NameType}:{el.NameElement}");
                var markNumber = 1;

                foreach (IGrouping<string, StructuralComponent> structuralComponents in nameGroups)
                {
                    foreach (StructuralComponent structuralComponent in structuralComponents)
                    {
                        structuralComponent.SetMark(markNumber, tr);
                    }

                    markNumber++;
                }
            }

            return Result.Succeeded;
        }
    }
}