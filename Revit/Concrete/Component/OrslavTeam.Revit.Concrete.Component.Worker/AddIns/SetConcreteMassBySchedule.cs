﻿using System.Collections.Generic;
using System.Linq;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using OrslavTeam.Revit.Axillary.PublicInformation;
using OrslavTeam.Revit.Concrete.Helper;

namespace OrslavTeam.Revit.Concrete.Component.Worker.AddIns
{
    [Transaction(TransactionMode.Manual)]
    public class SetConcreteMassBySchedule : IExternalCommand
    {
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            Document doc = commandData.Application.ActiveUIDocument.Document;
            Element view = doc.ActiveView;
            if (!IsCurrentComponent.IsScheduleView(view)) return Result.Failed;

            IEnumerable<Element> concreteElements = new FilteredElementCollector(doc, view.Id)
                .WhereElementIsNotElementType()
                .Where(el => IsCurrentComponent.IsCastInPlaceElement(el, doc));

            using (var tr = new Transaction(doc, "Set mass to single component"))
            {
                tr.Start();

                var groups = new Dictionary<int, int>();

                foreach (Element concreteElement in concreteElements)
                {
                    if (concreteElement.GroupId != ElementId.InvalidElementId)
                    {
                        Element group = doc.GetElement(concreteElement.GroupId);

                        if (!group.Name.StartsWith("&"))
                        {
                            if (!groups.ContainsKey(group.Id.IntegerValue))
                            {
                                Element[] groupMembers = ((Group) group)
                                    .GetMemberIds()
                                    .Select(doc.GetElement)
                                    .Where(el => IsCurrentComponent.IsConcreteComponent(el, doc))
                                    .ToArray();
                                double totalGroupMass = groupMembers
                                    .Sum(Helper.GetVolume);

                                foreach (Element groupMember in groupMembers)
                                {
                                    string groupVolume = totalGroupMass.ToString(PublicParameter.TwoDecimal) + " м³";
                                    PublicParameter.TechnicalComments(groupMember, groupVolume);
                                }

                                groups.Add(group.Id.IntegerValue, 0);

                                continue;
                            }
                        }
                    }

                    if (concreteElement is Material) continue;

                    string volume = Helper.GetVolume(concreteElement).ToString(PublicParameter.TwoDecimal) + " м³";
                    PublicParameter.TechnicalComments(concreteElement, volume);
                }

                tr.Commit();
            }

            return Result.Succeeded;
        }
    }
}