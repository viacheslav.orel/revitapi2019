﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Architecture;
using OrslavTeam.Revit.Axillary.PublicInformation;
using OrslavTeam.Revit.Axillary.PublicInformation.Failure;
using OrslavTeam.Revit.Axillary.PublicInformation.Helpers;
using OrslavTeam.Revit.Concrete.Helper;

namespace OrslavTeam.Revit.Concrete.Component.Worker.Models
{
    public class StructuralComponent
    {
        #region fields -------------------------------------------------------------------------------------------------

        public string NameType { get; }

        public string NameElement { get; } = "";

        public string MarkPrefix { get; }

        public List<Element> ConcreteComponents { get; }

        public AssemblyInstance AutoAssembly { get; }

        public AssemblyInstance UserAssembly { get; }

        #endregion


        public StructuralComponent(IReadOnlyCollection<Element> components, int hostNumber, Document doc, Transaction tr)
        {
            if (tr.GetStatus() == TransactionStatus.Started)
                throw new Exception("You try to start a new transaction but started another transaction!");

            Element familyType = GetRvtData.GetFamily(components.First(), doc);

            if (components.Count == 1 && PublicParameter.StructuralType(familyType) == "Збірні конструкції")
            {
                NameType = familyType.Name;
                NameElement = components.First().Name;
                ConcreteComponents = components.ToList();

                return;
            }

            ElementId userAssemblyId = components.FirstOrDefault()?.AssemblyInstanceId ?? ElementId.InvalidElementId;

            if (userAssemblyId != ElementId.InvalidElementId)
                UserAssembly = doc.GetElement(userAssemblyId) as AssemblyInstance;
            else if (PublicParameter.GetUniformatKode(familyType).StartsWith("B.01.03"))
            {
                ElementId groupId = components.First().GroupId;
                if (groupId == ElementId.InvalidElementId)
                    throw new Exception("You must create a group with a stair component!");

                NameType = ((Group) doc.GetElement(groupId)).GroupType.Name;
                MarkPrefix = NameType.Split('_')[0];
            }
            else
            {
                ElementId[] assemblyMembers = components.Select(el => el.Id).ToArray();
                ElementId categoryId = null;

                foreach (Element component in components)
                {
                    categoryId = component?.Category?.Id ?? null;
                    if (categoryId != null) break;
                }

                tr.Start("Create assembly");
                AutoAssembly = AssemblyInstance.Create(doc, assemblyMembers, categoryId);
                tr.Commit();

                NameType = AutoAssembly.AssemblyTypeName;
            }

            ConcreteComponents = new List<Element>();

            foreach (Element component in components)
            {
                if (component.Id.IntegerValue == hostNumber) MarkPrefix = PublicParameter.MarkTextPrefix(familyType);

                if (IsCurrentComponent.IsConcreteComponent(component, doc))
                    ConcreteComponents.Add(component);
            }
        }

        public void SetMark(int markNumber, Transaction tr)
        {
            if (tr.GetStatus() != TransactionStatus.Started)
                throw new Exception("You try to start a new transaction but started another transaction!");

            string mark = $"{MarkPrefix}{markNumber}";
            var positionNumber = 0;

            tr.Start("Set mark");
            FailureHandlingOptions failureOptions = tr.GetFailureHandlingOptions();
            failureOptions.SetFailuresPreprocessor(new ClearAllWarning());
            tr.SetFailureHandlingOptions(failureOptions);

            foreach (Element concreteComponent in ConcreteComponents)
            {
                PublicParameter.RevitMark(concreteComponent, $"{mark}-{positionNumber}");
                PublicParameter.MarkText(concreteComponent, mark);

                positionNumber++;
            }

            AutoAssembly?.Disassemble();

            tr.Commit();
        }
    }
}