﻿using System;
using System.Linq;
using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Architecture;
using OrslavTeam.Revit.Axillary.PublicInformation;

namespace OrslavTeam.Revit.Concrete.Component.Worker
{
    public static class Helper
    {
        public static double GetVolume(Element el)
        {
            if (el is Stairs)
            {
                double stairVolume = el
                    .GetMaterialIds(false)
                    .Select(materialId => el.GetMaterialVolume(materialId))
                    .Sum();

                return Math.Ceiling(
                             UnitUtils.ConvertFromInternalUnits(stairVolume, DisplayUnitType.DUT_CUBIC_METERS) *
                             100) / 100;
            }
            return PublicParameter.GetComponentVolume(el);
        }
    }
}