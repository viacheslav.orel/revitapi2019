﻿using Autodesk.Revit.DB;
using System.Collections.Generic;
using System.Linq;
using OrslavTeam.Revit.Axillary.PublicInformation;
using OrslavTeam.Revit.Concrete.Helper;

namespace OrslavTeam.Revit.Concrete.Component.GeneralMark.Models
{
    public class ConcreteComponent
    {
        #region fields -------------------------------------------------------------------------------------------------

        public string Name { get; }

        public string MarkPrefix { get; }
        public string MarkFirst { get; }
        public string MarkSecond { get; }
        public string MarkThird { get; }
        public string MarkFours { get; }

        public string FullMark { get; }

        public Element[] Elements { get; }

        #endregion


        #region Constructors -------------------------------------------------------------------------------------------

        public ConcreteComponent() { }

        public ConcreteComponent(Element el, Document doc)
        {
            MarkPrefix = PublicParameter.MarkPrefix(el);
            MarkFirst = PublicParameter.MarkFirst(el);
            MarkSecond = PublicParameter.MarkSecond(el);
            MarkThird = PublicParameter.MarkThird(el);
            MarkFours = PublicParameter.MarkFours(el);

            FullMark = $"{MarkPrefix}{MarkFirst}.{MarkSecond}-{MarkThird}";

            if (el is Group group)
            {
                Name = group.GroupType.Name;
                Elements = group
                    .GetMemberIds()
                    .Select(doc.GetElement)
                    .Where(element => IsCurrentComponent.IsCastInPlaceElement(element, doc))
                    .ToArray();
            }
            else
            {
                Name = el.get_Parameter(BuiltInParameter.ELEM_FAMILY_AND_TYPE_PARAM).AsValueString();
                Elements = new[] {el};
            }
        }

        #endregion


        #region methods ------------------------------------------------------------------------------------------------

        public void SetMark(int number)
        {
            string markThird = number.ToString();
            string mark = $"{MarkPrefix}{MarkFirst}.{MarkSecond}-{markThird}";
            string fullMark = $"{mark}-{MarkFours}";

            foreach (Element element in Elements)
            {
                PublicParameter.MarkThird(element, markThird);
                PublicParameter.RevitMark(element, fullMark);
                PublicParameter.MarkText(element, mark);
            }
        }

        #endregion
    }
}