﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Documents;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using OrslavTeam.Revit.Axillary.PublicInformation;
using OrslavTeam.Revit.Concrete.Component.GeneralMark.Models;
using OrslavTeam.Revit.Concrete.Helper;

namespace OrslavTeam.Revit.Concrete.Component.GeneralMark.AddIns
{
    [Transaction(TransactionMode.Manual)]
    public class SetFullMark : IExternalCommand
    {
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            Document doc = commandData.Application.ActiveUIDocument.Document;
            ElementId currentPhase = doc.ActiveView.get_Parameter(BuiltInParameter.VIEW_PHASE).AsElementId();

            var phaseFilter = new ElementPhaseStatusFilter(currentPhase, ElementOnPhaseStatus.New);

            Dictionary<string, List<ConcreteComponent>> modelGroupCollector = new FilteredElementCollector(doc)
                .WhereElementIsNotElementType()
                .WherePasses(phaseFilter)
                .Where(el =>
                {
                    if (el is Group) return !el.Name.StartsWith("&") && !el.Name.EndsWith("_rebars");
                    return el.GroupId != ElementId.InvalidElementId
                        ? doc.GetElement(el.GroupId).Name.StartsWith("&") &&
                          doc.GetElement(el.GroupId).Name.EndsWith("_rebars")
                        : IsCurrentComponent.IsCastInPlaceElement(el, doc);
                })
                .Select(el => new ConcreteComponent(el, doc))
                .Aggregate(new Dictionary<string, List<ConcreteComponent>>(), (acc, component) =>
                {
                    string prevMark = $"{component.MarkPrefix}{component.MarkFirst}.{component.MarkSecond}";

                    if (acc.ContainsKey(prevMark)) acc[prevMark].Add(component);
                    else acc.Add(prevMark, new List<ConcreteComponent> { component });
                    return acc;
                });

            using (var tr = new Transaction(doc, "Set fullMark"))
            {
                tr.Start();

                foreach (KeyValuePair<string, List<ConcreteComponent>> concreteComponents in modelGroupCollector)
                {
                    Dictionary<string, List<ConcreteComponent>> groupByName = concreteComponents.Value
                        .Aggregate(new Dictionary<string, List<ConcreteComponent>>(), (acc, component) =>
                        {
                            if (acc.ContainsKey(component.Name)) acc[component.Name].Add(component);
                            else acc.Add(component.Name, new List<ConcreteComponent> { component });
                            return acc;
                        });

                    var mark = 1;
                    foreach (KeyValuePair<string, List<ConcreteComponent>> group in groupByName)
                    {
                        foreach (ConcreteComponent component in group.Value)
                        {
                            component.SetMark(mark);
                        }

                        mark++;
                    }
                }

                tr.Commit();
            }

            return Result.Succeeded;
        }
    }
}