﻿using System.Windows;

namespace OrslavTeam.Revit.Concrete.Component.GeneralMark.View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void FullMark_Click(object sender, RoutedEventArgs e)
        {
            ((MainWindowsDataContext) this.DataContext).IsConfirm = MainWindowsDataContext.MarkMethods.FULL;
            this.Close();
        }

        private void ContinueMark_Click(object sender, RoutedEventArgs e)
        {
            ((MainWindowsDataContext)this.DataContext).IsConfirm = MainWindowsDataContext.MarkMethods.CONTINUE;
            this.Close();
        }
    }
}
