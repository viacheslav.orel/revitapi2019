﻿using System.Linq;
using Autodesk.Revit.DB;
using OrslavTeam.Revit.Axillary.ViewTemplate.Infrastructure;

namespace OrslavTeam.Revit.Concrete.Component.GeneralMark.View
{
    public class MainWindowsDataContext : ObservableObject
    {
        #region precast reinforced concrete components -----------------------------------------------------------------

        // foundation
        private static string _prcFoundation = "Ф";

        public string PrcFoundation
        {
            get => _prcFoundation;
            set
            {
                _prcFoundation = value;
                RaisePropertyChangedEvent("PrcFoundation");
            }
        }


        // solid block
        private static string _prcSolidBlock = "ФБС";

        public string PrcSolidBlock
        {
            get => _prcSolidBlock;
            set
            {
                _prcSolidBlock = value;
                RaisePropertyChangedEvent("PrcSolidBlock");
            }
        }


        // base slab
        private static string _prcBaseSlab = "ФП";

        public string PrcBaseSlab
        {
            get => _prcBaseSlab;
            set
            {
                _prcBaseSlab = value;
                RaisePropertyChangedEvent("PrcBaseSlab");
            }
        }


        // column
        private static string _prcColumn = "К";

        public string PrcColumn
        {
            get => _prcColumn;
            set
            {
                _prcColumn = value;
                RaisePropertyChangedEvent("PrcColumn");
            }
        }


        // beam
        private static string _prcBeam = "Б";

        public string PrcBeam
        {
            get => _prcBeam;
            set
            {
                _prcBeam = value;
                RaisePropertyChangedEvent("PrcBeam");
            }
        }


        // floor
        private static string _prcFloor = "П";

        public string PrcFloor
        {
            get => _prcFloor;
            set
            {
                _prcFloor = value;
                RaisePropertyChangedEvent("PrcFloor");
            }
        }


        // wall
        private static string _prcWall = "ПС";

        public string PrcWall
        {
            get => _prcWall;
            set
            {
                _prcWall = value;
                RaisePropertyChangedEvent("PrcWall");
            }
        }


        // stairs
        private static string _prcStairs = "С";

        public string PrcStairs
        {
            get => _prcStairs;
            set
            {
                _prcStairs = value;
                RaisePropertyChangedEvent("PrcStairs");
            }
        }

        #endregion


        #region in-suite reinforced concrete component -----------------------------------------------------------------

        // foundation
        private static string _srcFoundation = "Фм";

        public string SrcFoundation
        {
            get => _srcFoundation;
            set
            {
                _srcFoundation = value;
                RaisePropertyChangedEvent("SrcFoundation");
            }
        }


        // base slab
        private static string _srcBaseSlab = "ФПм";

        public string SrcBaseSlab
        {
            get => _srcBaseSlab;
            set
            {
                _srcBaseSlab = value;
                RaisePropertyChangedEvent("SrcBaseSlab");
            }
        }


        // column
        private static string _srcColumn = "Км";

        public string SrcColumn
        {
            get => _srcColumn;
            set
            {
                _srcColumn = value;
                RaisePropertyChangedEvent("SrcColumn");
            }
        }


        // beam
        private static string _srcBeam = "Бм";

        public string SrcBeam
        {
            get => _srcBeam;
            set
            {
                _srcBeam = value;
                RaisePropertyChangedEvent("SrcBeam");
            }
        }


        // floor
        private static string _srcFloor = "Пм";

        public string SrcFloor
        {
            get => _srcFloor;
            set
            {
                _srcFloor = value;
                RaisePropertyChangedEvent("SrcFloor");
            }
        }


        // wall
        private static string _srcWall = "Стм";

        public string SrcWall
        {
            get => _srcWall;
            set
            {
                _srcWall = value;
                RaisePropertyChangedEvent("SrcWall");
            }
        }


        // stairs
        private static string _srcStairs = "См";

        public string SrcStairs
        {
            get => _srcStairs;
            set
            {
                _srcStairs = value;
                RaisePropertyChangedEvent("SrcStairs");
            }
        }


        // buttress
        private static string _srcButtress = "КФм";

        public string SrcButtress
        {
            get => _srcButtress;
            set
            {
                _srcButtress = value;
                RaisePropertyChangedEvent("SrcButtress");
            }
        }

        #endregion


        #region controls block -----------------------------------------------------------------------------------------

        private Phase[] _phases;

        public Phase[] Phases
        {
            get => _phases;
            set
            {
                _phases = value;
                RaisePropertyChangedEvent("Phases");

                CurrentPhase = value.FirstOrDefault();
            }
        }


        private Phase _currentPhase;

        public Phase CurrentPhase
        {
            get => _currentPhase;
            set
            {
                _currentPhase = value;
                RaisePropertyChangedEvent("CurrentPhase");
            }
        }


        public enum MarkMethods
        {
            NONE,
            FULL,
            CONTINUE
        }

        public MarkMethods IsConfirm = MarkMethods.NONE;

        #endregion
    }
}