﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using OrslavTeam.Revit.Axillary.ViewTemplate.ReportWindow;
using OrslavTeam.Revit.Concrete.Component.GeneralMark.View;

namespace OrslavTeam.Revit.Concrete.Component.GeneralMark
{
    [Transaction(TransactionMode.Manual)]
    public class MarkCommand : IExternalCommand
    {
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            Document doc = commandData.Application.ActiveUIDocument.Document;
            Phase[] phases = (from object obj in doc.Phases select obj as Phase).ToArray();

            if (!ShowRequestView(phases, doc, out Helper helper)) return Result.Cancelled;

            try
            {
                helper.GetElements();
                helper.MarkElements();
                helper.ShowReportMessage();
            }
            catch (Exception e)
            {
                var messageView = new MessageView($"Message: {e.Message}\nStack: {e.StackTrace}\nData: {e.Data}");
                messageView.ShowDialog();
                
                return Result.Failed;
            }
            
            
            return Result.Succeeded;
        }

        private static bool ShowRequestView(Phase[] phases, Document doc, out Helper helper)
        {
            var isRemark = true;
            helper = null;
            
            var dataContext = new MainWindowsDataContext {Phases = phases};
            var view = new MainWindow() {DataContext = dataContext};
            view.ShowDialog();

            // ReSharper disable once SwitchStatementMissingSomeCases
            switch (dataContext.IsConfirm)
            {
                case MainWindowsDataContext.MarkMethods.NONE:
                    return false;
                case MainWindowsDataContext.MarkMethods.CONTINUE:
                    isRemark = false;
                    goto default;
                case MainWindowsDataContext.MarkMethods.FULL:
                    isRemark = true;
                    goto default;
                default:
                    Phase currentPhase = dataContext.CurrentPhase;
                    var prefixMarkByAssemblyKod = new Dictionary<string, string>
                    {
                        //Foundation
                        {"B.01.01.03.01", dataContext.PrcBaseSlab},
                        {"B.01.01.03.02", dataContext.SrcBaseSlab},
                        {"B.01.01.04.02", dataContext.PrcSolidBlock},
                        {"B.01.01.05.01", dataContext.PrcFoundation},
                        {"B.01.01.05.02", dataContext.SrcFoundation},
                        //Framework
                        {"B.01.02.01.01", dataContext.PrcColumn},
                        {"B.01.02.01.02", dataContext.SrcColumn},
                        {"B.01.02.02.01", dataContext.PrcBeam},
                        {"B.01.02.02.02", dataContext.SrcBeam},
                        //Stairs
                        {"B.01.03.01.01", dataContext.PrcStairs},
                        {"B.01.03.02.01", dataContext.PrcStairs},
                        {"B.01.03.01.02", dataContext.PrcStairs},
                        {"B.01.03.02.02", dataContext.PrcStairs},
                        //Floor
                        {"B.01.04.01", dataContext.PrcFloor},
                        {"B.01.04.02", dataContext.SrcFloor},
                        //Wall
                        {"B.01.05.01", dataContext.PrcWall},
                        {"B.01.05.02", dataContext.SrcWall}
                    };
                    
                    helper = new Helper(doc, isRemark, currentPhase, prefixMarkByAssemblyKod);
                    return true;
            }
        }
    }
}