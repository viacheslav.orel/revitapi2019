﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autodesk.Revit.DB;
using OrslavTeam.Revit.Axillary.PublicInformation;
using OrslavTeam.Revit.Axillary.PublicInformation.Failure;
using OrslavTeam.Revit.Axillary.ViewTemplate.ReportWindow;
using Group = Autodesk.Revit.DB.Group;

namespace OrslavTeam.Revit.Concrete.Component.GeneralMark
{
    public class Helper
    {
        #region fields -------------------------------------------------------------------------------------------------

        private readonly Document _doc;
        private static bool _isNewMark;
        private readonly Phase _currentPhase;

        private Dictionary<string, List<Element>> _modelGroups;
        private Dictionary<string, List<Element>> _structuralFoundations;
        private Dictionary<string, List<Element>> _genericModels;
        private Dictionary<string, List<Element>> _structuralColumns;
        private Dictionary<string, List<Element>> _structuralFramings;
        private Dictionary<string, List<Element>> _walls;
        private Dictionary<string, List<Element>> _floors;

        #endregion

        #region data ---------------------------------------------------------------------------------------------------

        private static Dictionary<string, string> _prefixMarkByAssemblyKod;
        private static Dictionary<string, int> _currentMarkNumber;

        private static int GetCurrentMarkNumber(string markPrefix)
        {
            if (_currentMarkNumber.ContainsKey(markPrefix)) return _currentMarkNumber[markPrefix];
            _currentMarkNumber.Add(markPrefix, 1);

            return 1;
        }

        private static void SetCurrentMarkNumber(string markPrefix, int markNumber)
        {
            if (_currentMarkNumber.ContainsKey(markPrefix))
            {
                if (_currentMarkNumber[markPrefix] < markNumber)
                    _currentMarkNumber[markPrefix] = markNumber;

                return;
            }

            _currentMarkNumber.Add(markPrefix, markNumber);
        }

        #endregion

        #region repport data -------------------------------------------------------------------------------------------

        private static string GetFamilyName(Element el) =>
            el.get_Parameter(BuiltInParameter.ELEM_FAMILY_AND_TYPE_PARAM).AsValueString();

        private static string GetElementId(Element el) => el.Id.ToString();


        private static int _elementQuantity;

        private static List<string> _successfullyData;

        private static void SuccessfullyData(Element el, string mark)
        {
            _successfullyData.Add(
                $"Element name: {GetFamilyName(el)};  Id: {GetElementId(el)}; Mark: {mark}");
            _elementQuantity++;
        }


        private static List<string> _errorData;

        private static void ErrorData(Element el)
        {
            _errorData.Add($"Element name: {GetFamilyName(el)};  Id: {GetElementId(el)};");
            _elementQuantity++;
        }


        private static List<string> _ignoredData;

        private static void IgnoredData(Element el, string message)
        {
            _ignoredData.Add($"Element name: {GetFamilyName(el)}; Id: {GetElementId(el)}; Exception: {message}");
            _elementQuantity++;
        }


        public void ShowReportMessage()
        {
            var reportView = new ReportView(_elementQuantity, _successfullyData, _errorData, _ignoredData);
            reportView.Show();
        }

        #endregion

        #region failure option -----------------------------------------------------------------------------------------

        private static void MessageOffDuplicateMark(Transaction tr)
        {
            FailureHandlingOptions failureOptions = tr.GetFailureHandlingOptions();
            failureOptions.SetFailuresPreprocessor(new DuplicateMarkSwallower());
            tr.SetFailureHandlingOptions(failureOptions);
        }

        private static void MessageOfAll(Transaction tr)
        {
            FailureHandlingOptions failureOptions = tr.GetFailureHandlingOptions();
            failureOptions.SetFailuresPreprocessor(new ClearAllWarning());
            tr.SetFailureHandlingOptions(failureOptions);
        }

        #endregion

        #region constructors -------------------------------------------------------------------------------------------

        public Helper(Document doc, bool isRemark, Phase currentPhase, Dictionary<string, string> prefixMark)
        {
            _doc = doc;
            _isNewMark = isRemark;
            _currentPhase = currentPhase;

            _prefixMarkByAssemblyKod = prefixMark;
            _currentMarkNumber = new Dictionary<string, int>();

            _elementQuantity = 0;
            _successfullyData = new List<string> {"Марку призначено"};
            _errorData = new List<string> {"Не вдалося призначити марку"};
            _ignoredData = new List<string> {"Пропущені елементи"};
        }

        #endregion


        #region methods get element ------------------------------------------------------------------------------------

        public void GetElements()
        {
            ElementId phaseId = _currentPhase.Id;

            _modelGroups = GetElementsModels(_doc, BuiltInCategory.OST_IOSModelGroups, phaseId, _isNewMark,
                FilteredGroup, AggregateGroupByPrefixMark);
            _structuralFoundations = GetElementsModels(_doc, BuiltInCategory.OST_StructuralFoundation, phaseId,
                _isNewMark, FilteredFoundation, AggregateElementByPrefixMark);
            _genericModels = GetElementsModels(_doc, BuiltInCategory.OST_GenericModel, phaseId, _isNewMark,
                FilteredElement, AggregateElementByPrefixMark);
            _structuralColumns = GetElementsModels(_doc, BuiltInCategory.OST_StructuralColumns, phaseId, _isNewMark,
                FilteredElement, AggregateElementByPrefixMark);
            _structuralFramings = GetElementsModels(_doc, BuiltInCategory.OST_StructuralFraming, phaseId, _isNewMark,
                FilteredElement, AggregateElementByPrefixMark);
            _walls = GetElementsModels(_doc, BuiltInCategory.OST_Walls, phaseId, _isNewMark,
                FilteredElement, AggregateElementByPrefixMark);
            _floors = GetElementsModels(_doc, BuiltInCategory.OST_Floors, phaseId, _isNewMark,
                FilteredElement, AggregateElementByPrefixMark);
        }

        private static Dictionary<string, List<Element>> GetElementsModels(Document doc, BuiltInCategory category,
            ElementId phaseId, bool isRemark, FilterDelegate fd, AggregateDelegate ad)
        {
            return new FilteredElementCollector(doc)
                .OfCategory(category)
                .Where(el => fd(el, phaseId, isRemark, doc))
                .Aggregate(new Dictionary<string, List<Element>>(), (acc, el) => ad(acc, el, doc));
        }

        #endregion

        #region methods filtered ---------------------------------------------------------------------------------------

        private delegate bool FilterDelegate(Element el, ElementId phaseId, bool isRemark, Document doc);

        private static bool FilteredGroup(Element el, ElementId phaseId, bool isRemark, Document doc)
        {
            Element groupMember;
            try
            {
                ElementId groupId = ((Group) el).GetMemberIds()
                    .FirstOrDefault(elId => IsConcreteElementInGroup(elId, doc));
                groupMember = doc.GetElement(groupId);
            }
            catch (Exception)
            {
                return false;
            }
            
            if (!IsCurrentPhase(groupMember, phaseId) || IsGroupIgnore(el)) return false;
            if (isRemark || !IsGroupHaveMark(el)) return true;

            SetCurrentMarkNumber(PublicParameter.MarkPrefix(el), int.Parse(PublicParameter.MarkFirst(el)));
            return false;
        }

        private static bool FilteredElement(Element el, ElementId phaseId, bool isRemark, Document doc)
        {
            // ReSharper disable once InvertIf
            if (IsCurrentPhase(el, phaseId) && IsConcreteComponent(el, doc) && !IsSubComponent(el, doc))
            {
                if (isRemark || !IsElementHaveMark(el)) return true;
                
                SetCurrentMarkNumber(PublicParameter.MarkPrefix(el), int.Parse(PublicParameter.MarkFirst(el)));
                return true;
            }

            return false;
        }

        private static bool FilteredFoundation(Element el, ElementId phaseId, bool isRemark, Document doc)
        {
            return !IsPile(el, doc) && FilteredElement(el, phaseId, isRemark, doc);
        }

        #endregion

        #region methods is ---------------------------------------------------------------------------------------------

        private static bool IsConcreteElementInGroup(ElementId elId, Document doc)
        {
            Element elem = doc.GetElement(elId);
            string fullName = elem.get_Parameter(BuiltInParameter.ELEM_FAMILY_AND_TYPE_PARAM)
                .AsValueString();
            return fullName.Contains("SRC");
        }
        
        private static bool IsCurrentPhase(Element el, ElementId phaseId) =>
            el.GetPhaseStatus(phaseId) == ElementOnPhaseStatus.New;

        private static bool IsGroupIgnore(Element el) => el.Name.StartsWith("&");

        private static bool IsGroupHaveMark(Element el) => el.Name.EndsWith("!");

        private static bool IsSubComponent(Element el, Document doc)
        {
            ElementId groupId = el.GroupId;
            if (groupId == ElementId.InvalidElementId) return false;

            return !doc.GetElement(groupId).Name.StartsWith("&");
        }

        private static bool IsConcreteComponent(Element el, Document doc) =>
            PublicParameter.GetUniformatKode(doc.GetElement(el.GetTypeId())).StartsWith("B.01");

        private static bool IsElementHaveMark(Element el) => PublicParameter.RevitMark(el) != "";

        private static bool IsPile(Element el, Document doc) =>
            PublicParameter.GetUniformatKode(doc.GetElement(el.GetTypeId())).StartsWith("B.01.01.02");

        #endregion

        #region methods group ------------------------------------------------------------------------------------------

        private static IEnumerable<IGrouping<int, Element>> GroupByLength(IEnumerable<Element> elements)
        {
            return elements.GroupBy(PublicParameter.DimensionLength).OrderBy(gr => gr.Key);
        }

        private static Dictionary<string, List<Element>> GroupByAssembly(IEnumerable<Element> elements, Document doc)
        {
            var result = new Dictionary<string, List<Element>>();
            var assemblyElementPairs = new List<KeyValuePair<AssemblyInstance, Element>>();

            using (var tr = new Transaction(doc, "Create assembly"))
            {
                MessageOfAll(tr);
                tr.Start();
                
                // ReSharper disable once ForeachCanBeConvertedToQueryUsingAnotherGetEnumerator
                foreach (Element element in elements)
                {
                    try
                    {
                        AssemblyInstance assemblyInstance = CreateAssembly((Group) element, tr, doc);
                        assemblyElementPairs.Add(new KeyValuePair<AssemblyInstance, Element>(assemblyInstance, element));
                    }
                    catch (Exception)
                    {
                        if (result.ContainsKey(element.Name)) result[element.Name].Add(element);
                        else result.Add(element.Name, new List<Element>{ element });
                    }
                    
                }

                tr.Commit();
            }

            foreach (KeyValuePair<AssemblyInstance,Element> assemblyElementPair in assemblyElementPairs)
            {
                if (result.ContainsKey(assemblyElementPair.Key.Name))
                    result[assemblyElementPair.Key.Name].Add(assemblyElementPair.Value);
                else
                    result.Add(assemblyElementPair.Key.Name, new List<Element>{ assemblyElementPair.Value });
            }

            using (var tr = new Transaction(doc, "Remove assembly"))
            {
                MessageOfAll(tr);
                tr.Start();

                foreach (KeyValuePair<AssemblyInstance,Element> assemblyElementPair in assemblyElementPairs)
                {
                    assemblyElementPair.Key.Disassemble();
                }

                tr.Commit();
            }
            
            return result;
        }

        private static IEnumerable<IGrouping<string, Element>> GroupByFamilyAndTypeName(IEnumerable<Element> elements)
        {
            return elements
                .OrderBy(el =>
                {
                    switch (el.Location)
                    {
                        case LocationPoint locationPoint:
                            return locationPoint.Point.Z;
                        case LocationCurve locationCurve:
                            return ((Line) locationCurve.Curve).Origin.Z;
                        default:
                            return PublicParameter.MountingLevel(el);
                    }
                })
                .GroupBy(el => el.get_Parameter(BuiltInParameter.ELEM_FAMILY_AND_TYPE_PARAM).AsValueString())
                .OrderBy(gr => gr.Key);
        }

        #endregion

        #region methods agregate ---------------------------------------------------------------------------------------

        private delegate Dictionary<string, List<Element>> AggregateDelegate(
            Dictionary<string, List<Element>> acc, Element element, Document doc);


        private static Dictionary<string, List<Element>> AggregateGroupByPrefixMark(
            Dictionary<string, List<Element>> acc, Element element, Document doc)
        {
            string markPrefix = element.Name.Split('_')[0];

            if (acc.ContainsKey(markPrefix)) acc[markPrefix].Add(element);
            else acc.Add(markPrefix, new List<Element> {element});

            return acc;
        }

        private static Dictionary<string, List<Element>> AggregateElementByPrefixMark(
            Dictionary<string, List<Element>> acc, Element element, Document doc)
        {
            ElementId typeId = element.GetTypeId();
            if (typeId == ElementId.InvalidElementId) return acc;

            string assemblyKod = PublicParameter.GetUniformatKode(doc.GetElement(typeId));
            if (!_prefixMarkByAssemblyKod.ContainsKey(assemblyKod))
            {
                IgnoredData(element, $"Error assembly kod: {assemblyKod}");
                return acc;
            }

            string prefixMark = _prefixMarkByAssemblyKod[assemblyKod];
            if (acc.ContainsKey(prefixMark)) acc[prefixMark].Add(element);
            else acc.Add(prefixMark, new List<Element> {element});

            return acc;
        }

        private static List<ElementId> AggregateElementFromGroup(List<ElementId> acc, ElementId elementId, Document doc)
        {
            Element element = doc.GetElement(elementId);
            if (!(element is FamilyInstance))
            {
                acc.Add(elementId);
                return acc;
            }

            ICollection<ElementId> subComponentIds = ((FamilyInstance) element).GetSubComponentIds();

            return subComponentIds.Any() ? subComponentIds.Aggregate(acc, (ids, id) => AggregateElementFromGroup(ids, id, doc)) : acc;
        }

        #endregion

        #region methods create or delete -------------------------------------------------------------------------------

        private static AssemblyInstance CreateAssembly(Group group, Transaction tr, Document doc)
        {
            if (tr.GetStatus() != TransactionStatus.Started) throw new Exception("Transaction is not started!");

            List<ElementId> componentIds = group.GetMemberIds()
                .Where(elId => IsConcreteElementInGroup(elId, doc))
                .Aggregate(new List<ElementId>(), (ids, id) => AggregateElementFromGroup(ids, id, doc));
            
            ElementId assemblyCategory = doc.GetElement(componentIds.First()).Category.Id;

            return AssemblyInstance.Create(doc, componentIds, assemblyCategory);
        }

        #endregion

        #region methods set --------------------------------------------------------------------------------------------

        private static int SetMarkToGroups(Document doc, IEnumerable<Element> groups, string markPrefix, int markFirst)
        {
            using (var tr = new Transaction(doc, $"Set mark to {markPrefix} groups"))
            {
                MessageOffDuplicateMark(tr);
                tr.Start();

                foreach (Element group in groups)
                {
                    SetMarkToGroup(doc, group as Group, markPrefix, markFirst.ToString(), tr);
                }

                tr.Commit();
            }

            return markFirst;
        }

        private static void SetMarkToGroup(Document doc, Group group, string markPrefix, string markFirst, Transaction tr)
        {
            if (tr.GetStatus() != TransactionStatus.Started) throw new Exception("Transaction is not started");

            if (!SetMarkInfo(group, markPrefix, markFirst)) ErrorData(group);

            IEnumerable<Element> componentMembers = group.GetMemberIds()
                .Where(elId => IsConcreteElementInGroup(elId, doc))
                .Select(doc.GetElement);

            var memberPosition = 0;
            foreach (Element element in componentMembers)
            {
                if(SetMarkInfo(element, markPrefix, markFirst, markFours: memberPosition.ToString())) memberPosition++;
                else
                {
                    ErrorData(group);
                    return;
                }
            }

            SuccessfullyData(group, $"{markPrefix}{markFirst}");
            if (!group.Name.EndsWith("_!")) group.GroupType.Name += "_!";
        }

        private static void SetMarkToElements(Document doc, IEnumerable<Element> elements, string markPrefix,
            string markFirst, string markSecond = "0")
        {
            using (var tr = new Transaction(doc, $"Set mark to {markPrefix}{markFirst}{markSecond} elements"))
            {
                MessageOffDuplicateMark(tr);
                tr.Start();

                foreach (Element el in elements)
                {
                    if (SetMarkInfo(el, markPrefix, markFirst, markSecond))
                        SuccessfullyData(el, $"{markPrefix}{markFirst}{markSecond}");
                    else ErrorData(el);
                }

                tr.Commit();
            }
        }

        private static bool SetMarkInfo(Element elem, string prefixMark, string markFirst,
            string markSecond = "0", string markThird = "0", string markFours = "0")
        {
            string firstPartMark = $"{prefixMark}{markFirst}";
            if (markSecond != "0") firstPartMark += $".{markSecond}";

            try
            {
                PublicParameter.MarkPrefix(elem, prefixMark);
                PublicParameter.MarkFirst(elem, markFirst);
                PublicParameter.MarkSecond(elem, markSecond);
                PublicParameter.MarkThird(elem, markThird);
                PublicParameter.MarkFours(elem, markFours);

                PublicParameter.RevitMark(elem, $"{firstPartMark}-{markThird}-{markFours}");
                PublicParameter.MarkText(elem, $"{firstPartMark}");
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        #endregion

        #region methods mark -------------------------------------------------------------------------------------------

        public void MarkElements()
        {
            MarkToGroup(_doc, _modelGroups);
            MarkByName(_doc, _structuralFoundations);
            MarkByName(_doc, _genericModels);
            MarkByNameByLength(_doc, _structuralColumns);
            MarkByNameByLength(_doc, _structuralFramings);
            MarkByAssembly(_doc, _walls);
            MarkByAssembly(_doc, _floors);
        }

        private static void MarkToGroup(Document doc, IDictionary<string, List<Element>> modelGroups)
        {
            if (modelGroups.ContainsKey("!"))
            {
                using (var tr = new Transaction(doc, "Set user mark to group"))
                {
                    MessageOfAll(tr);
                    tr.Start();

                    foreach (Element group in modelGroups["!"])
                    {
                        SetMarkToGroup(doc, group as Group, group.Name.Split('_')[1], "", tr);
                    }

                    tr.Commit();
                }

                modelGroups.Remove("!");
            }

            foreach (KeyValuePair<string, List<Element>> groupByPrefix in modelGroups)
            {
                int currentNumber = GetCurrentMarkNumber(groupByPrefix.Key);

                foreach (KeyValuePair<string, List<Element>> groupsPair in GroupByAssembly(groupByPrefix.Value, doc))
                {
                    currentNumber++;
                    currentNumber = SetMarkToGroups(doc, groupsPair.Value, groupByPrefix.Key, currentNumber);
                }

                _currentMarkNumber[groupByPrefix.Key] = currentNumber;
            }
        }

        private static void MarkByName(Document doc, IDictionary<string, List<Element>> elements)
        {
            foreach (KeyValuePair<string, List<Element>> elementsByPrefix in elements)
            {
                int currentNumber = GetCurrentMarkNumber(elementsByPrefix.Key);

                foreach (IGrouping<string, Element> elementsByName in GroupByFamilyAndTypeName(elementsByPrefix.Value))
                {
                    currentNumber++;
                    SetMarkToElements(doc, elementsByName, elementsByPrefix.Key, currentNumber.ToString());
                }

                _currentMarkNumber[elementsByPrefix.Key] = currentNumber;
            }
        }

        private static void MarkByNameByLength(Document doc, IDictionary<string, List<Element>> elements)
        {
            foreach (KeyValuePair<string, List<Element>> elementsByPrefix in elements)
            {
                int markFirst = GetCurrentMarkNumber(elementsByPrefix.Key);

                foreach (IGrouping<string, Element> elementsByName in GroupByFamilyAndTypeName(elementsByPrefix.Value))
                {
                    markFirst++;
                    var markSecond = 0;

                    foreach (IGrouping<int, Element> elementsByLength in GroupByLength(elementsByName))
                    {
                        markSecond++;
                        SetMarkToElements(doc, elementsByLength, elementsByPrefix.Key, markFirst.ToString(),
                            markSecond.ToString());
                    }
                }

                _currentMarkNumber[elementsByPrefix.Key] = markFirst;
            }
        }

        private static void MarkByAssembly(Document doc, IDictionary<string, List<Element>> elements)
        {
            foreach (KeyValuePair<string, List<Element>> elementsByPrefix in elements)
            {
                int markFirst = GetCurrentMarkNumber(elementsByPrefix.Key);

                foreach (KeyValuePair<string, List<Element>> elementsByAssembly in GroupByAssembly(elementsByPrefix.Value, doc))
                {
                    markFirst++;

                    SetMarkToElements(doc, elementsByAssembly.Value, elementsByPrefix.Key, markFirst.ToString());
                }

                _currentMarkNumber[elementsByPrefix.Key] = markFirst;
            }
        }

        #endregion
    }
}