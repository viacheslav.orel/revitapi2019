﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Structure;
using Autodesk.Revit.UI;
using Autodesk.Revit.UI.Selection;
using OrslavTeam.Revit.Axillary.PublicInformation;
using OrslavTeam.Revit.Axillary.RevitData;

namespace OrslavTeam.Revit.Concrete.OTRebar.Schedules
{
    [Transaction(TransactionMode.Manual)]
    public class CreateRebarSchedule : IExternalCommand
    {
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            #region Parameters

            //Document
            Document doc = commandData.Application.ActiveUIDocument.Document;
            Transaction tr = new Transaction(doc);
            Selection select = commandData.Application.ActiveUIDocument.Selection;
            ElementId phaseId = doc.ActiveView.get_Parameter(BuiltInParameter.VIEW_PHASE).AsElementId();

            NumberFormatInfo nfi = new NumberFormatInfo
            {
                NumberDecimalSeparator = ",",
                NumberGroupSeparator = " "
            };

            //Host element
            Element hostElement;
            ElementId hostId;
            string hostMark;
            
            //Rebar lists
            List<Element> rebarsInUnitLength = new List<Element>();
            List<Element> rebarsInPcs = new List<Element>();
            List<Element> frameworks = new List<Element>();
            List<Element> inserts = new List<Element>();

            //For steel-spread schedule
            List<SteelSpreadChar> rebarList = null;
            List<SteelSpreadChar> detailRebars = null;
            List<SteelSpreadChar> detailProfiles = null;
            #endregion

            #region HostElement

            using (SelectConcreteElement selectConcreteElement = new SelectConcreteElement())
            {
                try
                {
                    Reference pickedRef = select.PickObject(ObjectType.Element,
                        selectConcreteElement, "Оберіть залізобетонний елемент");
                    hostElement = doc.GetElement(pickedRef);
                    hostId = hostElement.Id;
                    hostMark = PublicParameter.MarkText(hostElement);
                }
                catch (Exception)
                {
                    return Result.Cancelled;
                }
            }
            
            MainWindow inputMarkInfo = new MainWindow();
            inputMarkInfo.ShowDialog();

            string detailMarkPrefix = inputMarkInfo.DetailPrefixMark.Text;
            int detailMarkNumber = 1;

            string frameworkMarkPrefix = inputMarkInfo.FrameworkPrefixMark.Text;
            int frameworkMarkNumber = 1;

            string insertMarkPrefix = inputMarkInfo.InsertPrefixMark.Text;
            int insertMarkNumber = 1;

            #endregion

            #region TransferParameter

            //AreaReinforcement
            using (FilteredElementCollector areaReinforcementCollection = new FilteredElementCollector(doc)
                .OfCategory(BuiltInCategory.OST_AreaRein)
                .WhereElementIsNotElementType())
            {
                tr.Start("TransferOtaAndTraForAreaReinforcement");
                foreach (Element element in areaReinforcementCollection)
                {
                    if (((AreaReinforcement) element).GetHostId() != hostId ||
                        element.get_Parameter(BuiltInParameter.PHASE_CREATED).AsElementId() != phaseId)
                        continue;
                    ElementId otaId = element.LookupParameter("_Основний тип арматури").AsElementId();
                    ElementId traId = element.LookupParameter("_Розміщення арматури").AsElementId();
                    ICollection<ElementId> subElementIds = ((AreaReinforcement) element).GetRebarInSystemIds();
                    if (subElementIds.Count == 0) continue;
                    foreach (ElementId subElementId in subElementIds)
                    {
                        Element subElement = doc.GetElement(subElementId);
                        subElement.LookupParameter("_Основний тип арматури").Set(otaId);
                        subElement.LookupParameter("_Розміщення арматури").Set(traId);
                    }
                }

                tr.Commit();
            }
            
            using (FilteredElementCollector pathReinforcementCollector = new FilteredElementCollector(doc)
                .OfCategory(BuiltInCategory.OST_PathRein)
                .WhereElementIsNotElementType())
            {
                tr.Start("TransferOtaAndTraForPathReinforcement");
                foreach (Element elem in pathReinforcementCollector)
                {
                    if (((PathReinforcement) elem).GetHostId() != hostId ||
                        elem.get_Parameter(BuiltInParameter.PHASE_CREATED).AsElementId() != phaseId)
                        continue;
                    ElementId otaId = elem.LookupParameter("_Основний тип арматури").AsElementId();
                    ElementId traId = elem.LookupParameter("_Розміщення арматури").AsElementId();
                    ICollection<ElementId> subElementIds = ((PathReinforcement) elem).GetRebarInSystemIds();
                    if (subElementIds.Count == 0) continue;
                    foreach (ElementId subElementId in subElementIds)
                    {
                        Element subElement = doc.GetElement(subElementId);
                        subElement.LookupParameter("_Основний тип арматури").Set(otaId);
                        subElement.LookupParameter("_Розміщення арматури").Set(traId);
                    }
                }

                tr.Commit();
            }

            #endregion

            #region Group rebar elements

            //Get rebar
            using (FilteredElementCollector rebarInProject = new FilteredElementCollector(doc)
                .OfCategory(BuiltInCategory.OST_Rebar)
                .WhereElementIsNotElementType())
            {
                foreach (Element rebar in rebarInProject)
                {
                    if (rebar is Autodesk.Revit.DB.Structure.Rebar rebar1)
                    {
                        if (rebar1.GetHostId() != hostId &&
                            rebar.get_Parameter(BuiltInParameter.PHASE_CREATED).AsElementId() != phaseId) continue;
                    }
                    else
                    {
                        if (((RebarInSystem) rebar).GetHostId() != hostId &&
                            rebar.get_Parameter(BuiltInParameter.PHASE_CREATED).AsElementId() != phaseId) continue;
                    }
                    

                    int rebarOta = PublicParameter.OTA(rebar);
                    if (rebarOta == 110 || rebarOta == 120)
                    {
                        rebarsInUnitLength.Add(rebar);
                    }
                    else
                    {
                        rebarsInPcs.Add(rebar);
                    }
                }
            }

            using (FilteredElementCollector structuralConnectionInProject = new FilteredElementCollector(doc)
                .OfCategory(BuiltInCategory.OST_StructConnections)
                .WhereElementIsNotElementType())
            {
                foreach (Element strCon in structuralConnectionInProject)
                {
                    if (((FamilyInstance)strCon).Host == null) continue;
                    if (((FamilyInstance)strCon).Host.Id != hostId &&
                        strCon.get_Parameter(BuiltInParameter.PHASE_CREATED).AsElementId() != phaseId &&
                        !Regex.IsMatch(strCon.get_Parameter(BuiltInParameter.ELEM_FAMILY_PARAM).AsValueString(), @"SRB_") &&
                        PublicParameter.HiddenRebarType(strCon) > 0) continue;
                        
                    //Transfer ota and tra
                    ICollection<ElementId> subComponentIds = ((FamilyInstance) strCon).GetSubComponentIds();
                    if (subComponentIds != null)
                    {
                        ElementId otaId = strCon.LookupParameter("_Основний тип арматури").AsElementId();
                        ElementId traId = strCon.LookupParameter("_Розміщення арматури").AsElementId();

                        tr.Start("TransferOtaAndTraFromStructuralConnection");
                        foreach (ElementId subComponentId in subComponentIds)
                        {
                            Element el = doc.GetElement(subComponentId);

                            el.LookupParameter("_Основний тип арматури").Set(otaId);
                            el.LookupParameter("_Розміщення арматури").Set(traId);
                        }

                        tr.Commit();
                    }

                    int strConUsing = PublicParameter.HiddenRebarType(strCon);
                    // ReSharper disable once SwitchStatementMissingSomeCases
                    switch (strConUsing)
                    {
                        case 2:
                        {
                            int strConOta = PublicParameter.OTA(strCon);
                            if (strConOta == 110 || strConOta == 120)
                            {
                                rebarsInUnitLength.Add(strCon);
                            }
                            else
                            {
                                rebarsInPcs.Add(strCon);
                            }

                            break;
                        }
                        case 4:
                            frameworks.Add(strCon);
                            break;
                        case 9:
                            inserts.Add(strCon);
                            break;
                    }
                }
            }

            var groupOfRebarInUnitLength = from rbInLength in rebarsInUnitLength
                let productMix = GetProductMixKod(rbInLength, doc)
                let elementKod = GetElementKod(rbInLength, doc)
                orderby productMix, elementKod
                group rbInLength by new {productMix, elementKod};

            var groupOfRebarInPcs = from rbInPcs in rebarsInPcs
                let productMix = GetProductMixKod(rbInPcs, doc)
                let elementKod = GetElementKod(rbInPcs, doc)
                let rebarFullLength = GetRebarLength(rbInPcs)
                let rebarForm = PublicParameter.RebarForm(rbInPcs)
                orderby productMix, elementKod, rebarFullLength, rebarForm
                group rbInPcs by new {productMix, elementKod, rebarFullLength, rebarForm};

            IEnumerable<IGrouping<string, Element>> groupOfFramework = from fr in frameworks
                let name = $"{PublicParameter.ProfileName(fr)}-{PublicParameter.DimensionLength(fr)}"
                orderby name
                group fr by name;

            IEnumerable<IGrouping<string, Element>> groupOfInsert = from ins in inserts
                let length = PublicParameter.DimensionLength(ins)
                let name = length == 0
                    ? PublicParameter.ProfileName(ins)
                    : $"{PublicParameter.ProfileName(ins)}-{length}"
                orderby name
                group ins by name;

            
            // ReSharper disable once UnusedVariable
            IEnumerable<IGrouping<int[], Element>> groupOfDetail = from rb in rebarsInPcs
                let form = PublicParameter.RebarForm(rb)
                where form != "01" && form != "00"
                let dimensions = GetRebarDimensions(rb)
                orderby form, dimensions
                group rb by dimensions;

            #endregion

            #region CreateSchedule
            
            //Parameters
            Element[] orderRowsInView = null;
            
            int rowInViewQuantity = 0;
            int rowInViewCurrentNumber = 0;

            double pointX = 0;
            double pointY = 0;
            double step = UnitUtils.ConvertToInternalUnits(-8, DisplayUnitType.DUT_MILLIMETERS);

            // ReSharper disable once PossibleMultipleEnumeration
            int rebarInUnitLengthQuantity = groupOfRebarInUnitLength.Count();
            // ReSharper disable once PossibleMultipleEnumeration
            int rebarInPcsQuantity = groupOfRebarInPcs.Count();
            // ReSharper disable once PossibleMultipleEnumeration
            int frameworkQuantity = groupOfFramework.Count();
            // ReSharper disable once PossibleMultipleEnumeration
            int insertQuantity = groupOfInsert.Count();

            Regex isScheduleHead = new Regex(@"^GEN_OTeam_Специфікація-2D_Форма7_РядокЗаголовка_ДСТУ Б А.2.4-4-2009", RegexOptions.Compiled);
            Regex isScheduleBody = new Regex(@"^GEN_OTeam_Специфікація-2D_Форма7_РядокДанних_ДСТУ Б А.2.4-4-2009", RegexOptions.Compiled);
            
            //Get family type
            FilteredElementCollector allDetailItem = new FilteredElementCollector(doc).OfClass(typeof(Family));
            if (!(doc.GetElement(((Family) allDetailItem.FirstOrDefault(el => isScheduleHead.IsMatch(el.Name)))
                ?.GetFamilySymbolIds()
                .First()) is FamilySymbol scheduleHadar))
            {
                MessageBox.Show("У моделі не знайдено сімейства з назвою 'GEN_OTeam_Специфікація-2D_Форма7_РядокЗаголовка_ДСТУ Б А.2.4-4-2009'");
                return Result.Failed;
            }

            if (!(doc.GetElement(((Family) allDetailItem.FirstOrDefault(el => isScheduleBody.IsMatch(el.Name)))
                ?.GetFamilySymbolIds()
                .FirstOrDefault()) is FamilySymbol scheduleBody))
            {
                MessageBox.Show("У моделі не знайдено сімейства з назвою 'GEN_OTeam_Специфікація-2D_Форма7_РядокДанних_ДСТУ Б А.2.4-4-2009'");
                return Result.Failed;
            }

            //Get view
            Element scheduleView = new FilteredElementCollector(doc).OfCategory(BuiltInCategory.OST_Views)
                .OfClass(typeof(ViewDrafting)).FirstOrDefault(view => IsScheduleByOneElement(view, hostMark));

            if (scheduleView == null)
            {
                ElementId draftingViewTypeId = new FilteredElementCollector(doc).OfClass(typeof(ViewFamilyType))
                    .Cast<ViewFamilyType>().First(vft => vft.ViewFamily == ViewFamily.Drafting).Id;
                
                tr.Start("createScheduleView");
                
                scheduleView = ViewDrafting.Create(doc, draftingViewTypeId);
                scheduleView.Name = $"Schedules_OneElement_{hostMark}";
                PublicParameter.ViewType(scheduleView, "99_Специфікація");
                PublicParameter.ViewSubType(scheduleView, hostMark);
                PublicParameter.ViewTemplateName(scheduleView, "OneElement");
                scheduleView.get_Parameter(BuiltInParameter.VIEW_SCALE_PULLDOWN_METRIC).Set(1);
                scheduleView.get_Parameter(BuiltInParameter.VIEW_DETAIL_LEVEL).Set(3);

                //Create header
                if (!scheduleHadar.IsActive) scheduleHadar.Activate();
                
                doc.Create.NewFamilyInstance(new XYZ(0, 0, 0), scheduleHadar, (ViewDrafting)scheduleView);
                
                tr.Commit();
            }
            else
            {
                //Get rows
                FilteredElementCollector rowsInView =
                    new FilteredElementCollector(doc, scheduleView.Id).OfCategory(BuiltInCategory.OST_DetailComponents);
                orderRowsInView = (from row in rowsInView
                    where row.get_Parameter(BuiltInParameter.ELEM_FAMILY_PARAM).AsValueString() ==
                          "GEN_OTeam_Специфікація-2D_Форма7_РядокДанних_ДСТУ Б А.2.4-4-2009"
                    orderby ((LocationPoint) row.Location).Point.X * 1000, ((LocationPoint) row.Location).Point.Y * -1000
                    select row).ToArray();

                rowInViewQuantity = orderRowsInView.Length;
            }

            tr.Start("CreateSchedule");

            if (rebarInUnitLengthQuantity != 0 || rebarInPcsQuantity != 0)
            {
                //Create header row
                if (orderRowsInView != null && rowInViewCurrentNumber < rowInViewQuantity)
                {
                    orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.First").Set("");
                    orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Second").Set("");
                    orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Third").Set("Деталі");
                    orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Fourth").Set("");
                    orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Fifth").Set("");
                    orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Sixth").Set("");
                    orderRowsInView[rowInViewCurrentNumber].LookupParameter("Row.Number").Set(rowInViewCurrentNumber);

                    pointX = ((LocationPoint) orderRowsInView[rowInViewCurrentNumber].Location).Point.X;
                    pointY = ((LocationPoint) orderRowsInView[rowInViewCurrentNumber].Location).Point.Y + step;
                }
                else
                {
                    if (!scheduleBody.IsActive) scheduleBody.Activate();
                    FamilyInstance row =
                        doc.Create.NewFamilyInstance(new XYZ(pointX, pointY, 0), scheduleBody, (ViewDrafting)scheduleView);

                    row.LookupParameter("Column.Third").Set("Деталі");
                    row.LookupParameter("Row.Number").Set(rowInViewCurrentNumber);

                    pointY += step;
                }

                rowInViewCurrentNumber++;
                    
                //Create body rows
                if (rebarInUnitLengthQuantity != 0)
                {
                    rebarList = new List<SteelSpreadChar>();
                    
                    // ReSharper disable once PossibleMultipleEnumeration
                    foreach (var rbGroup in groupOfRebarInUnitLength)
                    {
                        string mark =
                            $"{detailMarkPrefix}{detailMarkNumber}{PublicParameter.MarkSufix(rbGroup.First())}";
                        
                        string firstPartName = "";
                        double massPerUnitLength = 0;
                        GetData.GetRebarInfo(GetElementKod(rbGroup.First(), doc), ref firstPartName,
                            ref massPerUnitLength);
                        double length = 0;

                        foreach (Element rb in rbGroup)
                        {
                            length += GetRebarLengthWithScraft(rb, doc);

                            PublicParameter.MarkText(rb, mark);
                        }

                        length = Math.Round(length / 1000.0, 1);
                        
                        
                        string docName = GetData.GetProductMixNumber(GetProductMixKod(rbGroup.First(), doc));
                        string name = $"{firstPartName} l = {length} м.п.";
                        string massOne = massPerUnitLength.ToString("F2", nfi);
                        double fullMass = Math.Round(massPerUnitLength * length, 2);
                        string massAll = fullMass.ToString("F2", nfi);

                        rebarList.Add(new SteelSpreadChar
                        {
                            MaterialName = Axillary.GetMaterialName(rbGroup.First(), firstPartName, doc),
                            ProductMixNumber = docName,
                            CharName = firstPartName.Split(' ')[0],
                            Mass = fullMass
                        });
                        
                        if (orderRowsInView != null && rowInViewCurrentNumber < rowInViewQuantity)
                        {
                            orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.First").Set(mark);
                            orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Second").Set(docName);
                            orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Third").Set(name);
                            orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Fourth").Set("");
                            orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Fifth").Set(massOne);
                            orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Sixth").Set(massAll);

                            pointX = ((LocationPoint) orderRowsInView[rowInViewCurrentNumber].Location).Point.X;
                            pointY = ((LocationPoint) orderRowsInView[rowInViewCurrentNumber].Location).Point.Y +
                                     step;
                        }
                        else
                        {
                            if (!scheduleBody.IsActive) scheduleBody.Activate();
                            FamilyInstance row = doc.Create.NewFamilyInstance(new XYZ(pointX, pointY, 0),
                                scheduleBody, (ViewDrafting) scheduleView);

                            row.LookupParameter("Column.First").Set(mark);
                            row.LookupParameter("Column.Second").Set(docName);
                            row.LookupParameter("Column.Third").Set(name);
                            row.LookupParameter("Column.Fourth").Set("");
                            row.LookupParameter("Column.Fifth").Set(massOne);
                            row.LookupParameter("Column.Sixth").Set(massAll);

                            pointY += step;
                        }

                        detailMarkNumber++;
                        rowInViewCurrentNumber++;
                    }
                }

                if ( rebarInPcsQuantity != 0)
                {
                    if (rebarList == null) rebarList = new List<SteelSpreadChar>();
                    
                    // ReSharper disable once PossibleMultipleEnumeration
                    foreach (var rbGroup in groupOfRebarInPcs)
                    {
                        string mark =
                            $"{detailMarkPrefix}{detailMarkNumber}{PublicParameter.MarkSufix(rbGroup.First())}";
                        
                        string firstPartName = "";
                        double massPerUnitLength = 0;
                        GetData.GetRebarInfo(GetElementKod(rbGroup.First(), doc), ref firstPartName,
                            ref massPerUnitLength);
                        double length = 0;

                        foreach (Element rb in rbGroup)
                        {
                            length += GetRebarLength(rb);

                            PublicParameter.MarkText(rb, mark);
                        }

                        length = Math.Round(length / 1000.0, 1);

                        
                        string docName = GetData.GetProductMixNumber(GetProductMixKod(rbGroup.First(), doc));
                        string elementName = $"{firstPartName} l = {GetRebarLength(rbGroup.First())}";
                        int elementQuantity = rbGroup.Sum(GetRebarQuantity);
                        double massOne = Math.Round(length * massPerUnitLength, 3);
                        double massAll = Math.Round(massOne * elementQuantity, 2);
                        
                        rebarList.Add(new SteelSpreadChar
                        {
                            MaterialName = Axillary.GetMaterialName(rbGroup.First(), firstPartName, doc),
                            ProductMixNumber = docName,
                            CharName = firstPartName.Split(' ')[0],
                            Mass = massAll
                        });

                        if (orderRowsInView != null && rowInViewCurrentNumber < rowInViewQuantity)
                        {
                            orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.First").Set(mark);
                            orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Second").Set(docName);
                            orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Third").Set(elementName);
                            orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Fourth").Set(elementQuantity.ToString());
                            orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Fifth").Set(massOne.ToString("F3", nfi));
                            orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Sixth").Set(massAll.ToString("F2", nfi));

                            pointX = ((LocationPoint) orderRowsInView[rowInViewCurrentNumber].Location).Point.X;
                            pointY = ((LocationPoint) orderRowsInView[rowInViewCurrentNumber].Location).Point.Y +
                                     step;
                        }
                        else
                        {
                            if (!scheduleBody.IsActive) scheduleBody.Activate();
                            FamilyInstance row = doc.Create.NewFamilyInstance(new XYZ(pointX, pointY, 0),
                                scheduleBody, (ViewDrafting) scheduleView);

                            row.LookupParameter("Column.First").Set(mark);
                            row.LookupParameter("Column.Second").Set(docName);
                            row.LookupParameter("Column.Third").Set(elementName);
                            row.LookupParameter("Column.Fourth").Set(elementQuantity.ToString());
                            row.LookupParameter("Column.Fifth").Set(massOne.ToString("F3", nfi));
                            row.LookupParameter("Column.Sixth").Set(massAll.ToString("F2", nfi));

                            pointY += step;
                        }
                            
                        detailMarkNumber++;
                        rowInViewCurrentNumber++;
                    }
                }
                    
                //Create footer row
                if (orderRowsInView != null && rowInViewCurrentNumber < rowInViewQuantity)
                {
                    orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.First").Set("");
                    orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Second").Set("");
                    orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Third").Set("");
                    orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Fourth").Set("");
                    orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Fifth").Set("");
                    orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Sixth").Set("");
                    orderRowsInView[rowInViewCurrentNumber].LookupParameter("Row.Number").Set(rowInViewCurrentNumber);

                    pointX = ((LocationPoint) orderRowsInView[rowInViewCurrentNumber].Location).Point.X;
                    pointY = ((LocationPoint) orderRowsInView[rowInViewCurrentNumber].Location).Point.Y + step;
                }
                else
                {
                    if (!scheduleBody.IsActive) scheduleBody.Activate();
                    
                    FamilyInstance row =
                        doc.Create.NewFamilyInstance(new XYZ(pointX, pointY, 0), scheduleBody, (ViewDrafting)scheduleView);

                    row.LookupParameter("Row.Number").Set(rowInViewCurrentNumber);

                    pointY += step;
                }
                    
                rowInViewCurrentNumber++;
            }

            if (frameworkQuantity != 0)
            {
                detailRebars = new List<SteelSpreadChar>();
                detailProfiles = new List<SteelSpreadChar>();
                
                //Create head row
                if (orderRowsInView != null && rowInViewCurrentNumber < rowInViewQuantity)
                {
                    orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.First").Set("");
                    orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Second").Set("");
                    orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Third").Set("Каркаси");
                    orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Fourth").Set("");
                    orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Fifth").Set("");
                    orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Sixth").Set("");
                    orderRowsInView[rowInViewCurrentNumber].LookupParameter("Row.Number").Set(rowInViewCurrentNumber);

                    pointX = ((LocationPoint) orderRowsInView[rowInViewCurrentNumber].Location).Point.X;
                    pointY = ((LocationPoint) orderRowsInView[rowInViewCurrentNumber].Location).Point.Y + step;
                }
                else
                {
                    if (!scheduleBody.IsActive) scheduleBody.Activate();
                        
                    FamilyInstance row =
                        doc.Create.NewFamilyInstance(new XYZ(pointX, pointY, 0), scheduleBody, (ViewDrafting)scheduleView);

                    row.LookupParameter("Column.Third").Set("Каркаси");
                    row.LookupParameter("Row.Number").Set(rowInViewCurrentNumber);

                    pointY += step;
                }
                    
                rowInViewCurrentNumber++;
                //Create body rows
                // ReSharper disable once PossibleMultipleEnumeration
                foreach (IGrouping<string, Element> fr in groupOfFramework)
                {
                    string mark = $"{frameworkMarkPrefix}{frameworkMarkNumber}";
                    string docNumber = PublicParameter.DocNumber(fr.First());
                    string elementName = fr.Key;
                    int elementQuantity = fr.Count();
                    
                    double massOne = 0;
                    ICollection<ElementId> subElementIds = ((FamilyInstance) fr.First()).GetSubComponentIds();
                    foreach (ElementId subElementId in subElementIds)
                    {
                        Element el = doc.GetElement(subElementId);

                        string matName = Axillary.GetMaterialName(el, "Error Error", doc);
                        string productMixNumber = PublicParameter.DocNumber(el);
                        string elName =
                            $"{PublicParameter.ProfileNamePrefix(el)}{PublicParameter.ProfileName(el)}";
                        double mass = GetElementMass(el, doc);
                        massOne += mass;

                        if (PublicParameter.HiddenRebarType(el) == 0)
                        {
                            detailRebars.Add(new SteelSpreadChar
                            {
                                MaterialName = matName,
                                ProductMixNumber = productMixNumber,
                                CharName = elName,
                                Mass = mass * elementQuantity
                            });
                        }
                        else
                        {
                            detailProfiles.Add(new SteelSpreadChar
                            {
                                MaterialName = matName,
                                ProductMixNumber = productMixNumber,
                                CharName = elName,
                                Mass = mass * elementQuantity
                            });
                        }
                    }
                    
                    double massAll = Math.Round(massOne * elementQuantity, 2);

                    foreach (Element el in fr)
                    {
                        PublicParameter.MarkText(el, mark);
                    }

                    if (orderRowsInView != null && rowInViewCurrentNumber < rowInViewQuantity)
                    {
                        orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.First").Set(mark);
                        orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Second").Set(docNumber);
                        orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Third").Set(elementName);
                        orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Fourth").Set(elementQuantity.ToString());
                        orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Fifth").Set(massOne.ToString("F3", nfi));
                        orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Sixth").Set(massAll.ToString("F2", nfi));

                        pointX = ((LocationPoint) orderRowsInView[rowInViewCurrentNumber].Location).Point.X;
                        pointY = ((LocationPoint) orderRowsInView[rowInViewCurrentNumber].Location).Point.Y +
                                 step;
                    }
                    else
                    {
                        if (!scheduleBody.IsActive) scheduleBody.Activate();
                        FamilyInstance row = doc.Create.NewFamilyInstance(new XYZ(pointX, pointY, 0),
                            scheduleBody, (ViewDrafting) scheduleView);

                        row.LookupParameter("Column.First").Set(mark);
                        row.LookupParameter("Column.Second").Set(docNumber);
                        row.LookupParameter("Column.Third").Set(elementName);
                        row.LookupParameter("Column.Fourth").Set(elementQuantity.ToString());
                        row.LookupParameter("Column.Fifth").Set(massOne.ToString("F3", nfi));
                        row.LookupParameter("Column.Sixth").Set(massAll.ToString("F2", nfi));

                        pointY += step;
                    }
                        
                    detailMarkNumber++;
                    rowInViewCurrentNumber++;
                }
                    
                //Create footer row
                if (orderRowsInView != null && rowInViewCurrentNumber < rowInViewQuantity)
                {
                    orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.First").Set("");
                    orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Second").Set("");
                    orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Third").Set("");
                    orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Fourth").Set("");
                    orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Fifth").Set("");
                    orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Sixth").Set("");
                    orderRowsInView[rowInViewCurrentNumber].LookupParameter("Row.Number").Set(rowInViewCurrentNumber);

                    pointX = ((LocationPoint) orderRowsInView[rowInViewCurrentNumber].Location).Point.X;
                    pointY = ((LocationPoint) orderRowsInView[rowInViewCurrentNumber].Location).Point.Y + step;
                }
                else
                {
                    if (!scheduleBody.IsActive) scheduleBody.Activate();
                    
                    FamilyInstance row =
                        doc.Create.NewFamilyInstance(new XYZ(pointX, pointY, 0), scheduleBody, (ViewDrafting)scheduleView);

                    row.LookupParameter("Row.Number").Set(rowInViewCurrentNumber);

                    pointY += step;
                }
                    
                rowInViewCurrentNumber++;
            }

            if (insertQuantity != 0)
            {
                if(detailRebars == null) detailRebars = new List<SteelSpreadChar>();
                if(detailProfiles == null) detailProfiles = new List<SteelSpreadChar>();
                
                //Create head row
                if (orderRowsInView != null && rowInViewCurrentNumber < rowInViewQuantity)
                {
                    orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.First").Set("");
                    orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Second").Set("");
                    orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Third").Set("Закладні");
                    orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Fourth").Set("");
                    orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Fifth").Set("");
                    orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Sixth").Set("");
                    orderRowsInView[rowInViewCurrentNumber].LookupParameter("Row.Number").Set(rowInViewCurrentNumber);

                    pointX = ((LocationPoint) orderRowsInView[rowInViewCurrentNumber].Location).Point.X;
                    pointY = ((LocationPoint) orderRowsInView[rowInViewCurrentNumber].Location).Point.Y + step;
                }
                else
                {
                    if (!scheduleBody.IsActive) scheduleBody.Activate();
                        
                    FamilyInstance row =
                        doc.Create.NewFamilyInstance(new XYZ(pointX, pointY, 0), scheduleBody, (ViewDrafting)scheduleView);

                    row.LookupParameter("Column.Third").Set("Закладні");
                    row.LookupParameter("Row.Number").Set(rowInViewCurrentNumber);

                    pointY += step;
                }
                    
                rowInViewCurrentNumber++;
                    
                //Create body rows
                // ReSharper disable once PossibleMultipleEnumeration
                foreach (IGrouping<string,Element> insGroup in groupOfInsert)
                {
                    string mark = $"{insertMarkPrefix}{insertMarkNumber}";
                    string docNumber =
                        PublicParameter.DocNumber(((FamilyInstance) insGroup.First()).Symbol);
                    string elementName = insGroup.Key;
                    int elementQuantity = insGroup.Count();
                    
                    double massOne = 0;
                    ICollection<ElementId> subElementIds = ((FamilyInstance) insGroup.First()).GetSubComponentIds();
                    foreach (ElementId subElementId in subElementIds)
                    {
                        Element el = doc.GetElement(subElementId);

                        string matName = Axillary.GetMaterialName(el, "Error Error", doc);
                        string productMixNumber = PublicParameter.DocNumber(el);
                        string elName =
                            $"{PublicParameter.ProfileNamePrefix(el)}{PublicParameter.ProfileName(el)}";
                        double mass = GetElementMass(el, doc);
                        massOne += mass;

                        if (PublicParameter.HiddenRebarType(el) == 0)
                        {
                            detailRebars.Add(new SteelSpreadChar
                            {
                                MaterialName = matName,
                                ProductMixNumber = productMixNumber,
                                CharName = elName,
                                Mass = mass * elementQuantity
                            });
                        }
                        else
                        {
                            detailProfiles.Add(new SteelSpreadChar
                            {
                                MaterialName = matName,
                                ProductMixNumber = productMixNumber,
                                CharName = elName,
                                Mass = mass * elementQuantity
                            });
                        }
                    }
                    
                    double massAll = Math.Round(massOne * elementQuantity, 2);

                    foreach (Element ins in insGroup)
                    {
                        PublicParameter.MarkText(ins, mark);
                    }

                    if (orderRowsInView != null && rowInViewCurrentNumber < rowInViewQuantity)
                    {
                        orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.First").Set(mark);
                        orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Second").Set(docNumber);
                        orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Third").Set(elementName);
                        orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Fourth").Set(elementQuantity.ToString());
                        orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Fifth").Set(massOne.ToString("F3", nfi));
                        orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Sixth").Set(massAll.ToString("F2", nfi));

                        pointX = ((LocationPoint) orderRowsInView[rowInViewCurrentNumber].Location).Point.X;
                        pointY = ((LocationPoint) orderRowsInView[rowInViewCurrentNumber].Location).Point.Y + step;
                    }
                    else
                    {
                        if (!scheduleBody.IsActive) scheduleBody.Activate();
                        
                        FamilyInstance row =
                            doc.Create.NewFamilyInstance(new XYZ(pointX, pointY, 0), scheduleBody, (ViewDrafting)scheduleView);

                        row.LookupParameter("Column.First").Set(mark);
                        row.LookupParameter("Column.Second").Set(docNumber);
                        row.LookupParameter("Column.Third").Set(elementName);
                        row.LookupParameter("Column.Fourth").Set(elementQuantity.ToString());
                        row.LookupParameter("Column.Fifth").Set(massOne.ToString("F3", nfi));
                        row.LookupParameter("Column.Sixth").Set(massAll.ToString("F2", nfi));

                        pointY += step;
                    }
                        
                    detailMarkNumber++;
                    rowInViewCurrentNumber++;
                }
                    
                //Create footer row
                if (orderRowsInView != null && rowInViewCurrentNumber < rowInViewQuantity)
                {
                    orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.First").Set("");
                    orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Second").Set("");
                    orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Third").Set("");
                    orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Fourth").Set("");
                    orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Fifth").Set("");
                    orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Sixth").Set("");

                    pointX = ((LocationPoint) orderRowsInView[rowInViewCurrentNumber].Location).Point.X;
                    pointY = ((LocationPoint) orderRowsInView[rowInViewCurrentNumber].Location).Point.Y + step;
                }
                else
                {
                    if (!scheduleBody.IsActive) scheduleBody.Activate();
                    
                    FamilyInstance row =
                        doc.Create.NewFamilyInstance(new XYZ(pointX, pointY, 0), scheduleBody, (ViewDrafting)scheduleView);

                    pointY += step;
                }
                    
                rowInViewCurrentNumber++;
            }


            //Material head row
            if (orderRowsInView != null && rowInViewCurrentNumber < rowInViewQuantity)
            {
                orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.First").Set("");
                orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Second").Set("");
                orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Third").Set("Матеріали");
                orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Fourth").Set("");
                orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Fifth").Set("");
                orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Sixth").Set("");
                orderRowsInView[rowInViewCurrentNumber].LookupParameter("Row.Number").Set(rowInViewCurrentNumber);

                pointX = ((LocationPoint) orderRowsInView[rowInViewCurrentNumber].Location).Point.X;
                pointY = ((LocationPoint) orderRowsInView[rowInViewCurrentNumber].Location).Point.Y + step;
            }
            else
            {
                if (!scheduleBody.IsActive) scheduleBody.Activate();
                        
                FamilyInstance row =
                    doc.Create.NewFamilyInstance(new XYZ(pointX, pointY, 0), scheduleBody, (ViewDrafting)scheduleView);

                row.LookupParameter("Column.Third").Set("Матеріали");
                row.LookupParameter("Row.Number").Set(rowInViewCurrentNumber);

                pointY += step;
            }

            rowInViewCurrentNumber++;
            
            //Material body rows
            List<Element> hostElementCollection = new List<Element>();
            if (hostElement.AssemblyInstanceId == null)
            {
                ICollection<ElementId> subElementIds =
                    ((AssemblyInstance) doc.GetElement(hostElement.AssemblyInstanceId)).GetMemberIds();
                hostElementCollection.AddRange(subElementIds.Select(subElementId => doc.GetElement(subElementId)));
            }
            else
            {
                hostElementCollection.Add(hostElement);
            }

            foreach (Element hostEl in hostElementCollection)
            {
                ICollection<ElementId> materialIds = hostEl.GetMaterialIds(false);
                if (materialIds.Count == 1)
                {
                    Element firstMaterial = doc.GetElement(materialIds.First());
                    string docName = PublicParameter.MaterialProductMixNumber(firstMaterial);
                    string elementName =
                        $"Бетон кл. {PublicParameter.MaterialName(firstMaterial)}, {PublicParameter.MaterialNameSufix1(firstMaterial)}, {PublicParameter.MaterialNameSufix2(firstMaterial)}";
                    string materialValue = UnitUtils
                        .ConvertFromInternalUnits(hostElement.GetMaterialVolume(materialIds.First()),
                            DisplayUnitType.DUT_CUBIC_METERS).ToString("F2", nfi);
                    
                    if (orderRowsInView != null && rowInViewCurrentNumber < rowInViewQuantity)
                    {
                        orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.First").Set("");
                        orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Second").Set(docName);
                        orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Third").Set(elementName);
                        orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Fourth").Set("м3");
                        orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Fifth").Set(materialValue);
                        orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Sixth").Set("");
                        orderRowsInView[rowInViewCurrentNumber].LookupParameter("Row.Number").Set(rowInViewCurrentNumber);

                        pointX = ((LocationPoint) orderRowsInView[rowInViewCurrentNumber].Location).Point.X;
                        pointY = ((LocationPoint) orderRowsInView[rowInViewCurrentNumber].Location).Point.Y + step;
                    }
                    else
                    {
                        if (!scheduleBody.IsActive) scheduleBody.Activate();
                        
                        FamilyInstance row =
                            doc.Create.NewFamilyInstance(new XYZ(pointX, pointY, 0), scheduleBody, (ViewDrafting)scheduleView);

                        row.LookupParameter("Column.First").Set("");
                        row.LookupParameter("Column.Second").Set(docName);
                        row.LookupParameter("Column.Third").Set(elementName);
                        row.LookupParameter("Column.Fourth").Set("м3");
                        row.LookupParameter("Column.Fifth").Set(materialValue);
                        row.LookupParameter("Column.Sixth").Set("");
                        row.LookupParameter("Row.Number").Set(rowInViewCurrentNumber);

                        pointY += step;
                    }
                    
                    detailMarkNumber++;
                    rowInViewCurrentNumber++;
                }
                else
                {
                    IEnumerable<IGrouping<string, Element>> groupByMaterial = from materialId in materialIds
                        // ReSharper disable once ImplicitlyCapturedClosure
                        let el = doc.GetElement(materialId)
                        let name =
                            $"Бетон кл. {PublicParameter.MaterialName(el)}, {PublicParameter.MaterialNameSufix1(el)}, {PublicParameter.MaterialNameSufix2(el)}"
                        orderby name
                        group el by name;

                    foreach (IGrouping<string, Element> matGroup in groupByMaterial)
                    {
                        string docName = PublicParameter.MaterialProductMixNumber(matGroup.First());
                        double materialValue = matGroup.Sum(mat =>
                            UnitUtils.ConvertFromInternalUnits(hostEl.GetMaterialVolume(mat.Id),
                                DisplayUnitType.DUT_CUBIC_METERS));

                        if (orderRowsInView != null && rowInViewCurrentNumber < rowInViewQuantity)
                        {
                            orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.First").Set("");
                            orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Second").Set(docName);
                            orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Third").Set(matGroup.Key);
                            orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Fourth").Set("м3");
                            orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Fifth").Set(materialValue);
                            orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Sixth").Set("");
                            orderRowsInView[rowInViewCurrentNumber].LookupParameter("Row.Number").Set(rowInViewCurrentNumber);
                        
                            pointX = ((LocationPoint) orderRowsInView[rowInViewCurrentNumber].Location).Point.X;
                            pointY = ((LocationPoint) orderRowsInView[rowInViewCurrentNumber].Location).Point.Y + step;
                        }
                        else
                        {
                            if (!scheduleBody.IsActive) scheduleBody.Activate();
                                                
                            FamilyInstance row =
                                doc.Create.NewFamilyInstance(new XYZ(pointX, pointY, 0), scheduleBody, (ViewDrafting)scheduleView);
                        
                            row.LookupParameter("Column.First").Set("");
                            row.LookupParameter("Column.Second").Set(docName);
                            row.LookupParameter("Column.Third").Set(matGroup.Key);
                            row.LookupParameter("Column.Fourth").Set("м3");
                            row.LookupParameter("Column.Fifth").Set(materialValue);
                            row.LookupParameter("Column.Sixth").Set("");
                            row.LookupParameter("Row.Number").Set(rowInViewCurrentNumber);
                        
                            pointY += step;
                        }
                        
                        detailMarkNumber++;
                        rowInViewCurrentNumber++;
                    }
                }
                
                detailMarkNumber++;
                rowInViewCurrentNumber++;
            }
            
            //Material footer row
            if (orderRowsInView != null && rowInViewCurrentNumber < rowInViewQuantity)
            {
                orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.First").Set("");
                orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Second").Set("");
                orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Third").Set("");
                orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Fourth").Set("");
                orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Fifth").Set("");
                orderRowsInView[rowInViewCurrentNumber].LookupParameter("Column.Sixth").Set("");
                orderRowsInView[rowInViewCurrentNumber].LookupParameter("Row.Number").Set(rowInViewCurrentNumber);

                // ReSharper disable once RedundantAssignment
                pointX = ((LocationPoint) orderRowsInView[rowInViewCurrentNumber].Location).Point.X;
                // ReSharper disable once RedundantAssignment
                pointY = ((LocationPoint) orderRowsInView[rowInViewCurrentNumber].Location).Point.Y + step;
            }
            else
            {
                if (!scheduleBody.IsActive) scheduleBody.Activate();
                    
                FamilyInstance row =
                    doc.Create.NewFamilyInstance(new XYZ(pointX, pointY, 0), scheduleBody, (ViewDrafting)scheduleView);

                row.LookupParameter("Row.Number").Set(rowInViewCurrentNumber);

                // ReSharper disable once RedundantAssignment
                pointY += step;
            }
                    
            // ReSharper disable once RedundantAssignment
            rowInViewCurrentNumber++;

            if (orderRowsInView != null && rowInViewCurrentNumber < rowInViewQuantity)
            {
                for (int i = rowInViewCurrentNumber; i < rowInViewQuantity; i++)
                {
                    doc.Delete(orderRowsInView[i].Id);
                }
            }

            tr.Commit();

            #endregion

            #region CreateDetail

            //Order rebar
            IEnumerable<IGrouping<string, Element>> rebarForDetailSchedule = from rb in rebarsInPcs
                let rbForm = PublicParameter.RebarForm(rb)
                let rbLength = string.Join(" ",GetRebarDimensions(rb))
                where rbForm != "00" && rbForm != "01"
                orderby int.Parse(Regex.Replace(PublicParameter.MarkText(rb), @"[^0-9]", ""))
                group rb by rbLength;

            IEnumerable<IGrouping<string, Element>> forDetailSchedule = rebarForDetailSchedule as IGrouping<string, Element>[] ?? rebarForDetailSchedule.ToArray();
            if (forDetailSchedule.Any())
            {
                //Parameters

                Element detailScheduleView = new FilteredElementCollector(doc).OfCategory(BuiltInCategory.OST_Views)
                    .OfClass(typeof(ViewDrafting)).FirstOrDefault(view => IsDetailByOneElementView(view, hostMark));

                Element[] detailRows = null;
                int detailRowsQuantity = 0;
                int detailRowCurrentNumber = 0;

                pointX = 0;
                pointY = 0;
                step = UnitUtils.ConvertToInternalUnits(-30, DisplayUnitType.DUT_MILLIMETERS);

                Regex isDetailScheduleHead =
                    new Regex(@"^SRB_OTeam_ВідомістьДеталей-2D_Форма7_РядокЗаголовку_ДСТУ Б А.2.4-7-2009", RegexOptions.Compiled);
                Regex isDetailScheduleBody =
                    new Regex(@"^SRB_OTeam_ВідомістьДеталей-2D_Форма7_РядокДаних_АрматурнаФорма_ДСТУ Б А.2.4-7-2009", RegexOptions.Compiled);

                //Get schedule rows family
                if (!(doc.GetElement(((Family) allDetailItem.FirstOrDefault(el => isDetailScheduleHead.IsMatch(el.Name)))
                    ?.GetFamilySymbolIds().First()) is FamilySymbol detailScheduleHeadRow))
                {
                    MessageBox.Show(
                        "У моделі не знайдено сімейства з назвою 'SRB_OTeam_ВідомістьДеталей-2D_Форма7_РядокЗаголовку_ДСТУ Б А.2.4-7-2009'");
                    return Result.Failed;
                }


                ISet<ElementId> detailRowFamily =
                        ((Family) allDetailItem.FirstOrDefault(el => isDetailScheduleBody.IsMatch(el.Name)))?.GetFamilySymbolIds();
                if (detailRowFamily == null)
                {
                    MessageBox.Show(
                        "SRB_OTeam_ВідомістьДеталей-2D_Форма7_РядокДаних_АрматурнаФорма_ДСТУ Б А.2.4-7-2009'");
                    return Result.Failed;
                }

                if (detailScheduleView == null)
                {
                    ElementId draftingViewTypeId = new FilteredElementCollector(doc).OfClass(typeof(ViewFamilyType))
                        .Cast<ViewFamilyType>().First(vft => vft.ViewFamily == ViewFamily.Drafting).Id;

                    tr.Start("createDetailView");

                    detailScheduleView = ViewDrafting.Create(doc, draftingViewTypeId);
                    detailScheduleView.Name = $"Detail_OneElement_{hostMark}";
                    PublicParameter.ViewType(detailScheduleView, "99_Специфікація");
                    PublicParameter.ViewSubType(detailScheduleView, hostMark);
                    PublicParameter.ViewTemplateName(detailScheduleView, "OneElement");
                    detailScheduleView.get_Parameter(BuiltInParameter.VIEW_SCALE_PULLDOWN_METRIC).Set(1);
                    detailScheduleView.get_Parameter(BuiltInParameter.VIEW_DETAIL_LEVEL).Set(3);

                    //Create header
                    if (!detailScheduleHeadRow.IsActive) detailScheduleHeadRow.Activate();

                    doc.Create.NewFamilyInstance(new XYZ(0, 0, 0), detailScheduleHeadRow,
                        (ViewDrafting) detailScheduleView);

                    tr.Commit();
                }
                else
                {
                    FilteredElementCollector dataRowsInView =
                        new FilteredElementCollector(doc, detailScheduleView.Id).OfCategory(BuiltInCategory
                            .OST_DetailComponents);

                    detailRows = (from row in dataRowsInView
                        where row.get_Parameter(BuiltInParameter.ELEM_FAMILY_PARAM).AsValueString() ==
                              "SRB_OTeam_ВідомістьДеталей-2D_Форма7_РядокДаних_АрматурнаФорма_ДСТУ Б А.2.4-7-2009"
                        orderby ((LocationPoint) row.Location).Point.X * 1000, ((LocationPoint) row.Location).Point.Y *
                                                                               -1000
                        select row).ToArray();

                    detailRowsQuantity = detailRows.Length;
                }

                {
                    tr.Start("createDetailScheduleBody");

                    foreach (IGrouping<string, Element> rbGroup in forDetailSchedule)
                    {
                        string marks = string.Join(",\n", (rbGroup.Select(PublicParameter.MarkText).Distinct()));
                        string form = PublicParameter.RebarForm(rbGroup.First());
                        int[] dimensions = GetRebarDimensions(rbGroup.First());
                        ElementId detailFamilyType =
                            detailRowFamily.FirstOrDefault(el => doc.GetElement(el).Name == form);

                        if (detailRows == null || detailRowCurrentNumber >= detailRowsQuantity)
                        {
                            FamilySymbol detailFamilyRow = doc.GetElement(detailFamilyType) as FamilySymbol;
                            if (!detailFamilyRow.IsActive) detailFamilyRow.Activate();

                            FamilyInstance row = doc.Create.NewFamilyInstance(new XYZ(pointX, pointY, 0),
                                detailFamilyRow, (ViewDrafting) detailScheduleView);

                            row.LookupParameter("Column.First").Set(marks);
                            row.LookupParameter("A").Set(dimensions[0].ToString());
                            row.LookupParameter("B").Set(dimensions[1].ToString());
                            row.LookupParameter("C").Set(dimensions[2].ToString());
                            row.LookupParameter("D").Set(dimensions[3].ToString());
                            row.LookupParameter("E").Set(dimensions[4].ToString());
                            row.LookupParameter("F").Set(dimensions[5].ToString());
                            row.LookupParameter("R").Set(dimensions[11].ToString());

                            pointY += step;
                        }
                        else
                        {
                            detailRows[detailRowCurrentNumber].ChangeTypeId(detailFamilyType);
                            
                            detailRows[detailRowCurrentNumber].LookupParameter("Column.First").Set(marks);
                            detailRows[detailRowCurrentNumber].LookupParameter("A").Set(dimensions[0]);
                            detailRows[detailRowCurrentNumber].LookupParameter("B").Set(dimensions[1]);
                            detailRows[detailRowCurrentNumber].LookupParameter("C").Set(dimensions[2]);
                            detailRows[detailRowCurrentNumber].LookupParameter("D").Set(dimensions[3]);
                            detailRows[detailRowCurrentNumber].LookupParameter("E").Set(dimensions[4]);
                            detailRows[detailRowCurrentNumber].LookupParameter("F").Set(dimensions[5]);
                            detailRows[detailRowCurrentNumber].LookupParameter("R").Set(dimensions[11]);

                            pointX = ((LocationPoint) detailRows[detailRowCurrentNumber].Location).Point.X;
                            pointY = ((LocationPoint) detailRows[detailRowCurrentNumber].Location).Point.Y + step;
                            detailRowCurrentNumber++;
                        }
                    }

                    while (detailRowCurrentNumber < detailRowsQuantity)
                    {
                        doc.Delete(detailRows[detailRowCurrentNumber].Id);
                        detailRowCurrentNumber++;
                    }
                    
                    tr.Commit();
                }
            }

            #endregion

            #region CreateSpredSteel

            Element viewForSteelSpreadOne = new FilteredElementCollector(doc).OfCategory(BuiltInCategory.OST_Views)
                .OfClass(typeof(ViewDrafting)).FirstOrDefault(view => IsSteelSpreadByOneElementView(view, hostMark));
                
            Regex isSteelSpreadCell =
                new Regex(@"^GEN_OTeam_Специфікація-2D_Чарунок", RegexOptions.Compiled);
            Regex isSteelSpreadHeader =
                new Regex(@"^GEN_OTeam_Специфікація-2D_Назва", RegexOptions.Compiled);
                
            //Get schedule rows family
            if (!(doc.GetElement(((Family) allDetailItem.FirstOrDefault(el => isSteelSpreadCell.IsMatch(el.Name)))
                ?.GetFamilySymbolIds().First()) is FamilySymbol steelSpreadCell))
            {
                MessageBox.Show(
                    "У моделі не знайдено сімейства з назвою 'GEN_OTeam_Специфікація-2D_Чарунок'");
                return Result.Failed;
            }
            if (!(doc.GetElement(((Family) allDetailItem.FirstOrDefault(el => isSteelSpreadHeader.IsMatch(el.Name)))
                ?.GetFamilySymbolIds().First()) is FamilySymbol steelSpreadHeader))
            {
                MessageBox.Show(
                    "У моделі не знайдено сімейства з назвою 'GEN_OTeam_Специфікація-2D_Назва'");
                return Result.Failed;
            }

            if (rebarList == null && detailRebars == null && detailProfiles == null) return Result.Succeeded;
            
            if (viewForSteelSpreadOne == null)
            {
                ElementId draftingViewTypeId = new FilteredElementCollector(doc).OfClass(typeof(ViewFamilyType))
                    .Cast<ViewFamilyType>().First(vft => vft.ViewFamily == ViewFamily.Drafting).Id;

                tr.Start("SteelSpreadView");

                viewForSteelSpreadOne = ViewDrafting.Create(doc, draftingViewTypeId);
                viewForSteelSpreadOne.Name = $"SteelSpread_OneElement_{hostMark}";
                PublicParameter.ViewType(viewForSteelSpreadOne, "99_Специфікація");
                PublicParameter.ViewSubType(viewForSteelSpreadOne, hostMark);
                PublicParameter.ViewTemplateName(viewForSteelSpreadOne, "OneElement");
                viewForSteelSpreadOne.get_Parameter(BuiltInParameter.VIEW_SCALE_PULLDOWN_METRIC).Set(1);
                viewForSteelSpreadOne.get_Parameter(BuiltInParameter.VIEW_DETAIL_LEVEL).Set(3);

                tr.Commit();
            }
            else
            {
                FilteredElementCollector dataRowsInView =
                    new FilteredElementCollector(doc, viewForSteelSpreadOne.Id).OfCategory(BuiltInCategory
                        .OST_DetailComponents);

                tr.Start("deleteOldRow");

                foreach (Element symbol in dataRowsInView)
                {
                    doc.Delete(symbol.Id);
                }
                    
                tr.Commit();
            }
            
            //Coordinate
            double rowHeight = UnitUtils.ConvertToInternalUnits(8, DisplayUnitType.DUT_MILLIMETERS);
            double rowWidth = UnitUtils.ConvertToInternalUnits(15, DisplayUnitType.DUT_MILLIMETERS);
            double allMassWidth = UnitUtils.ConvertToInternalUnits(20, DisplayUnitType.DUT_MILLIMETERS);
                
            double[] rowPoint = {0, -rowHeight, 0};
            double[] profilePoint = {0, 0, 0};
            double[] productPoint = {0, rowHeight, 0};
            double[] materialPoint = {0, rowHeight * 2, 0};
            double[] steelTypePoint = {0, rowHeight * 3, 0};
            double[] firstRowPoint = {0, rowHeight * 4, 0};

            const string allMassName = "Всього";

            double allRebarMass = 0;
            double allRebarDetailMass = 0;
            double allDetailMass = 0;

            tr.Start("CreateHeadColumn");
            FamilyInstance markHead = doc.Create.NewFamilyInstance(
                new XYZ(profilePoint[0] - UnitUtils.ConvertToInternalUnits(40, DisplayUnitType.DUT_MILLIMETERS), profilePoint[1], profilePoint[2]), steelSpreadCell,
                (ViewDrafting) viewForSteelSpreadOne);
            markHead.LookupParameter("Dimensions.Height").Set(rowHeight * 5);
            markHead.LookupParameter("Dimesions.Width").Set(UnitUtils.ConvertToInternalUnits(40, DisplayUnitType.DUT_MILLIMETERS));
            markHead.LookupParameter("Content").Set("Марка елемента");
            
            FamilyInstance markValue = doc.Create.NewFamilyInstance(
                new XYZ(rowPoint[0] - UnitUtils.ConvertToInternalUnits(40, DisplayUnitType.DUT_MILLIMETERS), rowPoint[1], rowPoint[2]), steelSpreadCell,
                (ViewDrafting) viewForSteelSpreadOne);
            markValue.LookupParameter("Dimensions.Height").Set(rowHeight);
            markValue.LookupParameter("Dimesions.Width").Set(UnitUtils.ConvertToInternalUnits(40, DisplayUnitType.DUT_MILLIMETERS));
            markValue.LookupParameter("Content").Set(hostMark);
            tr.Commit();
            
            if (rebarList != null)
            {
                var orderedRebar = from rebarCell in rebarList
                    orderby rebarCell.CharName
                    group rebarCell by rebarCell.MaterialName
                    into groupByMaterial
                    from productGroup in (
                        from rebarCell in groupByMaterial
                        group rebarCell by rebarCell.ProductMixNumber
                        into groupByProduct
                        from elemGroup in (
                            from rebarCell in groupByProduct
                            group rebarCell by rebarCell.CharName
                        )
                        group elemGroup by groupByProduct.Key
                    )
                    group productGroup by groupByMaterial.Key;

                tr.Start("Create steel-spread schedule");
                
                if (!steelSpreadCell.IsActive) steelSpreadCell.Activate();

                foreach (IGrouping<string,IGrouping<string,IGrouping<string,SteelSpreadChar>>> byMaterial in orderedRebar)
                {
                    foreach (IGrouping<string,IGrouping<string,SteelSpreadChar>> byProduct in byMaterial)
                    {
                        double allMassByProductMix = 0;
                        foreach (IGrouping<string,SteelSpreadChar> byElement in byProduct)
                        {
                            double allMass = byElement.Sum(el => el.Mass);
                            allMassByProductMix += allMass;

                            FamilyInstance headerCell = doc.Create.NewFamilyInstance(
                                new XYZ(profilePoint[0], profilePoint[1], profilePoint[2]), steelSpreadCell,
                                (ViewDrafting) viewForSteelSpreadOne);
                            headerCell.LookupParameter("Dimensions.Height").Set(rowHeight);
                            headerCell.LookupParameter("Dimesions.Width").Set(rowWidth);
                            headerCell.LookupParameter("Content").Set(byElement.Key);
                            profilePoint[0] += rowWidth;

                            FamilyInstance contentCell = doc.Create.NewFamilyInstance(
                                new XYZ(rowPoint[0], rowPoint[1], rowPoint[2]), steelSpreadCell,
                                (ViewDrafting) viewForSteelSpreadOne);
                            contentCell.LookupParameter("Dimensions.Height").Set(rowHeight);
                            contentCell.LookupParameter("Dimesions.Width").Set(rowWidth);
                            contentCell.LookupParameter("Content").Set(allMass.ToString("F2", nfi));
                            rowPoint[0] += rowWidth;
                        }
                        
                        //allMassByProductMix
                        FamilyInstance allMassByProductMixHead = doc.Create.NewFamilyInstance(
                            new XYZ(profilePoint[0], profilePoint[1], profilePoint[2]), steelSpreadCell,
                            (ViewDrafting) viewForSteelSpreadOne);
                        allMassByProductMixHead.LookupParameter("Dimensions.Height").Set(rowHeight);
                        allMassByProductMixHead.LookupParameter("Dimesions.Width").Set(allMassWidth);
                        allMassByProductMixHead.LookupParameter("Content").Set(allMassName);
                        profilePoint[0] += allMassWidth;
                        
                        FamilyInstance allMassByProductMixValue = doc.Create.NewFamilyInstance(
                            new XYZ(rowPoint[0], rowPoint[1], rowPoint[2]), steelSpreadCell,
                            (ViewDrafting) viewForSteelSpreadOne);
                        allMassByProductMixValue.LookupParameter("Dimensions.Height").Set(rowHeight);
                        allMassByProductMixValue.LookupParameter("Dimesions.Width").Set(allMassWidth);
                        allMassByProductMixValue.LookupParameter("Content").Set(allMassByProductMix.ToString("F2", nfi));
                        rowPoint[0] += allMassWidth;
                        
                        //ProductNameRow
                        double productCellWidth = profilePoint[0] - productPoint[0];
                        
                        FamilyInstance profileNameHeader = doc.Create.NewFamilyInstance(
                            new XYZ(productPoint[0], productPoint[1], productPoint[2]), steelSpreadCell,
                            (ViewDrafting) viewForSteelSpreadOne);
                        profileNameHeader.LookupParameter("Dimensions.Height").Set(rowHeight);
                        profileNameHeader.LookupParameter("Dimesions.Width").Set(productCellWidth);
                        profileNameHeader.LookupParameter("Content").Set(byProduct.Key);
                        productPoint[0] += productCellWidth;

                        allRebarMass += allMassByProductMix;
                    }
                    
                    //materialNameCell
                    double materialNameCellWidth = rowPoint[0] - materialPoint[0];
                    
                    FamilyInstance materialNameCell = doc.Create.NewFamilyInstance(
                        new XYZ(materialPoint[0], materialPoint[1], materialPoint[2]), steelSpreadCell,
                        (ViewDrafting) viewForSteelSpreadOne);
                    materialNameCell.LookupParameter("Dimensions.Height").Set(rowHeight);
                    materialNameCell.LookupParameter("Dimesions.Width").Set(materialNameCellWidth);
                    materialNameCell.LookupParameter("Content").Set(byMaterial.Key);
                    materialPoint[0] += materialNameCellWidth;
                }
                
                //rebarClassCell
                double rebarClassCellWidth = materialPoint[0] - steelTypePoint[0];
                
                FamilyInstance rebarClassCellName = doc.Create.NewFamilyInstance(
                    new XYZ(steelTypePoint[0], steelTypePoint[1], steelTypePoint[2]), steelSpreadCell,
                    (ViewDrafting) viewForSteelSpreadOne);
                rebarClassCellName.LookupParameter("Dimensions.Height").Set(rowHeight);
                rebarClassCellName.LookupParameter("Dimesions.Width").Set(rebarClassCellWidth);
                rebarClassCellName.LookupParameter("Content").Set("Арматура класу");
                steelTypePoint[0] += rebarClassCellWidth;
                
                FamilyInstance rebarClassMassValue = doc.Create.NewFamilyInstance(
                    new XYZ(rowPoint[0], rowPoint[1], rowPoint[2]), steelSpreadCell,
                    (ViewDrafting) viewForSteelSpreadOne);
                rebarClassMassValue.LookupParameter("Dimensions.Height").Set(rowHeight);
                rebarClassMassValue.LookupParameter("Dimesions.Width").Set(allMassWidth);
                rebarClassMassValue.LookupParameter("Content").Set(allRebarMass.ToString("F2", nfi));
                rowPoint[0] += allMassWidth;
                
                FamilyInstance rebarClassMassHead = doc.Create.NewFamilyInstance(
                    new XYZ(profilePoint[0], profilePoint[1], profilePoint[2]), steelSpreadCell,
                    (ViewDrafting) viewForSteelSpreadOne);
                rebarClassMassHead.LookupParameter("Dimensions.Height").Set(rowHeight * 4);
                rebarClassMassHead.LookupParameter("Dimesions.Width").Set(allMassWidth);
                rebarClassMassHead.LookupParameter("Content").Set(allMassName);
                profilePoint[0] += allMassWidth;
                productPoint[0] += allMassWidth;
                materialPoint[0] += allMassWidth;
                steelTypePoint[0] += allMassWidth;
                
                //rebarBlockCell
                double rebarBlockCellWidth = rowPoint[0] - firstRowPoint[0];
                
                FamilyInstance rebarBlockCell = doc.Create.NewFamilyInstance(
                    new XYZ(firstRowPoint[0], firstRowPoint[1], firstRowPoint[2]), steelSpreadCell,
                    (ViewDrafting) viewForSteelSpreadOne);
                rebarBlockCell.LookupParameter("Dimensions.Height").Set(rowHeight);
                rebarBlockCell.LookupParameter("Dimesions.Width").Set(rebarBlockCellWidth);
                rebarBlockCell.LookupParameter("Content").Set("Вироби арматурні");
                firstRowPoint[0] += rebarBlockCellWidth;
                
                tr.Commit();
            }

            if (detailRebars == null && detailProfiles == null) return Result.Succeeded;
            
            if (detailRebars != null)
            {
                IEnumerable<IGrouping<string, IGrouping<string, IGrouping<string, SteelSpreadChar>>>> orderedRebar = from rebarCell in detailRebars
                    orderby rebarCell.CharName
                    group rebarCell by rebarCell.MaterialName
                    into groupByMaterial
                    from productGroup in (
                        from rebarCell in groupByMaterial
                        group rebarCell by rebarCell.ProductMixNumber
                        into groupByProduct
                        from elemGroup in (
                            from rebarCell in groupByProduct
                            group rebarCell by rebarCell.CharName
                        )
                        group elemGroup by groupByProduct.Key
                    )
                    group productGroup by groupByMaterial.Key;

                tr.Start("CreateDetailPart");
                
                foreach (IGrouping<string,IGrouping<string,IGrouping<string,SteelSpreadChar>>> byMaterial in orderedRebar)
                {
                    foreach (IGrouping<string,IGrouping<string,SteelSpreadChar>> byProductMix in byMaterial)
                    {
                        double massAllProfiles = 0;
                        foreach (IGrouping<string,SteelSpreadChar> byProfile in byProductMix)
                        {
                            double massAllProfile = byProfile.Sum(el => el.Mass);
                            massAllProfiles += massAllProfile;

                            FamilyInstance profileCellHead = doc.Create.NewFamilyInstance(
                                new XYZ(profilePoint[0], profilePoint[1], profilePoint[2]), steelSpreadCell,
                                (ViewDrafting) viewForSteelSpreadOne);
                            profileCellHead.LookupParameter("Dimensions.Height").Set(rowHeight);
                            profileCellHead.LookupParameter("Dimesions.Width").Set(rowWidth);
                            profileCellHead.LookupParameter("Content").Set(byProfile.Key);
                            profilePoint[0] += rowWidth;
                            
                            FamilyInstance profileCellValue = doc.Create.NewFamilyInstance(
                                new XYZ(rowPoint[0], rowPoint[1], rowPoint[2]), steelSpreadCell,
                                (ViewDrafting) viewForSteelSpreadOne);
                            profileCellValue.LookupParameter("Dimensions.Height").Set(rowHeight);
                            profileCellValue.LookupParameter("Dimesions.Width").Set(rowWidth);
                            profileCellValue.LookupParameter("Content").Set(massAllProfile.ToString("F2", nfi));
                            rowPoint[0] += rowWidth;
                        }
                        
                        //massAllProfileCell
                        FamilyInstance massAllProfileCellHead = doc.Create.NewFamilyInstance(
                            new XYZ(profilePoint[0], profilePoint[1], profilePoint[2]), steelSpreadCell,
                            (ViewDrafting) viewForSteelSpreadOne);
                        massAllProfileCellHead.LookupParameter("Dimensions.Height").Set(rowHeight);
                        massAllProfileCellHead.LookupParameter("Dimesions.Width").Set(rowWidth);
                        massAllProfileCellHead.LookupParameter("Content").Set(allMassName);
                        profilePoint[0] += rowWidth;
                        
                        FamilyInstance massAllProfileCellValue = doc.Create.NewFamilyInstance(
                            new XYZ(rowPoint[0], rowPoint[1], rowPoint[2]), steelSpreadCell,
                            (ViewDrafting) viewForSteelSpreadOne);
                        massAllProfileCellValue.LookupParameter("Dimensions.Height").Set(rowHeight);
                        massAllProfileCellValue.LookupParameter("Dimesions.Width").Set(rowWidth);
                        massAllProfileCellValue.LookupParameter("Content").Set(massAllProfiles.ToString("F2", nfi));
                        rowPoint[0] += rowWidth;
                        
                        //productMixNameCell
                        double productMixNameCellWidth = profilePoint[0] - productPoint[0];
                        
                        FamilyInstance productMixNameCell = doc.Create.NewFamilyInstance(
                            new XYZ(productPoint[0], productPoint[1], productPoint[2]), steelSpreadCell,
                            (ViewDrafting) viewForSteelSpreadOne);
                        productMixNameCell.LookupParameter("Dimensions.Height").Set(rowHeight);
                        productMixNameCell.LookupParameter("Dimesions.Width").Set(productMixNameCellWidth);
                        productMixNameCell.LookupParameter("Content").Set(byProductMix.Key);
                        productPoint[0] += productMixNameCellWidth;

                        allRebarDetailMass += massAllProfiles;
                    }

                    //materialNameCell
                    double materialNameCellWidth = productPoint[0] - materialPoint[0];
                    
                    FamilyInstance materialNameCell = doc.Create.NewFamilyInstance(
                        new XYZ(materialPoint[0], materialPoint[1], materialPoint[2]), steelSpreadCell,
                        (ViewDrafting) viewForSteelSpreadOne);
                    materialNameCell.LookupParameter("Dimensions.Height").Set(rowHeight);
                    materialNameCell.LookupParameter("Dimesions.Width").Set(materialNameCellWidth);
                    materialNameCell.LookupParameter("Content").Set(byMaterial.Key);
                    materialPoint[0] += materialNameCellWidth;
                }
                
                //rebarClassAllMass
                FamilyInstance rebarClassAllMassHead = doc.Create.NewFamilyInstance(
                    new XYZ(profilePoint[0], profilePoint[1], profilePoint[2]), steelSpreadCell,
                    (ViewDrafting) viewForSteelSpreadOne);
                rebarClassAllMassHead.LookupParameter("Dimensions.Height").Set(rowHeight * 3);
                rebarClassAllMassHead.LookupParameter("Dimesions.Width").Set(allMassWidth);
                rebarClassAllMassHead.LookupParameter("Content").Set(allMassName);
                profilePoint[0] += allMassWidth;
                productPoint[0] += allMassWidth;
                materialPoint[0] += allMassWidth;
                
                FamilyInstance rebarClassAllMassValue = doc.Create.NewFamilyInstance(
                    new XYZ(rowPoint[0], rowPoint[1], rowPoint[2]), steelSpreadCell,
                    (ViewDrafting) viewForSteelSpreadOne);
                rebarClassAllMassValue.LookupParameter("Dimensions.Height").Set(rowHeight);
                rebarClassAllMassValue.LookupParameter("Dimesions.Width").Set(allMassWidth);
                rebarClassAllMassValue.LookupParameter("Content").Set(allRebarDetailMass.ToString("F2", nfi));
                rowPoint[0] += allMassWidth;
                
                //rebarClassNameCell
                double rebarClassNameCellWidth = materialPoint[0] - steelTypePoint[0];
                
                FamilyInstance rebarClassNameCell = doc.Create.NewFamilyInstance(
                    new XYZ(steelTypePoint[0], steelTypePoint[1], steelTypePoint[2]), steelSpreadCell,
                    (ViewDrafting) viewForSteelSpreadOne);
                rebarClassNameCell.LookupParameter("Dimensions.Height").Set(rowHeight);
                rebarClassNameCell.LookupParameter("Dimesions.Width").Set(rebarClassNameCellWidth);
                rebarClassNameCell.LookupParameter("Content").Set("Арматура класу");
                steelTypePoint[0] += rebarClassNameCellWidth;
                
                tr.Commit();
            }
            
            if (detailProfiles.Any())
            {
                IEnumerable<IGrouping<string, IGrouping<string, IGrouping<string, SteelSpreadChar>>>> orderedRebar = from rebarCell in detailProfiles
                    orderby rebarCell.CharName
                    group rebarCell by rebarCell.MaterialName
                    into groupByMaterial
                    from productGroup in (
                        from rebarCell in groupByMaterial
                        group rebarCell by rebarCell.ProductMixNumber
                        into groupByProduct
                        from elemGroup in (
                            from rebarCell in groupByProduct
                            group rebarCell by rebarCell.CharName
                        )
                        group elemGroup by groupByProduct.Key
                    )
                    group productGroup by groupByMaterial.Key;

                tr.Start("secondPart");

                int materialCounter = 0;
                
                foreach (IGrouping<string,IGrouping<string,IGrouping<string,SteelSpreadChar>>> byMaterial in orderedRebar)
                {
                    int productMixInMaterial = 0;
                    double allElementMassInMaterial = 0;
                    
                    foreach (IGrouping<string,IGrouping<string,SteelSpreadChar>> byProductMix in byMaterial)
                    {
                        double allMassFromProductMix = 0;
                        foreach (IGrouping<string,SteelSpreadChar> byProfileName in byProductMix)
                        {
                            double profileMass = byProfileName.Sum(el => el.Mass);
                            allMassFromProductMix += profileMass;

                            FamilyInstance cellHead = doc.Create.NewFamilyInstance(
                                new XYZ(profilePoint[0], profilePoint[1], profilePoint[2]), steelSpreadCell,
                                (ViewDrafting) viewForSteelSpreadOne);
                            cellHead.LookupParameter("Dimensions.Height").Set(rowHeight);
                            cellHead.LookupParameter("Dimesions.Width").Set(rowWidth);
                            cellHead.LookupParameter("Content").Set(byProfileName.Key);
                            profilePoint[0] += rowWidth;
                            
                            FamilyInstance cellValue = doc.Create.NewFamilyInstance(
                                new XYZ(rowPoint[0], rowPoint[1], rowPoint[2]), steelSpreadCell,
                                (ViewDrafting) viewForSteelSpreadOne);
                            cellValue.LookupParameter("Dimensions.Height").Set(rowHeight);
                            cellValue.LookupParameter("Dimesions.Width").Set(rowWidth);
                            cellValue.LookupParameter("Content").Set(profileMass.ToString("F2", nfi));
                            rowPoint[0] += rowWidth;
                        }

                        allElementMassInMaterial += allMassFromProductMix;
                        productMixInMaterial++;
                        //productMixCell
                        FamilyInstance productMixCellHead = doc.Create.NewFamilyInstance(
                            new XYZ(profilePoint[0], profilePoint[1], profilePoint[2]), steelSpreadCell,
                            (ViewDrafting) viewForSteelSpreadOne);
                        productMixCellHead.LookupParameter("Dimensions.Height").Set(rowHeight);
                        productMixCellHead.LookupParameter("Dimesions.Width").Set(allMassWidth);
                        productMixCellHead.LookupParameter("Content").Set(allMassName);
                        profilePoint[0] += allMassWidth;
                            
                        FamilyInstance productMixCellValue = doc.Create.NewFamilyInstance(
                            new XYZ(rowPoint[0], rowPoint[1], rowPoint[2]), steelSpreadCell,
                            (ViewDrafting) viewForSteelSpreadOne);
                        productMixCellValue.LookupParameter("Dimensions.Height").Set(rowHeight);
                        productMixCellValue.LookupParameter("Dimesions.Width").Set(allMassWidth);
                        productMixCellValue.LookupParameter("Content").Set(allMassFromProductMix.ToString("F2", nfi));
                        rowPoint[0] += allMassWidth;

                        double productMixNameCellWidth = profilePoint[0] - productPoint[0];
                        FamilyInstance productMixNameCell = doc.Create.NewFamilyInstance(
                            new XYZ(productPoint[0], productPoint[1], productPoint[2]), steelSpreadCell,
                            (ViewDrafting) viewForSteelSpreadOne);
                        productMixNameCell.LookupParameter("Dimensions.Height").Set(rowHeight);
                        productMixNameCell.LookupParameter("Dimesions.Width").Set(productMixNameCellWidth);
                        productMixNameCell.LookupParameter("Content").Set(byProductMix.Key);
                        productPoint[0] += productMixNameCellWidth;
                    }

                    //materialCell
                    if (productMixInMaterial > 1)
                    {
                        FamilyInstance materialCellMassHead = doc.Create.NewFamilyInstance(
                            new XYZ(profilePoint[0], profilePoint[1], profilePoint[2]), steelSpreadCell,
                            (ViewDrafting) viewForSteelSpreadOne);
                        materialCellMassHead.LookupParameter("Dimensions.Height").Set(rowHeight * 2);
                        materialCellMassHead.LookupParameter("Dimesions.Width").Set(allMassWidth);
                        materialCellMassHead.LookupParameter("Content").Set(allMassName);
                        profilePoint[0] += allMassWidth;
                        productPoint[0] += allMassWidth;
                            
                        FamilyInstance materialCellMassValue = doc.Create.NewFamilyInstance(
                            new XYZ(rowPoint[0], rowPoint[1], rowPoint[2]), steelSpreadCell,
                            (ViewDrafting) viewForSteelSpreadOne);
                        materialCellMassValue.LookupParameter("Dimensions.Height").Set(rowHeight);
                        materialCellMassValue.LookupParameter("Dimesions.Width").Set(allMassWidth);
                        materialCellMassValue.LookupParameter("Content").Set(allElementMassInMaterial.ToString("F2", nfi));
                        rowPoint[0] += allMassWidth;
                    }

                    double materialCellWidth = productPoint[0] - materialPoint[0];
                    FamilyInstance materialCell = doc.Create.NewFamilyInstance(
                        new XYZ(materialPoint[0], materialPoint[1], materialPoint[2]), steelSpreadCell,
                        (ViewDrafting) viewForSteelSpreadOne);
                    materialCell.LookupParameter("Dimensions.Height").Set(rowHeight);
                    materialCell.LookupParameter("Dimesions.Width").Set(materialCellWidth);
                    materialCell.LookupParameter("Content").Set(byMaterial.Key);
                    materialPoint[0] += materialCellWidth;

                    allDetailMass += allElementMassInMaterial;
                    materialCounter++;
                }
                
                //steelClass
                if (materialCounter > 1)
                {
                    FamilyInstance steelClassMassHead = doc.Create.NewFamilyInstance(
                        new XYZ(profilePoint[0], profilePoint[1], profilePoint[2]), steelSpreadCell,
                        (ViewDrafting) viewForSteelSpreadOne);
                    steelClassMassHead.LookupParameter("Dimensions.Height").Set(rowHeight * 3);
                    steelClassMassHead.LookupParameter("Dimesions.Width").Set(allMassWidth);
                    steelClassMassHead.LookupParameter("Content").Set(allMassName);
                    profilePoint[0] += allMassWidth;
                    productPoint[0] += allMassWidth;
                    materialPoint[0] += allMassWidth;
                            
                    FamilyInstance steelClassMassValue = doc.Create.NewFamilyInstance(
                        new XYZ(rowPoint[0], rowPoint[1], rowPoint[2]), steelSpreadCell,
                        (ViewDrafting) viewForSteelSpreadOne);
                    steelClassMassValue.LookupParameter("Dimensions.Height").Set(rowHeight);
                    steelClassMassValue.LookupParameter("Dimesions.Width").Set(allMassWidth);
                    steelClassMassValue.LookupParameter("Content").Set(allDetailMass.ToString("F2", nfi));
                    rowPoint[0] += allMassWidth;
                }

                double steelClassWidth = materialPoint[0] - steelTypePoint[0];
                FamilyInstance steelClass = doc.Create.NewFamilyInstance(
                    new XYZ(steelTypePoint[0], steelTypePoint[1], steelTypePoint[2]), steelSpreadCell,
                    (ViewDrafting) viewForSteelSpreadOne);
                steelClass.LookupParameter("Dimensions.Height").Set(rowHeight);
                steelClass.LookupParameter("Dimesions.Width").Set(steelClassWidth);
                steelClass.LookupParameter("Content").Set("Прокат марки");
                steelTypePoint[0] += steelClassWidth;
                
                //allDetailInSteelSpread
                FamilyInstance allDetailInSteelSpreadMassHead = doc.Create.NewFamilyInstance(
                    new XYZ(profilePoint[0], profilePoint[1], profilePoint[2]), steelSpreadCell,
                    (ViewDrafting) viewForSteelSpreadOne);
                allDetailInSteelSpreadMassHead.LookupParameter("Dimensions.Height").Set(rowHeight * 4);
                allDetailInSteelSpreadMassHead.LookupParameter("Dimesions.Width").Set(allMassWidth);
                allDetailInSteelSpreadMassHead.LookupParameter("Content").Set(allMassName);
                profilePoint[0] += allMassWidth;
                productPoint[0] += allMassWidth;
                materialPoint[0] += allMassWidth;
                steelTypePoint[0] += allMassWidth;
                            
                FamilyInstance allDetailInSteelSpreadMassValue = doc.Create.NewFamilyInstance(
                    new XYZ(rowPoint[0], rowPoint[1], rowPoint[2]), steelSpreadCell,
                    (ViewDrafting) viewForSteelSpreadOne);
                allDetailInSteelSpreadMassValue.LookupParameter("Dimensions.Height").Set(rowHeight);
                allDetailInSteelSpreadMassValue.LookupParameter("Dimesions.Width").Set(allMassWidth);
                allDetailInSteelSpreadMassValue.LookupParameter("Content").Set((allRebarDetailMass + allDetailMass).ToString("F2", nfi));
                rowPoint[0] += allMassWidth;

                tr.Commit();
            }

            tr.Start("lastColumn");
            
            double allDetailInSteelSpreadWidth = steelTypePoint[0] - firstRowPoint[0];
            FamilyInstance allDetailInSteelSpread = doc.Create.NewFamilyInstance(
                new XYZ(firstRowPoint[0], firstRowPoint[1], firstRowPoint[2]), steelSpreadCell,
                (ViewDrafting) viewForSteelSpreadOne);
            allDetailInSteelSpread.LookupParameter("Dimensions.Height").Set(rowHeight);
            allDetailInSteelSpread.LookupParameter("Dimesions.Width").Set(allDetailInSteelSpreadWidth);
            allDetailInSteelSpread.LookupParameter("Content").Set("Вироби закладні");
            firstRowPoint[0] += allDetailInSteelSpreadWidth;
            
            FamilyInstance lastColumnMassHead = doc.Create.NewFamilyInstance(
                new XYZ(profilePoint[0], profilePoint[1], profilePoint[2]), steelSpreadCell,
                (ViewDrafting) viewForSteelSpreadOne);
            lastColumnMassHead.LookupParameter("Dimensions.Height").Set(rowHeight * 5);
            lastColumnMassHead.LookupParameter("Dimesions.Width").Set(allMassWidth);
            lastColumnMassHead.LookupParameter("Content").Set(allMassName);
            double headerLength = firstRowPoint[0] + allMassWidth + UnitUtils.ConvertToInternalUnits(40, DisplayUnitType.DUT_MILLIMETERS);
                            
            FamilyInstance lastColumnMassValue = doc.Create.NewFamilyInstance(
                new XYZ(rowPoint[0], rowPoint[1], rowPoint[2]), steelSpreadCell,
                (ViewDrafting) viewForSteelSpreadOne);
            lastColumnMassValue.LookupParameter("Dimensions.Height").Set(rowHeight);
            lastColumnMassValue.LookupParameter("Dimesions.Width").Set(allMassWidth);
            lastColumnMassValue.LookupParameter("Content").Set((allRebarMass + allRebarDetailMass + allDetailMass).ToString("F2", nfi));
            
            FamilyInstance header = doc.Create.NewFamilyInstance(
                new XYZ(-UnitUtils.ConvertToInternalUnits(40, DisplayUnitType.DUT_MILLIMETERS), firstRowPoint[1] + rowHeight, rowPoint[2]), steelSpreadHeader,
                (ViewDrafting) viewForSteelSpreadOne);
            header.LookupParameter("Dimensions.Height").Set(rowHeight);
            header.LookupParameter("Dimesions.Width").Set(headerLength);
            header.LookupParameter("Content").Set("Відомість витрат сталі, кг");
            
            tr.Commit();

            #endregion

            return Result.Succeeded;
        }
        
        private static int GetProductMixKod(Element el, Document doc)
        {
            switch (el)
            {
                case RebarInSystem _:
                case Rebar _:
                    return PublicParameter.ProductMixKey(doc.GetElement(el.GetTypeId()));
                case FamilyInstance _:
                    return PublicParameter.ProductMixKey(el);
                default:
                    return 0;
            }
        }

        private static int GetElementKod(Element el, Document doc)
        {
            switch (el)
            {
                case RebarInSystem _:
                case Rebar _:
                    return PublicParameter.ElementKey(doc.GetElement(el.GetTypeId()));
                case FamilyInstance _:
                    return PublicParameter.ElementKey(el);
                default:
                    return 0;
            }
        }

        private static int GetRebarLengthWithScraft(Element el, Document doc)
        {
            int rebarLength = 0;
            int scraftLength = 0;
            int rebarQuantity = 0;
            
            switch (el)
            {
                case RebarInSystem _:
                case Rebar _:
                    Element elType = doc.GetElement(el.GetTypeId());
                    rebarLength = PublicParameter.RebarLength(el);
                    scraftLength = PublicParameter.RebarScraftLength(elType);
                    rebarQuantity = PublicParameter.RebarQuantity(el);
                    break;
                case FamilyInstance _:
                    rebarLength = PublicParameter.DimensionLength(el);
                    scraftLength = PublicParameter.RebarScraftLength(el);
                    rebarQuantity = PublicParameter.ElementQuantity(el);
                    break;
            }
            
            return rebarLength > 12000
                ? ((int) (Math.Ceiling(rebarLength / 12000.0) - 1) * scraftLength * 2 + rebarLength) *
                  rebarQuantity
                : (int)(Math.Round(rebarLength / 5.0) * 5);
        }
        private static int GetRebarLength(Element el)
        {
            switch (el)
            {
                case RebarInSystem _:
                case Rebar _:
                    return PublicParameter.RebarLength(el);
                case FamilyInstance _:
                    return (int)(Math.Round(PublicParameter.DimensionLength(el) / 5.0) * 5);
                default:
                    return 0;
            }
        }
        private static int[] GetRebarDimensions(Element el)
        {
            switch (el)
            {
                case RebarInSystem _:
                case Rebar _:
                {
                    return new[]
                    {
                        PublicParameter.RebarA(el),//0
                        PublicParameter.RebarB(el),//1
                        PublicParameter.RebarC(el),//2
                        PublicParameter.RebarD(el),//3
                        PublicParameter.RebarE(el),//4
                        PublicParameter.RebarF(el),//5
                        PublicParameter.RebarG(el),//6
                        PublicParameter.RebarH(el),//7
                        PublicParameter.RebarJ(el),//8
                        PublicParameter.RebarK(el),//9
                        PublicParameter.RebarO(el),//10
                        PublicParameter.RebarR(el)//11
                    };
                }
                case FamilyInstance _:
                    return new[]
                    {
                        (int) (Math.Ceiling(PublicParameter.DimensionsA(el) / 5.0) * 5),
                        (int) (Math.Ceiling(PublicParameter.DimensionsB(el) / 5.0) * 5),
                        (int) (Math.Ceiling(PublicParameter.DimensionsC(el) / 5.0) * 5),
                        (int) (Math.Ceiling(PublicParameter.DimensionsD(el) / 5.0) * 5),
                        (int) (Math.Ceiling(PublicParameter.DimensionsE(el) / 5.0) * 5),
                        (int) (Math.Ceiling(PublicParameter.DimensionsF(el) / 5.0) * 5),
                        (int) (Math.Ceiling(PublicParameter.DimensionsG(el) / 5.0) * 5),
                        (int) (Math.Ceiling(PublicParameter.DimensionsH(el) / 5.0) * 5),
                        (int) (Math.Ceiling(PublicParameter.DimensionsJ(el) / 5.0) * 5),
                        (int) (Math.Ceiling(PublicParameter.DimensionsK(el) / 5.0) * 5),
                        (int) (Math.Ceiling(PublicParameter.DimensionsO(el) / 5.0) * 5),
                        (int) (Math.Ceiling(PublicParameter.DimensionsR(el) / 5.0) * 5)
                    };
                default:
                    return new[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
            }
        }

        private static int GetRebarQuantity(Element el)
        {
            switch (el)
            {
                case RebarInSystem _:
                case Rebar _:
                    return PublicParameter.RebarQuantity(el);
                case FamilyInstance _:
                    return PublicParameter.ElementQuantity(el);
                default:
                    return 0;
            }
        }
        
        private static bool IsScheduleByOneElement(Element el, string hostMark)
        {
            if (PublicParameter.ViewType(el) != "99_Специфікація") return false;
            if (PublicParameter.ViewSubType(el) != hostMark) return false;
            return PublicParameter.ViewTemplateName(el) == "OneElement" && Regex.IsMatch(el.Name, @"^Schedules_OneElement_");
        }

        private static bool IsDetailByOneElementView(Element el, string hostMark)
        {
            if (PublicParameter.ViewType(el) != "99_Специфікація") return false;
            if (PublicParameter.ViewSubType(el) != hostMark) return false;
            return PublicParameter.ViewTemplateName(el) == "OneElement" && Regex.IsMatch(el.Name, @"^Detail_OneElement_");
        }
        
        private static bool IsSteelSpreadByOneElementView(Element el, string hostMark)
        {
            if (PublicParameter.ViewType(el) != "99_Специфікація") return false;
            if (PublicParameter.ViewSubType(el) != hostMark) return false;
            return PublicParameter.ViewTemplateName(el) == "OneElement" && Regex.IsMatch(el.Name, @"^SteelSpread_OneElement_");
        }

        private static double GetElementMass(Element el, Document doc)
        {
            double massElement = PublicParameter.ElementMass(el);
            return Math.Abs(massElement + 1) > 0.1
                ? Math.Round(massElement * PublicParameter.ElementQuantity(el), 3)
                : Math.Round((PublicParameter.DimensionLength(el) / 1000.0) * PublicParameter.MassPerUnitLength(el) * PublicParameter.ElementQuantity(el), 3);
        }
    }
    
    internal class SelectConcreteElement : ISelectionFilter, IDisposable
    {
        public bool AllowElement(Element elem)
        {
            return Regex.IsMatch(elem.get_Parameter(BuiltInParameter.ELEM_FAMILY_AND_TYPE_PARAM).AsValueString(),
                "SRC_");
        }

        public bool AllowReference(Reference reference, XYZ position)
        {
            return false;
        }

        void IDisposable.Dispose()
        {
            GC.SuppressFinalize(this);
        }

    }
}