﻿using System;
using System.Windows;

namespace OrslavTeam.Revit.Concrete.OTRebar.Schedules
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow  : IDisposable
    {
        public bool createSchedules = false;
        public MainWindow()
        {
            InitializeComponent();
        }
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            createSchedules = true;
            Close();
        }
    }
}