﻿using System;
using System.Linq;
using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Structure;
using OrslavTeam.Revit.Axillary.PublicInformation;

namespace OrslavTeam.Revit.Concrete.OTRebar.Schedules
{
    public static class Axillary
    {
        public static string GetMaterialName(Element el, string fullName, Document doc)
        {
            switch (el)
            {
                case RebarInSystem _:
                case Rebar _:
                    return fullName.Split(' ')[1];
                case FamilyInstance _:
                    ElementId matId = el.GetMaterialIds(false).FirstOrDefault();
                    return PublicParameter.MaterialName(doc.GetElement(matId));
                default:
                    return "";
            }
        }
    }

    public class SteelSpreadChar : IComparable<SteelSpreadChar>
    {
        public string MaterialName { get; set; }
        public string ProductMixNumber { get; set; }
        public string CharName { get; set; }
        public double Mass { get; set; }

        public string GroupKey => $"{MaterialName}-{ProductMixNumber}-{CharName}";

        public int CompareTo(SteelSpreadChar other)
        {
            if (ReferenceEquals(this, other)) return 0;
            if (ReferenceEquals(null, other)) return 1;
            int materialNameComparison = string.Compare(MaterialName, other.MaterialName, StringComparison.Ordinal);
            if (materialNameComparison != 0) return materialNameComparison;
            int productMixNumberComparison =
                string.Compare(ProductMixNumber, other.ProductMixNumber, StringComparison.Ordinal);
            return productMixNumberComparison != 0
                ? productMixNumberComparison
                : string.Compare(CharName, other.CharName, StringComparison.Ordinal);
        }
    }
}
