﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using Autodesk.Revit.UI.Selection;
using OrslavTeam.Revit.Axillary.PublicInformation;
using OrslavTeam.Revit.Concrete.Helper.Selection;

namespace OrslavTeam.Revit.Concrete.OTReabar.Worker.AddIns
{
    [Transaction(TransactionMode.Manual)]
    public class SetElementHost : IExternalCommand
    {
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            UIApplication uiApp = commandData.Application;
            Document doc = uiApp.ActiveUIDocument.Document;
            Selection sel = uiApp.ActiveUIDocument.Selection;
            ElementId viewId = doc.ActiveView.Id;
            var selectConcreteElement = new SelectingConcreteElement(doc);

            Reference pickedHost;
            try
            {
                pickedHost = sel.PickObject(ObjectType.Element, selectConcreteElement, "Оберіть основу для арматури");
            }
            catch (Exception)
            {
                return Result.Cancelled;
            }

            Element host = doc.GetElement(pickedHost);
            ElementId hostId = host.Id;

            string markPrefix = PublicParameter.MarkPrefix(host);
            string markFirst = PublicParameter.MarkFirst(host);
            string markSecond = PublicParameter.MarkSecond(host);

            List<Element> components = new FilteredElementCollector(doc, viewId)
                .Where(element => Helper.IsEqualsMark(element, markPrefix, markFirst, markSecond))
                .ToList();
            
            List<Element> rebars = new FilteredElementCollector(doc, viewId)
                .Where(element => Helper.IsRebarComponent(element, doc))
                .ToList();

            using (var tr = new Transaction(doc, "Set host id"))
            {
                tr.Start();
                
                if (components.Any()) Helper.SetHostId(components, hostId, tr, doc);
                if (rebars.Any()) Helper.SetHostId(rebars, hostId, tr, doc);

                tr.Commit();
            }

            return Result.Succeeded;
        }
    }
}