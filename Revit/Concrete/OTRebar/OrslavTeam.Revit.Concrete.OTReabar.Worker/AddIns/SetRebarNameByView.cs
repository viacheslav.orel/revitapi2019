﻿using System.Collections.Generic;
using System.Linq;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;

namespace OrslavTeam.Revit.Concrete.OTReabar.Worker.AddIns
{
    [Transaction(TransactionMode.Manual)]
    public class SetRebarNameByView : IExternalCommand
    {
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            Document doc = commandData.Application.ActiveUIDocument.Document;
            Element view = doc.ActiveView;

            (List<Element>, List<Element>) rebars = new FilteredElementCollector(doc, view.Id)
                .Where(element => Helper.IsRebarComponent(element, doc))
                .Aggregate((new List<Element>(), new List<Element>()), Helper.AggregateRebarCarcass);

            Helper.SetRebarsInfo(rebars, doc);

            return Result.Succeeded;
        }
    }
}