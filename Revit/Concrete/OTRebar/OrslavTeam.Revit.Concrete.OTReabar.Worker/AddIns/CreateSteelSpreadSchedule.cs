﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using Autodesk.Revit.UI.Selection;
using OrslavTeam.Revit.Axillary.PublicInformation;
using OrslavTeam.Revit.Axillary.ViewTemplate.ReportWindow;
using OrslavTeam.Revit.Concrete.Helper.Selection;
using OrslavTeam.Revit.Concrete.OTReabar.Worker.model;

namespace OrslavTeam.Revit.Concrete.OTReabar.Worker.AddIns
{
    [Transaction(TransactionMode.Manual)]
    public class CreateSteelSpreadSchedule : IExternalCommand
    {
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            UIApplication uiApp = commandData.Application;
            Document doc = uiApp.ActiveUIDocument.Document;
            Selection sel = uiApp.ActiveUIDocument.Selection;

            var selectionScheduleHeader = new SelectingScheduleAnnotation();

            Reference scheduleHeaderReference;
            try
            {
                scheduleHeaderReference = sel.PickObject(ObjectType.Element, selectionScheduleHeader,
                    "Оберіть заголовок відомості витрат сталі");
            }
            catch (Exception)
            {
                return Result.Cancelled;
            }

            Element scheduleHeader = doc.GetElement(scheduleHeaderReference);
            Element scheduleHeaderType = ((FamilyInstance)scheduleHeader).Symbol;

            int hostId = scheduleHeaderType.LookupParameter("hostId").AsInteger();

            if (hostId == -1)
            {
                new MessageView("Параметере \"hostId\" містить невірну інформацію")
                    .ShowDialog();
                return Result.Failed;
            }

            Element hostElement = new FilteredElementCollector(doc)
                .WhereElementIsNotElementType()
                .FirstOrDefault(element => element.Id.IntegerValue == hostId);

            string hostMark = PublicParameter.MarkText(hostElement);

            Element[] rebars = new FilteredElementCollector(doc)
                .OfCategory(BuiltInCategory.OST_Rebar)
                .Where(element => PublicParameter.HostNumber(element).Equals(hostId)
                                  && Helper.IsRebarComponent(element, doc))
                .ToArray();

            Element[] rebarDetail = new FilteredElementCollector(doc)
                .OfCategory(BuiltInCategory.OST_StructConnections)
                .Where(element => PublicParameter.HostNumber(element).Equals(hostId)
                                  && Helper.IsRebarComponent(element, doc)
                                  && PublicParameter.RebarScheduleFilter(element) != 3)
                .ToArray();

            Element[] insertElement = new FilteredElementCollector(doc)
                .OfCategory(BuiltInCategory.OST_GenericModel)
                .Where(element => PublicParameter.HostNumber(element).Equals(hostId)
                                  && Helper.IsRebarComponent(element, doc)
                                  && PublicParameter.RebarScheduleFilter(element) != 3
                                  && element.Name != "деталі"
                                  && element.Name != "вироби")
                .ToArray();

            if (!rebars.Any() && !rebarDetail.Any() && !insertElement.Any())
            {
                new MessageView($"У моделі відсутні арматурні вироби пов'язані з елементом \"{hostId}\"")
                    .ShowDialog();
                return Result.Failed;
            }

            var steelSpreadSchedule = new SteelSpreadSchedule(hostMark, rebars, rebarDetail, insertElement, doc);


            if (!(scheduleHeader.Location is LocationPoint startLocation)) return Result.Failed;

            double locationX = startLocation.Point.X;
            double locationY = startLocation.Point.Y;
            double locationZ = startLocation.Point.Z;

            steelSpreadSchedule.GenerateSchedule(scheduleHeader as AnnotationSymbol, hostId.ToString(),
                locationX, locationY, locationZ, doc);

            return Result.Succeeded;
        }
    }
}