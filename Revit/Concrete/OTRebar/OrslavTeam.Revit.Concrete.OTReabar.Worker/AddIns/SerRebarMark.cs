﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using Autodesk.Revit.UI.Selection;
using OrslavTeam.Revit.Axillary.PublicInformation;
using OrslavTeam.Revit.Axillary.ViewTemplate.ReportWindow;

namespace OrslavTeam.Revit.Concrete.OTReabar.Worker.AddIns
{
    [Transaction(TransactionMode.Manual)]
    public class SerRebarMark : IExternalCommand
    {
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            UIDocument uiDoc = commandData.Application.ActiveUIDocument;
            Document doc = uiDoc.Document;
            Selection sel = uiDoc.Selection;


            (List<Element>, List<Element>) rebars = sel.GetElementIds()
                .Aggregate((new List<Element>(), new List<Element>()),
                    (acc, item) => Helper.AggregateRebarCarcass(acc, doc.GetElement(item)));

            

            using (var tr = new Transaction(doc, "Set rebar mark"))
            {
                tr.Start();
                
                Helper.SetRebarMark(rebars.Item1);
                Helper.SetFrameworkMark(rebars.Item2, doc);
            
                tr.Commit();
            }

            return Result.Succeeded;
        }
    }
}