﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using Autodesk.Revit.UI.Selection;
using OrslavTeam.Revit.Axillary.PublicInformation;
using OrslavTeam.Revit.Axillary.ViewTemplate.ReportWindow;
using OrslavTeam.Revit.Concrete.Helper.Selection;

namespace OrslavTeam.Revit.Concrete.OTReabar.Worker.AddIns
{
    [Transaction(TransactionMode.Manual)]
    public class AddRebarToGroup : IExternalCommand
    {
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            UIApplication uiApp = commandData.Application;
            Document doc = uiApp.ActiveUIDocument.Document;
            Selection sel = uiApp.ActiveUIDocument.Selection;
            var selectingModelGroup = new SelectingModelGroup();

            Reference pickedGroup;
            try
            {
                pickedGroup = sel.PickObject(ObjectType.Element, selectingModelGroup, "Оберіть групу");
            }
            catch (Exception)
            {
                return Result.Cancelled;
            }

            if (!(doc.GetElement(pickedGroup) is Group groupComponent)) return Result.Failed;

            string[] groupNameFields = groupComponent.Name.Split('_');
            if (groupNameFields[0] == "&")
            {
                new MessageView("Група не є конструктивним елементом").ShowDialog();
                return Result.Cancelled;
            }

            string groupName = groupNameFields[0] == "!" ? groupNameFields[2] : groupNameFields[1];

            IList<ElementId> memberIds = groupComponent.GetMemberIds();
            var hostId = "";

            foreach (ElementId memberId in memberIds)
            {
                Element member = doc.GetElement(memberId);
                hostId = PublicParameter.HostString(member);

                if (hostId != "") break;
            }

            if (hostId == "")
            {
                new MessageView("У обраної групи відсутні елементи для яких виконано армування!").ShowDialog();
                return Result.Cancelled;
            }

            ElementId[] rebarsCollector = new FilteredElementCollector(doc)
                .WhereElementIsNotElementType()
                .Where(element => Helper.IsRebarComponent(element, doc))
                .Select(el => el.Id)
                .ToArray();

            using (var tr = new Transaction(doc, "Create rebar group"))
            {
                tr.Start();

                Group groupRebars = doc.Create.NewGroup(rebarsCollector);
                groupRebars.GroupType.Name = $"&_{groupName}_reabars";

                tr.Commit();
            }

            return Result.Succeeded;
        }
    }
}