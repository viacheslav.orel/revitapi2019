﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using OrslavTeam.Revit.Axillary.PublicInformation;

namespace OrslavTeam.Revit.Concrete.OTReabar.Worker.AddIns
{
    [Transaction(TransactionMode.Manual)]
    public class SetElementHostByGroup : IExternalCommand
    {
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            Document doc = commandData.Application.ActiveUIDocument.Document;

            IEnumerable<Element> rebarsGroup = new FilteredElementCollector(doc)
                .OfCategory(BuiltInCategory.OST_IOSModelGroups)
                .WhereElementIsNotElementType()
                .Where(group => group.Name.EndsWith("_reabars"));

            foreach (Element group in rebarsGroup)
            {
                ElementId componentGroupId = group.GroupId;
                if (componentGroupId == ElementId.InvalidElementId) continue;

                if (!(doc.GetElement(componentGroupId) is Group componentGroup)) continue;

                Element[] components = componentGroup.GetMemberIds().Select(doc.GetElement).ToArray();
                ElementId rebarHostId = components.FirstOrDefault(component => PublicParameter.MarkFours(component).EndsWith("0"))
                    ?.Id;
                if(rebarHostId == null) continue;

                IEnumerable<Element> rebars = ((Group) group).GetMemberIds().Select(doc.GetElement);

                using (var tr = new Transaction(doc, "Set host id"))
                {
                    tr.Start();

                    Helper.SetHostId(components, rebarHostId, tr, doc);
                    Helper.SetHostId(rebars, rebarHostId, tr, doc);

                    tr.Commit();
                }
            }

            return Result.Succeeded;
        }
    }
}