﻿using System;
using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Structure;
using OrslavTeam.Revit.Axillary.PublicInformation;

namespace OrslavTeam.Revit.Concrete.OTReabar.Worker
{
    public class RebarModel : IComparable<RebarModel>, IEquatable<RebarModel>
    {
        #region fileds -------------------------------------------------------------------------------------------------

        public int LengthA { get; }
        public int LengthB { get; }
        public int LengthC { get; }
        public int LengthD { get; }
        public int LengthE { get; }
        public int LengthF { get; }
        public int LengthG { get; }
        public int LengthH { get; }
        public int LengthJ { get; }
        public int LengthK { get; }
        public int LengthO { get; }
        public int LengthR { get; }

        public string Form { get; }
        
        public string FullName { get; }

        #endregion

        public RebarModel()
        {
        }

        public RebarModel(Element rebar)
        {
            Form = PublicParameter.RebarForm(rebar);
            FullName = PublicParameter.FullName(rebar);

            if (Form == "00") return;

            if (rebar is Rebar)
            {
                LengthA = (int) (Math.Ceiling(PublicParameter.RebarA(rebar) / 5.0) * 5);
                LengthB = (int) (Math.Ceiling(PublicParameter.RebarB(rebar) / 5.0) * 5);
                LengthC = (int) (Math.Ceiling(PublicParameter.RebarC(rebar) / 5.0) * 5);
                LengthD = (int) (Math.Ceiling(PublicParameter.RebarD(rebar) / 5.0) * 5);
                LengthE = (int) (Math.Ceiling(PublicParameter.RebarE(rebar) / 5.0) * 5);
                LengthF = (int) (Math.Ceiling(PublicParameter.RebarF(rebar) / 5.0) * 5);
                LengthG = (int) (Math.Ceiling(PublicParameter.RebarG(rebar) / 5.0) * 5);
                LengthH = (int) (Math.Ceiling(PublicParameter.RebarH(rebar) / 5.0) * 5);
                LengthJ = (int) (Math.Ceiling(PublicParameter.RebarJ(rebar) / 5.0) * 5);
                LengthK = (int) (Math.Ceiling(PublicParameter.RebarK(rebar) / 5.0) * 5);
                LengthO = (int) (Math.Ceiling(PublicParameter.RebarO(rebar) / 5.0) * 5);
                LengthR = (int) (Math.Ceiling(PublicParameter.RebarR(rebar) / 5.0) * 5);
            }
            else
            {
                LengthA = (int) (Math.Ceiling(PublicParameter.DimensionsA(rebar) / 5.0) * 5);
                LengthB = (int) (Math.Ceiling(PublicParameter.DimensionsB(rebar) / 5.0) * 5);
                LengthC = (int) (Math.Ceiling(PublicParameter.DimensionsC(rebar) / 5.0) * 5);
                LengthD = (int) (Math.Ceiling(PublicParameter.DimensionsD(rebar) / 5.0) * 5);
                LengthE = (int) (Math.Ceiling(PublicParameter.DimensionsE(rebar) / 5.0) * 5);
                LengthF = (int) (Math.Ceiling(PublicParameter.DimensionsF(rebar) / 5.0) * 5);
                LengthG = (int) (Math.Ceiling(PublicParameter.DimensionsG(rebar) / 5.0) * 5);
                LengthH = (int) (Math.Ceiling(PublicParameter.DimensionsH(rebar) / 5.0) * 5);
                LengthJ = (int) (Math.Ceiling(PublicParameter.DimensionsJ(rebar) / 5.0) * 5);
                LengthK = (int) (Math.Ceiling(PublicParameter.DimensionsK(rebar) / 5.0) * 5);
                LengthO = (int) (Math.Ceiling(PublicParameter.DimensionsO(rebar) / 5.0) * 5);
                LengthR = (int) (Math.Ceiling(PublicParameter.DimensionsR(rebar) / 5.0) * 5);
            }
        }

        public int CompareTo(RebarModel other)
        {
            if (ReferenceEquals(this, other)) return 0;
            if (ReferenceEquals(null, other)) return 1;
            
            int fullNameComparison = string.Compare(FullName, other.FullName, StringComparison.Ordinal);
            if (fullNameComparison != 0) return fullNameComparison;

            int formComparison = string.Compare(Form, other.Form, StringComparison.Ordinal);
            if (formComparison != 0) return formComparison;

            if (Form == "00") return 0;

            int lengthAComparison = LengthA.CompareTo(other.LengthA);
            if (lengthAComparison != 0) return lengthAComparison;

            int lengthBComparison = LengthB.CompareTo(other.LengthB);
            if (lengthBComparison != 0) return lengthBComparison;

            int lengthCComparison = LengthC.CompareTo(other.LengthC);
            if (lengthCComparison != 0) return lengthCComparison;

            int lengthDComparison = LengthD.CompareTo(other.LengthD);
            if (lengthDComparison != 0) return lengthDComparison;

            int lengthEComparison = LengthE.CompareTo(other.LengthE);
            if (lengthEComparison != 0) return lengthEComparison;

            int lengthFComparison = LengthF.CompareTo(other.LengthF);
            if (lengthFComparison != 0) return lengthFComparison;

            int lengthGComparison = LengthG.CompareTo(other.LengthG);
            if (lengthGComparison != 0) return lengthGComparison;

            int lengthHComparison = LengthH.CompareTo(other.LengthH);
            if (lengthHComparison != 0) return lengthHComparison;

            int lengthJComparison = LengthJ.CompareTo(other.LengthJ);
            if (lengthJComparison != 0) return lengthJComparison;

            int lengthKComparison = LengthK.CompareTo(other.LengthK);
            if (lengthKComparison != 0) return lengthKComparison;

            int lengthOComparison = LengthO.CompareTo(other.LengthO);
            if (lengthOComparison != 0) return lengthOComparison;

            return LengthR.CompareTo(other.LengthR);
        }

        public bool Equals(RebarModel other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            if (Form == "00" && Form == other.Form) return true;
            return FullName == other.FullName &&
                   LengthA == other.LengthA && LengthB == other.LengthB && LengthC == other.LengthC &&
                   LengthD == other.LengthD && LengthE == other.LengthE && LengthF == other.LengthF &&
                   LengthG == other.LengthG && LengthH == other.LengthH && LengthJ == other.LengthJ &&
                   LengthK == other.LengthK && LengthO == other.LengthO && LengthR == other.LengthR &&
                   Form == other.Form;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((RebarModel) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = LengthA;
                hashCode = (hashCode * 397) ^ LengthB;
                hashCode = (hashCode * 397) ^ LengthC;
                hashCode = (hashCode * 397) ^ LengthD;
                hashCode = (hashCode * 397) ^ LengthE;
                hashCode = (hashCode * 397) ^ LengthF;
                hashCode = (hashCode * 397) ^ LengthG;
                hashCode = (hashCode * 397) ^ LengthH;
                hashCode = (hashCode * 397) ^ LengthJ;
                hashCode = (hashCode * 397) ^ LengthK;
                hashCode = (hashCode * 397) ^ LengthO;
                hashCode = (hashCode * 397) ^ LengthR;
                hashCode = (hashCode * 397) ^ (Form != null ? Form.GetHashCode() : 0);
                return hashCode;
            }
        }
    }
}