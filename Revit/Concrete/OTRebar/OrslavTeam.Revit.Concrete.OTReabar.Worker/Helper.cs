﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Structure;
using OrslavTeam.Revit.Axillary.PublicInformation;
using OrslavTeam.Revit.Axillary.RevitData;
using OrslavTeam.Revit.Axillary.RevitData.Model;

namespace OrslavTeam.Revit.Concrete.OTReabar.Worker
{
    public static class Helper
    {
        #region methods get --------------------------------------------------------------------------------------------

        

        #endregion


        #region methods is ---------------------------------------------------------------------------------------------

        public static bool IsEqualsMark(Element element, string markPrefix, string markFirst, string markSecond)
        {
            if (PublicParameter.MarkPrefix(element) != markPrefix) return false;
            if (PublicParameter.MarkFirst(element) != markFirst) return false;
            return PublicParameter.MarkSecond(element) == markSecond;
        }

        public static bool IsRebarComponent(Element element, Document doc)
        {
            try
            {
                Element family = doc.GetElement(element.GetTypeId());
                string assemblyKod = PublicParameter.GetUniformatKode(family);
                return assemblyKod.StartsWith("B.03");
            }
            catch (Exception)
            {
                return false;
            }
        }

        #endregion


        #region methods set --------------------------------------------------------------------------------------------

        public static void SetHostId(IEnumerable<Element> elements, ElementId hostId, Transaction tr, Document doc)
        {
            if (!tr.HasStarted()) throw new Exception("Transaction is not started");
            foreach (Element element in elements)
            {
                PublicParameter.RebarHost(element, hostId);

                if (element is FamilyInstance familyInstance)
                {
                    ICollection<ElementId> subComponents = familyInstance.GetSubComponentIds();

                    if (subComponents.Any())
                    {
                        foreach (ElementId subComponentId in subComponents)
                        {
                            PublicParameter.RebarHost(doc.GetElement(subComponentId), hostId);
                        }
                    }
                }
            }
        }

        public static void SetRebarsInfo((List<Element>, List<Element>) elements, Document doc)
        {
            (List<Element> rebars, List<Element> rebarDetail) = elements;

            if (rebars.Any())
            {
                Dictionary<(int, int), List<Element>> rebarsGroupById = rebars
                    .Aggregate(new Dictionary<(int, int), List<Element>>(),
                        (dictionary, rebar) => AggregateRebarById(dictionary, rebar, doc));

                using (var tr = new Transaction(doc, "Set rebar info"))
                {
                    tr.Start();

                    foreach (KeyValuePair<(int, int), List<Element>> rebarGroupById in rebarsGroupById)
                    {
                        ProductMixInfo productMixInfo = RevitDb.GetProductMixInfo(rebarGroupById.Key.Item1);
                        ProfileInfo rebarInfo =
                            RevitDb.GetComponentInfo(rebarGroupById.Key.Item1, rebarGroupById.Key.Item2);

                        double massPerUnitLength = rebarInfo.Parameters
                            .FirstOrDefault(pr => pr.Key == "mass_per_unit_length").Value;

                        var rebarPerMeterLength = 0.0;
                        var rebarPerMeter = new List<Element>();

                        foreach (Element rebar in rebarGroupById.Value)
                        {
                            int length;
                            int quantity;
                            string form;
                            string dimensions = "";

                            switch (rebar)
                            {
                                case Rebar rebarElement:
                                    length = PublicParameter.RebarLength(rebarElement);
                                    quantity = PublicParameter.RebarQuantity(rebarElement);
                                    form = PublicParameter.RebarForm(rebarElement);
                                    switch (form)
                                    {
                                        case "01a":
                                        case "13":
                                        case "14":
                                        case "15":
                                        case "21":
                                        case "23":
                                            dimensions = $"A = {PublicParameter.RebarA(rebar)}\n" +
                                                         $"B = {PublicParameter.RebarB(rebar)}\n" +
                                                         $"C = {PublicParameter.RebarC(rebar)}";
                                            break;
                                        case "11":
                                        case "12":
                                            dimensions = $"A = {PublicParameter.RebarA(rebar)}\n" +
                                                         $"B = {PublicParameter.RebarB(rebar)}";
                                            break;
                                        case "22":
                                        case "24":
                                        case "26":
                                        case "27":
                                        case "28":
                                        case "31":
                                        case "32":
                                        case "36":
                                        case "47":
                                        case "51a":
                                        case "51b":
                                            dimensions = $"A = {PublicParameter.RebarA(rebar)}\n" +
                                                         $"B = {PublicParameter.RebarB(rebar)}\n" +
                                                         $"C = {PublicParameter.RebarC(rebar)}\n" +
                                                         $"D = {PublicParameter.RebarD(rebar)}";
                                            break;
                                        case "25":
                                        case "29":
                                        case "34":
                                        case "35":
                                        case "41":
                                        case "44":
                                        case "46":
                                            dimensions = $"A = {PublicParameter.RebarA(rebar)}\n" +
                                                         $"B = {PublicParameter.RebarB(rebar)}\n" +
                                                         $"C = {PublicParameter.RebarC(rebar)}\n" +
                                                         $"D = {PublicParameter.RebarD(rebar)}\n" +
                                                         $"E = {PublicParameter.RebarE(rebar)}";
                                            break;
                                        case "56a":
                                        case "56b":
                                        case "64a":
                                        case "64b":
                                            dimensions = $"A = {PublicParameter.RebarA(rebar)}\n" +
                                                         $"B = {PublicParameter.RebarB(rebar)}\n" +
                                                         $"C = {PublicParameter.RebarC(rebar)}\n" +
                                                         $"D = {PublicParameter.RebarD(rebar)}\n" +
                                                         $"E = {PublicParameter.RebarE(rebar)}\n" +
                                                         $"F = {PublicParameter.RebarF(rebar)}";
                                            break;
                                        case "67":
                                            dimensions = $"A = {PublicParameter.RebarA(rebar)}\n" +
                                                         $"B = {PublicParameter.RebarB(rebar)}\n" +
                                                         $"R = {PublicParameter.RebarR(rebar)}";
                                            break;
                                    }

                                    break;
                                case FamilyInstance instanceRebar:
                                    length = PublicParameter.DimensionLength(instanceRebar);
                                    quantity = PublicParameter.ElementQuantity(instanceRebar);
                                    form = PublicParameter.RebarForm(instanceRebar);
                                    switch (form)
                                    {
                                        case "01a":
                                        case "13":
                                        case "14":
                                        case "15":
                                        case "21":
                                        case "23":
                                        case "98":
                                            dimensions = $"A = {PublicParameter.DimensionsA(rebar)}\n" +
                                                         $"B = {PublicParameter.DimensionsB(rebar)}\n" +
                                                         $"C = {PublicParameter.DimensionsC(rebar)}";
                                            break;
                                        case "11":
                                        case "12":
                                            dimensions = $"A = {PublicParameter.DimensionsA(rebar)}\n" +
                                                         $"B = {PublicParameter.DimensionsB(rebar)}";
                                            break;
                                        case "22":
                                        case "24":
                                        case "26":
                                        case "27":
                                        case "28":
                                        case "31":
                                        case "32":
                                        case "36":
                                        case "47":
                                        case "51a":
                                        case "51b":
                                            dimensions = $"A = {PublicParameter.DimensionsA(rebar)}\n" +
                                                         $"B = {PublicParameter.DimensionsB(rebar)}\n" +
                                                         $"C = {PublicParameter.DimensionsC(rebar)}\n" +
                                                         $"D = {PublicParameter.DimensionsD(rebar)}";
                                            break;
                                        case "25":
                                        case "29":
                                        case "34":
                                        case "35":
                                        case "41":
                                        case "44":
                                        case "46":
                                            dimensions = $"A = {PublicParameter.DimensionsA(rebar)}\n" +
                                                         $"B = {PublicParameter.DimensionsB(rebar)}\n" +
                                                         $"C = {PublicParameter.DimensionsC(rebar)}\n" +
                                                         $"D = {PublicParameter.DimensionsD(rebar)}\n" +
                                                         $"E = {PublicParameter.DimensionsE(rebar)}";
                                            break;
                                        case "56a":
                                        case "56b":
                                        case "64a":
                                        case "64b":
                                            dimensions = $"A = {PublicParameter.DimensionsA(rebar)}\n" +
                                                         $"B = {PublicParameter.DimensionsB(rebar)}\n" +
                                                         $"C = {PublicParameter.DimensionsC(rebar)}\n" +
                                                         $"D = {PublicParameter.DimensionsD(rebar)}\n" +
                                                         $"E = {PublicParameter.DimensionsE(rebar)}\n" +
                                                         $"F = {PublicParameter.DimensionsF(rebar)}";
                                            break;
                                        case "67":
                                            dimensions = $"A = {PublicParameter.DimensionsA(rebar)}\n" +
                                                         $"B = {PublicParameter.DimensionsB(rebar)}\n" +
                                                         $"R = {PublicParameter.DimensionsR(rebar)}";
                                            break;
                                    }

                                    break;
                                default:
                                    continue;
                            }

                            if (form == "00")
                            {
                                if (length > 12000)
                                {
                                    var reinforcingLap = (int) rebarInfo.Parameters
                                        .FirstOrDefault(pr => pr.Key == "thickness_first").Value;
                                    var lapQuantity = (int) Math.Ceiling(length / (12000 - reinforcingLap * 0.5));

                                    length += reinforcingLap * (lapQuantity - 1);
                                }

                                rebarPerMeterLength += length * quantity;
                                PublicParameter.FullName(rebar,
                                    $"{rebarInfo.ProfileName} {productMixInfo.ProductMixNumber}, м.п.");
                                PublicParameter.TechnicalMassOne(rebar, massPerUnitLength);
                                rebarPerMeter.Add(rebar);
                                continue;
                            }

                            string fullName = $"{rebarInfo.ProfileName} {productMixInfo.ProductMixNumber} l={length}";
                            PublicParameter.FullName(rebar, fullName);
                            PublicParameter.TechnicalQuantity(rebar, quantity);
                            PublicParameter.TechnicalMassOne(rebar, (length / 1000.0) * massPerUnitLength);
                            PublicParameter.TechnicalRebarDimensions(rebar, dimensions);
                        }

                        double technicalQuantity = (rebarPerMeterLength / rebarPerMeter.Count) / 1000;

                        foreach (Element rebar in rebarPerMeter)
                        {
                            PublicParameter.TechnicalQuantity(rebar, technicalQuantity);
                        }
                    }

                    tr.Commit();
                }
            }

            if (rebarDetail.Any())
            {
                using (var tr = new Transaction(doc, "Set full name to framework"))
                {
                    tr.Start();

                    foreach (Element framework in rebarDetail)
                    {
                        Element family = doc.GetElement(framework.GetTypeId());
                        string fullName;
                        int quantity;
                        double totalMass;

                        if (PublicParameter.GetUniformatKode(family).StartsWith("B.03.04"))
                        {
                            fullName = PublicParameter.ProfileName(family);
                            string lastPart = PublicParameter.ProfileNameSuffix(family);
                            if (lastPart != "") fullName += $"-{lastPart}";
                            quantity = PublicParameter.ElementQuantity(family);
                        }
                        else
                        {
                            fullName =
                                $"{PublicParameter.ProfileName(framework)} l={PublicParameter.DimensionLength(framework)}";
                            quantity = PublicParameter.ElementQuantity(framework);
                        }

                        if (framework is FamilyInstance familyInstance)
                            totalMass = familyInstance.GetSubComponentIds()
                                .Select(doc.GetElement)
                                .Sum(PublicParameter.ElementMass);
                        else totalMass = PublicParameter.ElementMass(framework);

                        PublicParameter.FullName(framework, fullName);
                        PublicParameter.TechnicalQuantity(framework, quantity);
                        PublicParameter.TechnicalMassOne(framework, totalMass);
                    }

                    tr.Commit();
                }
            }
        }

        public static void SetRebarMark(List<Element> rebars)
        {
            IEnumerable<IGrouping<RebarModel, Element>> models = from rebar in rebars
                let rebarModel = new RebarModel(rebar)
                orderby rebarModel
                group rebar by rebarModel;

            var currentNumber = 1;

            foreach (IGrouping<RebarModel, Element> elements in models)
            {
                string mark = currentNumber.ToString();

                foreach (Element element in elements)
                {
                    PublicParameter.MarkText(element, mark);
                }

                currentNumber++;
            }
        }

        public static void SetFrameworkMark(List<Element> frameworks, Document doc)
        {
            IEnumerable<IGrouping<string, Element>> groupByMarkPrefix = frameworks
                .GroupBy(fr => PublicParameter.MarkTextPrefix(doc.GetElement(fr.GetTypeId())));

            foreach (IGrouping<string, Element> elementByMarkPrefix in groupByMarkPrefix)
            {
                IEnumerable<IGrouping<string, Element>> groupByFullName =
                    elementByMarkPrefix.GroupBy(PublicParameter.FullName);

                var currentNumber = 1;

                foreach (IGrouping<string, Element> elements in groupByFullName)
                {
                    string mark = $"{elementByMarkPrefix.Key}{currentNumber}";

                    foreach (Element element in elements)
                    {
                        PublicParameter.MarkText(element, mark);
                    }

                    currentNumber++;
                }
            }
        }

        #endregion


        #region methods acc --------------------------------------------------------------------------------------------

        public static (List<Element>, List<Element>) AggregateRebarCarcass((List<Element>, List<Element>) acc,
            Element rebar)
        {
            int rebarScheduleFilter = PublicParameter.RebarScheduleFilter(rebar);

            switch (rebarScheduleFilter)
            {
                case 1:
                case 2:
                    acc.Item1.Add(rebar);
                    break;
                case 4:
                case 5:
                    acc.Item2.Add(rebar);
                    break;
                default:
                    throw new Exception($"Element {rebar.Id} don't have current filter ({rebarScheduleFilter})");
            }

            return acc;
        }

        private static Dictionary<(int, int), List<Element>> AggregateRebarById(
            Dictionary<(int, int), List<Element>> acc, Element rebar, Document doc)
        {
            int productMixId = PublicParameter.ProductMixKey(rebar);
            int profileId = PublicParameter.ElementKey(rebar);

            if (productMixId == 0)
            {
                Element family = doc.GetElement(rebar.GetTypeId());
                productMixId = PublicParameter.ProductMixKey(family);
                profileId = PublicParameter.ElementKey(family);
            }

            if (acc.ContainsKey((productMixId, profileId))) acc[(productMixId, profileId)].Add(rebar);
            else acc.Add((productMixId, profileId), new List<Element> {rebar});

            return acc;
        }

        private static Dictionary<string, List<Element>> AggregateByForm(
            Dictionary<string, List<Element>> acc, Element el)
        {
            string formNumber = PublicParameter.RebarForm(el);

            if (acc.ContainsKey(formNumber)) acc[formNumber].Add(el);
            else acc.Add(formNumber, new List<Element> {el});

            return acc;
        }

        private static Dictionary<string, List<RebarModel>> AggregateByFullName(
            Dictionary<string, List<RebarModel>> acc, Element el)
        {
            string fullName = PublicParameter.FullName(el);

            if (acc.ContainsKey(fullName)) acc[fullName].Add(new RebarModel(el));
            else acc.Add(fullName, new List<RebarModel> {new RebarModel(el)});

            return acc;
        }

        #endregion
    }
}