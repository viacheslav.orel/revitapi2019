﻿using System.Globalization;
using Autodesk.Revit.DB;

namespace OrslavTeam.Revit.Concrete.OTReabar.Worker.model
{
    public class SshCell
    {
        #region static data --------------------------------------------------------------------------------------------

        
        #endregion
        
        #region item data ----------------------------------------------------------------------------------------------

        private Element _cellType;
        
        private double _positionX;
        private double _positionY;
        
        private double _rowHeight;
        private double _columnWidth;
        
        private string _content;

        #endregion

        #region constructor --------------------------------------------------------------------------------------------

        public SshCell(Element cellType, double mass, double positionX, double positionY, int width = 8, int height = 15)
        {
            _cellType = cellType;
            
            _positionX = positionX;
            _positionY = positionY;

            _rowHeight = UnitUtils.ConvertToInternalUnits(height, DisplayUnitType.DUT_MILLIMETERS);
            _columnWidth = UnitUtils.ConvertToInternalUnits(width, DisplayUnitType.DUT_MILLIMETERS);

            _content = mass.ToString("F1", CultureInfo.CreateSpecificCulture("es-ES"));
        }

        #endregion
    }
}