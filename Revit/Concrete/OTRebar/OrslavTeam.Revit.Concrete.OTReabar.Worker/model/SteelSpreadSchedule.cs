﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using Autodesk.Revit.DB;
using OrslavTeam.Revit.Axillary.PublicInformation;
using OrslavTeam.Revit.Axillary.RevitData;
using OrslavTeam.Revit.Axillary.RevitData.Model;
using OrslavTeam.Revit.Axillary.ViewTemplate.ReportWindow;

namespace OrslavTeam.Revit.Concrete.OTReabar.Worker.model
{
    public class SteelSpreadSchedule
    {
        #region static fields ------------------------------------------------------------------------------------------

        private static readonly double
            _rowHeight = UnitUtils.ConvertToInternalUnits(8, DisplayUnitType.DUT_MILLIMETERS);

        private static readonly double
            _forRowsHeight = UnitUtils.ConvertToInternalUnits(32, DisplayUnitType.DUT_MILLIMETERS);
        
        private static readonly double
            _lengthForty = UnitUtils.ConvertToInternalUnits(40, DisplayUnitType.DUT_MILLIMETERS);

        private static readonly double
            _columnWidth = UnitUtils.ConvertToInternalUnits(15, DisplayUnitType.DUT_MILLIMETERS);

        private static readonly NumberFormatInfo _nfi = new NumberFormatInfo
        {
            NumberDecimalSeparator = ",",
            NumberGroupSeparator = " ",
            CurrencyDecimalDigits = 2
        };

        #endregion

        #region fileds -------------------------------------------------------------------------------------------------

        private readonly string _mark;

        private readonly Dictionary<int, Dictionary<int, Dictionary<int, double>>> _rebarComponentCollection;
        private readonly Dictionary<int, Dictionary<int, Dictionary<int, double>>> _insertRebarCollection;
        private readonly Dictionary<int, Dictionary<int, Dictionary<int, double>>> _insertDetailCollection;

        #endregion

        #region constructors -------------------------------------------------------------------------------------------

        public SteelSpreadSchedule(string mark, IEnumerable<Element> rebarForms, IEnumerable<Element> rebarComponents,
            IEnumerable<Element> insertComponents, Document doc)
        {
            _mark = mark;

            _rebarComponentCollection = new Dictionary<int, Dictionary<int, Dictionary<int, double>>>();
            _insertRebarCollection = new Dictionary<int, Dictionary<int, Dictionary<int, double>>>();
            _insertDetailCollection = new Dictionary<int, Dictionary<int, Dictionary<int, double>>>();

            foreach (Element rebarForm in rebarForms)
            {
                Element rebarType = doc.GetElement(rebarForm.GetTypeId());

                int productMixId = PublicParameter.ProductMixKey(rebarType);
                int profileId = PublicParameter.ElementKey(rebarType);

                ElementId materialId = rebarType.get_Parameter(BuiltInParameter.MATERIAL_ID_PARAM).AsElementId();
                Element material = doc.GetElement(materialId);
                int materialKod = PublicParameter.MaterialKod(material);

                double totalMass = GetTotalMassByRebar(rebarForm);

                AddElement(productMixId, profileId, materialKod, totalMass, ref _rebarComponentCollection);
            }

            foreach (Element rebarComponent in rebarComponents)
            {
                switch (PublicParameter.RebarScheduleFilter(rebarComponent))
                {
                    case 2:
                        {
                            Element family = ((FamilyInstance)rebarComponent).Symbol;

                            int productMixId = PublicParameter.ProductMixKey(rebarComponent);
                            int profileId;
                            if (productMixId == 0)
                            {
                                productMixId = PublicParameter.ProductMixKey(family);
                                profileId = PublicParameter.ElementKey(family);
                            }
                            else profileId = PublicParameter.ElementKey(rebarComponent);

                            int materialKod = GetMaterialId(rebarComponent, doc, family);
                            double totalMass = GetTotalMassByRebar(rebarComponent);

                            AddElement(productMixId, profileId, materialKod, totalMass, ref _rebarComponentCollection);

                            break;
                        }
                    case 4:
                        {
                            IEnumerable<Element> subComponents =
                                ((FamilyInstance)rebarComponent).GetSubComponentIds().Select(doc.GetElement);

                            foreach (Element element in subComponents)
                            {
                                if (element.Name == "деталі" || element.Name == "вироби") continue;

                                int productMixId = PublicParameter.ProductMixKey(element);
                                int profileId = PublicParameter.ElementKey(element);
                                int materialId = GetMaterialId(element, doc);
                                double totalMass = GetTotalMassByInstance(element);

                                AddElement(productMixId, profileId, materialId, totalMass, ref _rebarComponentCollection);
                            }

                            break;
                        }
                }
            }

            foreach (Element rebarComponent in insertComponents)
            {
                IEnumerable<Element> subComponents = ((FamilyInstance)rebarComponent)
                    .GetSubComponentIds()
                    .Select(doc.GetElement);

                foreach (Element element in subComponents)
                {
                    if(element.Name == "деталі" || element.Name == "вироби") continue;

                    int productMixId = PublicParameter.ProductMixKey(element);
                    int profileId = PublicParameter.ElementKey(element);
                    int materialId = GetMaterialId(element, doc);
                    double totalMass = GetTotalMassByInstance(element);

                    if (productMixId == 310)
                        AddElement(productMixId, profileId, materialId, totalMass, ref _insertRebarCollection);
                    else
                        AddElement(productMixId, profileId, materialId, totalMass, ref _insertDetailCollection);
                }
            }
        }

        #endregion

        #region methods ------------------------------------------------------------------------------------------------

        public void GenerateSchedule(AnnotationSymbol titleCell, string hostId, double startX, double startY,
            double startZ, Document doc)
        {
            View view = doc.ActiveView;
            double titleCellWidth = _lengthForty;
            var scheduleTotalMass = 0.0;
            (FamilySymbol headerCell, FamilySymbol bodyCell) = GetCellComponent(doc);
            if (headerCell == null || bodyCell == null)
            {
                new MessageView("Відсутні необхидні типи").ShowDialog();
                return;
            }

            startY -= _columnWidth;

            GetPrevGroup(hostId, doc);

            using (var tr = new Transaction(doc, "Create steel spread schedule"))
            {
                tr.Start();
                
                var markHeaderCellLocation = new XYZ(startX, startY, startZ);
                var markBodyCellLocation = new XYZ(startX, startY - _lengthForty, startZ);

                startX += _lengthForty;
                
                CreateCell(markHeaderCellLocation, "Марка\nелементу", headerCell, doc, view, _lengthForty, _lengthForty);
                CreateCell(markBodyCellLocation, _mark, bodyCell, doc, view, _lengthForty);


                if (_rebarComponentCollection.Any())
                {
                    var columnRebarTitleCellPoint = new XYZ(startX, startY, startZ);
                    double columnRebarTotalMass =
                        CreateColumn(doc, "Арматура класу", ref startX, startY - _rowHeight, startZ,
                            headerCell, bodyCell, view, _rebarComponentCollection);
                
                    var rebarsTotalHeaderPoint = new XYZ(startX, startY - _rowHeight, startZ);
                    var rebarsTotalBodyPoint = new XYZ(startX, startY - 5 * _rowHeight, startZ);

                    startX += _columnWidth;
                    scheduleTotalMass += columnRebarTotalMass;
                
                    CreateCell(rebarsTotalHeaderPoint, "Всього", headerCell, doc, view, _columnWidth, 4 * _rowHeight);
                    CreateCell(rebarsTotalBodyPoint, scheduleTotalMass.ToString(_nfi), bodyCell, doc, view);

                    double columnRebarTitleCellWidth = startX - columnRebarTitleCellPoint.X;
                    titleCellWidth += columnRebarTitleCellWidth;
                    
                    CreateCell(columnRebarTitleCellPoint, "Вироби арматурні", headerCell, doc, view, columnRebarTitleCellWidth);
                }

                if (_insertRebarCollection.Any() || _insertDetailCollection.Any())
                {
                    var insertTitleCellPoint = new XYZ(startX, startY, startZ);
                    var insertTotalMass = 0.0;

                    if (_insertRebarCollection.Any())
                    {
                        insertTotalMass += CreateColumn(doc, "Арматура класу",
                            ref startX, startY - _rowHeight, startZ,
                            headerCell, bodyCell, view, _insertRebarCollection);
                    }

                    if (_insertDetailCollection.Any())
                    {
                        insertTotalMass += CreateColumn(doc, "Прокат марки",
                            ref startX, startY - _rowHeight, startZ,
                            headerCell, bodyCell, view, _insertDetailCollection);
                    }
                    
                    var insertTotalHeaderCellPoint = new XYZ(startX, startY - _rowHeight, startZ);
                    var insertTotalBodyCellPoint = new XYZ(startX, startY - 5 * _rowHeight, startZ);

                    startX += _columnWidth;
                    scheduleTotalMass += insertTotalMass;
                    
                    CreateCell(insertTotalHeaderCellPoint, "Всього", headerCell, doc, view, _columnWidth, 4 * _rowHeight);
                    CreateCell(insertTotalBodyCellPoint, insertTotalMass.ToString(_nfi), bodyCell, doc, view);

                    double columnInsertCellWidth = startX - insertTitleCellPoint.X;
                    titleCellWidth += columnInsertCellWidth;
                    
                    CreateCell(insertTitleCellPoint, "Вироби закладні", headerCell, doc, view, columnInsertCellWidth);
                }
                
                var scheduleTotalHeaderCellPoint = new XYZ(startX, startY, startZ);
                var scheduleTotalBodyCellPoint = new XYZ(startX, startY - 5 * _rowHeight, startZ);
                
                CreateCell(scheduleTotalHeaderCellPoint, "Всього", headerCell, doc, view, _columnWidth, _lengthForty);
                CreateCell(scheduleTotalBodyCellPoint, scheduleTotalMass.ToString(_nfi), bodyCell, doc, view);

                titleCellWidth += _columnWidth;

                titleCell.AnnotationSymbolType.LookupParameter("width").Set(titleCellWidth);
                
                tr.Commit();
            }
        }

        #endregion

        #region helper -------------------------------------------------------------------------------------------------

        private static int GetMaterialId(Element el, Document doc, Element family = null)
        {
            ElementId materialId = PublicParameter.Material(el);
            if (materialId == ElementId.InvalidElementId && family != null)
                materialId = PublicParameter.Material(family);
            try
            {
                Element material = doc.GetElement(materialId);

                return PublicParameter.MaterialKod(material);
            }
            catch (Exception)
            {
                throw new Exception($"elem = {el.Id}; fam = {family?.Id}");
            }
            
        }

        private static double GetTotalMassByRebar(Element el)
        {
            double massSingle = PublicParameter.TechnicalMassOne(el);
            double quantity = PublicParameter.TechnicalQuantity(el);

            return massSingle * quantity;
        }

        private static double GetTotalMassByInstance(Element el)
        {
            double massSingle = PublicParameter.ElementMass(el);
            int quantity = PublicParameter.ElementQuantity(el);

            return massSingle * quantity;
        }

        private static void AddElement(int productMixId, int elementId, int materialId, double totalMass,
            ref Dictionary<int, Dictionary<int, Dictionary<int, double>>> acc)
        {
            if (acc.ContainsKey(materialId))
            {
                if (acc[materialId].ContainsKey(productMixId))
                {
                    if (acc[materialId][productMixId].ContainsKey(elementId))
                        acc[materialId][productMixId][elementId] += totalMass;
                    else acc[materialId][productMixId].Add(elementId, totalMass);
                }
                else
                {
                    var elementMass = new Dictionary<int, double> {{elementId, totalMass}};
                    acc[materialId].Add(productMixId, elementMass);
                }
            }
            else
            {
                var elementMass = new Dictionary<int, double> {{elementId, totalMass}};
                var productMixMass = new Dictionary<int, Dictionary<int, double>> {{productMixId, elementMass}};
                acc.Add(materialId, productMixMass);
            }
        }

        private static (FamilySymbol, FamilySymbol) GetCellComponent(Document doc)
        {
            FamilySymbol headerCell = null;
            FamilySymbol bodyCell = null;
            
            try
            {
                Element cell = new FilteredElementCollector(doc)
                    .OfClass(typeof(Family))
                    .First(element => element.Name == "GEN_OTeam_Специфікація-2D_Чарунок");

                Element[] symbols = ((Family) cell).GetFamilySymbolIds().Select(doc.GetElement).ToArray();
                headerCell = symbols.First(el => el.Name == "header") as FamilySymbol;
                bodyCell = symbols.First(el => el.Name == "body") as FamilySymbol;
            }
            catch (Exception)
            {
                throw new Exception("Сімейство з назвою \"GEN_OTeam_Специфікація-2D_Чарунок\" - не знайдено");
            }

            return (headerCell, bodyCell);
        }

        private static void GetPrevGroup(string id, Document doc)
        {
            ElementId[] prevCells = new FilteredElementCollector(doc)
                .OfCategory(BuiltInCategory.OST_GenericAnnotation)
                .Where(element => (element.Name == "body" || element.Name == "header")
                                  && PublicParameter.HostString(element) == id)
                .Select(el => el.Id)
                .ToArray();

            RemoveElements(doc, prevCells);
        }

        #endregion

        #region transaction --------------------------------------------------------------------------------------------

        private static void RemoveElements(Document doc, params ElementId[] elementIds)
        {
            using (var tr = new Transaction(doc, "Remove element"))
            {
                tr.Start();

                doc.Delete(elementIds);

                tr.Commit();
            }
        }

        #endregion

        #region create -------------------------------------------------------------------------------------------------

        private static void CreateCell(XYZ location, string content, FamilySymbol cell, Document doc, View view,
            double width = 0, double height = 0)
        {
            if (!cell.IsActive) cell.Activate();

            FamilyInstance inst = doc.Create.NewFamilyInstance(location, cell, view);
            inst.LookupParameter("content").Set(content);
            if (width > 0.0001) inst.LookupParameter("columnWidth").Set(width);
            if (height > 0.0001) inst.LookupParameter("rowHeight").Set(height);
        }

        private static double CreateColumn(Document doc, string columnHeader,
            ref double locationX, double locationY, double locationZ,
            FamilySymbol headerComponent, FamilySymbol bodyComponent, View view,
            Dictionary<int, Dictionary<int, Dictionary<int, double>>> elements)
        {
            var headerCellPoint = new XYZ(locationX, locationY, locationZ);
            var headerCellWidth = 0.0;
            var headerTotalMass = 0.0;
            

            IOrderedEnumerable<KeyValuePair<int, Dictionary<int, Dictionary<int, double>>>> orderedByMaterial =
                elements.OrderBy(el => el.Key);
            
            foreach (KeyValuePair<int, Dictionary<int, Dictionary<int, double>>> groupByMaterial in orderedByMaterial)
            {
                string materialCellData = RevitDb.GetMaterialName(groupByMaterial.Key);
                var materialCellPoint = new XYZ(locationX, locationY - _rowHeight, locationZ);
                var materialCellWidth = 0.0;
                var materialTotalMass = 0.0;

                IOrderedEnumerable<KeyValuePair<int, Dictionary<int, double>>> orderedByProductMix =
                    groupByMaterial.Value.OrderBy(el => el.Key);
                
                foreach (KeyValuePair<int, Dictionary<int, double>> groupByProductMix in orderedByProductMix)
                {
                    string productCellData = RevitDb.GetProductMixInfo(groupByProductMix.Key).ProductMixNumber;
                    var productCellPoint = new XYZ(locationX, locationY - 2 * _rowHeight, locationZ);
                    var productCellWidth = 0.0;

                    double cellWidth = _columnWidth;
                    if (groupByProductMix.Value.Count == 1) cellWidth *= 2;
                    
                    IOrderedEnumerable<KeyValuePair<int, double>> orderedByProfile =
                        groupByProductMix.Value.OrderBy(el => el.Key);
                    foreach (KeyValuePair<int, double> groupByProfile in orderedByProfile)
                    {
                        string profileHeaderCellData = RevitDb.GetComponentInfo(groupByProductMix.Key, groupByProfile.Key)
                            .ProfileName;
                        if (groupByProductMix.Key == 310) profileHeaderCellData = profileHeaderCellData.Split(' ')[0];
                        
                        var profileHeaderCellPoint = new XYZ(locationX, locationY - 3 * _rowHeight, locationZ);
                        var profileBodyCellPoint = new XYZ(locationX, locationY - 4 * _rowHeight, locationZ);

                        double roundedMass = Math.Ceiling(groupByProfile.Value * 100.0) / 100.0;
                        var profileBodyCellData = roundedMass > 0.001 ? roundedMass.ToString(_nfi) : "";

                        locationX += cellWidth;
                        productCellWidth += cellWidth;
                        materialTotalMass += roundedMass;

                        CreateCell(profileHeaderCellPoint, profileHeaderCellData, headerComponent, doc, view, cellWidth);
                        CreateCell(profileBodyCellPoint, profileBodyCellData, bodyComponent, doc, view, cellWidth);
                    }


                    materialCellWidth += productCellWidth;
                    CreateCell(productCellPoint, productCellData, headerComponent, doc, view, productCellWidth);
                }
                
                var materialTotalHeaderCellPoint = new XYZ(locationX, locationY - 2 * _rowHeight, locationZ);
                var materialTotalBodyCellPoint = new XYZ(locationX, locationY - 4 * _rowHeight, locationZ);

                locationX += _columnWidth;
                materialCellWidth += _columnWidth;

                headerTotalMass += materialTotalMass;
                
                CreateCell(materialTotalHeaderCellPoint, "Всього", headerComponent, doc, view, _columnWidth, 2 * _rowHeight);
                CreateCell(materialTotalBodyCellPoint, materialTotalMass.ToString(_nfi), bodyComponent, doc, view);

                headerCellWidth += materialCellWidth;
                
                CreateCell(materialCellPoint, materialCellData, headerComponent, doc, view, materialCellWidth);
            }
            
            
            CreateCell(headerCellPoint, columnHeader, headerComponent, doc, view, headerCellWidth);

            return headerTotalMass;
        }
        
        #endregion
    };
}

