﻿using System;
using Autodesk.Revit.DB;

namespace OrslavTeam.Revit.Concrete.OTRebar.Rebars
{
    public class RebarDetailClass : RebarElement, IComparable<RebarDetailClass>
    {
        #region Attribute

        public string ElementName { get; }
        public string DocumentNumber { get; }
        public double MassOne { get; }
        public double ValueElement { get; }

        #endregion

        #region Constructor

        public RebarDetailClass(string mark, string productNumber, string name, double massOne, int quantity,
            Element element)
            : base(mark, quantity, element)
        {
            ElementName = name;
            DocumentNumber = productNumber;
            MassOne = massOne;
        }

        public RebarDetailClass(string mark, string productNumber, string name, double valueElement)
            : base(mark)
        {
            ElementName = name;
            DocumentNumber = productNumber;
            MassOne = 0;
            ValueElement = valueElement;
        }

        #endregion

        #region Method

        public int CompareTo(RebarDetailClass other)
        {
            return string.Compare(ElementName, other.ElementName, StringComparison.Ordinal);
        }

        #endregion
    }
}