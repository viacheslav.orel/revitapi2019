﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autodesk.Revit.DB;
using OrslavTeam.Revit.Axillary.PublicInformation;

namespace OrslavTeam.Revit.Concrete.OTRebar.Rebars
{
    public class RebarElement : IComparable<RebarElement>
    {
        #region Attribute

        public string RebarMark { get; set; }
        public int RebarProductMixKod { get; }
        public int RebarElementKod { get; }
        public int RebarDimensionLength { get; }
        public int RebarForm { get; } = 1;
        public bool IsBackgroudRebar { get; }
        public int RebarQuantity { get; }
        public string MaterialName { get; }

        public List<ElementId> RebarElementIds { get; }

        #endregion

        #region Constructor

        //GeneralRebar
        public RebarElement(string mark, int productMixKey, int elementKey, int length, int quantity, int form,
            string materialName, Element elements)
        {
            RebarMark = mark;
            RebarProductMixKod = productMixKey;
            RebarElementKod = elementKey;
            RebarDimensionLength = length;
            RebarForm = form;
            RebarElementIds = new List<ElementId> {elements.Id};
            RebarQuantity = quantity;
            IsBackgroudRebar = false;
            MaterialName = materialName;
        }

        //GeneralRebarIsBackground
        public RebarElement(string mark, int productMixKey, int elementKey, int scraftLength, string materialName,
            Element elements)
        {
            RebarMark = mark;
            RebarProductMixKod = productMixKey;
            RebarElementKod = elementKey;

            double elLength = UnitUtils.ConvertFromInternalUnits(
                elements.get_Parameter(BuiltInParameter.REBAR_ELEM_LENGTH).AsDouble(), DisplayUnitType.DUT_MILLIMETERS);
            int elQuantity = elements.get_Parameter(BuiltInParameter.REBAR_ELEM_QUANTITY_OF_BARS).AsInteger();
            if (elLength > 12000)
                RebarDimensionLength = (int) (Math.Ceiling(elLength / 12000) * scraftLength + elLength) * elQuantity;
            else
                RebarDimensionLength = (int) (elLength * elQuantity);

            RebarQuantity = 0;
            RebarForm = 1;
            RebarElementIds = new List<ElementId> {elements.Id};
            IsBackgroudRebar = true;
            MaterialName = materialName;
        }

        //AreaReinforcement
        public RebarElement(string mark, int productMixKey, int elementKey, int length, int quantity, string materialName,
            List<Element> elements)
        {
            RebarMark = mark;
            RebarProductMixKod = productMixKey;
            RebarElementKod = elementKey;
            RebarDimensionLength = length;
            RebarQuantity = quantity;
            RebarElementIds = elements.Select(el => el.Id).ToList();
            IsBackgroudRebar = false;
            MaterialName = materialName;
        }

        //AreaReinforcementIsBackground
        public RebarElement(string mark, int productMixKey, int elementKey, int scraftLength, string materialName,
            IReadOnlyCollection<Element> elements, bool background)
        {
            RebarMark = mark;
            RebarProductMixKod = productMixKey;
            RebarElementKod = elementKey;
            RebarDimensionLength = 0;
            foreach (Element el in elements)
            {
                double elLength = UnitUtils.ConvertFromInternalUnits(
                    el.get_Parameter(BuiltInParameter.REBAR_ELEM_LENGTH).AsDouble(), DisplayUnitType.DUT_MILLIMETERS);
                int elQuantity = el.get_Parameter(BuiltInParameter.REBAR_ELEM_QUANTITY_OF_BARS).AsInteger();
                if (elLength > 12000)
                    RebarDimensionLength +=
                        (int) (Math.Ceiling(elLength / 12000) * scraftLength + elLength) * elQuantity;
                else
                    RebarDimensionLength += (int) elLength * elQuantity;
            }

            RebarQuantity = 0;
            RebarForm = 1;
            RebarElementIds = elements.Select(el => el.Id).ToList();
            IsBackgroudRebar = background;
            MaterialName = materialName;
        }

        //RebarDetail
        public RebarElement(string mark, int quantity, Element element)
        {
            RebarMark = mark;
            RebarProductMixKod = 0;
            RebarElementKod = 0;
            RebarQuantity = quantity;
            RebarForm = 0;
            RebarElementIds = new List<ElementId> {element.Id};
            IsBackgroudRebar = false;
        }

        public RebarElement(string mark)
        {
            RebarMark = mark;
            RebarProductMixKod = 0;
            RebarElementKod = 0;
            RebarQuantity = 0;
            RebarForm = 0;
            RebarElementIds = new List<ElementId>();
            IsBackgroudRebar = false;
        }

        public RebarElement(string mark, int form)
        {
            RebarMark = mark;
            RebarProductMixKod = 0;
            RebarElementKod = 0;
            RebarQuantity = 0;
            RebarForm = form;
            RebarElementIds = new List<ElementId>();
            IsBackgroudRebar = false;
        }

        #endregion

        #region Method

        public void SetMark(Document doc, string markValue)
        {
            RebarMark = markValue;
            foreach (ElementId elId in RebarElementIds)
            {
                PublicParameter.MarkText(doc.GetElement(elId), markValue);
            }
        }

        #endregion

        public int CompareTo(RebarElement other)
        {
            if (IsBackgroudRebar.CompareTo(other.IsBackgroudRebar) != 0)
                return IsBackgroudRebar.CompareTo(other.IsBackgroudRebar) * -1;
            if (IsBackgroudRebar)
            {
                return RebarProductMixKod == other.RebarProductMixKod
                    ? RebarElementKod.CompareTo(other.RebarElementKod)
                    : RebarProductMixKod.CompareTo(other.RebarProductMixKod);
            }

            if (RebarProductMixKod != other.RebarProductMixKod)
                return RebarProductMixKod.CompareTo(other.RebarProductMixKod);
            return RebarElementKod == other.RebarElementKod
                ? RebarDimensionLength.CompareTo(other.RebarDimensionLength)
                : RebarElementKod.CompareTo(other.RebarElementKod);
        }
    }
}