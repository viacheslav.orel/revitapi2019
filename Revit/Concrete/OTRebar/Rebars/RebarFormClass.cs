﻿using System;
using System.Collections.Generic;
using Autodesk.Revit.DB;

namespace OrslavTeam.Revit.Concrete.OTRebar.Rebars
{
    public class RebarFormClass : RebarElement, IComparable<RebarFormClass>
    {
        #region Attribute

        public int LengthFirst { get; }

        public int LengthSecond { get; }

        public int LengthThird { get; }

        public int LengthFourth { get; }

        public int LengthFifth { get; }

        public int LengthSixth { get; }

        public int Angel { get; }

        public int LengthDiameter { get; }

        #endregion

        #region Constructor

        public RebarFormClass(string mark, int productMixKey, int elementKey, int length, int quantity, int form,
            string materialName, Element elements, IReadOnlyList<int> dimensions)
            : base(mark, productMixKey, elementKey, length, quantity, form, materialName, elements)
        {
            LengthFirst = dimensions[0];
            LengthSecond = dimensions[1];
            LengthThird = dimensions[2];
            LengthFourth = dimensions[3];
            LengthFifth = dimensions[4];
            LengthSixth = dimensions[5];
            Angel = dimensions[6];
            LengthDiameter = dimensions[7];
        }

        #endregion

        #region Method

        public int CompareTo(RebarFormClass other)
        {
            if (LengthFirst != other.LengthFirst) return LengthFirst.CompareTo(other.LengthFirst);
            if (LengthSecond != other.LengthSecond) return LengthSecond.CompareTo(other.LengthSecond);
            if (LengthThird != other.LengthThird) return LengthThird.CompareTo(other.LengthThird);
            if (LengthFourth != other.LengthFourth) return LengthFourth.CompareTo(other.LengthFourth);
            if (LengthFifth != other.LengthFifth) return LengthFifth.CompareTo(other.LengthFifth);
            if (LengthSixth == other.LengthSixth)
                return Angel == other.Angel
                    ? LengthDiameter.CompareTo(other.LengthDiameter)
                    : Angel.CompareTo(other.Angel);

            return LengthSixth.CompareTo(other.LengthSixth);
        }

        #endregion
    }
}