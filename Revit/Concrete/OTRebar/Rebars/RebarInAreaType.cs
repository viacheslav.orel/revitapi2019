﻿using System;

namespace OrslavTeam.Revit.Concrete.OTRebar.Rebars
{
    public class RebarInAreaType
    {
        #region Guid
        
        public static Guid Form00 = new Guid("6a7e5884-ed5a-4c8c-9575-f655ab69cfdc");
        public static Guid Form01 = new Guid("1c35c1b1-5b18-4191-846c-71c802ff7186");
        // ReSharper disable once InconsistentNaming
        public static Guid Form01a = new Guid("8c722a01-2ac6-4620-98bd-0e79ba2a2a7e");

        public static Guid Form11 = new Guid("d601d012-e2be-4ae5-ab0a-b6d1eb71a521");
        public static Guid Form12 = new Guid("cc0ff334-ec2f-44f7-ba27-fa34a00c964a");
        public static Guid Form13 = new Guid("1084a881-7fc5-4aea-8aac-8ed18a928597");
        public static Guid Form14 = new Guid("e8e09a49-0c7f-4966-92f0-75e76cc8c0f3");
        public static Guid Form15 = new Guid("7412888c-c606-4b4f-a791-4fe8f3582938");

        public static Guid Form21 = new Guid("7ce24789-0736-4449-8aba-e6c3f321feb4");
        public static Guid Form22 = new Guid("04df0e94-3629-428f-8c5d-529d7d461730");
        public static Guid Form23 = new Guid("7c430a69-c1e9-49ba-a5fa-e9d733d1c1a6");
        public static Guid Form24 = new Guid("0dc4258d-dd39-426c-8a80-c5cb731522f7");
        public static Guid Form25 = new Guid("16d05531-122b-4543-80a1-0c80999eeed7");
        public static Guid Form26 = new Guid("9a15af04-dc2b-4704-b49d-7b447e299eb2");
        public static Guid Form27 = new Guid("512a0a9a-0818-428d-bff7-5e4c08a0140c");
        public static Guid Form28 = new Guid("dd389897-f500-450e-abee-df6f1e73032f");
        public static Guid Form29 = new Guid("65b54272-5e63-4166-8fdb-a34ec2050bda");

        public static Guid Form31 = new Guid("aadf2b59-9b9a-4700-8e89-8e1bbabd628f");
        public static Guid Form32 = new Guid("1266935b-66d0-4ab1-a1e1-431cef82cb13");
        public static Guid Form34 = new Guid("d54185b9-ba83-4367-9b2b-84cbe7f34032");
        public static Guid Form35 = new Guid("53ddf421-59c3-4502-a754-2fb7adf88734");
        public static Guid Form36 = new Guid("c65f05d9-20c7-4307-ac3c-2b4f2cd94b2c");

        public static Guid Form41 = new Guid("0b1a1856-4bf1-4aae-be22-45b999d4e520");
        public static Guid Form44 = new Guid("9ecffc6a-437d-44c2-9d01-740a8fe57db2");
        public static Guid Form46 = new Guid("9d7d783a-0825-4883-873f-44b252bad97d");
        public static Guid Form47 = new Guid("282e8dc4-2fa0-46a0-895f-d43de37b12c7");

        // ReSharper disable once InconsistentNaming
        public static Guid Form51a = new Guid("160108e3-bd53-413a-8d09-9dd11d76832f");
        // ReSharper disable once InconsistentNaming
        public static Guid Form51b = new Guid("9f2b3279-f768-4225-9752-3b2b3c2b7419");
        // ReSharper disable once InconsistentNaming
        public static Guid Form56a = new Guid("d94eb1d9-a504-41fa-8ae3-7cf2363f640f");
        // ReSharper disable once InconsistentNaming
        public static Guid Form56b = new Guid("7cf149fb-0060-48a3-b410-694ec3720484");

        // ReSharper disable once InconsistentNaming
        public static Guid Form64a = new Guid("e35c97fe-7f8e-4dbc-ba51-c76d896365cc");
        // ReSharper disable once InconsistentNaming
        public static Guid Form64b = new Guid("5faafd7b-11fe-4ede-b504-043f7f24fdb3");
        public static Guid Form67 = new Guid("56e91d0f-945a-4a1e-9012-0f657ae89e59");

        public static Guid Form75 = new Guid("04115020-d712-4c57-bce1-fe21516966f7");
        public static Guid Form77 = new Guid("fe2284c3-ea33-4077-b2ad-d63e03ef569a");

        public static Guid Form98 = new Guid("42046f15-765c-4063-ad80-c28aa66d1563");
        
        #endregion
    }
}