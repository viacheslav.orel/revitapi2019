﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Structure;
using Autodesk.Revit.UI;
using OrslavTeam.Revit.Axillary.PublicInformation;

namespace OrslavTeam.Revit.Concrete.OTRebar.TransferParameter
{
    [Transaction(TransactionMode.Manual)]
    public class TransferInView : IExternalCommand
    {
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            Document doc = commandData.Application.ActiveUIDocument.Document;
            FilteredElementCollector areasReinforced = new FilteredElementCollector(doc, doc.ActiveView.Id)
                .OfClass(typeof(AreaReinforcement)).WhereElementIsNotElementType();
            FilteredElementCollector pathsReinforced = new FilteredElementCollector(doc, doc.ActiveView.Id)
                .OfClass(typeof(PathReinforcement)).WhereElementIsNotElementType();
            FilteredElementCollector structuralConnectionRebar = new FilteredElementCollector(doc, doc.ActiveView.Id)
                .OfClass(typeof(FamilyInstance)).OfCategory(BuiltInCategory.OST_StructConnections);
            using (var tr = new Transaction(doc, "TransferInView"))
            {
                tr.Start();
                if (areasReinforced.Any())
                {
                    foreach (Element element in areasReinforced)
                    {
                        Axillary.TransferOtaAndTraByPathOrAreaReinforced(element, doc);
                    }
                }

                if (pathsReinforced.Any())
                {
                    foreach (Element element in pathsReinforced)
                    {
                        Axillary.TransferOtaAndTraByPathOrAreaReinforced(element, doc);
                    }
                }

                if (structuralConnectionRebar.Any())
                {
                    foreach (Element element in structuralConnectionRebar)
                    {
                        if (!Regex.IsMatch(((FamilyInstance) element).Symbol.FamilyName, @"^SRB")) continue;

                        ElementId generalRebarType = PublicParameter.GeneralRebarType(element);
                        ElementId rebarLocation = PublicParameter.RebarLocation(element);
                        
                        ICollection<ElementId> subComponentIds = (element as FamilyInstance)?.GetSubComponentIds();
                        if (subComponentIds == null) continue;
                        
                        foreach (ElementId subComponentId in subComponentIds)
                        {
                            Element subComponent = doc.GetElement(subComponentId);
                            PublicParameter.GeneralRebarType(subComponent, generalRebarType);
                            PublicParameter.RebarLocation(subComponent, rebarLocation);
                        }
                    }
                }

                tr.Commit();
            }

            return Result.Succeeded;
        }
    }
}