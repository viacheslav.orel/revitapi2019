﻿using Autodesk.Revit.DB;
using Autodesk.Revit.UI.Selection;

namespace OrslavTeam.Revit.Concrete.OTRebar.TransferParameter
{
    public class SelectPathAreaRebar : ISelectionFilter
    {
        public bool AllowElement(Element elem)
        {
            return Axillary.IsAreaPathReinforced(elem);
        }

        public bool AllowReference(Reference reference, XYZ position)
        {
            return false;
        }
    }
}