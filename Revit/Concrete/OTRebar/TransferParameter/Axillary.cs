﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Structure;
using OrslavTeam.Revit.Axillary.PublicInformation;

namespace OrslavTeam.Revit.Concrete.OTRebar.TransferParameter
{
    public static class Axillary
    {
        public static void TransferOtaAndTraByPathOrAreaReinforced(Element el, Document doc)
        {
            ElementId otaId = el.LookupParameter("_Основний тип арматури").AsElementId();
            ElementId traId = el.LookupParameter("_Розміщення арматури").AsElementId();
            IEnumerable<Element> subElement;
            try
            {
                subElement = ((AreaReinforcement) el).GetRebarInSystemIds().Select(doc.GetElement);
            }
            catch (Exception)
            {
                subElement = ((PathReinforcement) el).GetRebarInSystemIds().Select(doc.GetElement);
            }
            foreach (Element element in subElement)
            {
                element.LookupParameter("_Основний тип арматури").Set(otaId);
                element.LookupParameter("_Розміщення арматури").Set(traId);
                PublicParameter.ElementGuid(element, "InPathOrAreaReinforced");
            }
        }
        public static bool IsAreaPathReinforced(Element el)
        {
            int hashCategory = el.Category.GetHashCode();
            return hashCategory == -2009009 || hashCategory == -2009003;
        }
    }
}