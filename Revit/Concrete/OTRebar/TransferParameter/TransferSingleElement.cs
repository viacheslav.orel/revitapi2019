﻿using System;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using Autodesk.Revit.UI.Selection;

namespace OrslavTeam.Revit.Concrete.OTRebar.TransferParameter
{
    [Transaction(TransactionMode.Manual)]
    public class TransferSingleElement : IExternalCommand
    {
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            UIApplication uiApp = commandData.Application;
            Document doc = uiApp.ActiveUIDocument.Document;
            Selection sel = uiApp.ActiveUIDocument.Selection;
            SelectPathAreaRebar selectPathAreaRebar = new SelectPathAreaRebar();
            do
            {
                Reference pickedPathOrArea;
                try
                {
                    pickedPathOrArea = sel.PickObject(ObjectType.Element, selectPathAreaRebar,
                        "Оберіть армування за площею чи за траекторіею");
                }
                catch (Exception)
                {
                    return Result.Succeeded;
                }

                Element el = doc.GetElement(pickedPathOrArea);
                using (Transaction tr = new Transaction(doc, "PathAreaRebar"))
                {
                    tr.Start();
                    Axillary.TransferOtaAndTraByPathOrAreaReinforced(el, doc);
                    tr.Commit();
                }
            } while (true);
        }
    }
}