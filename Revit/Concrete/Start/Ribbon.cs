﻿using System;
using System.Windows.Media.Imaging;
using Autodesk.Revit.UI;
using OrslavTeam.Revit.Axillary.PublicInformation;
using OrslavTeam.Revit.Concrete.Element.Mark;
using OrslavTeam.Revit.Concrete.Element.Pile;
using MainWindow = OrslavTeam.Revit.Concrete.Element.Mark.MainWindow;

namespace OrslavTeam.Revit.Concrete.Start
{
    public class Ribbon : IExternalApplication
    {
        internal static Ribbon ThisApp = null;
        
        public Result OnStartup(UIControlledApplication application)
        {
            #region config ---------------------------------------------------------------------------------------------

            ThisApp = this;
            if (System.Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties().DomainName.ToLower() ==
                "adept-group.biz") return Result.Succeeded;

            //CreateRibbonTab
            const string concreteTabName = "otConcrete";
            application.CreateRibbonTab(concreteTabName);

            #endregion


            #region concrete panel -------------------------------------------------------------------------------------

            // button data ---------------------------------------------------------------------------------------------

            var pileMarkData = new PushButtonData("pileMarkData", "Нумерація\nпаль",
                PublicParameter.FilePath + "OrslavTeam.Revit.Concrete.Start.dll",
                "OrslavTeam.Revit.Concrete.Start.StartCommand.PileMarkCommandStart")
            {
                LargeImage = new BitmapImage(new Uri($"{PublicParameter.FilePath}res\\Concrete\\btn_ico\\ico_PileMark.png")),
                ToolTip = "Запуск меню марквання та нумерації паль"
            };

            var concreteMarkData = new PushButtonData("firstConcreteMarkData", "Маркувати\n(рівень 1)",
                PublicParameter.FilePath + "OrslavTeam.Revit.Concrete.Component.Worker.dll",
                "OrslavTeam.Revit.Concrete.Component.Worker.AddIns.SetConcreteMark")
            {
                ToolTip = "Призначена для маркування всіх бетонних елементів на виді відповідно до типів",
                LargeImage = new BitmapImage(new Uri($"{PublicParameter.FilePath}res\\Concrete\\btn_ico\\ico_ConcreteMark.png"))
            };

            var setComponentVolume = new PushButtonData("setComponentVolume", "Призначити\nоб'єм",
                PublicParameter.FilePath + "OrslavTeam.Revit.Concrete.Component.Worker.dll",
                "OrslavTeam.Revit.Concrete.Component.Worker.AddIns.SetConcreteMassBySchedule")
            {
                ToolTip = "Внести значення об'єму для бетонних компонентів у специфікації",
                LargeImage = new BitmapImage(new Uri($"{PublicParameter.FilePath}res\\Concrete\\btn_ico\\setVolume.png"))
            };


            // panel ---------------------------------------------------------------------------------------------------

            RibbonPanel concretePanel = application.CreateRibbonPanel(concreteTabName, "Бетонні елементи");

            concretePanel.AddItem(pileMarkData);
            concretePanel.AddSeparator();

            concretePanel.AddItem(setComponentVolume);
            concretePanel.AddSeparator();

            concretePanel.AddItem(concreteMarkData);

            #endregion


            #region rebar base panel -----------------------------------------------------------------------------------

            // button data ---------------------------------------------------------------------------------------------
            var transferOtaFromPathORebarAsSingleData = new PushButtonData(
                "transferOtaFromPathORebarAsSingleData", "ОТА&ТРА\nодин елемент",
                PublicParameter.FilePath + "OrslavTeam.Revit.Concrete.OTRebar.TransferParameter.dll",
                "OrslavTeam.Revit.Concrete.OTRebar.TransferParameter.TransferSingleElement")
            {
                ToolTip = "Призначена для перенесання параметрів з армування за площею (траекторією) для одного елементу",
                LargeImage = new BitmapImage(new Uri($"{PublicParameter.FilePath}res\\Concrete\\btn_ico\\ico_PathOrAreaReinforced.png"))
            };

            var transferOtaFromPathORebarInViewData = new PushButtonData(
                "transferOtaFromPathORebarInViewData", "ОТА&ТРА\nна виді",
                PublicParameter.FilePath + "OrslavTeam.Revit.Concrete.OTRebar.TransferParameter.dll",
                "OrslavTeam.Revit.Concrete.OTRebar.TransferParameter.TransferInView")
            {
                ToolTip = "Призначена для перенесання параметрів з армування за площею (траекторією) на поточному виді",
                LargeImage = new BitmapImage(new Uri($"{PublicParameter.FilePath}res\\Concrete\\btn_ico\\ico_PathOrAreaReinforced.png"))
            };

            var setRebarHostData = new PushButtonData("setRebarHostData", "Призначити\nоснову",
                PublicParameter.FilePath + "OrslavTeam.Revit.Concrete.OTReabar.Worker.dll",
                "OrslavTeam.Revit.Concrete.OTReabar.Worker.AddIns.SetElementHost")
            {
                ToolTip = "Прив'язати марку основи арматурних виробів до бетонного елементу",
                LargeImage = new BitmapImage(new Uri($"{PublicParameter.FilePath}res\\Concrete\\btn_ico\\setRebarHost.png"))
            };

            var setFullNameByView = new PushButtonData("setFullNameByView", "Оновити арм.",
                PublicParameter.FilePath + "OrslavTeam.Revit.Concrete.OTReabar.Worker.dll",
                "OrslavTeam.Revit.Concrete.OTReabar.Worker.AddIns.SetRebarNameByView")
            {
                ToolTip = "Оновити дані по арматурі на поточному виді",
                LargeImage = new BitmapImage(new Uri($"{PublicParameter.FilePath}res\\Concrete\\btn_ico\\setFullName.png"))
            };

            var createRebarScheduleData = new PushButtonData("createRebarScheduleData",
                "Відомість\nарматури",
                PublicParameter.FilePath + "OrslavTeam.Revit.Concrete.OTReabar.Worker.dll",
                "OrslavTeam.Revit.Concrete.OTReabar.Worker.AddIns.CreateSteelSpreadSchedule")
            {
                ToolTip = "Створити відомість витрат сталі",
                LargeImage = new BitmapImage(new Uri($"{PublicParameter.FilePath}res\\Concrete\\btn_ico\\ico_RebarSchedulesForOne.png"))
            };

            var setRebarMarkData = new PushButtonData("setRebarMark", "Маркувати",
                PublicParameter.FilePath + "OrslavTeam.Revit.Concrete.OTReabar.Worker.dll",
                "OrslavTeam.Revit.Concrete.OTReabar.Worker.AddIns.SerRebarMark")
            {
                ToolTip = "Промаркувати арматуру у специфікації",
                LargeImage = new BitmapImage(new Uri($"{PublicParameter.FilePath}res\\Concrete\\btn_ico\\rebarMark.png"))
            };

            var createRebarGroup = new PushButtonData("createRebarGroup", "Сворити групу\nарматури",
                PublicParameter.FilePath + "OrslavTeam.Revit.Concrete.OTReabar.Worker.dll",
                "OrslavTeam.Revit.Concrete.OTReabar.Worker.AddIns.AddRebarToGroup")
            {
                ToolTip = "Сворити групу арматури що відноситься до конструктивного елементу",
                LargeImage = new BitmapImage(new Uri($"{PublicParameter.FilePath}res\\Concrete\\btn_ico\\concreteCreateGroup.png")),
            };

            var setRebarHostByGroup = new PushButtonData("setRebarHostByGroup", "Призначити основу\nдля груп",
                PublicParameter.FilePath + "OrslavTeam.Revit.Concrete.OTReabar.Worker.dll",
                "OrslavTeam.Revit.Concrete.OTReabar.Worker.AddIns.SetElementHostByGroup")
            {
                ToolTip = "Прив'язати марку основи арматурних виробів до бетонного елементу для груп арматури",
                LargeImage = new BitmapImage(new Uri($"{PublicParameter.FilePath}res\\Concrete\\btn_ico\\setRebarHostByGroup.png")),
            };


            // panel ---------------------------------------------------------------------------------------------------
            RibbonPanel rebarPanel = application.CreateRibbonPanel(concreteTabName, "Арматурні елементи");

            rebarPanel.AddItem(transferOtaFromPathORebarAsSingleData);
            rebarPanel.AddItem(transferOtaFromPathORebarInViewData);
            rebarPanel.AddSeparator();

            rebarPanel.AddItem(setRebarHostData);
            rebarPanel.AddItem(setFullNameByView);
            rebarPanel.AddItem(setRebarMarkData);
            rebarPanel.AddItem(createRebarScheduleData);
            rebarPanel.AddSeparator();

            rebarPanel.AddItem(createRebarGroup);
            rebarPanel.AddItem(setRebarHostByGroup);

            #endregion

            return Result.Succeeded;
        }

        public Result OnShutdown(UIControlledApplication application)
        {
            ThisApp = null;
            return Result.Succeeded;
        }
        
        #region ShowView
        
        public static void PileMarkShowView()
        {
            PileMarkEventHandler.Hadler = new PileMarkEventHandler();
            PileMarkEventHandler.ExEvent = ExternalEvent.Create(PileMarkEventHandler.Hadler);
            var pileMarkView = new Element.Pile.MainWindow();
            pileMarkView.Show();
        }

        #endregion
    }
}