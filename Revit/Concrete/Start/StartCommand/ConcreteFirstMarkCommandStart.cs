﻿using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;

namespace OrslavTeam.Revit.Concrete.Start.StartCommand
{
    [Transaction(TransactionMode.Manual)]
    public class ConcreteFirstMarkCommandStart : IExternalCommand
    {
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            Ribbon.ConcreteMarkShowView();
            return Result.Succeeded;
        }
    }
}