﻿using System.Diagnostics;
using System.Windows;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;

namespace OrslavTeam.Revit.Concrete.Start.StartCommand
{
    [Transaction(TransactionMode.Manual)]
    public class PileMarkCommandStart : IExternalCommand
    {
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            Ribbon.PileMarkShowView();
            return Result.Succeeded;
        }
    }
}