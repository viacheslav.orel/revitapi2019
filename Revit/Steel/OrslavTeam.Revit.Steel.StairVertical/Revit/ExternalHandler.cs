﻿using Autodesk.Revit.UI;
using OrslavTeam.Revit.Steel.Axillary.Repository.ComponentComplex;
using OrslavTeam.Revit.Steel.Axillary.Repository.ComponentComplex.StairVertical;
using OrslavTeam.Revit.Steel.StairVertical.View;

namespace OrslavTeam.Revit.Steel.StairVertical.Revit
{
    public class ExternalHandler : IExternalEventHandler
    {
        #region Fields

        public static ExternalHandler Handler;
        public static ExternalEvent ExEvent;

        private static MainWindow _stairVerticalParameterView;

        public static StairVerticalComponent StairVerticalComponent;

        public delegate void RevitCommandDelegate(UIApplication uiApp);
        public static RevitCommandDelegate RevitCommand { get; set; }

        #endregion

        public static void ShowDialog()
        {
            _stairVerticalParameterView = new MainWindow();
            _stairVerticalParameterView.Show();
        }

        #region Implementation of IExternalEventHandler

        public void Execute(UIApplication app)
        {
            RevitCommand(app);
            ExEvent.Dispose();
            ExEvent = null;
            Handler = null;

            _stairVerticalParameterView.Close();
            _stairVerticalParameterView = null;
        }

        public string GetName()
        {
            return nameof(ExternalHandler);
        }

        #endregion
    }
}