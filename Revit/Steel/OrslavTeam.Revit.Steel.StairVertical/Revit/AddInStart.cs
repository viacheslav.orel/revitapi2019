﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using Autodesk.Revit.UI.Selection;
using Newtonsoft.Json;
using OrslavTeam.Revit.Axillary.PublicInformation;
using OrslavTeam.Revit.Steel.Axillary.Repository.ComponentComplex;
using OrslavTeam.Revit.Steel.Axillary.Repository.ComponentComplex.StairVertical;
using OrslavTeam.Revit.Steel.Axillary.SelectionFilters;
using EH = OrslavTeam.Revit.Steel.StairVertical.Revit.ExternalHandler;

namespace OrslavTeam.Revit.Steel.StairVertical.Revit
{
    [Transaction(TransactionMode.Manual)]
    public class AddInStart : IExternalCommand
    {
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            UIApplication uiApp = commandData.Application;
            Document doc = uiApp.ActiveUIDocument.Document;
            Selection sel = uiApp.ActiveUIDocument.Selection;

            var isStairVertical = new IsStairVertical();
            ICollection<ElementId> stairVerticalCollection = sel.GetElementIds();

            Element selectedSymbol;

            if (stairVerticalCollection.Count > 0)
            {
                Element selectedElement = doc.GetElement(stairVerticalCollection.FirstOrDefault());
                selectedSymbol = (selectedElement as FamilyInstance)?.Symbol ?? selectedElement;

                if (!isStairVertical.AllowElement(selectedElement))
                    return Result.Failed;
            }
            else
            {
                Reference pickedStairVertical;
                do
                {
                    try
                    {
                        pickedStairVertical = sel.PickObject(ObjectType.Element, isStairVertical,
                            "Оберіть вертикальні сходи");
                    }
                    catch (Exception)
                    {
                        return Result.Cancelled;
                    }
                } while (pickedStairVertical == null);

                selectedSymbol = ((FamilyInstance) doc.GetElement(pickedStairVertical)).Symbol;
            }

            string getIt = PublicParameter.GetIt(selectedSymbol);
            if (getIt != "")
            {
                try
                {
                    ExternalHandler.StairVerticalComponent =
                        JsonConvert.DeserializeObject<StairVerticalComponent>(getIt);
                }
                catch (Exception)
                {
                    ExternalHandler.StairVerticalComponent = new StairVerticalComponent().GetStairVerticalComponent;
                }
            }
            
            ExternalHandler.StairVerticalComponent.RvtFamily = (FamilySymbol) selectedSymbol;

            ExternalHandler.Handler = new ExternalHandler();
            ExternalHandler.ExEvent = ExternalEvent.Create(ExternalHandler.Handler);
            ExternalHandler.ShowDialog();

            return Result.Succeeded;
        }
    }
}