﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Autodesk.Revit.DB;
using OrslavTeam.Revit.Axillary.ViewTemplate.Infrastructure;
using OrslavTeam.Revit.Steel.Axillary.Repository.ComponentSimple;
using OrslavTeam.Revit.Steel.Axillary.Repository.Primitive;
using OrslavTeam.Revit.Steel.StairVertical.Revit;
using EH = OrslavTeam.Revit.Steel.StairVertical.Revit.ExternalHandler;

namespace OrslavTeam.Revit.Steel.StairVertical.ViewModel
{
    public class MainWindow_ViewModel : ObservableObject
    {
        #region Fields

        private string _elementName;

        private SimpleComponent _currentComponent;

        private ObservableCollection<OtProductMix> _productMixCollection;
        private OtProductMix _productMixCollectionSelectedItem;

        private ObservableCollection<OtElement> _elementCollection;
        private OtElement _elementCollectionSelectedItem;

        private ObservableCollection<OtMaterial> _materialCollection;
        private OtMaterial _materialCollectionSelectedItem;

        #endregion

        #region FieldsAccess

        //Stair
        public int StairsWidth
        {
            get => EH.StairVerticalComponent.Width;
            set
            {
                EH.StairVerticalComponent.Width = value;
                RaisePropertyChangedEvent(nameof(StairsWidth));
            }
        }

        public int SupportSpacing
        {
            get => EH.StairVerticalComponent.BaseOffset;
            set
            {
                EH.StairVerticalComponent.BaseOffset = value;
                RaisePropertyChangedEvent(nameof(SupportSpacing));
            }
        }

        public int HandrailHeight
        {
            get => EH.StairVerticalComponent.HandRailingBinding;
            set
            {
                EH.StairVerticalComponent.HandRailingBinding = value;
                RaisePropertyChangedEvent(nameof(HandrailHeight));

                if (value == 0)
                    TopSupportNeed = false;
            }
        }

        //Stair--Column
        public string ColumnComponentName =>
            $"Стійка сходів (1):\n{EH.StairVerticalComponent.GetSupportBaseComponent.Element.Name} " +
            $"({EH.StairVerticalComponent.GetSupportBaseComponent.Material.Name})";

        //Stair--Steps
        public int StepsStep
        {
            get => EH.StairVerticalComponent.StepsStep;
            set
            {
                EH.StairVerticalComponent.StepsStep = value;
                RaisePropertyChangedEvent(nameof(StepsStep));
            }
        }

        public int StepsOffset
        {
            get => EH.StairVerticalComponent.GetStepComponent.SupportBinding;
            set
            {
                EH.StairVerticalComponent.StepsComponent.SupportBinding = value;
                RaisePropertyChangedEvent(nameof(StepsOffset));
            }
        }

        public string StepsProfileName =>
            $"Сходинка (4):\n{EH.StairVerticalComponent.GetStepComponent.BaseComponent.Element.Name} " +
            $"({EH.StairVerticalComponent.GetStepComponent.BaseComponent.Material.Name})";

        //Stair--TopSupport
        public bool TopSupportNeed
        {
            get => EH.StairVerticalComponent.SupportTopNeed;
            set
            {
                EH.StairVerticalComponent.SupportTopNeed = value;
                RaisePropertyChangedEvent(nameof(TopSupportNeed));
            }
        }

        public int TopSupportLength
        {
            get => EH.StairVerticalComponent.GetTopSupportComponent.Length;
            set
            {
                EH.StairVerticalComponent.SupportTopComponent.Length = value;
                RaisePropertyChangedEvent(nameof(TopSupportLength));
            }
        }

        public string SupportTopProfileName =>
            $"Верхня опора (2):\n{EH.StairVerticalComponent.GetTopSupportComponent.Element.Name} " +
            $"({EH.StairVerticalComponent.GetTopSupportComponent.Material.Name})";

        //Stair--BottomSupport
        public bool BottomSupportNeed
        {
            get => EH.StairVerticalComponent.SupportBottomNeed;
            set
            {
                EH.StairVerticalComponent.SupportBottomNeed = value;
                RaisePropertyChangedEvent(nameof(BottomSupportNeed));
                RaisePropertyChangedEvent(nameof(BottomSupportAnchorNeed));
            }
        }

        public int BottomSupportWidth
        {
            get => EH.StairVerticalComponent.GetSupportBottomComponent.Width;
            set
            {
                EH.StairVerticalComponent.SupportBottomComponent.Width = value;
                RaisePropertyChangedEvent(nameof(BottomSupportWidth));
            }
        }

        public int BottomSupportDepth
        {
            get => EH.StairVerticalComponent.GetSupportBottomComponent.Depth;
            set
            {
                EH.StairVerticalComponent.SupportBottomComponent.Depth = value;
                RaisePropertyChangedEvent(nameof(BottomSupportDepth));
            }
        }

        public int BottomSupportOffsetForWidth
        {
            get => EH.StairVerticalComponent.SupportBottomBindingWidth;
            set
            {
                EH.StairVerticalComponent.SupportBottomBindingWidth = value;
                RaisePropertyChangedEvent(nameof(BottomSupportOffsetForWidth));
            }
        }

        public int BottomSupportOffsetForDepth
        {
            get => EH.StairVerticalComponent.SupportBottomBindingDepth;
            set
            {
                EH.StairVerticalComponent.SupportBottomBindingDepth = value;
                RaisePropertyChangedEvent(nameof(BottomSupportOffsetForDepth));
            }
        }

        public string BottomSupportProfileName =>
            $"Опорна плита (3):\n{EH.StairVerticalComponent.GetSupportBottomComponent.Element.Name} " +
            $"({EH.StairVerticalComponent.GetSupportBottomComponent.Material.Name})";

        //Stair--BottomSupport--Anchor
        public bool BottomSupportAnchorNeed
        {
            get => EH.StairVerticalComponent.SupportBottomAnchorNeed && EH.StairVerticalComponent.SupportBottomNeed;
            set
            {
                EH.StairVerticalComponent.SupportBottomAnchorNeed = value;
                RaisePropertyChangedEvent(nameof(BottomSupportAnchorNeed));
            }
        }

        public int BottomSupportAnchorOffsetForWidth
        {
            get => EH.StairVerticalComponent.SupportBottomAnchorBindingWidth;
            set
            {
                EH.StairVerticalComponent.SupportBottomAnchorBindingWidth = value;
                RaisePropertyChangedEvent(nameof(BottomSupportAnchorOffsetForWidth));
            }
        }

        public int BottomSupportAnchorOffsetForDepth
        {
            get => EH.StairVerticalComponent.SupportBottomAnchorBindingDepth;
            set
            {
                EH.StairVerticalComponent.SupportBottomAnchorBindingDepth = value;
                RaisePropertyChangedEvent(nameof(BottomSupportAnchorOffsetForDepth));
            }
        }

        //Railing
        public bool CageNeed
        {
            get => EH.StairVerticalComponent.RailingNeed;
            set
            {
                EH.StairVerticalComponent.RailingNeed = value;
                RaisePropertyChangedEvent(nameof(CageNeed));
            }
        }

        public int CageSupportDepth
        {
            get => EH.StairVerticalComponent.RailingSupportBinding;
            set
            {
                EH.StairVerticalComponent.RailingSupportBinding = value;
                RaisePropertyChangedEvent(nameof(CageSupportDepth));
            }
        }

        public int CageDepth
        {
            get => EH.StairVerticalComponent.RailingDepth;
            set
            {
                EH.StairVerticalComponent.RailingDepth = value;
                RaisePropertyChangedEvent(nameof(CageDepth));
            }
        }

        public int CageHorizontallyElementStep
        {
            get => EH.StairVerticalComponent.RailingHorizontalStep;
            set
            {
                EH.StairVerticalComponent.RailingHorizontalStep = value;
                RaisePropertyChangedEvent(nameof(CageHorizontallyElementStep));
            }
        }

        public int CageBottomOffset
        {
            get => EH.StairVerticalComponent.RailingBindingBottom;
            set
            {
                EH.StairVerticalComponent.RailingBindingBottom = value;
                RaisePropertyChangedEvent(nameof(CageBottomOffset));
            }
        }

        public int CageTopOffset
        {
            get => EH.StairVerticalComponent.RailingBindingTop;
            set
            {
                EH.StairVerticalComponent.RailingBindingTop = value;
                RaisePropertyChangedEvent(nameof(CageTopOffset));
            }
        }

        //Railing--Baluster
        public string RailingHorizontalProfileName =>
            $"Горизонтальний елемент огорожі (6):\n" +
            $"{EH.StairVerticalComponent.GetRailingHorizontalComponent.Element.Name} " +
            $"({EH.StairVerticalComponent.GetRailingHorizontalComponent.Material.Name})";

        //Railing--Rail
        public string RailingVerticalProfileName =>
            $"Вертикальний елемент огорожі (5):\n" +
            $"{EH.StairVerticalComponent.GetRailingVerticalComponent.Element.Name} " +
            $"({EH.StairVerticalComponent.GetRailingVerticalComponent.Material.Name})";

        //Railing--Axillary
        public string RailingBindingProfileName =>
            $"Додатковий елемент огорожі (7):\n" +
            $"{EH.StairVerticalComponent.GetRailingBindingComponent.Element.Name} " +
            $"({EH.StairVerticalComponent.GetRailingBindingComponent.Material.Name})";

        #endregion

        #region CollectionAccess

        //ProductMix
        public ObservableCollection<OtProductMix> ProductMixCollection
        {
            get => _productMixCollection;
            set
            {
                _productMixCollection = value;
                RaisePropertyChangedEvent(nameof(ProductMixCollection));
            }
        }

        public OtProductMix ProductMixCollectionSelectedItem
        {
            get => _productMixCollectionSelectedItem;
            set
            {
                _productMixCollectionSelectedItem = value;
                RaisePropertyChangedEvent(nameof(ProductMixCollectionSelectedItem));

                ElementCollection = OtRequest.GetElementCollection(ProductMixCollectionSelectedItem.Id);
            }
        }

        //ElementCollection
        public ObservableCollection<OtElement> ElementCollection
        {
            get => _elementCollection;
            set
            {
                _elementCollection = value;
                RaisePropertyChangedEvent(nameof(ElementCollection));

                ElementCollectionSelectedItem =
                    _elementCollection.FirstOrDefault(el => el.Id == _currentComponent.Element.Id);
            }
        }

        public OtElement ElementCollectionSelectedItem
        {
            get => _elementCollectionSelectedItem;
            set
            {
                _elementCollectionSelectedItem = value;
                RaisePropertyChangedEvent(nameof(ElementCollectionSelectedItem));
            }
        }

        //MaterialCollection
        public ObservableCollection<OtMaterial> MaterialCollection
        {
            get => _materialCollection ?? (_materialCollection = OtRequest.GetMaterialCollection());
            set
            {
                _materialCollection = value;
                RaisePropertyChangedEvent(nameof(MaterialCollection));
            }
        }

        public OtMaterial MaterialCollectionSelectedItem
        {
            get => _materialCollectionSelectedItem;
            set
            {
                _materialCollectionSelectedItem = value;
                RaisePropertyChangedEvent(nameof(MaterialCollectionSelectedItem));
            }
        }

        #endregion

        #region Metods

        private void SetCollection(params int [] productMixIds)
        {
            //ProductMix
            ProductMixCollection = OtRequest.GetProductMixCollection(productMixIds);
            ProductMixCollectionSelectedItem = _productMixCollection
                .FirstOrDefault(pm => pm.Id == _currentComponent.ProductMix.Id);

            //Element
            ElementCollectionSelectedItem =
                ElementCollection.FirstOrDefault(element => element.Id == _currentComponent.Element.Id);
            MaterialCollectionSelectedItem = MaterialCollection.FirstOrDefault(material =>
                material.Id == _currentComponent.Material.Id);
        }

        #endregion

        #region Command

        //Stair
        private DelegateCommand _getCollectionForColumn;

        public ICommand GetCollectionForColumn =>
            _getCollectionForColumn ?? (_getCollectionForColumn = new DelegateCommand(o =>
            {
                _currentComponent = EH.StairVerticalComponent.GetSupportBaseComponent;
                _elementName = nameof(ColumnComponentName);
                SetCollection(201, 202, 203);
            }));

        //Stair--SupportTop
        private DelegateCommand _getCollectionForTopSupport;

        public ICommand GetCollectionForTopSupport =>
            _getCollectionForTopSupport ?? (_getCollectionForTopSupport = new DelegateCommand(o =>
            {
                _currentComponent = EH.StairVerticalComponent.GetTopSupportComponent;
                _elementName = nameof(SupportTopProfileName);
                SetCollection(201, 202, 203);
            }));

        //Stair--SupportBottom
        private DelegateCommand _getCollectionForSupportBottom;

        public ICommand GetCollectionForSupportBottom =>
            _getCollectionForSupportBottom ?? (_getCollectionForSupportBottom = new DelegateCommand(o =>
            {
                _currentComponent = EH.StairVerticalComponent.GetSupportBottomComponent;
                _elementName = nameof(BottomSupportProfileName);
                SetCollection(604, 622);
            }));

        //Stair--Steps
        private DelegateCommand _getCollectionForSteps;

        public ICommand GetCollectionForSteps =>
            _getCollectionForSteps ?? (_getCollectionForSteps = new DelegateCommand(o =>
            {
                _currentComponent = EH.StairVerticalComponent.GetStepComponent.BaseComponent;
                _elementName = nameof(StepsProfileName);
                SetCollection(310, 312);
            }));

        //Railing--Vertical
        private DelegateCommand _getCollectionForRail;

        public ICommand GetCollectionForRailingVertical =>
            _getCollectionForRail ?? (_getCollectionForRail = new DelegateCommand(o =>
            {
                _currentComponent = EH.StairVerticalComponent.GetRailingVerticalComponent;
                _elementName = nameof(RailingVerticalProfileName);
                SetCollection(605);
            }));

        //Railing--Baluster
        private DelegateCommand _getCollectionForBaluster;

        public ICommand GetCollectionForRailingHorizontal =>
            _getCollectionForBaluster ?? (_getCollectionForBaluster = new DelegateCommand(o =>
            {
                _currentComponent = EH.StairVerticalComponent.GetRailingHorizontalComponent;
                _elementName = nameof(RailingHorizontalProfileName);
                SetCollection(605);
            }));

        //Railing--Axillary
        private DelegateCommand _getCollectionForAxillary;

        public ICommand GetCollectionForRailingBinding =>
            _getCollectionForAxillary ?? (_getCollectionForAxillary = new DelegateCommand(o =>
            {
                _currentComponent = EH.StairVerticalComponent.GetRailingBindingComponent;
                _elementName = nameof(RailingBindingProfileName);
                SetCollection(605);
            }));

        //ApplyCollection
        private DelegateCommand _saveChangeCommand;

        public ICommand SaveChangeCommand =>
            _saveChangeCommand ?? (_saveChangeCommand = new DelegateCommand(o =>
            {
                
                _currentComponent.ProductMix = (OtProductMix) ProductMixCollectionSelectedItem.Clone();
                _currentComponent.Element = OtRequest.GetComponentInfo(ProductMixCollectionSelectedItem.Id,
                    ElementCollectionSelectedItem.Id);
                _currentComponent.Material = (OtMaterial) MaterialCollectionSelectedItem.Clone();

                RaisePropertyChangedEvent(_elementName);

                _currentComponent = null;
                _productMixCollection = null;
                _productMixCollectionSelectedItem = null;
                _elementCollection = null;
                _elementCollectionSelectedItem = null;
                _elementName = null;
            }));

        //SaveToRvtElement
        private DelegateCommand _setToRvtElement;

        public ICommand SetToRvtElement =>
            _setToRvtElement ?? (_setToRvtElement = new DelegateCommand(o =>
            {
                ExternalHandler.RevitCommand = app =>
                {
                    Document doc = app.ActiveUIDocument.Document;

                    using (var tr = new Transaction(doc, "SetParameterToVerticalStairElement"))
                    {
                        tr.Start();
                        EH.StairVerticalComponent.SetGeometryToRevit(
                            new FilteredElementCollector(doc).OfCategory(BuiltInCategory.OST_Materials));
                        tr.Commit();
                    }
                };
                ExternalHandler.ExEvent.Raise();
            }));

        #endregion
    }
}