﻿using System.Windows;
using System.Windows.Controls;
using OrslavTeam.Revit.Steel.IBeam.ViewModel;

namespace OrslavTeam.Revit.Steel.IBeam.View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class EditComplexIBeamView
    {
        private EditComplexIBeamViewModel _vm;
        public EditComplexIBeamView()
        {
            InitializeComponent();
            _vm = (EditComplexIBeamViewModel) DataContext;
        }

        private void EditBeamComponent_ButtonClick(object sender, RoutedEventArgs e)
        {
            _vm.GetBeamComponentProperties.Execute(((Button) sender).DataContext);
        }

        private void EditColumnProperties_ButtonClick(object sender, RoutedEventArgs e)
        {
            _vm.GetColumnComponentProperties.Execute(((Button) sender).DataContext);
        }
    }
}