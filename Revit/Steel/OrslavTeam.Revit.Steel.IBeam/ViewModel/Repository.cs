﻿using OrslavTeam.Revit.Steel.Axillary.Repository;

namespace OrslavTeam.Revit.Steel.IBeam.ViewModel
{
    public static class Repository
    {
        #region Fields

        public static OtIBeam[] BeamComponentCollection { get; set; }
        public static OtIBeam[] ColumnComponentCollection { get; set; }

        #endregion
    }
}