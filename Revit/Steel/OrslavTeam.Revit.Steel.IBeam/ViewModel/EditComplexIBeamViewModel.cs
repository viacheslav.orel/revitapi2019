﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Autodesk.Revit.DB;
using OrslavTeam.Revit.Axillary.ViewTemplate.Infrastructure;
using OrslavTeam.Revit.Steel.Axillary;
using OrslavTeam.Revit.Steel.Axillary.Repository;
using OrslavTeam.Revit.Steel.Axillary.Repository.Primitive;
using OrslavTeam.Revit.Steel.IBeam.Revit;
using RP = OrslavTeam.Revit.Steel.IBeam.ViewModel.Repository;
using Visibility = System.Windows.Visibility;

namespace OrslavTeam.Revit.Steel.IBeam.ViewModel
{
    public class EditComplexIBeamViewModel : ObservableObject
    {
        #region Fields

        //Base
        private ObservableCollection<OtProfile> _elementsCollection;
        private static readonly OtProductMix _productMix = new OtProductMix
        {
            Id = 604,
            Name = "Прокат листовий гарячекатаний",
            Number = "ДСТУ 8540:2015"
        };

        //Beam
        private OtIBeam _currentBeamComponent;

        private ObservableCollection<OtIBeam> _beamComponents;
        private OtIBeamComponent _beamWebElementCollectionSelected;
        private OtIBeamComponent _beamChordElementCollectionSelected;

        //Column
        private OtIBeam _currentColumnComponent;
        private ObservableCollection<OtIBeam> _columnComponents;
        private OtIBeamComponent _columnWebCollectionSelected;
        private OtIBeamComponent _columnChordCollectionSelected;

        #endregion

        #region FieldsAccess

        //Base
        public ObservableCollection<OtProfile> ElementCollection =>
            _elementsCollection ?? (_elementsCollection = RevitDb.GetProfileCollection(604));

        //Beam
        public ObservableCollection<OtIBeam> BeamComponents
        {
            get
            {
                if (_beamComponents != null) return _beamComponents;

                _beamComponents = new ObservableCollection<OtIBeam>(RP.BeamComponentCollection.OrderBy(elem => elem.Name));
                return _beamComponents;
            }
            set
            {
                _beamComponents = value;
                RaisePropertyChangedEvent(nameof(BeamComponents));
                RaisePropertyChangedEvent(nameof(BeamTabVisible));
            }
        }

        public Visibility BeamTabVisible => BeamComponents.Count > 0 ? Visibility.Visible : Visibility.Collapsed;

        public OtProfile BeamWebElementCollectionSelected
        {
            get => _beamWebElementCollectionSelected.Profile ??
                   (_beamWebElementCollectionSelected.Profile = _elementsCollection.FirstOrDefault(elem => 
                       elem.Id == (_currentBeamComponent.BeamWeb.Profile?.Id ?? 21)));
            set
            {
                _beamWebElementCollectionSelected.Profile = value;
                RaisePropertyChangedEvent(nameof(BeamWebElementCollectionSelected));
            }
        }

        public OtProfile BeamChordElementCollectionSelected
        {
            get => _beamChordElementCollectionSelected.Profile ??
                   (_beamWebElementCollectionSelected.Profile = _elementsCollection
                       .FirstOrDefault(elem => elem.Id == (_currentBeamComponent.BeamChord.Profile?.Id ?? 21)));
            set
            {
                _beamChordElementCollectionSelected.Profile = value;
                RaisePropertyChangedEvent(nameof(BeamChordElementCollectionSelected));
            }
        }

        public int BeamWebWidth
        {
            get => _beamWebElementCollectionSelected.Width;
            set
            {
                _beamWebElementCollectionSelected.Width = value;
                RaisePropertyChangedEvent(nameof(BeamWebWidth));
            }
        }

        public int BeamChordWidth
        {
            get => _beamChordElementCollectionSelected.Width;
            set
            {
                _beamChordElementCollectionSelected.Width = value;
                RaisePropertyChangedEvent(nameof(BeamChordWidth));
            }
        }

        //Column
        public ObservableCollection<OtIBeam> ColumnComponents
        {
            get => _columnComponents
                   ?? (_columnComponents = new ObservableCollection<OtIBeam>(RP.ColumnComponentCollection.OrderBy(elem => elem.Name)));
            set
            {
                _columnComponents = value;
                RaisePropertyChangedEvent(nameof(ColumnComponents));
                RaisePropertyChangedEvent(nameof(ColumnTabVisible));
            }
        }

        public Visibility ColumnTabVisible => ColumnComponents.Count > 0 ? Visibility.Visible : Visibility.Collapsed;

        public OtProfile ColumnWebCollectionSelected
        {
            get => _columnWebCollectionSelected.Profile ??
                   (_columnWebCollectionSelected.Profile = _elementsCollection
                       .FirstOrDefault(elem => elem.Id == (_currentColumnComponent.BeamWeb.Profile?.Id ?? 21)));
            set
            {
                _columnWebCollectionSelected.Profile = value;
                RaisePropertyChangedEvent(nameof(ColumnWebCollectionSelected));
            }
        }

        public OtProfile ColumnChordCollectionSelected
        {
            get => _columnChordCollectionSelected.Profile ??
                   (_columnChordCollectionSelected.Profile = _elementsCollection
                       .FirstOrDefault(elem => elem.Id == (_currentColumnComponent.BeamChord.Profile?.Id ?? 21)));
            set
            {
                _columnChordCollectionSelected.Profile = value;
                RaisePropertyChangedEvent(nameof(ColumnChordCollectionSelected));
            }
        }

        public int ColumnWebWidth
        {
            get => _columnWebCollectionSelected.Width;
            set
            {
                _columnWebCollectionSelected.Width = value;
                RaisePropertyChangedEvent(nameof(ColumnWebWidth));
            }
        }

        public int ColumnChordWidth
        {
            get => _columnChordCollectionSelected.Width;
            set
            {
                _columnChordCollectionSelected.Width = value;
                RaisePropertyChangedEvent(nameof(ColumnChordWidth));
            }
        }

        #endregion

        #region Commands

        //Beam
        private DelegateCommand _addBeamComponent;

        public ICommand AddBeamComponent => _addBeamComponent ?? (_addBeamComponent = new DelegateCommand(o =>
        {
            _currentBeamComponent = new OtIBeam("[New]");
            BeamComponents.Add(_currentBeamComponent);

            BeamChordElementCollectionSelected = BeamWebElementCollectionSelected =
                ElementCollection.First(elem => elem.Id == 21);
            BeamWebWidth = BeamChordWidth = 200;
        }));

        public ICommand GetBeamComponentProperties => new DelegateCommand(obj =>
        {
            _currentBeamComponent = (OtIBeam)obj;

            BeamWebElementCollectionSelected = ElementCollection
                .FirstOrDefault(item => item.Id == (_currentBeamComponent.BeamWeb.Profile?.Id ?? 21));
            BeamWebWidth = _currentBeamComponent.BeamWeb.Width;

            BeamChordElementCollectionSelected = ElementCollection
                .FirstOrDefault(item => item.Id == (_currentBeamComponent.BeamChord.Profile?.Id ?? 21));
            BeamChordWidth = _currentBeamComponent.BeamChord.Width;
        });

        private DelegateCommand _setNewBeamPropertiesToComponent;

        public ICommand SetNewBeamPropertiesToComponent =>
            _setNewBeamPropertiesToComponent ?? (_setNewBeamPropertiesToComponent = new DelegateCommand(obj =>
            {
                _currentBeamComponent.BeamWeb = new OtIBeamComponent
                {
                    ProductMix = _productMix,
                    Profile = RevitDb.GetComponentInfo(604, BeamWebElementCollectionSelected.Id),
                    Width = BeamWebWidth
                };

                _currentBeamComponent.BeamChord = new OtIBeamComponent
                {
                    ProductMix = _productMix,
                    Profile = RevitDb.GetComponentInfo(604, BeamChordElementCollectionSelected.Id),
                    Width = BeamChordWidth
                };

                bool createNew = _currentBeamComponent.Name == "[New]";

                _currentBeamComponent.Name =
                    $"Стінка{BeamWebWidth}x{_currentBeamComponent.BeamWeb.Profile.Parameters.FirstOrDefault(param => param.Key == "width").Value}_" +
                    $"Пояс{BeamChordWidth}x{_currentBeamComponent.BeamChord.Profile.Parameters.FirstOrDefault(param => param.Key == "width").Value}";

                _currentBeamComponent.RaisePropertyChangedEvent("Name");

                ExternalHandler.RevitCommand = app =>
                {
                    Document doc = app.ActiveUIDocument.Document;

                    if (createNew)
                    {
                        using (var tr = new Transaction(doc, "CreateNewBeamType"))
                        {
                            tr.Start();

                            _currentBeamComponent.RvtFamily =
                                ((FamilySymbol)RP.BeamComponentCollection.First().RvtFamily)
                                .Duplicate(_currentBeamComponent.Name);

                            tr.Commit();
                        }
                    }

                    _currentBeamComponent.SetProperties(doc);
                };

                ExternalHandler.ExEvent.Raise();
            }));

        //Column
        private DelegateCommand _addColumnComponent;

        public ICommand AddColumnComponent => _addColumnComponent ?? (_addColumnComponent = new DelegateCommand(o =>
        {
            _currentColumnComponent = new OtIBeam("[New]");
            ColumnComponents.Add(_currentColumnComponent);

            ColumnWebCollectionSelected = ColumnChordCollectionSelected = ElementCollection.First(elem => elem.Id == 21);
            ColumnWebWidth = ColumnChordWidth = 200;
        }));

        public ICommand GetColumnComponentProperties => new DelegateCommand(obj =>
        {
            _currentColumnComponent = (OtIBeam)obj;

            ColumnWebCollectionSelected = ElementCollection
                .First(item => item.Id == (_currentColumnComponent.BeamWeb.Profile?.Id ?? 21));
            ColumnWebWidth = _currentColumnComponent.BeamWeb.Width;

            ColumnChordCollectionSelected = ElementCollection
                .First(item => item.Id == (_currentColumnComponent.BeamChord.Profile?.Id ?? 21));
            ColumnChordWidth = _currentColumnComponent.BeamChord.Width;
        });

        private DelegateCommand _setNewColumnPropertiesToComponent;

        public ICommand SetNewColumnPropertiesToComponent =>
            _setNewColumnPropertiesToComponent ?? (_setNewColumnPropertiesToComponent = new DelegateCommand(obj =>
            {
                _currentColumnComponent.BeamChord = new OtIBeamComponent
                {
                    ProductMix = _productMix,
                    Profile = RevitDb.GetComponentInfo(604, ColumnChordCollectionSelected.Id),
                    Width = ColumnChordWidth
                };

                _currentColumnComponent.BeamWeb = new OtIBeamComponent
                {
                    ProductMix = _productMix,
                    Profile = RevitDb.GetComponentInfo(604, ColumnWebCollectionSelected.Id),
                    Width = ColumnWebWidth
                };

                bool createNew = _currentColumnComponent.Name == "[New]";

                _currentColumnComponent.Name =
                    $"Стінка{ColumnWebWidth}x{_currentColumnComponent.BeamWeb.Profile.Parameters.FirstOrDefault(param => param.Key == "width").Value}_" +
                    $"Пояс{ColumnChordWidth}x{_currentColumnComponent.BeamChord.Profile.Parameters.FirstOrDefault(param => param.Key == "width").Value}";

                _currentColumnComponent.RaisePropertyChangedEvent("Name");

                ExternalHandler.RevitCommand = app =>
                {
                    Document doc = app.ActiveUIDocument.Document;

                    if (createNew)
                    {
                        using (var tr = new Transaction(doc, "CreateNewColumnType"))
                        {
                            tr.Start();

                            _currentColumnComponent.RvtFamily =
                                ((FamilySymbol)RP.ColumnComponentCollection.First().RvtFamily)
                                .Duplicate(_currentColumnComponent.Name);

                            tr.Commit();
                        }
                    }

                    _currentColumnComponent.SetProperties(doc);
                };

                ExternalHandler.ExEvent.Raise();
            }));

        #endregion
    }
}