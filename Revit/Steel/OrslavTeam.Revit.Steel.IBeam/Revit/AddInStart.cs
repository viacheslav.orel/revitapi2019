﻿using System.Linq;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.UI;
using Autodesk.Revit.DB;
using Newtonsoft.Json;
using OrslavTeam.Revit.Axillary.PublicInformation;
using OrslavTeam.Revit.Steel.Axillary.Repository;
using OrslavTeam.Revit.Steel.Axillary.SelectionFilters;
using RP = OrslavTeam.Revit.Steel.IBeam.ViewModel.Repository;

namespace OrslavTeam.Revit.Steel.IBeam.Revit
{
    [Transaction(TransactionMode.Manual)]
    public class AddInStart : IExternalCommand
    {
        #region Implementation of IExternalCommand

        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            #region Fields

            Document doc = commandData.Application.ActiveUIDocument.Document;
            var isIBeam = new IsIBeam();

            #endregion

            #region GetFamilySumbol

            RP.BeamComponentCollection =
                new FilteredElementCollector(doc)
                    .OfCategory(BuiltInCategory.OST_StructuralFraming)
                    .OfClass(typeof(FamilySymbol))
                    .Where(isIBeam.AllowElement)
                    .Select(CreateComplexComponent)
                    .ToArray();
            
            RP.ColumnComponentCollection =
                new FilteredElementCollector(doc)
                    .OfCategory(BuiltInCategory.OST_StructuralColumns)
                    .OfClass(typeof(FamilySymbol))
                    .Where(isIBeam.AllowElement)
                    .Select(CreateComplexComponent)
                    .ToArray();

            #endregion

            #region CreateView

            ExternalHandler.Handler = new ExternalHandler();
            ExternalHandler.ExEvent = ExternalEvent.Create(ExternalHandler.Handler);
            ExternalHandler.ShowDialog();

            #endregion

            return Result.Succeeded;
        }

        #endregion

        #region Methods

        private static OtIBeam CreateComplexComponent(Element fam)
        {
            string getItString = PublicParameter.GetIt(fam);
            OtIBeam component = JsonConvert.DeserializeObject<OtIBeam>(getItString) ?? new OtIBeam(fam.Name);

            component.RvtFamily = fam;
            component.Name = fam.Name;
            return component;
        }

        #endregion
    }
}