﻿using System;
using Autodesk.Revit.UI;
using OrslavTeam.Revit.Steel.IBeam.View;

namespace OrslavTeam.Revit.Steel.IBeam.Revit
{
    public class ExternalHandler : IExternalEventHandler
    {
        #region Fields

        public static ExternalHandler Handler;
        public static ExternalEvent ExEvent;

        private static EditComplexIBeamView _complexIBeamView;
        
        public delegate void RevitCommandDelegate(UIApplication uiApp);
        public static RevitCommandDelegate RevitCommand { get; set; }

        #endregion

        #region Methods

        private static void WhenClosed(object sender, EventArgs args)
        {
            ExEvent.Dispose();
            ExEvent = null;
            Handler = null;
            _complexIBeamView = null;
            RevitCommand = null;

            ((EditComplexIBeamView)sender).Closed -= WhenClosed;
        }

        public static void ShowDialog()
        {
            _complexIBeamView = new EditComplexIBeamView();
            _complexIBeamView.Closed += WhenClosed; 
            
            _complexIBeamView.Show();
        }

        #endregion
        
        #region Implementation of IExternalEventHandler

        public void Execute(UIApplication app)
        {
            RevitCommand(app);
        }

        public string GetName()
        {
            return nameof(ExternalHandler);
        }

        #endregion
    }
}