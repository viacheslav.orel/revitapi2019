﻿using System;
using System.Collections.Generic;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using OrslavTeam.Revit.Axillary.ViewTemplate.ReportWindow;

namespace OrslavTeam.Revit.Steel.BracingGeometry
{
    [Transaction(TransactionMode.Manual)]
    public class BracingGeometrySwitcher : IExternalCommand
    {
        #region Implementation of IExternalCommand

        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            Document doc = commandData.Application.ActiveUIDocument.Document;
            FilteredElementCollector structuralFramingCollection = new FilteredElementCollector(doc, doc.ActiveView.Id)
                .OfCategory(BuiltInCategory.OST_StructuralFraming);

            int counter = 0;
            var changedElements = new List<string> {"Змінені елементи:"};
            var dontChangedElements = new List<string> {"Пропущені елементи:"};
            var errorChangedElements = new List<string> {"Помилки запису:"};

            var tr = new Transaction(doc, "setBracingTrue");
            tr.Start();
            foreach (Element element in structuralFramingCollection)
            {
                counter++;
                int structuralUsage = element.get_Parameter(BuiltInParameter.INSTANCE_STRUCT_USAGE_PARAM).AsInteger();
                Parameter parameter = element.LookupParameter("В'язі");

                if (parameter == null)
                {
                    dontChangedElements.Add(element.Id.ToString());
                    continue;
                }

                if (structuralUsage == 7 || structuralUsage == 8)
                {
                    if (parameter.AsInteger() == 0)
                    {
                        try
                        {
                            parameter.Set(1);
                            changedElements.Add(element.Id.ToString());
                        }
                        catch (Exception)
                        {
                            errorChangedElements.Add(element.Id.ToString());
                        }
                    }
                    else
                    {
                        dontChangedElements.Add(element.Id.ToString());
                    }
                }
                else
                {
                    if (parameter.AsInteger() == 1)
                    {
                        try
                        {
                            parameter.Set(0);
                            changedElements.Add(element.Id.ToString());
                        }
                        catch (Exception)
                        {
                            errorChangedElements.Add(element.Id.ToString());
                        }
                    }
                    else
                    {
                        dontChangedElements.Add(element.Id.ToString());
                    }
                }
            }

            tr.Commit();

            var reportView = new ReportView(counter, changedElements, dontChangedElements, errorChangedElements);
            reportView.Show();

            return Result.Succeeded;
        }

        #endregion
    }
}