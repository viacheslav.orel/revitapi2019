﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using Autodesk.Revit.UI.Selection;
using Newtonsoft.Json;
using OrslavTeam.Revit.Axillary.PublicInformation;
using OrslavTeam.Revit.Steel.Axillary.Repository.ComponentComplex;
using OrslavTeam.Revit.Steel.Axillary.Repository.ComponentComplex.Railing;
using OrslavTeam.Revit.Steel.Axillary.SelectionFilters;
using EH = OrslavTeam.Revit.Steel.Railing.Revit.ExternalHandler;

namespace OrslavTeam.Revit.Steel.Railing.Revit
{
    [Transaction(TransactionMode.Manual)]
    public class AddInStart : IExternalCommand
    {
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            UIApplication uiApp = commandData.Application;
            Document doc = uiApp.ActiveUIDocument.Document;
            Selection sel = uiApp.ActiveUIDocument.Selection;

            var isPlanRailing = new IsPlanRailing();
            ICollection<ElementId> planRailingCollection = sel.GetElementIds();

            Element selectedSymbol;

            if (planRailingCollection.Count > 0)
            {
                Element selectedElement = doc.GetElement(planRailingCollection.FirstOrDefault());
                selectedSymbol = (selectedElement as FamilyInstance)?.Symbol ?? selectedElement;
                if (!isPlanRailing.AllowElement(selectedSymbol))
                {
                    MessageBox.Show("Error");
                    return Result.Cancelled;
                }
            }
            else
            {
                Reference pickedPlaneRailing;
                do
                {
                    try
                    {
                        pickedPlaneRailing = sel.PickObject(ObjectType.Element, isPlanRailing, "Оберіть огорожу майданчику");
                    }
                    catch (Exception)
                    {
                        return Result.Cancelled;
                    }
                } while (pickedPlaneRailing == null);

                selectedSymbol = ((FamilyInstance)doc.GetElement(pickedPlaneRailing)).Symbol;
            }

            string getIt = PublicParameter.GetIt(selectedSymbol);
            if (getIt != "")
            {
                try
                {
                    EH.RailingComponentData = JsonConvert.DeserializeObject<RailingComponent>(getIt);
                }
                catch (Exception)
                {
                    EH.RailingComponentData = new RailingComponent().GetRailingComponent;
                }
            }

            EH.RailingComponentData.RvtFamily = (FamilySymbol)selectedSymbol;

            ExternalHandler.Handler = new ExternalHandler();
            ExternalHandler.ExEvent = ExternalEvent.Create(ExternalHandler.Handler);
            ExternalHandler.ShowDialog();

            return Result.Succeeded;
        }
    }
}