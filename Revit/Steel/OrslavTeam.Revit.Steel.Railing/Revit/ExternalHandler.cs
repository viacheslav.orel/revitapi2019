﻿using Autodesk.Revit.UI;
using OrslavTeam.Revit.Steel.Axillary.Repository.ComponentComplex;
using OrslavTeam.Revit.Steel.Axillary.Repository.ComponentComplex.Railing;
using OrslavTeam.Revit.Steel.Railing.View;

namespace OrslavTeam.Revit.Steel.Railing.Revit
{
    public class ExternalHandler : IExternalEventHandler
    {
        #region Fields

        public static ExternalHandler Handler;
        public static ExternalEvent ExEvent;

        private static MainWindow _railingParameterView;
        public static RailingComponent RailingComponentData;

        public delegate void RevitCommandDelegate(UIApplication uiApp);

        public static RevitCommandDelegate RevitCommand { get; set; }

        #endregion

        public static void ShowDialog()
        {
            _railingParameterView = new MainWindow();
            _railingParameterView.Show();
        }

        public void Execute(UIApplication app)
        {
            RevitCommand(app);
            ExEvent.Dispose();
            ExEvent = null;
            Handler = null;

            // ReSharper disable once PossibleNullReferenceException
            _railingParameterView.Close();
            _railingParameterView = null;
            RailingComponentData = null;
        }

        public string GetName()
        {
            return nameof(ExternalHandler);
        }
    }
}