﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Autodesk.Revit.DB;
using OrslavTeam.Revit.Axillary.ViewTemplate.Infrastructure;
using OrslavTeam.Revit.Steel.Railing.Revit;
using OrslavTeam.Revit.Steel.Railing.UserControls;
using OrslavTeam.Revit.Steel.Axillary.Repository.ComponentSimple;
using OrslavTeam.Revit.Steel.Axillary.Repository.Primitive;
using EH = OrslavTeam.Revit.Steel.Railing.Revit.ExternalHandler;
using Visibility = System.Windows.Visibility;

namespace OrslavTeam.Revit.Steel.Railing.ViewModel
{
    public class MainWindow_ViewModel : ObservableObject
    {
        #region Fields

        private SimpleComponent _currentComponent;

        private ObservableCollection<OtProductMix> _productMixCollection;
        private OtProductMix _productMixCollectionSelectedItem;

        private ObservableCollection<OtElement> _elementCollection;
        private OtElement _elementCollectionSelectedItem;

        private ObservableCollection<OtMaterial> _materialCollection;
        private OtMaterial _materialCollectionSelectedItem;

        #endregion

        #region FieldsAccess

        #region HandRailing

        //HandRailing
        //HandRailing--Geometry
        public int Height
        {
            get => EH.RailingComponentData.Height;
            set
            {
                EH.RailingComponentData.Height = value;
                RaisePropertyChangedEvent(nameof(Height));
            }
        }

        //HandRailing--Component
        public string HandRailingProfileName =>
            $"{EH.RailingComponentData.GetHandRailingComponent.Element.Name}\n" +
            $"({EH.RailingComponentData.GetHandRailingComponent.Material.Name})";

        #endregion

        #region Baluster

        //Baluster
        //Baluster--Visibility
        public bool BalusterNeed
        {
            get => EH.RailingComponentData.BalusterNeed;
            set
            {
                EH.RailingComponentData.BalusterNeed = value;
                RaisePropertyChangedEvent(nameof(BalusterNeed));
                RaisePropertyChangedEvent(nameof(ConnectionTopElement));
                RaisePropertyChangedEvent(nameof(ConnectionMiddleElement));
                RaisePropertyChangedEvent(nameof(TopConnectionNeed));
                RaisePropertyChangedEvent(nameof(MiddleConnectionNeed));
            }
        }

        //Baluster--Geometry
        public int BalusterFirstBinding
        {
            get => EH.RailingComponentData.BalusterFirstBinding;
            set
            {
                EH.RailingComponentData.BalusterFirstBinding = value;
                RaisePropertyChangedEvent(nameof(BalusterFirstBinding));
            }
        }

        public int BalusterLastBinding
        {
            get => EH.RailingComponentData.BalusterLastBinding;
            set
            {
                EH.RailingComponentData.BalusterLastBinding = value;
                RaisePropertyChangedEvent(nameof(BalusterLastBinding));
            }
        }

        public int BalusterStep
        {
            get => EH.RailingComponentData.BalusterStep;
            set
            {
                EH.RailingComponentData.BalusterStep = value;
                RaisePropertyChangedEvent(nameof(BalusterStep));
            }
        }

        public int SupportBinding
        {
            get => EH.RailingComponentData.SupportBinding;
            set
            {
                EH.RailingComponentData.SupportBinding = value;
                RaisePropertyChangedEvent(nameof(SupportBinding));
            }
        }

        public bool IsCustomBalusterWidth =>
            EH.RailingComponentData.GetBalusterComponent.ProductMix.WayType == TypeEnum.TwoWayComponent;

        public int BalusterWidth
        {
            get
            {
                if (IsCustomBalusterWidth)
                    return EH.RailingComponentData.GetBalusterComponent.Width;

                return (int)EH.RailingComponentData.GetBalusterComponent.Element.Parameters
                    .FirstOrDefault(pair => pair.Key == "width").Value;
            }
            set
            {
                if (IsCustomBalusterWidth)
                    EH.RailingComponentData.GetBalusterComponent.Width = value;
                RaisePropertyChangedEvent(nameof(BalusterWidth));
            }
        }

        //Baluster--Geometry--Binding
        public int BalusterClippingAngle
        {
            get => EH.RailingComponentData.BalusterClippingAngle;
            set
            {
                EH.RailingComponentData.BalusterClippingAngle = value;
                RaisePropertyChangedEvent(nameof(BalusterClippingAngle));
            }
        }

        public int BalusterClippingTop
        {
            get => EH.RailingComponentData.BalusterClippingTop;
            set
            {
                EH.RailingComponentData.BalusterClippingTop = value;
                RaisePropertyChangedEvent(nameof(BalusterClippingTop));
            }
        }

        public int BalusterTopSegmentFirst
        {
            get => EH.RailingComponentData.BalusterTopSegmentFirst;
            set
            {
                EH.RailingComponentData.BalusterTopSegmentFirst = value;
                RaisePropertyChangedEvent(nameof(BalusterTopSegmentFirst));
            }
        }

        public int BalusterTopSegmentSecond
        {
            get => EH.RailingComponentData.BalusterTopSegmentSecond;
            set
            {
                EH.RailingComponentData.BalusterTopSegmentSecond = value;
                RaisePropertyChangedEvent(nameof(BalusterTopSegmentSecond));
            }
        }

        public bool MiddleConnectionBinding
        {
            get => EH.RailingComponentData.MiddleConnectionBinding == 1;
            set
            {
                EH.RailingComponentData.MiddleConnectionBinding = value ? 1 : 0;
                RaisePropertyChangedEvent(nameof(MiddleConnectionBinding));
            }
        }

        public int RailBindingToBaluster
        {
            get => EH.RailingComponentData.MiddleConnectionBinding;
            set
            {
                EH.RailingComponentData.MiddleConnectionBinding = value;
                RaisePropertyChangedEvent(nameof(RailBindingToBaluster));
            }
        }

        //Baluster--Component
        public string BalusterProfileName => $"{EH.RailingComponentData.GetBalusterComponent.Element.Name}\n" +
                                             $"({EH.RailingComponentData.GetBalusterComponent.Material.Name})";

        #endregion

        #region Rail

        //Rail
        //Rail--Visibility
        public bool RailNeed
        {
            get => EH.RailingComponentData.RailNeed;
            set
            {
                EH.RailingComponentData.RailNeed = value;
                RaisePropertyChangedEvent(nameof(RailNeed));
                RaisePropertyChangedEvent(nameof(ConnectionMiddleElement));
                RaisePropertyChangedEvent(nameof(MiddleConnectionNeed));
            }
        }

        //Rail--Geometry
        public int RailBinding
        {
            get => EH.RailingComponentData.RailBinding;
            set
            {
                EH.RailingComponentData.RailBinding = value;
                RaisePropertyChangedEvent(nameof(RailBinding));
            }
        }

        public bool IsCustomRailWidth => 
            EH.RailingComponentData.GetRailComponent.ProductMix.WayType == TypeEnum.TwoWayComponent;

        public int RailWidth
        {
            get
            {
                if (IsCustomRailWidth)
                    return EH.RailingComponentData.GetRailComponent.Width;

                return (int)EH.RailingComponentData.GetRailComponent.Element.Parameters
                    .FirstOrDefault(pair => pair.Key == "width").Value;
            }
            set
            {
                if (IsCustomRailWidth)
                    EH.RailingComponentData.GetRailComponent.Width = value;
                RaisePropertyChangedEvent(nameof(RailWidth));
            }
        }

        //Rail--Component
        public string RailProfileName =>
            $"{EH.RailingComponentData.GetRailComponent.Element.Name}\n" +
            $"({EH.RailingComponentData.GetRailComponent.Material.Name})";

        #endregion

        #region Axillary

        //Axillary
        //Axillary--Visibility
        public bool AxillaryNeed
        {
            get => EH.RailingComponentData.AxillaryNeed;
            set
            {
                EH.RailingComponentData.AxillaryNeed = value;
                RaisePropertyChangedEvent(nameof(AxillaryNeed));
            }
        }

        //Axillary--Geometry
        public int AxillaryBinding
        {
            get => EH.RailingComponentData.AxillaryBinding;
            set
            {
                EH.RailingComponentData.AxillaryBinding = value;
                RaisePropertyChangedEvent(nameof(AxillaryBinding));
            }
        }

        public bool IsCustomAxillaryWidth => 
            EH.RailingComponentData.GetAxillaryComponent.ProductMix.WayType == TypeEnum.TwoWayComponent;

        public int AxillaryWidth
        {
            get
            {
                if (IsCustomAxillaryWidth)
                    return EH.RailingComponentData.GetAxillaryComponent.Width;

                return (int)EH.RailingComponentData.GetAxillaryComponent.Element.Parameters
                    .FirstOrDefault(pair => pair.Key == "width").Value;
            }
            set
            {
                if (IsCustomAxillaryWidth)
                    EH.RailingComponentData.GetAxillaryComponent.Width = value;
                RaisePropertyChangedEvent(nameof(AxillaryWidth));
            }
        }

        //Axillary--Component
        public string AxillaryProfileName =>
            $"{EH.RailingComponentData.GetAxillaryComponent.Element.Name}\n" +
            $"({EH.RailingComponentData.GetAxillaryComponent.Material.Name})";

        #endregion

        #region ProfileInfoCollection

        //ProfileInfoCollection
        //ProfileInfoCollection--ProductMix
        public ObservableCollection<OtProductMix> ProductMixCollection
        {
            get => _productMixCollection;
            set
            {
                _productMixCollection = value;
                RaisePropertyChangedEvent(nameof(ProductMixCollection));
            }
        }

        public OtProductMix ProductMixCollectionSelectedItem
        {
            get => _productMixCollectionSelectedItem;
            set
            {
                _productMixCollectionSelectedItem = value;
                RaisePropertyChangedEvent(nameof(ProductMixCollectionSelectedItem));

                ElementCollection = OtRequest.GetElementCollection(ProductMixCollectionSelectedItem.Id);
            }
        }

        //ProfileInfoCollection--Element
        public ObservableCollection<OtElement> ElementCollection
        {
            get => _elementCollection;
            set
            {
                _elementCollection = value;
                RaisePropertyChangedEvent(nameof(ElementCollection));

                ElementCollectionSelectedItem = null;
            }
        }

        public OtElement ElementCollectionSelectedItem
        {
            get => _elementCollectionSelectedItem;
            set
            {
                _elementCollectionSelectedItem = value;
                RaisePropertyChangedEvent(nameof(ElementCollectionSelectedItem));
            }
        }

        //ProfileInfoCollection--Material
        public ObservableCollection<OtMaterial> MaterialCollection
        {
            get => _materialCollection ?? (_materialCollection = OtRequest.GetMaterialCollection());
            set
            {
                _materialCollection = value;
                RaisePropertyChangedEvent(nameof(MaterialCollection));
            }
        }

        public OtMaterial MaterialCollectionSelectedItem
        {
            get => _materialCollectionSelectedItem;
            set
            {
                _materialCollectionSelectedItem = value;
                RaisePropertyChangedEvent(nameof(MaterialCollectionSelectedItem));
            }
        }

        #endregion

        #region Connection

        public Visibility TopConnectionNeed => BalusterNeed ? Visibility.Visible : Visibility.Collapsed;

        public Visibility MiddleConnectionNeed => BalusterNeed && RailNeed ? Visibility.Visible : Visibility.Collapsed;

        public FrameworkElement ConnectionTopElement
        {
            get
            {
                int balusterId = EH.RailingComponentData.GetBalusterComponent.ProductMix.Id;
                int handRailingId = EH.RailingComponentData.GetHandRailingComponent.ProductMix.Id;

                if (balusterId > 200 && balusterId < 300)
                {
                    if (handRailingId > 200 && handRailingId < 300)
                        return new Control_Top_LSteel_LSteel();
                    if (handRailingId == 301)
                        return new Control_Top_LSteel_Tube();
                    if (handRailingId > 500 && handRailingId < 600)
                        return new Control_Top_LSteel_Rectangle();
                }
                else if (balusterId > 300 && balusterId < 400)
                {
                    if (handRailingId > 300 && handRailingId < 400)
                        return new Control_Top_Tube_Tube();
                    if(handRailingId > 500 && handRailingId < 600)
                        return new Control_Top_Tube_Rectangle();
                }
                else if (balusterId > 500 && balusterId < 600)
                {
                    if (handRailingId > 500 && handRailingId < 600)
                        return new Control_Top_Square_Rectangle();
                }
                else if (balusterId > 600 && balusterId < 700)
                {
                    if (handRailingId > 300 && handRailingId < 400)
                        return new Control_Top_Sheet_Tube();
                    if (handRailingId > 500 && handRailingId < 600)
                        return new Control_Top_Sheet_Rectangle();
                }

                return new Control_Error();
            }
        }

        public FrameworkElement ConnectionMiddleElement
        {
            get
            {
                int balusterId = EH.RailingComponentData.GetBalusterComponent.ProductMix.Id;
                int railId = EH.RailingComponentData.GetRailComponent.ProductMix.Id;

                if (balusterId > 200 && balusterId < 300)
                {
                    if (railId > 200 && railId < 300)
                        return new Control_Middle_LSteel_LSteel();
                    if (railId >= 310 && railId < 400)
                        return new Control_Middle_LSteel_Rebar();
                    if (railId > 600 && railId < 700)
                        return new Control_Middle_LSteel_Sheet();
                }
                else if (balusterId > 300 && balusterId < 400)
                {
                    if (railId > 300 && railId < 310)
                        return new Control_Middle_Tube_Tube();
                    if (railId >= 310 && railId < 400)
                        return new Control_Middle_Tube_Rebar();
                    if (railId > 600 && railId < 700)
                        return new Control_Middle_Tube_Sheet();
                }
                else if (balusterId > 500 && balusterId < 600)
                {
                    if (railId > 300 && railId < 310)
                        return new Control_Square_Tube();
                    if (railId >= 310 && railId < 400)
                        return new Control_Middle_Tube_Rebar();
                    if (railId > 500 && railId < 600)
                        return new Control_Middle_Square_Square();
                    if (railId > 600 && railId < 700)
                        return new Control_Middle_LSteel_Sheet();
                }
                else if (balusterId > 600 && balusterId < 700)
                {
                    if (railId > 300 && railId < 310)
                        return new Control_Sheet_Tube();
                    if (railId >= 310 && railId < 400)
                        return new Control_Meddle_Sheet_Rebar();
                    if (railId > 600 && railId < 700)
                        return new Control_Middle_Sheet_Sheet();
                }

                return new Control_Error();
            }
        }

        #endregion

        #endregion

        #region Command

        //ChangeBaluster
        private DelegateCommand _changeBalusterCommand;

        public ICommand ChangeBalusterCommand =>
            _changeBalusterCommand ?? (_changeBalusterCommand = new DelegateCommand(
                o =>
                {
                    _currentComponent = EH.RailingComponentData.GetBalusterComponent;

                    ProductMixCollection = OtRequest.GetProductMixCollection(201, 202, 203, 301, 501, 502, 550, 604, 605, 622);
                    ProductMixCollectionSelectedItem = ProductMixCollection.FirstOrDefault(productMix =>
                        productMix.Id == _currentComponent.ProductMix.Id);

                    ElementCollectionSelectedItem =
                        ElementCollection.FirstOrDefault(element =>
                            element.Id == _currentComponent.Element.Id);

                    MaterialCollectionSelectedItem = MaterialCollection.FirstOrDefault(material =>
                        material.Id == _currentComponent.Material.Id);

                }));

        //ChangeHandRailing
        private DelegateCommand _changeHandRailing;

        public ICommand ChangeHandRailing =>
            _changeHandRailing ?? (_changeHandRailing = new DelegateCommand(o =>
            {
                _currentComponent = EH.RailingComponentData.GetHandRailingComponent;

                ProductMixCollection =
                    OtRequest.GetProductMixCollection(201, 202, 203, 301, 501, 502, 550);
                ProductMixCollectionSelectedItem = ProductMixCollection.FirstOrDefault(productMix =>
                    productMix.Id == _currentComponent.ProductMix.Id);

                ElementCollectionSelectedItem =
                    ElementCollection.FirstOrDefault(element => element.Id == _currentComponent.Element.Id);

                MaterialCollectionSelectedItem =
                    MaterialCollection.FirstOrDefault(material => material.Id == _currentComponent.Material.Id);
            }));

        //ChangeRail
        private DelegateCommand _changeRailCommand;

        public ICommand ChangeRailCommand =>
            _changeRailCommand ?? (_changeRailCommand = new DelegateCommand(o =>
            {
                _currentComponent = EH.RailingComponentData.GetRailComponent;

                ProductMixCollection =
                    OtRequest.GetProductMixCollection(201, 202, 203, 301, 310, 312, 501, 502, 604, 605, 622);
                ProductMixCollectionSelectedItem = ProductMixCollection.FirstOrDefault(productMix =>
                    productMix.Id == _currentComponent.ProductMix.Id);

                ElementCollectionSelectedItem =
                    ElementCollection.FirstOrDefault(element => element.Id == _currentComponent.Element.Id);

                MaterialCollectionSelectedItem =
                    MaterialCollection.FirstOrDefault(material => material.Id == _currentComponent.Material.Id);
            }));

        //ChangeAxillary
        private DelegateCommand _changeAxillaryCommand;

        public ICommand ChangeAxillaryCommand =>
            _changeAxillaryCommand ?? (_changeAxillaryCommand = new DelegateCommand(o =>
            {
                _currentComponent = EH.RailingComponentData.GetAxillaryComponent;

                ProductMixCollection =
                    OtRequest.GetProductMixCollection(604, 605, 622);
                ProductMixCollectionSelectedItem = ProductMixCollection.FirstOrDefault(productMix =>
                    productMix.Id == _currentComponent.ProductMix.Id);

                ElementCollectionSelectedItem =
                    ElementCollection.FirstOrDefault(element => element.Id == _currentComponent.Element.Id);

                MaterialCollectionSelectedItem =
                    MaterialCollection.FirstOrDefault(material => material.Id == _currentComponent.Material.Id);
            }));

        //SetChange
        private DelegateCommand _saveChangeCommand;

        public ICommand SaveChangeCommand => _saveChangeCommand ?? (_saveChangeCommand = new DelegateCommand(o =>
            {
                _currentComponent.ProductMix = (OtProductMix)ProductMixCollectionSelectedItem.Clone();
                _currentComponent.Element =
                    OtRequest.GetComponentInfo(ProductMixCollectionSelectedItem.Id, ElementCollectionSelectedItem.Id);
                _currentComponent.Material = (OtMaterial)MaterialCollectionSelectedItem.Clone();

                RaisePropertyChangedEvent(nameof(BalusterProfileName));
                RaisePropertyChangedEvent(nameof(HandRailingProfileName));
                RaisePropertyChangedEvent(nameof(RailProfileName));
                RaisePropertyChangedEvent(nameof(AxillaryProfileName));

                RaisePropertyChangedEvent(nameof(IsCustomBalusterWidth));
                RaisePropertyChangedEvent(nameof(IsCustomRailWidth));
                RaisePropertyChangedEvent(nameof(IsCustomAxillaryWidth));

                RaisePropertyChangedEvent(nameof(ConnectionTopElement));
                RaisePropertyChangedEvent(nameof(ConnectionMiddleElement));

                _currentComponent = null;
                _productMixCollection = null;
                _productMixCollectionSelectedItem = null;
                _elementCollection = null;
                _elementCollectionSelectedItem = null;
            }));

        //Confirm
        private DelegateCommand _confirmCommand;

        public ICommand ConfirmCommand => _confirmCommand ?? (_confirmCommand = new DelegateCommand(o =>
            {
                ExternalHandler.RevitCommand = app =>
                {
                    Document doc = app.ActiveUIDocument.Document;

                    using (var tr = new Transaction(doc, "SetParameterToRailingElement"))
                    {
                        tr.Start();
                        EH.RailingComponentData.SetGeometryToRevit(
                            new FilteredElementCollector(doc).OfCategory(BuiltInCategory.OST_Materials));
                        tr.Commit();
                    }
                };

                ExternalHandler.ExEvent.Raise();
            }));

        #endregion
    }
}