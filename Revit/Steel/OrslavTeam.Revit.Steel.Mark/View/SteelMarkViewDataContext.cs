﻿using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Structure;
using Autodesk.Revit.UI;
using OrslavTeam.Revit.Axillary.PublicInformation;
using OrslavTeam.Revit.Axillary.ViewTemplate.Infrastructure;
using OrslavTeam.Revit.Axillary.ViewTemplate.ReportWindow;
using OrslavTeam.Revit.Steel.Mark.Repository;
using OrslavTeam.Revit.Steel.Mark.Revit;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using OrslavTeam.Revit.Steel.Axillary.Settings.SteelStorage;
using OrslavTeam.Revit.Steel.Axillary.Settings.StructuralCategories;
using OrslavTeam.Revit.Steel.Axillary.Settings.UsingType;

namespace OrslavTeam.Revit.Steel.Mark.View
{
    public class SteelMarkViewDataContext : ObservableObject
    {
        #region Constructors -------------------------------------------------------------------------------------------

        public SteelMarkViewDataContext()
        {
        }

        public SteelMarkViewDataContext(RevitElement revitElements)
        {
            _revitElement = revitElements;
            Phases = revitElements.GetPhases;
        }

        #endregion

        #region Fields -------------------------------------------------------------------------------------------------

        // BaseDependency ----------------------------------------------------------------------------------------------
        private readonly RevitElement _revitElement;

        // PhaseDependency ---------------------------------------------------------------------------------------------
        private IEnumerable<Phase> _phases;
        private Phase _currentPhase;

        // Object ------------------------------------------------------------------------------------------------------
        private Dictionary<FullUsingType, List<Component>> _objectGroup;
        private ObservableCollection<FullUsingType> _groupTypes;

        // CategoryComponent -------------------------------------------------------------------------------------------
        private FullUsingType _currentFullUsingType;

        // SteelProperties ---------------------------------------------------------------------------------------------
        private SteelSettingsStorage _steelProperties;

        // Report ------------------------------------------------------------------------------------------------------
        private List<string> _successfullyList = new List<string> {"Успішно промарковано"};
        private List<string> _errorList = new List<string> {"Помилки при маркуванні"};
        private int _reportCounter;

        #endregion

        #region FieldsAccess -------------------------------------------------------------------------------------------

        // PhaseDependency ---------------------------------------------------------------------------------------------
        public IEnumerable<Phase> Phases
        {
            get => _phases;
            set
            {
                _phases = value;
                RaisePropertyChangedEvent("Phases");
                CurrentPhase = _phases.FirstOrDefault();
            }
        }

        public Phase CurrentPhase
        {
            get => _currentPhase;
            set
            {
                _currentPhase = value;
                RaisePropertyChangedEvent("CurrentPhase");
            }
        }

        // Object ------------------------------------------------------------------------------------------------------
        private Dictionary<FullUsingType, List<Component>> ObjectGroup
        {
            get => _objectGroup;
            set
            {
                _objectGroup = value;
                GroupTypes = new ObservableCollection<FullUsingType>();
                foreach (FullUsingType fullUsingType in value.Keys)
                {
                    if (fullUsingType.Id > 0)
                        GroupTypes.Add(fullUsingType);
                }
            }
        }

        private void AddNewComponent(FullUsingType fullUsingType, Element element, Document doc,
            params int[] otherFields)
        {
            if (_objectGroup.ContainsKey(fullUsingType))
            {
                _objectGroup[fullUsingType].Add(new Component(element, doc, otherFields));
            }
            else
            {
                _objectGroup.Add(fullUsingType, new List<Component> {new Component(element, doc, otherFields)});

                if (fullUsingType.Id <= 0) return;

                GroupTypes.Add(fullUsingType);
                RaisePropertyChangedEvent("GroupTypes");
            }
        }

        public ObservableCollection<FullUsingType> GroupTypes
        {
            get => _groupTypes;
            set => _groupTypes = value;
        }

        #endregion

        #region Methods ------------------------------------------------------------------------------------------------

        // FilteredByPhase ---------------------------------------------------------------------------------------------
        private void FilteredByPhase(UIApplication uiApp)
        {
            Document doc = uiApp.ActiveUIDocument.Document;
            ElementId phaseId = _currentPhase.Id;

            ObjectGroup = new Dictionary<FullUsingType, List<Component>>();

            if (_revitElement.Columns.Any())
                foreach (Element columnElement in _revitElement.Columns)
                {
                    if (columnElement.GetPhaseStatus(phaseId) != ElementOnPhaseStatus.New) continue;

                    FullUsingType fullUsingType =
                        GetFullUsingType(columnElement, doc, 100, "01 Каркас", 100, "01 Колона");
                    AddNewComponent(fullUsingType, columnElement, doc);
                }

            if (_revitElement.StructuralFraming.Any())
                foreach (Element structuralFraming in _revitElement.StructuralFraming)
                {
                    if (structuralFraming.GetPhaseStatus(phaseId) != ElementOnPhaseStatus.New) continue;

                    StructuralInstanceUsage structuralUsage = ((FamilyInstance) structuralFraming).StructuralUsage;
                    if (structuralUsage == StructuralInstanceUsage.TrussChord
                        || structuralUsage == StructuralInstanceUsage.TrussWeb) continue;

                    FullUsingType fullUsingType =
                        GetStructuralFramingFullUsingType(structuralFraming, doc, structuralUsage);
                    AddNewComponent(fullUsingType, structuralFraming, doc);
                }

            if (_revitElement.StructuralTrusses.Any())
                foreach (Element structuralTruss in _revitElement.StructuralTrusses)
                {
                    if (structuralTruss.GetPhaseStatus(phaseId) != ElementOnPhaseStatus.New) continue;

                    FullUsingType fullUsingType =
                        GetFullUsingType(structuralTruss, doc, 100, "01 Каркас", 500, "05 Ферма");

                    var height = (int) UnitUtils.ConvertFromInternalUnits(
                        structuralTruss.get_Parameter(BuiltInParameter.TRUSS_HEIGHT).AsDouble(),
                        DisplayUnitType.DUT_MILLIMETERS);

                    var length = (int) UnitUtils.ConvertFromInternalUnits(
                        structuralTruss.get_Parameter(BuiltInParameter.TRUSS_ELEMENT_SPAN_PARAM).AsDouble(),
                        DisplayUnitType.DUT_MILLIMETERS);

                    var startBinding = (int) UnitUtils.ConvertFromInternalUnits(
                        structuralTruss.LookupParameter("Відступ.Початок").AsDouble(),
                        DisplayUnitType.DUT_MILLIMETERS);

                    var endBinding = (int) UnitUtils.ConvertFromInternalUnits(
                        structuralTruss.LookupParameter("Відступ.Кінець").AsDouble(),
                        DisplayUnitType.DUT_MILLIMETERS);

                    AddNewComponent(fullUsingType, structuralTruss, doc, height, length, startBinding, endBinding);
                }

            if (_revitElement.GenericModels.Any())
                foreach (Element genericModel in _revitElement.GenericModels)
                {
                    if (genericModel.GetPhaseStatus(phaseId) != ElementOnPhaseStatus.New) continue;

                    FullUsingType fullUsingType = GetGenericModelFullUsingType(genericModel, doc);

                    int dim1 = PublicParameter.Dim1(genericModel);
                    int dim2 = PublicParameter.Dim2(genericModel);

                    AddNewComponent(fullUsingType, genericModel, doc, dim1, dim2);
                }

            if (_revitElement.Floors.Any())
                foreach (Element floor in _revitElement.Floors)
                {
                    if (floor.GetPhaseStatus(phaseId) != ElementOnPhaseStatus.New) continue;

                    FullUsingType fullUsingType = GetFullUsingType(floor, doc, 1500, "15 Перекриття");
                    AddNewComponent(fullUsingType, floor, doc);
                }

            if (_revitElement.Walls.Any())
                foreach (Element wall in _revitElement.Walls)
                {
                    if (wall.GetPhaseStatus(phaseId) != ElementOnPhaseStatus.New) continue;

                    FullUsingType fullUsingType = GetFullUsingType(wall, doc, 1600, "16 Стіни");
                    AddNewComponent(fullUsingType, wall, doc);
                }

            if (_steelProperties == null)
            {
                var transaction = new Transaction(doc, "GetSteelProperties");
                transaction.Start();
                _steelProperties = new SteelSettingsStorage(doc);
                transaction.Commit();
            }

            FullUsingType[] oldTypes = _steelProperties.GetUsingTypeCollection(phaseId.IntegerValue);

            if (oldTypes == null) return;
            foreach (FullUsingType fullUsingType in GroupTypes)
            {
                try
                {
                    FullUsingType oldType = oldTypes.First(type => fullUsingType.Equals(type));

                    fullUsingType.FirstMarkNumber = oldType.FirstMarkNumber;
                    fullUsingType.IsNecessaryToMark = oldType.IsNecessaryToMark;
                    fullUsingType.MarkPrefix = oldType.MarkPrefix;
                    fullUsingType.StructuralCategory = oldType.StructuralCategory;
                    fullUsingType.Aewj = oldType.Aewj;
                }
                catch (Exception)
                {
                    //
                }
            }
        }

        // OpenStructuralTypeProperties --------------------------------------------------------------------------------
        public void OpenStructuralTypeProperties(object obj)
        {
            _currentFullUsingType = (FullUsingType) obj;

            var structuralCategoryView = new StructuralCategoryView(_currentFullUsingType.StructuralCategory);
            var dialogClose = structuralCategoryView.ShowDialog();

            if (dialogClose == true)
            {
                _currentFullUsingType.StructuralCategory = structuralCategoryView.CheckedStructuralCategory;
            }
        }

        // MarkSteelElement --------------------------------------------------------------------------------------------
        private void MarkSteelElement(UIApplication uiApp)
        {
            Document doc = uiApp.ActiveUIDocument.Document;

            string importanceClass = PublicParameter.ImportanceClass(doc.ProjectInformation);

            int structuralGroup = (importanceClass == "СС3" || importanceClass == "СС3" || importanceClass == "CC3")
                ? 4
                : 0;

            using (var transaction = new Transaction(doc, "SetSteelMark"))
            {
                transaction.Start();

                if (_steelProperties == null)
                    _steelProperties = new SteelSettingsStorage(doc);


                _steelProperties.SetUsingTypeCollection(_currentPhase.Id.IntegerValue, GroupTypes.ToArray());

                Dictionary<string, List<FullUsingType>> fullUsingTypeGroups = GroupFullUsingTypeByMark(GroupTypes);

                foreach (KeyValuePair<string, List<FullUsingType>> fullUsingTypeGroup in fullUsingTypeGroups)
                {
                    var markNumber = 1;
                    foreach (FullUsingType fullUsingType in fullUsingTypeGroup.Value.Where(fullUsingType =>
                        fullUsingType.IsNecessaryToMark))
                    {
                        if (fullUsingType.FirstMarkNumber > 0)
                            markNumber = fullUsingType.FirstMarkNumber;

                        structuralGroup += StructuralCategoryData
                            .FindElementById(fullUsingType.StructuralCategory)
                            .PointNumber;

                        markNumber = MarkGroup(
                            doc,
                            ObjectGroup[fullUsingType],
                            fullUsingTypeGroup.Key,
                            markNumber,
                            structuralGroup,
                            ref _successfullyList,
                            ref _errorList,
                            ref _reportCounter);
                    }
                }

                transaction.Commit();
            }

            var skipList = new List<string> {"Пропущені"};
            var reportView = new ReportView(_reportCounter, _successfullyList, _errorList, skipList);
            reportView.Show();
        }

        // ElementMark -------------------------------------------------------------------------------------------------
        private static int MarkGroup(Document doc, List<Component> components, string prefixMark, int startNumber,
            int structuralGroup,
            ref List<string> successfullyList, ref List<string> errorList, ref int elementCounter)
        {
            foreach (Component component in components)
            {
                component.SetGroup(structuralGroup);
            }

            components.Sort();

            Component currentComponent = components.First();
            string mark = $"{prefixMark}{startNumber}";

            if (currentComponent.SetMark(mark))
            {
                successfullyList.Add(currentComponent.GetElementId());

                if (currentComponent.RvtElement.Category.Id == new ElementId(-2001336))
                {
                    MarkTrussElement(currentComponent, mark, doc);
                }
            }
            else
            {
                errorList.Add(currentComponent.GetElementId());
            }

            elementCounter++;

            if (components.Count > 1)
                for (var i = 1; i < components.Count; i++)
                {
                    if (!currentComponent.Equals(components[i]))
                    {
                        startNumber++;
                        currentComponent = components[i];
                        mark = $"{prefixMark}{startNumber}";
                    }

                    if (components[i].SetMark(mark))
                    {
                        successfullyList.Add(components[i].GetElementId());

                        if (currentComponent.RvtElement.Category.Id == new ElementId(-2001336))
                        {
                            MarkTrussElement(components[i], mark, doc);
                        }
                    }
                    else
                    {
                        errorList.Add(components[i].GetElementId());
                    }

                    elementCounter++;
                }

            startNumber++;
            return startNumber;
        }

        private static void MarkTrussElement(Component component, string mark, Document doc)
        {
            List<Component> trussMembersGroup = ((Truss) component.RvtElement).Members
                .Select(elementId => new Component(doc.GetElement(elementId), doc)).ToList();
            trussMembersGroup.Sort();

            var elementNumber = 1;
            Component currentComponent = trussMembersGroup.First();

            PublicParameter.MarkText(currentComponent.RvtElement, mark);
            PublicParameter.Position(currentComponent.RvtElement, elementNumber.ToString());

            if (trussMembersGroup.Count < 2) return;

            for (var i = 1; i < trussMembersGroup.Count; i++)
            {
                if (!currentComponent.Equals(trussMembersGroup[i]))
                {
                    elementNumber++;
                    currentComponent = trussMembersGroup[i];
                }

                PublicParameter.MarkText(currentComponent.RvtElement, mark);
                PublicParameter.Position(trussMembersGroup[i].RvtElement, elementNumber.ToString());
            }
        }

        // GroupType ---------------------------------------------------------------------------------------------------
        private static Dictionary<string, List<FullUsingType>> GroupFullUsingTypeByMark(
            IEnumerable<FullUsingType> types)
        {
            var result = new Dictionary<string, List<FullUsingType>>();

            foreach (FullUsingType fullUsingType in types)
            {
                if (fullUsingType.MarkPrefix == null)
                    fullUsingType.MarkPrefix = String.Empty;

                if (result.ContainsKey(fullUsingType.MarkPrefix))
                    result[fullUsingType.MarkPrefix].Add(fullUsingType);
                else
                    result.Add(fullUsingType.MarkPrefix, new List<FullUsingType> {fullUsingType});
            }

            return result;
        }

        #endregion

        #region FilteredMethods ----------------------------------------------------------------------------------------

        // GetFullUsingType --------------------------------------------------------------------------------------------
        private static FullUsingType GetFullUsingType(Element el, Document doc, int gutId, string gutName,
            int sutId = 0, string sutName = "")
        {
            ElementId generalUsingTypeObj = PublicParameter.GeneralUsingType(el);
            ElementId subUsingTypeObj = PublicParameter.SubUsingType(el);

            UsingType generalUsingType;
            UsingType subUsingType;

            if (generalUsingTypeObj == ElementId.InvalidElementId)
            {
                generalUsingType = new UsingType(gutId, gutName);
                subUsingType = new UsingType(sutId, sutName);
            }
            else
            {
                generalUsingType = new UsingType(doc.GetElement(generalUsingTypeObj));
                subUsingType =
                    subUsingTypeObj == ElementId.InvalidElementId
                        ? new UsingType()
                        : new UsingType(doc.GetElement(subUsingTypeObj));
            }

            return new FullUsingType(generalUsingType, subUsingType);
        }

        private static FullUsingType GetStructuralFramingFullUsingType(Element el, Document doc,
            StructuralInstanceUsage structuralUsage)
        {
            ElementId generalUsingTypeObj = PublicParameter.GeneralUsingType(el);
            ElementId subUsingTypeObj = PublicParameter.SubUsingType(el);

            UsingType generalUsingType;
            UsingType subUsingType;

            if (generalUsingTypeObj == ElementId.InvalidElementId)
            {
                string assemblyKod = PublicParameter.GetUniformatKode(((FamilyInstance) el).Symbol);

                // ReSharper disable once SwitchStatementMissingSomeCases
                switch (structuralUsage)
                {
                    case StructuralInstanceUsage.Brace:
                        generalUsingType = new UsingType(100, "01 Каркас");
                        subUsingType = new UsingType(400, "04 В'язь.Вертикальна");
                        break;
                    case StructuralInstanceUsage.HorizontalBracing:
                        generalUsingType = new UsingType(200, "02 Майданчик");
                        subUsingType = new UsingType(300, "03 В'язь.Горизонтальна");
                        break;
                    default:
                        if (assemblyKod == "B.02.02.02.03")
                        {
                            generalUsingType = new UsingType(500, "05 Вантажне");
                            subUsingType = new UsingType();
                        }
                        else
                        {
                            generalUsingType = new UsingType(200, "02 Майданчик");
                            subUsingType = new UsingType(200, "02 Балка");
                        }

                        break;
                }
            }
            else
            {
                generalUsingType = new UsingType(doc.GetElement(generalUsingTypeObj));
                subUsingType =
                    subUsingTypeObj == ElementId.InvalidElementId
                        ? new UsingType(200, "02 Балка")
                        : new UsingType(doc.GetElement(subUsingTypeObj));
            }

            return new FullUsingType(generalUsingType, subUsingType);
        }

        private static FullUsingType GetGenericModelFullUsingType(Element el, Document doc)
        {
            ElementId generalUsingTypeObj = PublicParameter.GeneralUsingType(el);
            ElementId subUsingTypeObj = PublicParameter.SubUsingType(el);

            UsingType generalUsingType;
            UsingType subUsingType;

            if (generalUsingTypeObj == ElementId.InvalidElementId)
            {
                switch (PublicParameter.GetUniformatKode(((FamilyInstance) el).Symbol))
                {
                    case "B.02.02.03.01":
                    case "B.02.02.03.02":
                    case "B.02.02.03":
                        generalUsingType = new UsingType(1000, "10 Сходи");
                        break;
                    case "B.02.03":
                        generalUsingType = new UsingType(1100, "11 Огорожа");
                        break;
                    default:
                        generalUsingType = new UsingType();
                        break;
                }

                subUsingType = new UsingType();
            }
            else
            {
                generalUsingType = new UsingType(doc.GetElement(generalUsingTypeObj));
                subUsingType = new UsingType(doc.GetElement(subUsingTypeObj));
            }

            return new FullUsingType(generalUsingType, subUsingType);
        }

        #endregion

        #region Command ------------------------------------------------------------------------------------------------

        // ReloadElementFromPhase --------------------------------------------------------------------------------------
        private DelegateCommand _reloadElementFromPhase;

        public ICommand ReloadElementFromPhase =>
            _reloadElementFromPhase ?? (_reloadElementFromPhase = new DelegateCommand(obj =>
            {
                ExternalHandler.RevitCommand = FilteredByPhase;
                ExternalHandler.ExEvent.Raise();
            }));

        // MarkSteelElementCommand -------------------------------------------------------------------------------------
        private DelegateCommand _markSteelElementCommand;

        public ICommand MarkSteelElementCommand =>
            _markSteelElementCommand ?? (_markSteelElementCommand = new DelegateCommand(obj =>
            {
                ExternalHandler.RevitCommand = MarkSteelElement;
                ExternalHandler.ExEvent.Raise();
            }));

        #endregion
    }
}