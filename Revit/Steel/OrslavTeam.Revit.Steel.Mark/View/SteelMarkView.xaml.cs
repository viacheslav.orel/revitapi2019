﻿using OrslavTeam.Revit.Steel.Mark.Repository;
using System.Windows;
using System.Windows.Controls;

namespace OrslavTeam.Revit.Steel.Mark.View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class SteelMarkView
    {
        public SteelMarkView()
        {
            InitializeComponent();
        }

        public SteelMarkView(RevitElement revitElements)
        {
            InitializeComponent();
            DataContext = new SteelMarkViewDataContext(revitElements);
        }

        private void ChangeStructuralCategory_OnClick(object sender, RoutedEventArgs e)
        {
            ((SteelMarkViewDataContext)DataContext).OpenStructuralTypeProperties(((Button)sender).DataContext);
        }
    }
}