﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using OrslavTeam.Revit.Steel.Axillary.Settings.StructuralCategories;

namespace OrslavTeam.Revit.Steel.Mark.View
{
    public partial class StructuralCategoryView
    {
        #region Constructors -------------------------------------------------------------------------------------------

        public StructuralCategoryView()
        {
            InitializeComponent();
        }

        public StructuralCategoryView(int[] ids)
        {
            InitializeComponent();

            var structuralCategoriesContainer = new Grid();
            CreateListItems(ref structuralCategoriesContainer, StructuralCategoryData.CategoriesData, ids);

            StructuralCategories.Content = structuralCategoriesContainer;
        }

        #endregion -----------------------------------------------------------------------------------------------------

        #region Fields -------------------------------------------------------------------------------------------------

        private int[] _checkedStructuralCategory;

        public int[] CheckedStructuralCategory => _checkedStructuralCategory;

        #endregion -----------------------------------------------------------------------------------------------------

        #region Methods ------------------------------------------------------------------------------------------------

        private int CreateListItems(ref Grid structuralTypeGrid, StructuralCategory[] categories, int[] ids,
            int currentRowNumber = 1)
        {
            foreach (StructuralCategory category in categories)
            {
                structuralTypeGrid.RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });

                Border border = CreateListItem(category, ids != null && category.Id == ids);
                Grid.SetRow(border, currentRowNumber++);
                structuralTypeGrid.Children.Add(border);

                if (category.SubCategory != null)
                {
                    currentRowNumber = CreateListItems(ref structuralTypeGrid, category.SubCategory, ids, currentRowNumber);
                }
            }

            return currentRowNumber;
        }

        private Border CreateListItem(StructuralCategory structuralCategory, bool currentItem)
        {
            // CreateVariable ------------------------------------------------------------------------------------------
            var border = new Border
            {
                BorderThickness = new Thickness(0, 0, 0, 1),
                BorderBrush = (Brush)FindResource("PrimaryBrash")
            };
            var grid = new Grid();

            grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(70) });
            grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(100) });
            grid.ColumnDefinitions.Add(new ColumnDefinition());
            grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(100) });
            grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(100) });

            if (structuralCategory.CategoryByStressType > 0)
            {
                var checker = new RadioButton
                {
                    IsChecked = currentItem,
                    GroupName = "currentStructuralGroup",
                    HorizontalAlignment = HorizontalAlignment.Center,
                    VerticalAlignment = VerticalAlignment.Center,
                    Style = (Style)FindResource("RadioButtonSimple"),
                    Tag = structuralCategory.Id
                };

                checker.Checked += StructuralCategory_Checked;

                Grid.SetColumn(checker, 0);
                grid.Children.Add(checker);
            }

            var idCell = new TextBlock
            {
                TextAlignment = TextAlignment.Left,
                VerticalAlignment = VerticalAlignment.Center,
                Text = string.Join("-", structuralCategory.Id)
            };
            Grid.SetColumn(idCell, 1);
            grid.Children.Add(idCell);

            var descriptionCell = new TextBlock
            {
                TextWrapping = TextWrapping.Wrap,
                Text = structuralCategory.Name
            };
            Grid.SetColumn(descriptionCell, 2);
            grid.Children.Add(descriptionCell);

            var categoryByUsingCell = new TextBlock
            {
                TextAlignment = TextAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center,
                Text = structuralCategory.CategoryByUsingType.ToString()
            };
            Grid.SetColumn(categoryByUsingCell, 3);
            grid.Children.Add(categoryByUsingCell);

            var categoryByStressCell = new TextBlock
            {
                TextAlignment = TextAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center,
                Text = structuralCategory.CategoryByStressType != 0 ? structuralCategory.CategoryByStressType.ToString() : ""
            };
            Grid.SetColumn(categoryByStressCell, 4);
            grid.Children.Add(categoryByStressCell);

            border.Child = grid;

            return border;
        }

        #endregion -----------------------------------------------------------------------------------------------------

        #region Events -------------------------------------------------------------------------------------------------

        private void StructuralCategory_Checked(object sender, RoutedEventArgs e)
        {
            _checkedStructuralCategory = ((RadioButton)sender).Tag as int[];
        }

        private void ApplyStructuralType_ButtonClick(object sender, RoutedEventArgs e)
        {
            if (_checkedStructuralCategory != null)
            {
                DialogResult = true;
                this.Close();
            }
            else
            {
                MessageBox.Show("Ви не обрали жодної категорії");
            }
        }

        #endregion -----------------------------------------------------------------------------------------------------
    }
}