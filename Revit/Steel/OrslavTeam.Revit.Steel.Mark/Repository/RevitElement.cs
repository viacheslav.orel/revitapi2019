﻿using Autodesk.Revit.DB;
using System.Collections.Generic;

namespace OrslavTeam.Revit.Steel.Mark.Repository
{
    public class RevitElement
    {
        #region Fileds

        // Elements ----------------------------------------------------------------------------------------------------
        private IEnumerable<Element> _columns;
        private IEnumerable<Element> _structuralFraming;
        private IEnumerable<Element> _structuralTrusses;
        private IEnumerable<Element> _genericModels;
        private IEnumerable<Element> _floors;
        private IEnumerable<Element> _walls;

        // PhaseArray --------------------------------------------------------------------------------------------------
        private List<Phase> _phases;

        #endregion

        #region FiledsAccess

        // Elements ----------------------------------------------------------------------------------------------------
        public IEnumerable<Element> Columns
        {
            get => _columns;
            set => _columns = value;
        }

        public IEnumerable<Element> StructuralFraming
        {
            get => _structuralFraming;
            set => _structuralFraming = value;
        }

        public IEnumerable<Element> StructuralTrusses
        {
            get => _structuralTrusses;
            set => _structuralTrusses = value;
        }

        public IEnumerable<Element> GenericModels
        {
            get => _genericModels;
            set => _genericModels = value;
        }

        public IEnumerable<Element> Floors
        {
            get => _floors;
            set => _floors = value;
        }

        public IEnumerable<Element> Walls
        {
            get => _walls;
            set => _walls = value;
        }

        // PhaseArray --------------------------------------------------------------------------------------------------
        public IEnumerable<Phase> GetPhases => _phases;

        public void SetPhases(PhaseArray phaseArray)
        {
            _phases = new List<Phase>();
            foreach (object phase in phaseArray)
            {
                _phases.Add(phase as Phase);
            }
        }

        #endregion
    }
}