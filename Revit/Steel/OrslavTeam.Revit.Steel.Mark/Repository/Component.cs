﻿using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Structure;
using Newtonsoft.Json;
using OrslavTeam.Revit.Axillary.PublicInformation;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;

namespace OrslavTeam.Revit.Steel.Mark.Repository
{
    public class Component : IComparable<Component>, IEquatable<Component>
    {
        #region Fields -------------------------------------------------------------------------------------------------

        // Identification ----------------------------------------------------------------------------------------------
        public string TypeName { get; set; }
        public int ProductMixId { get; }
        public int ProfileId { get; }

        public int MaterialId { get; }

        // StressQ -----------------------------------------------------------------------------------------------------
        public string StressQValue { get; }
        public double[] StressQ { get; set; }

        // StressM -----------------------------------------------------------------------------------------------------
        public string StressMValue { get; }
        public double[] StressM { get; set; }

        // StressN -----------------------------------------------------------------------------------------------------
        public string StressNValue { get; }
        public double[] StressN { get; set; }

        // OtherOrderParameters ----------------------------------------------------------------------------------------
        public int[] OtherOrderedParameters { get; }

        // GroupConstruction -------------------------------------------------------------------------------------------
        // ReSharper disable once UnassignedGetOnlyAutoProperty
        public int GroupConstruction { get; set; }

        // ModelElement ------------------------------------------------------------------------------------------------
        // ReSharper disable once NotAccessedField.Local
        private readonly Element _rvtElement;
        [JsonIgnore] public Element RvtElement => _rvtElement;

        private readonly ElementId _elementId;

        #endregion -----------------------------------------------------------------------------------------------------

        #region Contstructors ------------------------------------------------------------------------------------------

        public Component()
        {
        }

        public Component(Element element, Document doc, int[] otherOrderedParameters = null)
        {
            _rvtElement = element;
            _elementId = element.Id;

            switch (element)
            {
                case FamilyInstance familyInstance:
                    ProductMixId = PublicParameter.ProductMixKey(familyInstance.Symbol);
                    ProfileId = PublicParameter.ElementKey(familyInstance.Symbol);
                    MaterialId = GetMaterialId(element, doc);
                    break;
                case Truss truss:
                    ProductMixId = PublicParameter.ProductMixKey(truss.TrussType);
                    ProfileId = PublicParameter.ElementKey(truss.TrussType);
                    break;
                case Floor floor:
                    ProductMixId = PublicParameter.ProductMixKey(floor.FloorType);
                    ProfileId = PublicParameter.ElementKey(floor.FloorType);
                    break;
                case Wall wall:
                    ProductMixId = PublicParameter.ProductMixKey(wall.WallType);
                    ProfileId = PublicParameter.ElementKey(wall.WallType);
                    break;
            }

            if (ProductMixId == 0)
            {
                TypeName = element.get_Parameter(BuiltInParameter.ELEM_FAMILY_AND_TYPE_PARAM).AsValueString();
            }

            StressQ = new[]
            {
                PublicParameter.StressAMax(element),
                PublicParameter.StressAMin(element)
            };
            StressQValue = GetStress(StressQ);

            StressN = new[]
            {
                PublicParameter.StressNMax(element),
                PublicParameter.StressNMin(element)
            };
            StressNValue = GetStress(StressN);

            StressM = new[]
            {
                PublicParameter.StressMMax(element),
                PublicParameter.StressMMin(element)
            };
            StressMValue = GetStress(StressM);

            if (otherOrderedParameters != null)
                OtherOrderedParameters = otherOrderedParameters;

            _rvtElement = element;
        }

        #endregion -----------------------------------------------------------------------------------------------------

        #region Methods ------------------------------------------------------------------------------------------------

        private static string GetStress(IReadOnlyList<double> stressArray)
        {
            double stressMax = RoundStress(stressArray[0]);
            double stressMin = RoundStress(stressArray[1]);

            if (Math.Abs(stressMax) < 1 && Math.Abs(stressMin) < 1) return "";

            if (Math.Abs(stressMax - stressMin) < 1)
                return (stressMax > stressMin ? stressMax : stressMin).ToString(CultureInfo.CurrentCulture);

            if (Math.Abs(stressMax) - Math.Abs(stressMin) < 5) return $"±{stressMax}";

            return stressMax > stressMin ? $"{stressMax} / {stressMin}" : $"{stressMin} / {stressMax}";
        }

        private static double RoundStress(double stress)
        {
            if (stress < 50) return Math.Round(stress / 5.0, MidpointRounding.AwayFromZero) * 5;
            if (stress < 500) return Math.Round(stress / 50.0, MidpointRounding.AwayFromZero) * 50;
            if (stress < 10000) return Math.Round(stress / 500, MidpointRounding.AwayFromZero) * 500;
            return Math.Round(stress / 1000, MidpointRounding.AwayFromZero) * 1000;
        }

        public bool SetMark(string mark)
        {
            try
            {
                PublicParameter.MarkText(_rvtElement, mark);
                PublicParameter.StressN(_rvtElement, StressNValue);
                PublicParameter.StressA(_rvtElement, StressQValue);
                PublicParameter.StressM(_rvtElement, StressMValue);

                PublicParameter.StructuralGroup(_rvtElement, GroupConstruction.ToString());

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void SetGroup(int structuralGroup)
        {
            var structuralGroupPoint = 2;

            if (StressN.Any(n => n > 0))
                structuralGroupPoint = 7;

            structuralGroupPoint += structuralGroup;

            if (structuralGroupPoint <= 18) GroupConstruction = 4;
            else if (structuralGroupPoint <= 22) GroupConstruction = 3;
            else if (structuralGroupPoint <= 26) GroupConstruction = 2;
            else GroupConstruction = 1;
        }

        public string GetElementId()
        {
            return _elementId.ToString();
        }

        public int GetMaterialId(Element element, Document doc)
        {
            ElementId materialId = element.GetMaterialIds(false)?.FirstOrDefault() ?? ElementId.InvalidElementId;

            if (materialId == ElementId.InvalidElementId)
                return 0;

            Element material = doc.GetElement(materialId);
            return PublicParameter.MaterialKod(material);
        }

        #endregion -----------------------------------------------------------------------------------------------------

        #region ComparableMembers --------------------------------------------------------------------------------------

        [SuppressMessage("ReSharper", "PossibleNullReferenceException")]
        public int CompareTo(Component other)
        {
            if (ReferenceEquals(this, other)) return 0;
            if (ReferenceEquals(null, other)) return 1;

            if (ProductMixId == 0)
            {
                int typeNameComparison = string.CompareOrdinal(TypeName, other.TypeName);
                if (typeNameComparison != 0) return typeNameComparison;
            }
            else
            {
                int productMixIdComparison = ProductMixId.CompareTo(other.ProductMixId);
                if (productMixIdComparison != 0) return productMixIdComparison;

                int profileIdComparison = ProfileId.CompareTo(other.ProfileId);
                if (profileIdComparison != 0) return profileIdComparison;
            }

            int materialIdComparison = MaterialId.CompareTo(other.MaterialId);
            if (materialIdComparison != 0) return materialIdComparison;

            int groupConstructionComparison = GroupConstruction.CompareTo(other.GroupConstruction);
            if (groupConstructionComparison != 0) return groupConstructionComparison;

            int stressNComparison = string.CompareOrdinal(StressNValue, other.StressNValue);
            if (stressNComparison != 0) return stressNComparison;

            int stressQComparison = string.CompareOrdinal(StressQValue, other.StressQValue);
            if (stressQComparison != 0) return stressQComparison;

            int stressMComparison = string.CompareOrdinal(StressMValue, other.StressMValue);
            if (stressMComparison != 0) return stressMComparison;

            if (OtherOrderedParameters == null && other.OtherOrderedParameters == null)
                return 0;
            if (OtherOrderedParameters != null && other.OtherOrderedParameters == null)
                return 1;
            if (OtherOrderedParameters == null && other.OtherOrderedParameters != null)
                return -1;

            int otherOrderParametersLengthComparison =
                OtherOrderedParameters.Length.CompareTo(other.OtherOrderedParameters.Length);
            if (otherOrderParametersLengthComparison != 0) return otherOrderParametersLengthComparison;

            return OtherOrderedParameters.Select((t, i) =>
                    t.CompareTo(other.OtherOrderedParameters[i]))
                .FirstOrDefault(otherOrderParameterComparison => otherOrderParameterComparison != 0);
        }

        #endregion -----------------------------------------------------------------------------------------------------

        #region EquatableMembers ---------------------------------------------------------------------------------------

        public bool Equals(Component other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            bool baseEquality = (ProductMixId == 0
                                    ? TypeName == other.TypeName
                                    : ProductMixId == other.ProductMixId && ProfileId == other.ProfileId)
                                && MaterialId == other.MaterialId
                                && StressQValue == other.StressQValue
                                && StressMValue == other.StressMValue
                                && StressNValue == other.StressNValue;

            if (!baseEquality) return false;

            if (OtherOrderedParameters != null && other.OtherOrderedParameters != null)
            {
                if (OtherOrderedParameters.Length != other.OtherOrderedParameters.Length) return false;

                if (OtherOrderedParameters.Where((t, i) => t != other.OtherOrderedParameters[i]).Any())
                    return false;
            }
            else if (OtherOrderedParameters != null || other.OtherOrderedParameters != null)
                return false;

            return GroupConstruction == other.GroupConstruction;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Component)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode;

                if (ProductMixId == 0)
                {
                    // ReSharper disable once NonReadonlyMemberInGetHashCode
                    hashCode = TypeName.GetHashCode();
                }
                else
                {
                    hashCode = ProductMixId;
                    hashCode = hashCode * 397 + ProfileId;
                }

                hashCode = hashCode * 397 + MaterialId;
                hashCode = hashCode * 397 + StressQValue.GetHashCode();
                hashCode = hashCode * 397 + StressMValue.GetHashCode();
                hashCode = hashCode * 397 + StressNValue.GetHashCode();
                // ReSharper disable once NonReadonlyMemberInGetHashCode
                hashCode = hashCode * 397 + GroupConstruction;

                if (OtherOrderedParameters != null)
                {
                    hashCode = OtherOrderedParameters
                        .Aggregate(hashCode, (current, otherOrderedParameter) => current * 397 + otherOrderedParameter);
                }

                return hashCode;
            }
        }

        #endregion -----------------------------------------------------------------------------------------------------
    }
}