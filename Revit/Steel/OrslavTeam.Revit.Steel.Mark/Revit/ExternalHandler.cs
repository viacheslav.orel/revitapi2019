﻿using Autodesk.Revit.UI;
using OrslavTeam.Revit.Steel.Mark.Repository;
using OrslavTeam.Revit.Steel.Mark.View;
using System;

namespace OrslavTeam.Revit.Steel.Mark.Revit
{
    public class ExternalHandler : IExternalEventHandler
    {
        #region Fields -------------------------------------------------------------------------------------------------

        // ExternalEventData -------------------------------------------------------------------------------------------
        public static ExternalHandler Handler;
        public static ExternalEvent ExEvent;

        // ExternalEventCommand ----------------------------------------------------------------------------------------
        public delegate void RevitCommandDelegate(UIApplication uiApp);
        public static RevitCommandDelegate RevitCommand { get; set; }

        // SteelMarkView -----------------------------------------------------------------------------------------------
        private static SteelMarkView _steelMarkView;

        #endregion -----------------------------------------------------------------------------------------------------

        #region Methods ------------------------------------------------------------------------------------------------

        // ExternalEventData -------------------------------------------------------------------------------------------
        private static void WhenClosed(object sender, EventArgs args)
        {
            ExEvent.Dispose();
            ExEvent = null;
            Handler = null;
            _steelMarkView = null;
            RevitCommand = null;

            ((SteelMarkView)sender).Closed -= WhenClosed;
        }

        // SteelMarkView -----------------------------------------------------------------------------------------------
        public static void ShowDialog(RevitElement revitElements)
        {
            _steelMarkView = new SteelMarkView(revitElements);
            _steelMarkView.Closed += WhenClosed;

            _steelMarkView.Show();
        }

        #endregion -----------------------------------------------------------------------------------------------------

        #region Implementation of IExternalEventHandler ----------------------------------------------------------------

        public void Execute(UIApplication app)
        {
            RevitCommand(app);
        }

        public string GetName()
        {
            return nameof(ExternalHandler);
        }

        #endregion -----------------------------------------------------------------------------------------------------
    }
}