﻿using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Structure;
using Autodesk.Revit.UI;
using OrslavTeam.Revit.Axillary.PublicInformation;
using OrslavTeam.Revit.Steel.Mark.Repository;
using System.Collections.Generic;
using System.Linq;

namespace OrslavTeam.Revit.Steel.Mark.Revit
{
    [Transaction(TransactionMode.Manual)]
    public class AddInStart : IExternalCommand
    {
        #region Implementation of IExternalCommand ---------------------------------------------------------------------

        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            #region Fields ---------------------------------------------------------------------------------------------

            Document doc = commandData.Application.ActiveUIDocument.Document;

            FilteredElementCollector beamArea = new FilteredElementCollector(doc)
                .OfCategory(BuiltInCategory.OST_StructuralFramingSystem)
                .OfClass(typeof(BeamSystem));

            var transaction = new Transaction(doc, "TransferGutAndSut");
            transaction.Start();

            foreach (Element element in beamArea)
            {
                ElementId gutId = PublicParameter.GeneralUsingType(element);
                ElementId sutId = PublicParameter.SubUsingType(element);

                if (gutId == ElementId.InvalidElementId && sutId == ElementId.InvalidElementId) continue;

                ICollection<ElementId> beamIds = ((BeamSystem)element).GetBeamIds();

                foreach (ElementId elementId in beamIds)
                {
                    Element beam = doc.GetElement(elementId);

                    ElementId elementGutId = PublicParameter.GeneralUsingType(beam);
                    if (!elementGutId.Equals(gutId))
                        PublicParameter.GeneralUsingType(beam, gutId);

                    ElementId elementSutId = PublicParameter.SubUsingType(beam);
                    if (!elementSutId.Equals(sutId))
                        PublicParameter.SubUsingType(beam, sutId);
                }
            }

            transaction.Commit();

            // RevitElement --------------------------------------------------------------------------------------------
            var revitElements = new RevitElement
            {
                Columns = new FilteredElementCollector(doc)
                    .OfCategory(BuiltInCategory.OST_StructuralColumns)
                    .OfClass(typeof(FamilyInstance))
                    .Where(IsCorrectFamilyInstance),
                StructuralFraming = new FilteredElementCollector(doc)
                    .OfCategory(BuiltInCategory.OST_StructuralFraming)
                    .OfClass(typeof(FamilyInstance))
                    .Where(IsCorrectFamilyInstance),
                StructuralTrusses = new FilteredElementCollector(doc)
                    .OfCategory(BuiltInCategory.OST_StructuralTruss)
                    .OfClass(typeof(Truss))
                    .Where(truss => PublicParameter.GetUniformatKode(((Truss)truss).TrussType).StartsWith("B.02")),
                GenericModels = new FilteredElementCollector(doc)
                    .OfCategory(BuiltInCategory.OST_GenericModel)
                    .OfClass(typeof(FamilyInstance))
                    .Where(IsCorrectFamilyInstance),
                Floors = new FilteredElementCollector(doc)
                    .OfCategory(BuiltInCategory.OST_Floors)
                    .OfClass(typeof(Floor))
                    .Where(floor => PublicParameter.GetUniformatKode(((Floor)floor).FloorType).StartsWith("B.02")),
                Walls = new FilteredElementCollector(doc)
                    .OfCategory(BuiltInCategory.OST_Walls)
                    .OfClass(typeof(Wall))
                    .Where(wall => PublicParameter.GetUniformatKode(((Wall)wall).WallType).StartsWith("B.02"))
            };

            revitElements.SetPhases(doc.Phases);

            #endregion -------------------------------------------------------------------------------------------------

            #region CreateView -----------------------------------------------------------------------------------------

            ExternalHandler.Handler = new ExternalHandler();
            ExternalHandler.ExEvent = ExternalEvent.Create(ExternalHandler.Handler);
            ExternalHandler.ShowDialog(revitElements);

            #endregion -------------------------------------------------------------------------------------------------

            return Result.Succeeded;
        }

        #endregion -----------------------------------------------------------------------------------------------------

        #region Methods ------------------------------------------------------------------------------------------------

        private static bool IsCorrectFamilyInstance(Element elem)
        {
            if (!(elem is FamilyInstance familyInstance)) return false;

            bool isCorrectAssemblyKod = PublicParameter.GetUniformatKode(familyInstance.Symbol).StartsWith("B.02");
            int specificationFilter = PublicParameter.SpecificationFilter(elem);
            bool isChildElement = specificationFilter == 3;

            return isCorrectAssemblyKod && !isChildElement;
        }

        #endregion -----------------------------------------------------------------------------------------------------
    }
}