﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SQLite;
using OrslavTeam.Revit.Axillary.PublicInformation;
using OrslavTeam.Revit.Steel.Axillary.DbElement;
using OrslavTeam.Revit.Steel.Axillary.Repository.Primitive;

namespace OrslavTeam.Revit.Steel.Axillary
{
    public static class RevitDb
    {
        public static string GetProductMixName(int productMixId)
        {
            string result;

            try
            {
                using (var connection = new SQLiteConnection(PublicParameter.DBPath))
                {
                    connection.Open();

                    using (var command = new SQLiteCommand(connection))
                    {
                        command.CommandText = "SELECT product_name, product_number " +
                                              "FROM product_mix " +
                                              $"WHERE id = {productMixId}";
                        command.CommandType = CommandType.Text;

                        SQLiteDataReader reader = command.ExecuteReader();
                        reader.Read();

                        result = $"{reader.GetString(0)}\n{reader.GetString(1)}";

                        reader.Close();
                    }

                    connection.Close();
                }
            }
            catch (Exception)
            {
                result = "Error";
            }

            return result;
        }

        public static (string, double) GetProfileInfo(SshElement element)
        {
            string profileName;
            double massPerUnit;

            try
            {
                using (var connection = new SQLiteConnection(PublicParameter.DBPath))
                {
                    connection.Open();

                    using (var command = new SQLiteCommand(connection))
                    {
                        command.CommandText = "SELECT profile_name, mass_per_unit_length, mass_per_unit_area " +
                                              "FROM element_steel " +
                                              $"WHERE product_mix_id = {element.ProductMixId} AND element_id = {element.ProfileId}";
                        command.CommandType = CommandType.Text;

                        SQLiteDataReader reader = command.ExecuteReader();
                        reader.Read();

                        profileName = reader.GetString(0);
                        massPerUnit = reader.GetDouble(1);

                        if (massPerUnit + 1 < 0.001)
                            massPerUnit = reader.GetDouble(2);

                        reader.Close();
                    }

                    connection.Close();
                }
            }
            catch (Exception)
            {
                profileName = "Error";
                massPerUnit = 666666666;
            }

            return (profileName, massPerUnit);
        }

        public static string[] GetMaterialInfo(int materialId)
        {
            string[] result = new string[2];

            try
            {
                using (var connection = new SQLiteConnection(PublicParameter.DBPath))
                {
                    connection.Open();

                    using (var command = new SQLiteCommand(connection))
                    {
                        command.CommandText = "SELECT element_name, product_number " +
                                              "FROM material_steel " +
                                              "JOIN product_mix ON product_mix.id = material_steel.product_mix_id " +
                                              $"WHERE material_steel.id = {materialId}";
                        command.CommandType = CommandType.Text;

                        SQLiteDataReader reader = command.ExecuteReader();
                        reader.Read();

                        result[0] = reader.GetString(0);
                        result[1] = reader.GetString(1);
                    }
                }
            }
            catch (Exception)
            {
                result[0] = "Error";
            }

            return result;
        }


        public static ObservableCollection<OtProfile> GetProfileCollection(int productMixId)
        {
            var result = new ObservableCollection<OtProfile>();

            try
            {
                using (var connection = new SQLiteConnection(PublicParameter.DBPath))
                {
                    connection.Open();

                    using (var command = new SQLiteCommand(connection))
                    {
                        command.CommandText = "SELECT element_id, element_name " +
                                              "FROM element_steel " +
                                              $"WHERE product_mix_id = {productMixId}";
                        command.CommandType = CommandType.Text;

                        using (SQLiteDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                result.Add(new OtProfile
                                {
                                    Id = reader.GetInt32(0),
                                    Name = reader.GetString(1),
                                });
                            }

                            reader.Close();
                        }
                    }

                    connection.Close();
                }
            }
            catch (Exception)
            {
                //
            }

            return result;
        }

        public static OtProfile GetComponentInfo(int productId, int elementId)
        {
            var result = new OtProfile
            {
                Id = elementId,
                Name = "Error!"
            };

            using (var connection = new SQLiteConnection(PublicParameter.DBPath))
            {
                connection.Open();
                var parameterNames = new List<string> {"element_name", "profile_name"};

                using (SQLiteCommand getParameterNames = connection.CreateCommand())
                {
                    getParameterNames.CommandText =
                        @"SELECT name FROM product_mix_for_element_steel_parameters pmfesp 
                    JOIN element_steel_parameters ON pmfesp.element_steel_parameters_id = element_steel_parameters.id " +
                        $"WHERE product_mix_id = {productId}";
                    getParameterNames.CommandType = CommandType.Text;

                    using (SQLiteDataReader reader = getParameterNames.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            parameterNames.Add(reader.GetString(0));
                        }

                        reader.Close();
                    }
                }

                using (SQLiteCommand getComponentInfo = connection.CreateCommand())
                {
                    getComponentInfo.CommandText =
                        $"SELECT {string.Join(", ", parameterNames)} FROM element_steel " +
                        $"WHERE product_mix_id = {productId} AND element_id = {elementId}";
                    getComponentInfo.CommandType = CommandType.Text;

                    using (SQLiteDataReader reader = getComponentInfo.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            result.Name = reader.GetString(0);
                            result.ProfileName = reader.GetString(1);

                            var resultDimensions = new List<KeyValuePair<string, double>>();
                            for (int i = 2, max = reader.FieldCount; i < max; i++)
                            {
                                resultDimensions.Add(
                                    new KeyValuePair<string, double>(reader.GetName(i), reader.GetDouble(i)));
                            }

                            result.Parameters = resultDimensions.ToArray();
                        }

                        reader.Close();
                    }
                }

                connection.Close();
            }

            return result;
        }
    }
}