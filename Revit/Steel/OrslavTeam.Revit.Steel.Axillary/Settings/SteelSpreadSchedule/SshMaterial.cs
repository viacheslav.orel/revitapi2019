﻿using System.Collections.Generic;
using System.Linq;
using OrslavTeam.Revit.Steel.Axillary.DbElement;

namespace OrslavTeam.Revit.Steel.Axillary.Settings.SteelSpreadSchedule
{
    public class SshMaterial
    {
        #region fields -------------------------------------------------------------------------------------------------

        public string MaterialName { get; set; }
        public string MaterialProductMixNumber { get; set; }
        public Dictionary<int, SshProfile> ProfileRows { get; set; }

        #endregion


        #region content rule -------------------------------------------------------------------------------------------

        public SshMaterial() { }

        public SshMaterial(IReadOnlyList<SshElement> elements, int columnQuantity)
        {
            string[] materialInfo = RevitDb.GetMaterialInfo(elements[0].MaterialId);

            MaterialName = materialInfo[0];
            MaterialProductMixNumber = materialInfo[1];

            ProfileRows = elements
                .GroupBy(element => element.ProfileId)
                .ToDictionary(
                    elementGroups => elementGroups.Key,
                    elementGroups => new SshProfile(elementGroups.ToList(), columnQuantity));
        }

        public void AddMaterialRow(IReadOnlyList<SshElement> elements, int columnQuantity)
        {
            IEnumerable<IGrouping<int, SshElement>> elementsGroups = elements.GroupBy(element => element.ProfileId);

            foreach (IGrouping<int, SshElement> elementGroup in elementsGroups)
            {
                if (ProfileRows.ContainsKey(elementGroup.Key))
                {
                    ProfileRows[elementGroup.Key].AddElement(elementGroup.ToList());
                }
                else
                {
                    ProfileRows.Add(elementGroup.Key, new SshProfile(elementGroup.ToList(), columnQuantity));
                }
            }
        }

        #endregion
    }
}