﻿using System;
using System.Collections.Generic;
using OrslavTeam.Revit.Steel.Axillary.DbElement;

namespace OrslavTeam.Revit.Steel.Axillary.Settings.SteelSpreadSchedule
{
    public class SshProfile
    {
        #region fields -------------------------------------------------------------------------------------------------

        private readonly double _massPerUnit;

        #endregion


        #region fields public ------------------------------------------------------------------------------------------

        public string ProfileName { get; set; }

        public double[] MassByColumn { get; set; }

        #endregion


        #region content rule -------------------------------------------------------------------------------------------

        public SshProfile() { }

        public SshProfile(IReadOnlyList<SshElement> elements, int columnQuantity)
        {
            (string, double) profileInfo = RevitDb.GetProfileInfo(elements[0]);
            ProfileName = profileInfo.Item1;
            _massPerUnit = profileInfo.Item2;

            MassByColumn = new double[columnQuantity];

            foreach (SshElement element in elements)
            {
                MassByColumn[element.ColumnNumber - 1] += _massPerUnit * element.Units * element.Quantity;
            }
        }

        public void AddElement(IReadOnlyList<SshElement> elements)
        {
            foreach (SshElement element in elements)
            {
                MassByColumn[element.ColumnNumber - 1] += _massPerUnit * element.Units * element.Quantity;
            }
        }

        #endregion
    }
}