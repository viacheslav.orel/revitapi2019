﻿using System.Collections.Generic;
using System.Linq;
using OrslavTeam.Revit.Steel.Axillary.DbElement;
using OrslavTeam.Revit.Steel.Axillary.Settings.SteelSpread;

namespace OrslavTeam.Revit.Steel.Axillary.Settings.SteelSpreadSchedule
{
    public class SshTable
    {
        #region fields -------------------------------------------------------------------------------------------------

        public SteelSpreadObj Header { get; set; } = new SteelSpreadObj();
        public string[] Columns { get; set; }

        public Dictionary<int, SshProductMix> ProductMixCollection { get; set; }

        public Dictionary<string, double[]> TotalMassByMaterial { get; set; }
        public double[] TotalMassByColumn { get; set; }

        #endregion


        #region fields technical ---------------------------------------------------------------------------------------

        private readonly int _columnQuantity;

        #endregion


        #region content rule -------------------------------------------------------------------------------------------

        public SshTable() { }

        public SshTable(SteelSpreadObj header)
        {
            Header = header;

            Columns = header.ColumnsInfo
                .OrderBy(column => column.Point)
                .Select(column => column.Header)
                .ToArray();

            _columnQuantity = Columns.Length;

            ProductMixCollection = new Dictionary<int, SshProductMix>();

            TotalMassByMaterial = new Dictionary<string, double[]>();
            TotalMassByColumn = new double[_columnQuantity];
        }

        public void AddElements(IReadOnlyList<SshElement> elements)
        {
            IEnumerable<IGrouping<int, SshElement>> elementsGroups = elements.GroupBy(element => element.ProductMixId);

            foreach (IGrouping<int, SshElement> elementsGroup in elementsGroups)
            {
                if (ProductMixCollection.ContainsKey(elementsGroup.Key))
                {
                    ProductMixCollection[elementsGroup.Key].AddProductMixRow(elementsGroup.ToList(), _columnQuantity);
                }
                else
                {
                    ProductMixCollection.Add(elementsGroup.Key, new SshProductMix(elementsGroup.ToList(), _columnQuantity));
                }
            }
        }

        #endregion
    }
}