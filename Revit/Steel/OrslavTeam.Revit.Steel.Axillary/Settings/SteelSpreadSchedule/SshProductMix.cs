﻿using System.Collections.Generic;
using System.Linq;
using OrslavTeam.Revit.Steel.Axillary.DbElement;

namespace OrslavTeam.Revit.Steel.Axillary.Settings.SteelSpreadSchedule
{
    public class SshProductMix
    {
        #region fields -------------------------------------------------------------------------------------------------

        public string ProductMixName { get; set; }
        public Dictionary<int, SshMaterial> MaterialCollection { get; set; }

        #endregion


        #region content rule -------------------------------------------------------------------------------------------

        public SshProductMix() { }

        public SshProductMix(IReadOnlyList<SshElement> elements, int columnQuantity)
        {
            ProductMixName = RevitDb.GetProductMixName(elements.First().ProductMixId);

            MaterialCollection = elements
                .GroupBy(element => element.MaterialId)
                .ToDictionary(
                    elementGroup => elementGroup.Key,
                    elementGroup => new SshMaterial(elementGroup.ToList(), columnQuantity));
        }

        public void AddProductMixRow(IReadOnlyList<SshElement> elements, int columnQuantity)
        {
            IEnumerable<IGrouping<int, SshElement>> elementGroups = elements.GroupBy(element => element.MaterialId);

            foreach (IGrouping<int, SshElement> elementGroup in elementGroups)
            {
                if (MaterialCollection.ContainsKey(elementGroup.Key))
                {
                    MaterialCollection[elementGroup.Key].AddMaterialRow(elementGroup.ToList(), columnQuantity);
                }
                else
                {
                    MaterialCollection.Add(elementGroup.Key, new SshMaterial(elementGroup.ToList(), columnQuantity));
                }
            }
        }

        #endregion
    }
}