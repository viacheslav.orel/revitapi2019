﻿using System;
using System.Linq;
using OrslavTeam.Revit.Axillary.ViewTemplate.Infrastructure;
using OrslavTeam.Revit.Steel.Axillary.Settings.SteelSpread;
using OrslavTeam.Revit.Steel.Axillary.Settings.UsingType;

namespace OrslavTeam.Revit.Steel.Axillary.Settings.SteelSpreadSchedule
{
    public class SettingRow : ObservableObject
    {
        private readonly FullUsingType _fullUsingType;

        public string FullUsingTypeName => _fullUsingType.FullName;

        public int FullUsingTypeId => _fullUsingType.Id;

        public static SteelSpreadColumn[] Columns { get; set; }

        

        private SteelSpreadColumn _column;

        public SteelSpreadColumn Column
        {
            get => _column;
            set
            {
                _column = value;
                RaisePropertyChangedEvent("Column");
            }
        }



        public SettingRow(FullUsingType fullUsingType)
        {
            _fullUsingType = fullUsingType;

            if (Columns == null) return;

            Column = GetCurrentColumn(Columns, fullUsingType.SteelSpreadColumnId);
        }



        public void RaiseColumnsChanged()
        {
            RaisePropertyChangedEvent("Columns");
            Column = GetCurrentColumn(Columns, _fullUsingType.SteelSpreadColumnId);
        }

        private static SteelSpreadColumn GetCurrentColumn(SteelSpreadColumn[] columns, Guid columnId)
        {
            return columns.FirstOrDefault(column => column.Id == columnId);
        }

        public FullUsingType GetFullUsingType()
        {
            _fullUsingType.SteelSpreadColumnId = Column?.Id ?? Guid.Empty;

            return _fullUsingType;
        }
    }
}