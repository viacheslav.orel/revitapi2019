﻿using Newtonsoft.Json;
using OrslavTeam.Revit.Axillary.ViewTemplate.Infrastructure;
using OrslavTeam.Revit.Steel.Axillary.Settings.UsingType;

namespace OrslavTeam.Revit.Steel.Axillary.Settings.SteelSpread
{
    public class SteelSpreadObj : ObservableObject
    {
        #region Format -------------------------------------------------------------------------------------------------
        [JsonIgnore]
        private string _formatName = "А4";
        [JsonIgnore]
        public string FormatName
        {
            get => _formatName;
            set
            {
                _formatName = value;
                RaisePropertyChangedEvent("FormatName");
            }
        }

        [JsonIgnore]
        private int _formatWidth = 210;
        [JsonIgnore]
        public double FormatWidth
        {
            get => (_formatWidth / 10.0) * (96 / 2.54);
            set
            {
                _formatWidth = (int)value;
                RaisePropertyChangedEvent("FormatWidth");
            }
        }

        [JsonIgnore]
        private int _formatHeight = 297;
        [JsonIgnore]
        public double FormatHeight
        {
            get => (_formatHeight / 10.0) * (96 / 2.54);
            set
            {
                _formatHeight = (int)value;
                RaisePropertyChangedEvent("FormatHeight");
            }
        }

        public int GetHeight => _formatHeight;

        #endregion
        

        #region BaseFields ---------------------------------------------------------------------------------------------

        public int PhaseId { get; set; }

        public FullUsingType[] FullUsingTypes { get; set; }

        public SteelSpreadColumn[] ColumnsInfo { get; set; }

        #endregion


        #region SteelSpreadHeaders -------------------------------------------------------------------------------------

        private string _steelSpreadName = "Специфікація металопрокату";

        public string SteelSpreadName
        {
            get => _steelSpreadName;
            set
            {
                _steelSpreadName = value;
                RaisePropertyChangedEvent("SteelSpreadName");
            }
        }


        private string _steelSpreadColumn1 = "Найменування профіля\nДСТУ, ГОСТ, ТУ";

        public string SteelSpreadColumn1
        {
            get => _steelSpreadColumn1;
            set
            {
                _steelSpreadColumn1 = value;
                RaisePropertyChangedEvent("SteelSpreadColumn1");
            }
        }


        private string _steelSpreadColumn2 = "Найменування або марка металу\nДСТУ, ГОСТ, ТУ";

        public string SteelSpreadColumn2
        {
            get => _steelSpreadColumn2;
            set
            {
                _steelSpreadColumn2 = value;
                RaisePropertyChangedEvent("SteelSpreadColumn2");
            }
        }


        private string _steelSpreadColumn3 = "Номер або розміри профілю, мм";

        public string SteelSpreadColumn3
        {
            get => _steelSpreadColumn3;
            set
            {
                _steelSpreadColumn3 = value;
                RaisePropertyChangedEvent("SteelSpreadColumn3");
            }
        }


        private string _steelSpreadColumn4 = "№\nр.с.";

        public string SteelSpreadColumn4
        {
            get => _steelSpreadColumn4;
            set
            {
                _steelSpreadColumn4 = value;
                RaisePropertyChangedEvent("SteelSpreadColumn4");
            }
        }


        private string _unitType = "кг";

        public string UnitType
        {
            get => _unitType;
            set
            {
                _unitType = value;
                RaisePropertyChangedEvent("SteelSpreadColumn5WithUnit");
                RaisePropertyChangedEvent("SteelSpreadColumn6WithUnit");
            }
        }


        private string _steelSpreadColumn5 = "Маса металу за видами елементів конструкці ";

        public string SteelSpreadColumn5
        {
            get => _steelSpreadColumn5;
            set
            {
                _steelSpreadColumn5 = value;
                RaisePropertyChangedEvent("SteelSpreadColumn5");
            }
        }
        [JsonIgnore]
        public string SteelSpreadColumn5WithUnit
        {
            get => $"{_steelSpreadColumn5}, {UnitType}";
            set
            {
                _steelSpreadColumn5 = value;
                RaisePropertyChangedEvent("SteelSpreadColumn5");
            }
        }


        private string _steelSpreadColumn6 = "Загальна маса ";

        public string SteelSpreadColumn6
        {
            get => _steelSpreadColumn6;
            set
            {
                _steelSpreadColumn6 = value;
                RaisePropertyChangedEvent("SteelSpreadColumn6");
            }
        }
        [JsonIgnore]
        public string SteelSpreadColumn6WithUnit
        {
            get => $"{_steelSpreadColumn6}, {UnitType}";
            set
            {
                _steelSpreadColumn6 = value;
                RaisePropertyChangedEvent("SteelSpreadColumn6");
            }
        }


        private string _steelSpreadTotalMassByMaterial = "Разом:";

        public string SteelSpreadTotalMassByMaterial
        {
            get => _steelSpreadTotalMassByMaterial;
            set
            {
                _steelSpreadTotalMassByMaterial = value;
                RaisePropertyChangedEvent("SteelSpreadTotalMassByMaterial");
            }
        }


        private string _steelSpreadTotalMassByProductMix = "Всього профілю:";

        public string SteelSpreadTotalMassByProductMix
        {
            get => _steelSpreadTotalMassByProductMix;
            set
            {
                _steelSpreadTotalMassByProductMix = value;
                RaisePropertyChangedEvent("SteelSpreadTotalMassByProductMix");
            }
        }


        private string _steelSpreadTotalMass = "Всього маса металу:";

        public string SteelSpreadTotalMass
        {
            get => _steelSpreadTotalMass;
            set
            {
                _steelSpreadTotalMass = value;
                RaisePropertyChangedEvent("SteelSpreadTotalMass");
            }
        }


        private string _steelSpreadTotalMassBySteelName = "У тому числі за марками або найменуванням:";

        public string SteelSpreadTotalMassBySteelName
        {
            get => _steelSpreadTotalMassBySteelName;
            set
            {
                _steelSpreadTotalMassBySteelName = value;
                RaisePropertyChangedEvent("SteelSpreadTotalMassBySteelName");
            }
        }

        #endregion


        #region GeneralTextFiled ---------------------------------------------------------------------------------------

        private string _pageField1 = "Позначення документа";

        public string PageField1
        {
            get => _pageField1;
            set
            {
                _pageField1 = value;
                RaisePropertyChangedEvent("PageField1");
            }
        }


        private string _pageField2 = "Найменування підприємства";

        public string PageField2
        {
            get => _pageField2;
            set
            {
                _pageField2 = value;
                RaisePropertyChangedEvent("PageField2");
            }
        }


        private string _pageField3 = "Найменування будинку";

        public string PageField3
        {
            get => _pageField3;
            set
            {
                _pageField3 = value;
                RaisePropertyChangedEvent("PageField3");
            }
        }


        private string _pageField4 = "Специфікація металопрокату";

        public string PageField4
        {
            get => _pageField4;
            set
            {
                _pageField4 = value;
                RaisePropertyChangedEvent("PageField4");
            }
        }


        private string _pageField6Name = "Стадія";

        public string PageField6Name
        {
            get => _pageField6Name;
            set
            {
                _pageField6Name = value;
                RaisePropertyChangedEvent("PageField6Name");
            }
        }


        private string _pageField6 = "П";

        public string PageField6
        {
            get => _pageField6;
            set
            {
                _pageField6 = value;
                RaisePropertyChangedEvent("PageField6");
            }
        }


        private string _pageField7Name = "Аркуш";

        public string PageField7Name
        {
            get => _pageField7Name;
            set
            {
                _pageField7Name = value;
                RaisePropertyChangedEvent("PageField7Name");
            }
        }


        private string _pageField8Name = "Аркушів";

        public string PageField8Name
        {
            get => _pageField8Name;
            set
            {
                _pageField8Name = value;
                RaisePropertyChangedEvent("PageField8Name");
            }
        }


        private string _pageField8;

        public string PageField8
        {
            get => _pageField8;
            set
            {
                _pageField8 = value;
                RaisePropertyChangedEvent("PageField8");
            }
        }


        private string _pageField9 = "Промаспект\nм.Одеса";

        public string PageField9
        {
            get => _pageField9;
            set
            {
                _pageField9 = value;
                RaisePropertyChangedEvent("PageField9");
            }
        }


        // -------------------------------------------------------------------------------------------------------------
        //row1
        private string _pageField10Row1 = "Розробив";

        public string PageField10Row1
        {
            get => _pageField10Row1;
            set
            {
                _pageField10Row1 = value;
                RaisePropertyChangedEvent("PageField10Row1");
            }
        }


        private string _pageField11Row1;

        public string PageField11Row1
        {
            get => _pageField11Row1;
            set
            {
                _pageField11Row1 = value;
                RaisePropertyChangedEvent("PageField11Row1");
            }
        }


        private string _pageField12Row1;

        public string PageField12Row1
        {
            get => _pageField12Row1;
            set
            {
                _pageField12Row1 = value;
                RaisePropertyChangedEvent("PageField12Row1");
            }
        }


        private string _pageField13Row1;

        public string PageField13Row1
        {
            get => _pageField13Row1;
            set
            {
                _pageField13Row1 = value;
                RaisePropertyChangedEvent("PageField13Row1");
            }
        }


        // -------------------------------------------------------------------------------------------------------------
        // row2
        private string _pageField10Row2 = "Перевірив";

        public string PageField10Row2
        {
            get => _pageField10Row2;
            set
            {
                _pageField10Row2 = value;
                RaisePropertyChangedEvent("PageField10Row2");
            }
        }


        private string _pageField11Row2;

        public string PageField11Row2
        {
            get => _pageField11Row2;
            set
            {
                _pageField11Row2 = value;
                RaisePropertyChangedEvent("PageField11Row2");
            }
        }


        private string _pageField12Row2;

        public string PageField12Row2
        {
            get => _pageField12Row2;
            set
            {
                _pageField12Row2 = value;
                RaisePropertyChangedEvent("PageField12Row2");
            }
        }


        private string _pageField13Row2;

        public string PageField13Row2
        {
            get => _pageField13Row2;
            set
            {
                _pageField13Row2 = value;
                RaisePropertyChangedEvent("PageField13Row2");
            }
        }


        // -------------------------------------------------------------------------------------------------------------
        // row3
        private string _pageField10Row3;

        public string PageField10Row3
        {
            get => _pageField10Row3;
            set
            {
                _pageField10Row3 = value;
                RaisePropertyChangedEvent("PageField10Row3");
            }
        }


        private string _pageField11Row3;

        public string PageField11Row3
        {
            get => _pageField11Row3;
            set
            {
                _pageField11Row3 = value;
                RaisePropertyChangedEvent("PageField11Row3");
            }
        }


        private string _pageField12Row3;

        public string PageField12Row3
        {
            get => _pageField12Row3;
            set
            {
                _pageField12Row3 = value;
                RaisePropertyChangedEvent("PageField12Row3");
            }
        }


        private string _pageField13Row3;

        public string PageField13Row3
        {
            get => _pageField13Row3;
            set
            {
                _pageField13Row3 = value;
                RaisePropertyChangedEvent("PageField13Row3");
            }
        }


        // -------------------------------------------------------------------------------------------------------------
        // row4
        private string _pageField10Row4 = "Н.-контр.";

        public string PageField10Row4
        {
            get => _pageField10Row4;
            set
            {
                _pageField10Row4 = value;
                RaisePropertyChangedEvent("PageField10Row4");
            }
        }


        private string _pageField11Row4;

        public string PageField11Row4
        {
            get => _pageField11Row4;
            set
            {
                _pageField11Row4 = value;
                RaisePropertyChangedEvent("PageField11Row4");
            }
        }


        private string _pageField12Row4;

        public string PageField12Row4
        {
            get => _pageField12Row4;
            set
            {
                _pageField12Row4 = value;
                RaisePropertyChangedEvent("PageField12Row4");
            }
        }


        private string _pageField13Row4;

        public string PageField13Row4
        {
            get => _pageField13Row4;
            set
            {
                _pageField13Row4 = value;
                RaisePropertyChangedEvent("PageField13Row4");
            }
        }


        // -------------------------------------------------------------------------------------------------------------
        // row5
        private string _pageField10Row5 = "ГІП";

        public string PageField10Row5
        {
            get => _pageField10Row5;
            set
            {
                _pageField10Row5 = value;
                RaisePropertyChangedEvent("PageField10Row5");
            }
        }


        private string _pageField11Row5;

        public string PageField11Row5
        {
            get => _pageField11Row5;
            set
            {
                _pageField11Row5 = value;
                RaisePropertyChangedEvent("PageField11Row5");
            }
        }


        private string _pageField12Row5;

        public string PageField12Row5
        {
            get => _pageField12Row5;
            set
            {
                _pageField12Row5 = value;
                RaisePropertyChangedEvent("PageField12Row5");
            }
        }


        private string _pageField13Row5;

        public string PageField13Row5
        {
            get => _pageField13Row5;
            set
            {
                _pageField13Row5 = value;
                RaisePropertyChangedEvent("PageField13Row5");
            }
        }


        // -------------------------------------------------------------------------------------------------------------
        // row6
        private string _pageField10Row6 = "Затвердив";

        public string PageField10Row6
        {
            get => _pageField10Row6;
            set
            {
                _pageField10Row6 = value;
                RaisePropertyChangedEvent("PageField10Row6");
            }
        }


        private string _pageField11Row6;

        public string PageField11Row6
        {
            get => _pageField11Row6;
            set
            {
                _pageField11Row6 = value;
                RaisePropertyChangedEvent("PageField11Row6");
            }
        }


        private string _pageField12Row6;

        public string PageField12Row6
        {
            get => _pageField12Row6;
            set
            {
                _pageField12Row6 = value;
                RaisePropertyChangedEvent("PageField12Row6");
            }
        }


        private string _pageField13Row6;

        public string PageField13Row6
        {
            get => _pageField13Row6;
            set
            {
                _pageField13Row6 = value;
                RaisePropertyChangedEvent("PageField13Row6");
            }
        }


        // -------------------------------------------------------------------------------------------------------------
        // row1
        private string _pageField14Row1;

        public string PageField14Row1
        {
            get => _pageField14Row1;
            set
            {
                _pageField14Row1 = value;
                RaisePropertyChangedEvent("PageField14Row1");
            }
        }


        private string _pageField15Row1;

        public string PageField15Row1
        {
            get => _pageField15Row1;
            set
            {
                _pageField15Row1 = value;
                RaisePropertyChangedEvent("PageField15Row1");
            }
        }


        private string _pageField16Row1;

        public string PageField16Row1
        {
            get => _pageField16Row1;
            set
            {
                _pageField16Row1 = value;
                RaisePropertyChangedEvent("PageField16Row1");
            }
        }


        private string _pageField17Row1;

        public string PageField17Row1
        {
            get => _pageField17Row1;
            set
            {
                _pageField17Row1 = value;
                RaisePropertyChangedEvent("PageField17Row1");
            }
        }


        private string _pageField18Row1;

        public string PageField18Row1
        {
            get => _pageField18Row1;
            set
            {
                _pageField18Row1 = value;
                RaisePropertyChangedEvent("_pageField18Row1");
            }
        }


        private string _pageField19Row1;

        public string PageField19Row1
        {
            get => _pageField19Row1;
            set
            {
                _pageField19Row1 = value;
                RaisePropertyChangedEvent("PageField19Row1");
            }
        }


        // -------------------------------------------------------------------------------------------------------------
        // row2
        private string _pageField14Row2;

        public string PageField14Row2
        {
            get => _pageField14Row2;
            set
            {
                _pageField14Row2 = value;
                RaisePropertyChangedEvent("PageField14Row2");
            }
        }


        private string _pageField15Row2;

        public string PageField15Row2
        {
            get => _pageField15Row2;
            set
            {
                _pageField15Row2 = value;
                RaisePropertyChangedEvent("PageField15Row2");
            }
        }


        private string _pageField16Row2;

        public string PageField16Row2
        {
            get => _pageField16Row2;
            set
            {
                _pageField16Row2 = value;
                RaisePropertyChangedEvent("PageField16Row2");
            }
        }


        private string _pageField17Row2;

        public string PageField17Row2
        {
            get => _pageField17Row2;
            set
            {
                _pageField17Row2 = value;
                RaisePropertyChangedEvent("PageField17Row2");
            }
        }


        private string _pageField18Row2;

        public string PageField18Row2
        {
            get => _pageField18Row2;
            set
            {
                _pageField18Row2 = value;
                RaisePropertyChangedEvent("_pageField18Row2");
            }
        }


        private string _pageField19Row2;

        public string PageField19Row2
        {
            get => _pageField19Row2;
            set
            {
                _pageField19Row2 = value;
                RaisePropertyChangedEvent("PageField19Row2");
            }
        }


        // -------------------------------------------------------------------------------------------------------------
        // row3
        private string _pageField14Row3;

        public string PageField14Row3
        {
            get => _pageField14Row3;
            set
            {
                _pageField14Row3 = value;
                RaisePropertyChangedEvent("PageField14Row3");
            }
        }


        private string _pageField15Row3;

        public string PageField15Row3
        {
            get => _pageField15Row3;
            set
            {
                _pageField15Row3 = value;
                RaisePropertyChangedEvent("PageField15Row3");
            }
        }


        private string _pageField16Row3;

        public string PageField16Row3
        {
            get => _pageField16Row3;
            set
            {
                _pageField16Row3 = value;
                RaisePropertyChangedEvent("PageField16Row3");
            }
        }


        private string _pageField17Row3;

        public string PageField17Row3
        {
            get => _pageField17Row3;
            set
            {
                _pageField17Row3 = value;
                RaisePropertyChangedEvent("PageField17Row3");
            }
        }


        private string _pageField18Row3;

        public string PageField18Row3
        {
            get => _pageField18Row3;
            set
            {
                _pageField18Row3 = value;
                RaisePropertyChangedEvent("_pageField18Row3");
            }
        }


        private string _pageField19Row3;

        public string PageField19Row3
        {
            get => _pageField19Row3;
            set
            {
                _pageField19Row3 = value;
                RaisePropertyChangedEvent("PageField19Row3");
            }
        }


        // -------------------------------------------------------------------------------------------------------------
        // row4
        private string _pageField14Row4;

        public string PageField14Row4
        {
            get => _pageField14Row4;
            set
            {
                _pageField14Row4 = value;
                RaisePropertyChangedEvent("PageField14Row4");
            }
        }


        private string _pageField15Row4;

        public string PageField15Row4
        {
            get => _pageField15Row4;
            set
            {
                _pageField15Row4 = value;
                RaisePropertyChangedEvent("PageField15Row4");
            }
        }


        private string _pageField16Row4;

        public string PageField16Row4
        {
            get => _pageField16Row4;
            set
            {
                _pageField16Row4 = value;
                RaisePropertyChangedEvent("PageField16Row4");
            }
        }


        private string _pageField17Row4;

        public string PageField17Row4
        {
            get => _pageField17Row4;
            set
            {
                _pageField17Row4 = value;
                RaisePropertyChangedEvent("PageField17Row4");
            }
        }


        private string _pageField18Row4;

        public string PageField18Row4
        {
            get => _pageField18Row4;
            set
            {
                _pageField18Row4 = value;
                RaisePropertyChangedEvent("_pageField18Row4");
            }
        }


        private string _pageField19Row4;

        public string PageField19Row4
        {
            get => _pageField19Row4;
            set
            {
                _pageField19Row4 = value;
                RaisePropertyChangedEvent("PageField19Row4");
            }
        }


        // -------------------------------------------------------------------------------------------------------------
        // row5
        private string _pageField14Row5 = "Зм.";

        public string PageField14Row5
        {
            get => _pageField14Row5;
            set
            {
                _pageField14Row5 = value;
                RaisePropertyChangedEvent("PageField14Row5");
            }
        }


        private string _pageField15Row5 = "Кільк";

        public string PageField15Row5
        {
            get => _pageField15Row5;
            set
            {
                _pageField15Row5 = value;
                RaisePropertyChangedEvent("PageField15Row5");
            }
        }


        private string _pageField16Row5 = "Арк";

        public string PageField16Row5
        {
            get => _pageField16Row5;
            set
            {
                _pageField16Row5 = value;
                RaisePropertyChangedEvent("PageField16Row5");
            }
        }


        private string _pageField17Row5 = "№ док";

        public string PageField17Row5
        {
            get => _pageField17Row5;
            set
            {
                _pageField17Row5 = value;
                RaisePropertyChangedEvent("PageField17Row5");
            }
        }


        private string _pageField18Row5 = "Підпис";

        public string PageField18Row5
        {
            get => _pageField18Row5;
            set
            {
                _pageField18Row5 = value;
                RaisePropertyChangedEvent("_pageField18Row5");
            }
        }


        private string _pageField19Row5 = "Дата";

        public string PageField19Row5
        {
            get => _pageField19Row5;
            set
            {
                _pageField19Row5 = value;
                RaisePropertyChangedEvent("PageField19Row5");
            }
        }

        #endregion


        #region DocumentNumberTextField --------------------------------------------------------------------------------

        private string _formatText = "Формат";

        public string FormatText
        {
            get => _formatText;
            set
            {
                _formatText = value;
                RaisePropertyChangedEvent("FormatText");
            }
        }


        private string _copywriterText = "Копіював";

        public string CopywriterText
        {
            get => _copywriterText;
            set
            {
                _copywriterText = value;
                RaisePropertyChangedEvent("CopywriterText");
            }
        }


        private string _docNumberFiled20Title = "Інв. № об.";

        public string DocNumberFiled20Title
        {
            get => _docNumberFiled20Title;
            set
            {
                _docNumberFiled20Title = value;
                RaisePropertyChangedEvent("DocNumberFiled20Title");
            }
        }


        private string _docNumberFiled21Title = "Підпис і дата";

        public string DocNumberFiled21Title
        {
            get => _docNumberFiled21Title;
            set
            {
                _docNumberFiled21Title = value;
                RaisePropertyChangedEvent("DocNumberFiled21Title");
            }
        }


        private string _docNumberFiled22Title = "Зам. штв. №";

        public string DocNumberFiled22Title
        {
            get => _docNumberFiled22Title;
            set
            {
                _docNumberFiled22Title = value;
                RaisePropertyChangedEvent("DocNumberFiled22Title");
            }
        }

        #endregion


        #region Others -------------------------------------------------------------------------------------------------

        private string _otherHeader = "0-0";

        public string OtherHeader
        {
            get => _otherHeader;
            set
            {
                _otherHeader = value;
                RaisePropertyChangedEvent("OtherHeader");
            }
        }


        private string _other11 = "1-1";

        public string Other11
        {
            get => _other11;
            set
            {
                _other11 = value;
                RaisePropertyChangedEvent("Other11");
            }
        }


        private string _other12 = "1-2";

        public string Other12
        {
            get => _other12;
            set
            {
                _other12 = value;
                RaisePropertyChangedEvent("Other12");
            }
        }


        private string _other13 = "1-3";

        public string Other13
        {
            get => _other13;
            set
            {
                _other13 = value;
                RaisePropertyChangedEvent("Other13");
            }
        }


        private string _other14 = "1-4";

        public string Other14
        {
            get => _other14;
            set
            {
                _other14 = value;
                RaisePropertyChangedEvent("Other14");
            }
        }


        private string _other21 = "2-1";

        public string Other21
        {
            get => _other21;
            set
            {
                _other21 = value;
                RaisePropertyChangedEvent("Other21");
            }
        }


        private string _other22 = "2-2";

        public string Other22
        {
            get => _other22;
            set
            {
                _other22 = value;
                RaisePropertyChangedEvent("Other22");
            }
        }


        private string _other23 = "2-3";

        public string Other23
        {
            get => _other23;
            set
            {
                _other23 = value;
                RaisePropertyChangedEvent("Other23");
            }
        }


        private string _other24 = "2-4";

        public string Other24
        {
            get => _other24;
            set
            {
                _other24 = value;
                RaisePropertyChangedEvent("Other24");
            }
        }

        #endregion
    }
}