﻿using System;
using OrslavTeam.Revit.Axillary.ViewTemplate.Infrastructure;

namespace OrslavTeam.Revit.Steel.Axillary.Settings.SteelSpread
{
    public class SteelSpreadColumn : ObservableObject
    {
        public Guid Id { get; set; } = Guid.NewGuid();

        private int _point;

        public int Point
        {
            get => _point;
            set
            {
                _point = value;
                RaisePropertyChangedEvent("ColumnPoint");
            }
        }

        private string _header;

        public string Header
        {
            get => _header;
            set
            {
                _header = value;
                RaisePropertyChangedEvent("Header");
            }
        }
    }
}