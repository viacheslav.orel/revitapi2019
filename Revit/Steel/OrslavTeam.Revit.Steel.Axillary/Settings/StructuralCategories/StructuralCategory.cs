﻿namespace OrslavTeam.Revit.Steel.Axillary.Settings.StructuralCategories
{
    public class StructuralCategory
    {
        #region Fields -------------------------------------------------------------------------------------------------

        public int[] Id { get; set; }

        public string Name { get; set; }

        public char CategoryByUsingType { get; set; }

        public int CategoryByStressType { get; set; }

        public int PointNumber { get; set; }

        public StructuralCategory[] SubCategory { get; set; }

        #endregion -----------------------------------------------------------------------------------------------------
    }
}