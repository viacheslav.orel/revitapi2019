﻿using System;

namespace OrslavTeam.Revit.Steel.Axillary.Settings.StructuralCategories
{
    public static class StructuralCategoryData
    {
        #region Data ---------------------------------------------------------------------------------------------------

        public static readonly StructuralCategory[] CategoriesData = {
            new StructuralCategory
            {
                Id = new[] {1},
                Name = "Конструкції кранових колій",
                SubCategory = new[]
                {
                    new StructuralCategory
                    {
                        Id = new[] {1, 1},
                        Name = "підкранові балки (крім ребер жорсткості) і ферми (пояси, елементи решіток, фасонки)",
                        CategoryByUsingType = 'А',
                        CategoryByStressType = 1,
                        PointNumber = 19
                    },
                    new StructuralCategory
                    {
                        Id = new[] {1, 2},
                        Name = "гальмові балки і ферми, деталі кріплення до колон, ребра жорсткості",
                        CategoryByUsingType = 'А',
                        CategoryByStressType = 2,
                        PointNumber = 16
                    },
                    new StructuralCategory
                    {
                        Id = new[] {1, 3},
                        Name = "балки колій підвісного транспорту",
                        SubCategory = new[]
                        {
                            new StructuralCategory
                            {
                                Id = new[] {1, 3, 1},
                                Name = "зварні",
                                CategoryByUsingType = 'А',
                                CategoryByStressType = 1,
                                PointNumber = 19
                            },
                            new StructuralCategory
                            {
                                Id = new[] {1, 3, 2},
                                Name =
                                    "прокатні під технологічні електричні талі і кран-балки або ручні талі і кран-балки",
                                CategoryByUsingType = 'Б',
                                CategoryByStressType = 1,
                                PointNumber = 12
                            },
                            new StructuralCategory
                            {
                                Id = new[] {1, 3, 3},
                                Name = "прокатні під ремонтні або ручні талі і кран-балки",
                                CategoryByUsingType = 'Б',
                                CategoryByStressType = 2,
                                PointNumber = 9
                            }
                        }
                    },
                    new StructuralCategory
                    {
                        Id = new[] {1, 4},
                        Name = "допоміжні горизонтальні ферми, вертикальні ферми, тупикові упори",
                        CategoryByUsingType = 'Б',
                        CategoryByStressType = 2,
                        PointNumber = 9
                    },
                    new StructuralCategory
                    {
                        Id = new[] {1, 5},
                        Name = "деталі кріплення рейок",
                        CategoryByUsingType = 'В',
                        CategoryByStressType = 3,
                        PointNumber = 2
                    }
                }
            },
            new StructuralCategory
            {
                Id = new[] {2},
                Name = "Робочі площадки за наявності рухомого транспорту",
                SubCategory = new[]
                {
                    new StructuralCategory
                    {
                        Id = new[] {2, 1},
                        Name = "балки при залізничному рухомому складі",
                        CategoryByUsingType = 'А',
                        CategoryByStressType = 1,
                        PointNumber = 19
                    },
                    new StructuralCategory
                    {
                        Id = new[] {2, 2},
                        Name = "балки при автонавантажувачах та іншому транспорті",
                        CategoryByUsingType = 'А',
                        CategoryByStressType = 2,
                        PointNumber = 16
                    },
                    new StructuralCategory
                    {
                        Id = new[] {2, 3},
                        Name = "металевий настил, включений у сумісну роботу з балками настилу, ребра жорсткості балок",
                        CategoryByUsingType = 'Б',
                        CategoryByStressType = 1,
                        PointNumber = 12
                    },
                    new StructuralCategory
                    {
                        Id = new[] {2, 4},
                        Name =
                            "металевий настил, не включений до сумісної роботи з балками настилу, ребра жорсткості настилу",
                        CategoryByUsingType = 'Б',
                        CategoryByStressType = 2,
                        PointNumber = 9
                    }
                }
            },
            new StructuralCategory
            {
                Id = new[] {3},
                Name = "Конструкції технологічних площадок і покриттів",
                SubCategory = new[]
                {
                    new StructuralCategory
                    {
                        Id = new[] {3, 1},
                        Name = "головні балки і ригелі рам при динамічному навантаженні",
                        CategoryByUsingType = 'А',
                        CategoryByStressType = 1,
                        PointNumber = 19
                    },
                    new StructuralCategory
                    {
                        Id = new[] {3, 2},
                        Name = "головні балки при статичному навантаженні",
                        CategoryByUsingType = 'А',
                        CategoryByStressType = 3,
                        PointNumber = 12
                    },
                    new StructuralCategory
                    {
                        Id = new[] {3, 3},
                        Name = "другорядні балки при динамічному навантаженні",
                        CategoryByUsingType = 'А',
                        CategoryByStressType = 2,
                        PointNumber = 16
                    },
                    new StructuralCategory
                    {
                        Id = new[] {3, 4},
                        Name = "другорядні балки при статичному навантаженні",
                        CategoryByUsingType = 'А',
                        CategoryByStressType = 3,
                        PointNumber = 12
                    },
                    new StructuralCategory
                    {
                        Id = new[] {3, 5},
                        Name = "металевий настил, включений до суміснох роботи з балками при динамічному навантаженні",
                        CategoryByUsingType = 'Б',
                        CategoryByStressType = 1,
                        PointNumber = 12
                    },
                    new StructuralCategory
                    {
                        Id = new[] {3, 6},
                        Name = "металевий настил, окрім зазначеного в поз. 3-5",
                        CategoryByUsingType = 'Б',
                        CategoryByStressType = 2,
                        PointNumber = 9
                    },
                    new StructuralCategory
                    {
                        Id = new[] {3, 7},
                        Name = "ребра жорсткості балок",
                        CategoryByUsingType = 'В',
                        CategoryByStressType = 3,
                        PointNumber = 2
                    }
                }
            },
            new StructuralCategory
            {
                Id = new []{4},
                Name = "Колони виробничих споруд і відкритих кранових естакад, стояки робочих і технологічних площадок",
                SubCategory = new []
                {
                    new StructuralCategory
                    {
                        Id = new []{4, 1},
                        Name = "основні елементи поперечного перерізу (у тому числі пояса і решітки при наскрізному перерізі), опорні плити, підкранові траверси колон",
                        CategoryByUsingType = 'А',
                        CategoryByStressType = 3,
                        PointNumber = 12
                    },
                    new StructuralCategory
                    {
                        Id = new []{4, 2},
                        Name = "вертикальні в'язі між колонами",
                        CategoryByUsingType = 'А',
                        CategoryByStressType = 3,
                        PointNumber = 12
                    },
                    new StructuralCategory
                    {
                        Id =  new []{4, 3},
                        Name = "ребра жорсткості і діафрагми колон, елементи решіток двоплощинних в'язей, в'язі з напруженням, меншим за 0,4Ry",
                        CategoryByUsingType = 'В',
                        CategoryByStressType = 3,
                        PointNumber = 2
                    }
                }
            },
            new StructuralCategory
            {
                Id = new []{5},
                Name = "Конструкції покриття",
                SubCategory = new []
                {
                    new StructuralCategory
                    {
                        Id = new []{5, 1},
                        Name = "ферми, ригелі та інші елементи, що підлягають безпосередній дії динамічних навантажень від технологічного чи транспортного устаткування",
                        CategoryByUsingType = 'А',
                        CategoryByStressType = 1,
                        PointNumber = 19
                    },
                    new StructuralCategory
                    {
                        Id = new []{5, 2},
                        Name = "те саме при статичному навантаженні",
                        CategoryByUsingType = 'А',
                        CategoryByStressType = 2,
                        PointNumber = 16
                    },
                    new StructuralCategory
                    {
                        Id = new [] {5, 3},
                        Name = "вузлові фасонки",
                        CategoryByUsingType = 'А',
                        CategoryByStressType = 2,
                        PointNumber = 16
                    },
                    new StructuralCategory
                    {
                        Id = new []{5, 4},
                        Name = "ліхтарні панелі, панелі покрівлі, прогони, горизонтальні торцеві в'язі в рівні покрівлі, пощдовжні в'язі при кроці колон, що є більшим за крок кроквяних ферм",
                        CategoryByUsingType = 'Б',
                        CategoryByStressType = 2,
                        PointNumber = 9
                    },
                    new StructuralCategory
                    {
                        Id = new []{5, 5},
                        Name = "інші в'язі",
                        CategoryByUsingType = 'В',
                        CategoryByStressType = 3,
                        PointNumber = 2
                    }
                }
            },
            new StructuralCategory
            {
                Id = new [] {6},
                Name = "Конструкції фахверка",
                SubCategory = new []
                {
                    new StructuralCategory
                    {
                        Id = new []{6, 1},
                        Name = "ригелі під цегляні стіни і над воротами",
                        CategoryByUsingType = 'А',
                        CategoryByStressType = 3,
                        PointNumber = 12
                    },
                    new StructuralCategory
                    {
                        Id = new []{6, 2},
                        Name = "стояки, торцві і вітрові ферми",
                        CategoryByUsingType = 'Б',
                        CategoryByStressType = 2,
                        PointNumber = 9
                    },
                    new StructuralCategory
                    {
                        Id = new []{6, 3},
                        Name = "ригелі, крім зазначених у поз. 6-1 та інші елементи",
                        CategoryByUsingType = 'В',
                        CategoryByStressType = 3,
                        PointNumber = 2
                    }
                }
            },
            new StructuralCategory
            {
                Id = new []{7},
                Name = "Допоміжні конструкції виробничих споруд",
                SubCategory = new []
                {
                    new StructuralCategory
                    {
                        Id = new []{7, 1},
                        Name = "косоури сходів",
                        CategoryByUsingType = 'А',
                        CategoryByStressType = 3,
                        PointNumber = 12
                    },
                    new StructuralCategory
                    {
                        Id = new []{7,2},
                        Name = "сходи, перехідні площадки, огорожі, площадки світильників, посадкові площадки на кран, балки підвісних стель, імпости, віконні і ліхтарні рами",
                        CategoryByUsingType = 'В',
                        CategoryByStressType = 3,
                        PointNumber = 2
                    }
                }
            },
            new StructuralCategory
            {
                Id = new []{8},
                Name = "Транспортні галереї",
                SubCategory = new []
                {
                    new StructuralCategory
                    {
                        Id = new []{8, 1},
                        Name = "прогінні споріди галереї, несучі балки під конвеєри, фасонки ферм",
                        CategoryByUsingType = 'А',
                        CategoryByStressType = 1,
                        PointNumber = 19
                    },
                    new StructuralCategory
                    {
                        Id = new []{8, 2},
                        Name = "опори, в'язі між колонами, опорні ребра",
                        CategoryByUsingType = 'А',
                        CategoryByStressType = 2,
                        PointNumber = 16
                    },
                    new StructuralCategory
                    {
                        Id = new []{8, 3},
                        Name = "елементи фахверка, в'язі, прогони і балки покриття прогінних споруд, ребра жорсткості балок",
                        CategoryByUsingType = 'Б',
                        CategoryByStressType = 2,
                        PointNumber = 9
                    }
                }
            },
            new StructuralCategory
            {
                Id = new []{9},
                Name = "Опори повітряних ліній електропередавання, конструкцій відкритих розподільних пристроїв",
                SubCategory = new []
                {
                    new StructuralCategory
                    {
                        Id = new []{9, 1},
                        Name = "зварні спеціальні опори великих переходів заввишки понад 60 м",
                        CategoryByUsingType = 'А',
                        CategoryByStressType = 1,
                        PointNumber = 19
                    },
                    new StructuralCategory
                    {
                        Id = new []{9, 2},
                        Name = "опори ПЛ, окрім зазначених у поз. 9-1, опори під вимикачі і портали під ошинування ВРП",
                        CategoryByUsingType = 'А',
                        CategoryByStressType = 3,
                        PointNumber = 12
                    },
                    new StructuralCategory
                    {
                        Id = new []{9, 3},
                        Name = "опори під устаткування ВРП тощо, крім зазначених у поз. 9-1 і 9-2",
                        CategoryByUsingType = 'В',
                        CategoryByStressType = 2,
                        PointNumber = 9
                    }
                }
            },
            new StructuralCategory
            {
                Id = new []{10},
                Name = "Антенні споруди звязку заввишки до 500 м",
                SubCategory = new []
                {
                    new StructuralCategory
                    {
                        Id = new []{10, 1},
                        Name = "стовбури щогл і башт, решітки, елементи обпирання на фундаменти",
                        CategoryByUsingType = 'А',
                        CategoryByStressType = 2,
                        PointNumber = 16
                    },
                    new StructuralCategory
                    {
                        Id = new []{10, 2},
                        Name = "елементи (механічні деталі) витяжок щогл і антенних полотен, деталі кріплення відтяжокдо фундаментів і до стовбурів сталевих опор",
                        CategoryByUsingType = 'А',
                        CategoryByStressType = 1,
                        PointNumber = 19
                    },
                    new StructuralCategory
                    {
                        Id = new []{10, 3},
                        Name = "діафрагми баштових опор, хідники, перехідні площадки",
                        CategoryByUsingType = 'Б',
                        CategoryByStressType = 3,
                        PointNumber = 5
                    }
                }
            },
            new StructuralCategory
            {
                Id = new []{11},
                Name = "Витяжні башти",
                SubCategory = new []
                {
                    new StructuralCategory
                    {
                        Id = new []{11, 1},
                        Name = "пояси башт, вузлові фасонки",
                        CategoryByUsingType = 'А',
                        CategoryByStressType = 2,
                        PointNumber = 16
                    },
                    new StructuralCategory
                    {
                        Id = new []{11, 2},
                        Name = "газовідвідний стовбур, елементи решітки, балки і площадки діафрагм, що безпосередньо сприймають вагу стовбура",
                        CategoryByUsingType = 'Б',
                        CategoryByStressType = 2,
                        PointNumber = 9
                    },
                    new StructuralCategory
                    {
                        Id = new []{11, 3},
                        Name = "опорні плити, хідники, огорожі, настил площадок, балок і площадки діафрагм, що не сприймають вагу стовбура",
                        CategoryByUsingType = 'В',
                        CategoryByStressType = 3,
                        PointNumber = 2
                    }
                }
            },
            new StructuralCategory
            {
                Id = new []{12},
                Name = "Димові труби",
                SubCategory = new []
                {
                    new StructuralCategory
                    {
                        Id = new []{12, 1},
                        Name = "сталева обонка і ребра жорсткості труби",
                        CategoryByUsingType = 'А',
                        CategoryByStressType = 2,
                        PointNumber = 16
                    },
                    new StructuralCategory
                    {
                        Id = new []{12, 2},
                        Name = "площадки, опорні кільця, хідники та огорожі",
                        CategoryByUsingType = 'А',
                        CategoryByStressType = 3,
                        PointNumber = 12
                    }
                }
            },
            new StructuralCategory
            {
                Id = new []{13},
                Name = "Градирні баштові і вентиляторні, водонапірні башти",
                SubCategory = new []
                {
                    new StructuralCategory
                    {
                        Id = new []{13, 1},
                        Name = "пояси решітчастих башт, кільця жорсткості, решітки",
                        CategoryByUsingType = 'А',
                        CategoryByStressType = 3,
                        PointNumber = 12
                    },
                    new StructuralCategory
                    {
                        Id = new []{13, 2},
                        Name = "вузлові фасонки",
                        CategoryByUsingType = 'А',
                        CategoryByStressType = 2,
                        PointNumber = 16
                    },
                    new StructuralCategory
                    {
                        Id = new []{13, 3},
                        Name = "фахверки, допоміжні площадки, обшивки градирень",
                        CategoryByUsingType = 'В',
                        CategoryByStressType = 3,
                        PointNumber = 2
                    }
                }
            },
            new StructuralCategory
            {
                Id = new []{14},
                Name = "Бункери",
                SubCategory = new []
                {
                    new StructuralCategory
                    {
                        Id = new []{14, 1},
                        Name = "бункерні балки, оболонки параболічних бункері",
                        CategoryByUsingType = 'А',
                        CategoryByStressType = 1,
                        PointNumber = 19
                    },
                    new StructuralCategory
                    {
                        Id = new []{14, 2},
                        Name = "стінки інших бункерів, ребра жорсткості бункерів",
                        CategoryByUsingType = 'А',
                        CategoryByStressType = 3,
                        PointNumber = 12
                    }
                }
            },
            new StructuralCategory
            {
                Id = new []{15},
                Name = "Резервуари і газгольдери",
                SubCategory = new []
                {
                    new StructuralCategory
                    {
                        Id = new []{15, 1},
                        Name = "стінки та кромки днищ резервуарів місткістю 10тис. м3 і більше, фасонки покриттів",
                        CategoryByUsingType = 'А',
                        CategoryByStressType = 1,
                        PointNumber = 19
                    },
                    new StructuralCategory
                    {
                        Id = new []{15, 2},
                        Name = "стінки та кромки днищ резервуарів місткістю менше ніж 10 тис. м3",
                        CategoryByUsingType = 'А',
                        CategoryByStressType = 2,
                        PointNumber = 16
                    },
                    new StructuralCategory
                    {
                        Id = new []{15, 3},
                        Name = "центральні частини днищ, опорні кільця покриття, кільця жорсткості, плавучі покрівлі і понтони покриття",
                        CategoryByUsingType = 'А',
                        CategoryByStressType = 3,
                        PointNumber = 12
                    },
                    new StructuralCategory
                    {
                        Id = new []{15, 4},
                        Name = "внутрішні корпуси ізотермічних резервуарів за температури зберігання на вище -50°C",
                        CategoryByUsingType = 'А',
                        CategoryByStressType = 1,
                        PointNumber = 19
                    }
                }
            },
            new StructuralCategory
            {
                Id = new []{16},
                Name = "Конструкції контактної мережі транспорту",
                SubCategory = new []
                {
                    new StructuralCategory
                    {
                        Id = new []{16, 1},
                        Name = "конструкції та елементи, пов'язані з натягом проводів (тяги, штанги, хомути)",
                        CategoryByUsingType = 'А',
                        CategoryByStressType = 2,
                        PointNumber = 16
                    },
                    new StructuralCategory
                    {
                        Id = new []{16, 2},
                        Name = "конструкції та елементи несучих, підтримувальних і фіксувальних пристроїв (опори, ригелі жорсткості поперечин, прожекторні щогли, фіксатори)",
                        CategoryByUsingType = 'Б',
                        CategoryByStressType = 2,
                        PointNumber = 9
                    },
                    new StructuralCategory
                    {
                        Id = new []{16, 3},
                        Name = "допоміжні конструкції",
                        CategoryByUsingType = 'В',
                        CategoryByStressType = 3,
                        PointNumber = 2
                    }
                }
            },
            new StructuralCategory
            {
                Id = new []{17},
                Name = "Силоси (оболонка,ребра жорскості)",
                CategoryByUsingType = 'А',
                CategoryByStressType = 3,
                PointNumber = 12
            },
            new StructuralCategory
            {
                Id = new []{18},
                Name = "Громадські споруди (театри, кінотеатри, цирки, спортивні споруди, приті ринки, навчальні заклади, дошкільні навчальні заклади, лікарні, пологові будинки, музеї, державні архиви тощо, споруди заввишки понад 75 м",
                SubCategory = new []
                {
                    new StructuralCategory
                    {
                        Id = new []{18, 1},
                        Name = "перекриття і покриття, косоури сходів",
                        CategoryByUsingType = 'А',
                        CategoryByStressType = 2,
                        PointNumber = 16
                    },
                    new StructuralCategory
                    {
                        Id = new []{18, 2},
                        Name = "колони",
                        CategoryByUsingType = 'А',
                        CategoryByStressType = 2,
                        PointNumber = 16
                    }
                }
            }
        };

        #endregion -----------------------------------------------------------------------------------------------------

        #region Methods ------------------------------------------------------------------------------------------------

        public static StructuralCategory FindElementById(int[] ids)
        {
            StructuralCategory result = Array.Find(CategoriesData, category => category.Id[0] == ids[0]);
            if (ids.Length == 1) return result;

            for (var i = 1; i < ids.Length; i++)
            {
                result = Array.Find(result.SubCategory, category => category.Id[i] == ids[i]);
            }

            return result;
        }

        #endregion -----------------------------------------------------------------------------------------------------
    }
}