﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autodesk.Revit.DB;
using Autodesk.Revit.DB.ExtensibleStorage;
using Newtonsoft.Json;
using OrslavTeam.Revit.Steel.Axillary.Settings.SteelSpread;
using OrslavTeam.Revit.Steel.Axillary.Settings.UsingType;

namespace OrslavTeam.Revit.Steel.Axillary.Settings.SteelStorage
{
    public class SteelSettingsStorage
    {
        #region Fields -------------------------------------------------------------------------------------------------

        private readonly DataStorage _dataStorage;
        private readonly Entity _entity;
        private readonly Field _settings;

        private readonly Dictionary<int, SteelSpreadObj> _steelSpreadCollection;

        #endregion


        #region Constructors -------------------------------------------------------------------------------------------

        public SteelSettingsStorage(Document doc)
        {
            //GetSchema
            Schema schema = SteelSettingSchema.GetSchema();

            try
            {
                _dataStorage = new FilteredElementCollector(doc)
                    .OfClass(typeof(DataStorage))
                    .OfType<DataStorage>()
                    .First(item => item.GetEntitySchemaGuids().Contains(SteelSettingSchema.SchemaId));

                _entity = _dataStorage.GetEntity(schema);
            }
            catch (Exception)
            {
                _dataStorage = DataStorage.Create(doc);
                _entity = new Entity(schema);
            }

            // SettingsField
            _settings = schema.GetField(SteelSettingSchema.STEEL_SETTINGS_FIELD_NAME);

            var settingsData = _entity.Get<string>(_settings);

            _steelSpreadCollection = 
                settingsData == "" 
                ? new Dictionary<int, SteelSpreadObj>()
                : JsonConvert.DeserializeObject<Dictionary<int, SteelSpreadObj>>(settingsData);
        }

        #endregion


        #region SteelSettings ------------------------------------------------------------------------------------------

        public SteelSpreadObj GetSteelSettings(int phaseId)
        {
            return _steelSpreadCollection.ContainsKey(phaseId) ? _steelSpreadCollection[phaseId] : null;
        }

        public void SetSteelSettings(int phaseId, SteelSpreadObj settings)
        {
            _steelSpreadCollection[phaseId] = settings;

            SaveStorage();
        }

        #endregion


        #region UsingType ----------------------------------------------------------------------------------------------

        public FullUsingType[] GetUsingTypeCollection(int phaseId)
        {
            return _steelSpreadCollection.ContainsKey(phaseId) ? _steelSpreadCollection[phaseId].FullUsingTypes : null;
        }

        public void SetUsingTypeCollection(int phaseId, FullUsingType[] fullUsingTypes)
        {
            if (_steelSpreadCollection.ContainsKey(phaseId))
            {
                _steelSpreadCollection[phaseId].FullUsingTypes = fullUsingTypes;
            }
            else
            {
                _steelSpreadCollection.Add(phaseId, new SteelSpreadObj {FullUsingTypes = fullUsingTypes});
            }

            SaveStorage();
        }

        #endregion


        #region SteelSpreadColumnInfo ----------------------------------------------------------------------------------

        public SteelSpreadColumn[] GetColumnInfo(int phaseId)
        {
            return _steelSpreadCollection.ContainsKey(phaseId) ? _steelSpreadCollection[phaseId].ColumnsInfo : null;
        }

        public void SetColumnInfo(int phaseId, SteelSpreadColumn[] steelSpreadColumns)
        {
            _steelSpreadCollection[phaseId].ColumnsInfo = steelSpreadColumns;

            SaveStorage();
        }

        #endregion


        #region Axillary -----------------------------------------------------------------------------------------------

        private void SaveStorage()
        {
            string settingsData = JsonConvert.SerializeObject(_steelSpreadCollection);
            _entity.Set(_settings, settingsData);
            _dataStorage.SetEntity(_entity);
        }

        #endregion
    }
}