﻿using System;
using Autodesk.Revit.DB.ExtensibleStorage;

namespace OrslavTeam.Revit.Steel.Axillary.Settings.SteelStorage
{
    public static class SteelSettingSchema
    {
        public static readonly Guid SchemaId = new Guid("{E46FE0C2-3BB6-4BBF-B134-971F93F3A289}");
        public const string STEEL_SETTINGS_FIELD_NAME = "SteelSettings";

        public static Schema GetSchema()
        {
            Schema schema = Schema.Lookup(SchemaId);

            if (schema != null) return schema;

            var schemaBuilder = new SchemaBuilder(SchemaId);
            schemaBuilder.SetSchemaName("SteelSetting");
            schemaBuilder.SetReadAccessLevel(AccessLevel.Public);
            schemaBuilder.SetVendorId("ORSLAVTEAM");
            schemaBuilder.SetWriteAccessLevel(AccessLevel.Public);

            schemaBuilder.AddSimpleField(STEEL_SETTINGS_FIELD_NAME, typeof(string));

            return schemaBuilder.Finish();
        }
    }
}