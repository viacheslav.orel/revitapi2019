﻿using System;
using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Structure;
using Newtonsoft.Json;
using OrslavTeam.Revit.Axillary.PublicInformation;
using OrslavTeam.Revit.Axillary.ViewTemplate.Infrastructure;
using OrslavTeam.Revit.Steel.Axillary.Settings.StructuralCategories;

namespace OrslavTeam.Revit.Steel.Axillary.Settings.UsingType
{
    public class FullUsingType : ObservableObject, IComparable<FullUsingType>, IEquatable<FullUsingType>, ICloneable
    {
        #region Fields -------------------------------------------------------------------------------------------------

        // Id ----------------------------------------------------------------------------------------------------------
        private int _id;

        public int Id
        {
            get => _id;
            set
            {
                _id = value;
                RaisePropertyChangedEvent(nameof(Id));
            }
        }

        // ColumnId ----------------------------------------------------------------------------------------------------
        public Guid SteelSpreadColumnId { get; set; }

        // MarkPrefix --------------------------------------------------------------------------------------------------
        private string _markPrefix = "";

        public string MarkPrefix
        {
            get => _markPrefix;
            set
            {
                _markPrefix = value;
                RaisePropertyChangedEvent(nameof(MarkPrefix));
            }
        }

        // FirstMarkNumber ---------------------------------------------------------------------------------------------
        private int _firstMarkNumber;

        [JsonIgnore]
        public int FirstMarkNumber
        {
            get => _firstMarkNumber;
            set
            {
                _firstMarkNumber = value;
                RaisePropertyChangedEvent(nameof(FirstMarkNumber));
            }
        }

        // IsNecessaryToMark -------------------------------------------------------------------------------------------
        private bool _isNecessaryToMark = true;

        public bool IsNecessaryToMark
        {
            get => _isNecessaryToMark;
            set
            {
                _isNecessaryToMark = value;
                RaisePropertyChangedEvent("IsNecessaryToMark");
            }
        }

        // GeneralUsingType --------------------------------------------------------------------------------------------
        private UsingType _generalUsingType = new UsingType();

        public UsingType GeneralUsingType
        {
            get => _generalUsingType ?? new UsingType();
            set
            {
                _generalUsingType = value;
                RaisePropertyChangedEvent(nameof(GeneralUsingType));
            }
        }

        // SubUsingType ------------------------------------------------------------------------------------------------
        private UsingType _subUsingType = new UsingType();

        public UsingType SubUsingType
        {
            get => _subUsingType ?? new UsingType();
            set
            {
                _subUsingType = value;
                RaisePropertyChangedEvent(nameof(SubUsingType));
            }
        }

        // FullMark ----------------------------------------------------------------------------------------------------
        [JsonIgnore]
        public string FullName =>
            $"{_generalUsingType?.Name ?? ""} - {_subUsingType?.Name ?? ""}";

        // StructuralCategory ------------------------------------------------------------------------------------------
        private int[] _structuralCategory;

        public int[] StructuralCategory
        {
            get => _structuralCategory;
            set
            {
                _structuralCategory = value;
                RaisePropertyChangedEvent("StructuralCategory");
                RaisePropertyChangedEvent("StructuralCategoryHeader");
            }
        }

        public string StructuralCategoryHeader
        {
            get
            {
                if (_structuralCategory == null) return null;
                StructuralCategory category = StructuralCategoryData.FindElementById(StructuralCategory);

                return $"{category.CategoryByUsingType} - {category.CategoryByStressType}";
            }
        }

        // AdverseEffectOfWeldedJoin -----------------------------------------------------------------------------------
        private bool _aewj;

        public bool Aewj
        {
            get => _aewj;
            set
            {
                _aewj = value;
                RaisePropertyChangedEvent("Aewj");
            }
        }

        #endregion -----------------------------------------------------------------------------------------------------

        #region Contstructors ------------------------------------------------------------------------------------------

        public FullUsingType()
        {
        }

        public FullUsingType(Element generalUsingType, Element subUsingType)
        {
            GeneralUsingType = new UsingType(generalUsingType);
            SubUsingType = new UsingType(subUsingType);

            Id = GeneralUsingType.Id * 31 + SubUsingType.Id;
        }

        public FullUsingType(ICloneable generalUsingType, ICloneable subUsingType)
        {
            GeneralUsingType = (UsingType)generalUsingType.Clone();
            SubUsingType = (UsingType)subUsingType.Clone();

            Id = GeneralUsingType.Id * 31 + SubUsingType.Id;
        }

        #endregion -----------------------------------------------------------------------------------------------------

        #region Methods ------------------------------------------------------------------------------------------------

        public static FullUsingType GetFullUsingType(Document doc, Element element, string assemblyKod)
        {
            ElementId generalUsingTypeId = PublicParameter.GeneralUsingType(element);

            if (generalUsingTypeId != ElementId.InvalidElementId)
            {
                Element generalUsingTypeElem = doc.GetElement(generalUsingTypeId);

                ElementId subUsingTypeId = PublicParameter.SubUsingType(element);
                Element subUsingTypeElem =
                    subUsingTypeId != ElementId.InvalidElementId
                    ? doc.GetElement(subUsingTypeId)
                    : null;

                return new FullUsingType(generalUsingTypeElem, subUsingTypeElem);
            }

            var categoryId = (BuiltInCategory)element.Category.Id.IntegerValue;
            var emptyUsingType = new UsingType(0, "");

            switch (categoryId)
            {
                case BuiltInCategory.OST_Floors:
                    return new FullUsingType(new UsingType(1500, "15 Перекриття"), emptyUsingType);
                case BuiltInCategory.OST_GenericModel:
                    switch (assemblyKod)
                    {
                        case "B.02.02.03.01":
                        case "B.02.02.03.02":
                        case "B.02.02.03":
                            return new FullUsingType(new UsingType(1000, "10 Сходи"), emptyUsingType);
                        case "B.02.03":
                            return new FullUsingType(new UsingType(1100, "11 Огорожа"), emptyUsingType);
                        default:
                            return new FullUsingType(emptyUsingType, emptyUsingType);
                    }
                case BuiltInCategory.OST_StructuralColumns:
                    return new FullUsingType(
                        new UsingType(100, "01 Каркас"),
                        new UsingType(100, "01 Колона")
                        );
                case BuiltInCategory.OST_StructuralFraming:
                    StructuralInstanceUsage structuralUsage = ((FamilyInstance) element).StructuralUsage;
                    switch (structuralUsage)
                    {
                        case StructuralInstanceUsage.Brace:
                            return new FullUsingType(
                                new UsingType(100, "01 Каркас"), 
                                new UsingType(400, "04 В'язь.Вертикальна"));
                        case StructuralInstanceUsage.HorizontalBracing:
                            return new FullUsingType(
                                new UsingType(200, "02 Майданчик"), 
                                new UsingType(300, "03 В'язь.Горизонтальна"));
                        default:
                            if (assemblyKod == "B.02.02.02.03")
                            {
                                return new FullUsingType(new UsingType(500, "05 Вантажне"), emptyUsingType);
                            }
                            else
                            {
                                return new FullUsingType(
                                    new UsingType(200, "02 Майданчик"),
                                    new UsingType(200, "02 Балка"));
                            }
                    }
                case BuiltInCategory.OST_StructuralTruss:
                    return new FullUsingType(
                        new UsingType(100, "01 Каркас"), 
                        new UsingType(500, "05 Ферма"));
                case BuiltInCategory.OST_Walls:
                    return new FullUsingType(new UsingType(1600, "16 Стіни"), emptyUsingType);
            }

            return new FullUsingType(emptyUsingType, emptyUsingType);
        }

        #endregion

        #region Relational members -------------------------------------------------------------------------------------

        public int CompareTo(FullUsingType other)
        {
            if (ReferenceEquals(this, other)) return 0;
            if (ReferenceEquals(null, other)) return 1;
            return Id.CompareTo(other.Id);
        }

        #endregion -----------------------------------------------------------------------------------------------------

        #region Equality members ---------------------------------------------------------------------------------------

        public bool Equals(FullUsingType other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Id == other.Id;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((FullUsingType)obj);
        }

        public override int GetHashCode()
        {
            return Id;
        }

        #endregion -----------------------------------------------------------------------------------------------------

        #region Clonable members ---------------------------------------------------------------------------------------

        public object Clone()
        {
            return new FullUsingType
            {
                Id = this.Id,
                MarkPrefix = this.MarkPrefix,
                FirstMarkNumber = this.FirstMarkNumber,
                IsNecessaryToMark = this.IsNecessaryToMark,
                GeneralUsingType = this.GeneralUsingType.Clone() as UsingType,
                SubUsingType = this.SubUsingType.Clone() as UsingType,
                StructuralCategory = this.StructuralCategory
            };
        }

        #endregion -----------------------------------------------------------------------------------------------------
    }
}