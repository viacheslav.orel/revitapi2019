﻿using System;
using Autodesk.Revit.DB;

namespace OrslavTeam.Revit.Steel.Axillary.Settings.UsingType
{
    public class UsingType : ICloneable
    {
        #region Fields -------------------------------------------------------------------------------------------------

        public string Name { get; set; }
        public int Id { get; set; }

        #endregion -----------------------------------------------------------------------------------------------------

        #region Constructors -------------------------------------------------------------------------------------------

        public UsingType()
        {
            
        }

        public UsingType(Element usingType)
        {
            try
            {
                Name = usingType.Name;

                Parameter param = usingType.LookupParameter("GeneralUsingType") ??
                                  usingType.LookupParameter("SubUsingType");

                Id = (int)(param.AsDouble() * 100);
            }
            catch (Exception)
            {
                Name = "";
                Id = 0;
            }
        }

        public UsingType(int id, string name)
        {
            Id = id;
            Name = name;
        }

        #endregion -----------------------------------------------------------------------------------------------------

        #region Clonable members ---------------------------------------------------------------------------------------

        public object Clone()
        {
            return new UsingType(this.Id, this.Name);
        }

        #endregion -----------------------------------------------------------------------------------------------------
    }
}