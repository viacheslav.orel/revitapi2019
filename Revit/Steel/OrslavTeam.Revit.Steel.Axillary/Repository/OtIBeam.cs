﻿using System.ComponentModel;
using System.Linq;
using Autodesk.Revit.DB;
using Newtonsoft.Json;
using OrslavTeam.Revit.Axillary.PublicInformation;
using OrslavTeam.Revit.Steel.Axillary.Repository.Primitive;

namespace OrslavTeam.Revit.Steel.Axillary.Repository
{
    public class OtIBeam : INotifyPropertyChanged
    {
        //Constructors
        public OtIBeam() { }

        public OtIBeam(string name)
        {
            Name = name;
        }

        // Fields
        public static readonly string Guid = "d8ba024f-193f-432e-8091-9e6c0c56d8ea";

        [JsonIgnore] public Element RvtElement { get; set; }
        [JsonIgnore] public Element RvtFamily { get; set; }

        public string Name { get; set; }

        public OtIBeamComponent BeamChord { get; set; } = new OtIBeamComponent();
        
        public OtIBeamComponent BeamWeb { get; set; } = new OtIBeamComponent();

        // Methods
        public void SetProperties(Document doc)
        {
            using (var tr = new Transaction(doc, "SetProperty"))
            {
                tr.Start();

                RvtFamily.Name = Name;

                double webThickness =
                    BeamWeb.Profile.Parameters.FirstOrDefault(pr => pr.Key == "width").Value;
                RvtFamily.LookupParameter("geometry.web.thickness")
                    .Set(UnitUtils.ConvertToInternalUnits(webThickness, DisplayUnitType.DUT_MILLIMETERS));
                RvtFamily.LookupParameter("geometry.web.width")
                    .Set(UnitUtils.ConvertToInternalUnits(BeamWeb.Width, DisplayUnitType.DUT_MILLIMETERS));


                double chordThickness =
                    BeamChord.Profile.Parameters.FirstOrDefault(pr => pr.Key == "width").Value;
                RvtFamily.LookupParameter("geometry.chord.thickness")
                    .Set(UnitUtils.ConvertToInternalUnits(chordThickness, DisplayUnitType.DUT_MILLIMETERS));
                RvtFamily.LookupParameter("geometry.chord.width")
                    .Set(UnitUtils.ConvertToInternalUnits(BeamChord.Width, DisplayUnitType.DUT_MILLIMETERS));


                PublicParameter.ProfileName(RvtFamily,
                    $"a: -{BeamWeb.Width}x{webThickness}\nб: -{BeamChord.Width}x{chordThickness}");

                PublicParameter.GetIt(RvtFamily, JsonConvert.SerializeObject(this, Formatting.Indented));


                tr.Commit();
            }
        }


        #region Event

        public event PropertyChangedEventHandler PropertyChanged;

        public void RaisePropertyChangedEvent(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }

    public struct OtIBeamComponent
    {
        public OtProductMix ProductMix { get; set; }
        public OtProfile Profile { get; set; }
        public int Width { get; set; }
    }
}