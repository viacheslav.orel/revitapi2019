﻿﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace OrslavTeam.Revit.Steel.Axillary.Repository.Primitive
{
    public class OtMaterial : ICloneable, IComparable<OtMaterial>, IEquatable<OtMaterial>
    {
        #region Fields

        public int ProductMixId { get; set; }

        public string ProductMixNumber { get; set; }

        public string ProductMixName { get; set; }

        public int Id { get; set; }

        public string Name { get; set; }

        #endregion

        #region Methods

        [JsonIgnore] public string GetProductMixFullName => $"{ProductMixNumber}. {ProductMixName}";

        #endregion

        #region Constructors

        public OtMaterial()
        {
        }

        public OtMaterial(int id)
        {
            Id = id;
        }

        #endregion

        #region Implementation of ICloneable

        public object Clone()
        {
            return new OtMaterial
            {
                    ProductMixId = ProductMixId,
                    ProductMixNumber = ProductMixNumber,
                    ProductMixName = ProductMixName,
                    Id = Id,
                    Name = Name
            };
        }

        #endregion

        #region Implementation of IComparable<in OtMaterial>

        public int CompareTo(OtMaterial other)
        {
            if (other == null) return 1;
            if (ReferenceEquals(this, other)) return 0;
            return Id.CompareTo(other.Id);
        }
        
        [JsonIgnore]
        public static IComparer<OtMaterial> BaseOrder = new OtMaterialBaseComparable();

        #endregion

        #region Implementation of IEquatable<OtMaterial>

        public bool Equals(OtMaterial other)
        {
            if (other == null) return false;
            if (ReferenceEquals(this, other)) return true;
            return Id == other.Id;
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj is OtMaterial material && Equals(material);
        }
        
        public override int GetHashCode()
        {
            // ReSharper disable once NonReadonlyMemberInGetHashCode
            return Id;
        }
        
        public static IEqualityComparer<OtMaterial> BaseEqual = new OtMaterialBaseEqualityComparer();

        #endregion
    }

    public sealed class OtMaterialBaseComparable : IComparer<OtMaterial>
    {
        #region Implementation of IComparer<in OtMaterial>

        public int Compare(OtMaterial x, OtMaterial y)
        {
            if (x == null && y == null) return 0;
            return x?.CompareTo(y) ?? -1;
        }

        #endregion
    }

    public sealed class OtMaterialBaseEqualityComparer : IEqualityComparer<OtMaterial>
    {
        #region Implementation of IEqualityComparer<in OtMaterial>

        public bool Equals(OtMaterial x, OtMaterial y)
        {
            if (x == null && y == null) return true;
            return x?.Equals(y) ?? false;
        }

        public int GetHashCode(OtMaterial obj)
        {
            return obj.GetHashCode();
        }

        #endregion
    }
}