﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace OrslavTeam.Revit.Steel.Axillary.Repository.Primitive
{
    public class OtProfile : ICloneable, IComparable<OtProfile>, IEquatable<OtProfile>
    {
        #region Fields

        public int Id { get; set; }

        public string Name { get; set; }

        public string ProfileName { get; set; }

        public KeyValuePair<string, double>[] Parameters { get; set; }

        #endregion

        #region Methods

        public double GetParameter(string name)
        {
            return Parameters.FirstOrDefault(parameter => parameter.Key == name).Value;
        }

        #endregion

        #region Constructors

        public OtProfile(){}

        public OtProfile(int id)
        {
            Id = id;
        }

        #endregion
        
        //Clone
        //--------------------------------------------------------------------------------------------------------------
        #region Implementation of ICloneable

        public object Clone()
        {
            return new OtProfile
            {
                    Id = this.Id,
                    Name = this.Name,
                    ProfileName = this.ProfileName,
                    Parameters = this.Parameters
            };
        }

        #endregion
        
        //Sort
        //--------------------------------------------------------------------------------------------------------------

        #region Relational members

        public int CompareTo(OtProfile other)
        {
            if (ReferenceEquals(this, other)) return 0;
            if (ReferenceEquals(null, other)) return 1;
            return Id.CompareTo(other.Id);
        }

        #endregion
        
        [JsonIgnore]
        public static IComparer<OtProfile> BaseOrder = new OtElementBaseComparable();
        
        // Compare
        //--------------------------------------------------------------------------------------------------------------

        #region Equality members

        public bool Equals(OtProfile other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Id == other.Id;
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj is OtProfile element && Equals(element);
        }

        public override int GetHashCode()
        {
            // ReSharper disable once NonReadonlyMemberInGetHashCode
            return Id;
        }

        #endregion
        [JsonIgnore]
        public static IEqualityComparer<OtProfile> BaseEqual = new OtElementBaseEqualityComparer();
    }

    public sealed class OtElementBaseEqualityComparer : IEqualityComparer<OtProfile>
    {
        #region Implementation of IEqualityComparer<in OtProfile>

        public bool Equals(OtProfile x, OtProfile y)
        {
            if (x == null && y == null) return true;
            return x?.Equals(y) ?? false;
        }

        public int GetHashCode(OtProfile obj)
        {
            return obj.GetHashCode();
        }

        #endregion
    }
    
    public sealed class OtElementBaseComparable : IComparer<OtProfile>
    {
        #region Implementation of IComparer<in OtProfile>

        public int Compare(OtProfile x, OtProfile y)
        {
            if (x == null && y == null) return 0;
            return x?.CompareTo(y) ?? -1;
        }

        #endregion
    }
}