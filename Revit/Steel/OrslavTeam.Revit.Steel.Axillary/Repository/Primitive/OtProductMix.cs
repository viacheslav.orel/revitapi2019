﻿﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace OrslavTeam.Revit.Steel.Axillary.Repository.Primitive
{
    public class OtProductMix : ICloneable, IEquatable<OtProductMix>, IComparable<OtProductMix>
    {
        #region Fields

        public int Id { get; set; }

        public string Number { get; set; }

        public string Name { get; set; }

        #endregion

        #region Methods

        [JsonIgnore] public string GetFullName => $"{Number}. {Name}";

        #endregion

        #region Constructor

        public OtProductMix()
        {
        }

        public OtProductMix(int id)
        {
            Id = id;
        }

        #endregion

        //Clone
        //------------------------------------------------------------------------------------------------------------- 

        #region Implementation of ICloneable

        public object Clone()
        {
            return new OtProductMix
            {
                    Id = Id,
                    Number = Number,
                    Name = Name,
            };
        }

        #endregion

        //Sort
        //------------------------------------------------------------------------------------------------------------- 

        #region Relational members

        public int CompareTo(OtProductMix other)
        {
            if (ReferenceEquals(this, other)) return 0;
            if (ReferenceEquals(null, other)) return 1;
            return Id.CompareTo(other.Id);
        }

        #endregion
        [JsonIgnore]
        public static IComparer<OtProductMix> BaseOrder = new OtProductMixBaseComparable();

        // Compare
        //------------------------------------------------------------------------------------------------------------- 

        #region Equality members

        public bool Equals(OtProductMix other)
        {
            if (other == null) return false;
            return Id == other.Id;
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj is OtProductMix productMix && Equals(productMix);
        }

        public override int GetHashCode()
        {
            // ReSharper disable once NonReadonlyMemberInGetHashCode
            return Id;
        }

        #endregion
        [JsonIgnore]
        public static IEqualityComparer<OtProductMix> BaseEqual = new OtProductMixBaseEqualityComparer();
    }

    public sealed class OtProductMixBaseEqualityComparer : IEqualityComparer<OtProductMix>
    {
        #region Implementation of IEqualityComparer<in OtProductMix>

        public bool Equals(OtProductMix x, OtProductMix y)
        {
            if (x == null && y == null) return true;
            return x?.Equals(y) ?? false;
        }

        public int GetHashCode(OtProductMix obj)
        {
            return obj.GetHashCode();
        }

        #endregion
    }

    public sealed class OtProductMixBaseComparable : IComparer<OtProductMix>
    {
        #region Implementation of IComparer<in OtProductMixBaseComparable>

        public int Compare(OtProductMix x, OtProductMix y)
        {
            if (x == null && y == null) return 0;
            return x?.CompareTo(y) ?? -1;
        }

        #endregion
    }
}