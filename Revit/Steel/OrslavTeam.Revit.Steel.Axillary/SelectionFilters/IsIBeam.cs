﻿using System;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI.Selection;
using OrslavTeam.Revit.Axillary.PublicInformation;

namespace OrslavTeam.Revit.Steel.Axillary.SelectionFilters
{
    public class IsIBeam : ISelectionFilter
    {
        #region Implementation of ISelectionFilter

        public bool AllowElement(Element elem)
        {
            try
            {
                string guid =
                    PublicParameter.ElementGuid(
                        elem is FamilySymbol
                            ? elem
                            : ((FamilyInstance) elem).Symbol);
                return guid == "d8ba024f-193f-432e-8091-9e6c0c56d8ea";
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool AllowReference(Reference reference, XYZ position)
        {
            return false;
        }

        #endregion
    }
}