﻿using System;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI.Selection;
using OrslavTeam.Revit.Axillary.PublicInformation;

namespace OrslavTeam.Revit.Steel.Axillary.SelectionFilters
{
    public class IsStairRun : ISelectionFilter
    {
        public bool AllowElement(Element elem)
        {
            try
            {
                string uniKey =
                    PublicParameter.GetUniformatKode(elem is FamilySymbol ? elem : ((FamilyInstance)elem).Symbol);
                return uniKey == "B.02.02.03.02";
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public bool AllowReference(Reference reference, XYZ position)
        {
            return false;
        }
    }
}