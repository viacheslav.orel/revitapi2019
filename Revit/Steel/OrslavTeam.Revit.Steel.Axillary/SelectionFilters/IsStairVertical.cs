﻿using Autodesk.Revit.DB;
using Autodesk.Revit.UI.Selection;
using OrslavTeam.Revit.Axillary.PublicInformation;

namespace OrslavTeam.Revit.Steel.Axillary.SelectionFilters
{
    public class IsStairVertical : ISelectionFilter
    {
        public bool AllowElement(Element elem)
        {
            try
            {
                string uniKey =
                    PublicParameter.GetUniformatKode(elem is FamilySymbol ? elem : ((FamilyInstance) elem).Symbol);
                return uniKey == "B.02.02.03.01";
            }
            catch (System.Exception)
            {
                return false;
            }
        }

        public bool AllowReference(Reference reference, XYZ position)
        {
            return false;
        }
    }
}