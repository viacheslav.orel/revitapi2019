﻿namespace OrslavTeam.Revit.Steel.Axillary.DbElement
{
    public struct SshElement
    {
        public int ProductMixId { get; set; }

        public int ProfileId { get; set; }

        public int MaterialId { get; set; }

        public double Units { get; set; }
        public int Quantity { get; set; }

        public int ColumnNumber { get; set; }
    }
}