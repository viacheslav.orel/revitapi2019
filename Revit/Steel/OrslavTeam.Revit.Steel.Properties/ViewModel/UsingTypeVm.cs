﻿using System;
using Autodesk.Revit.DB;
using OrslavTeam.Revit.Axillary.ViewTemplate.Infrastructure;

namespace OrslavTeam.Revit.Steel.Properties.ViewModel
{
    public class UsingTypeVm : ObservableObject, ICloneable
    {
        #region Fields

        private int _id;
        private string _name;

        #endregion

        #region FieldsAccess

        public int Id
        {
            get => _id;
            set
            {
                _id = value; 
                RaisePropertyChangedEvent(nameof(Id));
            }
        }

        public string Name
        {
            get => _name;
            set
            {
                _name = value; 
                RaisePropertyChangedEvent(nameof(Name));
            }
        }

        #endregion

        public object Clone()
        {
            return new UsingTypeVm
            {
                Id = this.Id,
                Name = this.Name,
            };
        }
    }
}