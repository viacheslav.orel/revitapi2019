﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Newtonsoft.Json;
using OrslavTeam.Revit.Axillary.ViewTemplate.Infrastructure;
using OrslavTeam.Revit.Steel.Properties.Model;
using OrslavTeam.Revit.Steel.Properties.Revit;

namespace OrslavTeam.Revit.Steel.Properties.ViewModel
{
    public class SteelPropertiesViewModel : ObservableObject
    {
        #region Fields

        //GeneralUsingType
        private ObservableCollection<UsingTypeVm> _generalUsingTypeCollection =
            new ObservableCollection<UsingTypeVm>();

        private UsingTypeVm _generalUsingTypeCollectionSelected;

        //SubUsingType
        private ObservableCollection<UsingTypeVm> _subUsingTypeCollection =
            new ObservableCollection<UsingTypeVm>();

        private UsingTypeVm _subUsingTypeCollectionSelected;
        
        //SteelSpread
        private ObservableCollection<UsingTypeVm> _steelSpreadHeaderCollection =
            new ObservableCollection<UsingTypeVm>();

        private UsingTypeVm _steelSpreadHeaderCollectionSelected;
        private int _steelSpreadHeaderId = 1;
        private string _steelSpreadHeaderName;

        //SteelGroup
        private ObservableCollection<SteelGroupVm> _steelGroup = new ObservableCollection<SteelGroupVm>();
        private string _groupMarkPrefix;

        //SteelGroup--Classification
        private ObservableCollection<SteelClassification> _steelClassificationFirstCollection;
        private SteelClassification _steelClassificationFirstCollectionSelected;

        private ObservableCollection<SteelClassification> _steelClassificationSecondCollection;
        private SteelClassification _steelClassificationSecondCollectionSelected;

        private ObservableCollection<SteelClassification> _steelClassificationThirdCollection;
        private SteelClassification _steelClassificationThirdCollectionSelected;

        #endregion

        #region FieldsAccess

        //GeneralUsingType
        public ObservableCollection<UsingTypeVm> GeneralUsingTypeCollection
        {
            get => _generalUsingTypeCollection;
            set
            {
                _generalUsingTypeCollection = value;
                RaisePropertyChangedEvent(nameof(GeneralUsingTypeCollection));
            }
        }

        public UsingTypeVm GeneralUsingTypeCollectionSelected
        {
            get => _generalUsingTypeCollectionSelected;
            set
            {
                _generalUsingTypeCollectionSelected = value;
                RaisePropertyChangedEvent(nameof(GeneralUsingTypeCollectionSelected));
            }
        }

        //SubUsingType
        public ObservableCollection<UsingTypeVm> SubUsingTypeCollection
        {
            get => _subUsingTypeCollection;
            set
            {
                _subUsingTypeCollection = value;
                RaisePropertyChangedEvent(nameof(SubUsingTypeCollection));
            }
        }

        public UsingTypeVm SubUsingTypeCollectionSelected
        {
            get => _subUsingTypeCollectionSelected;
            set
            {
                _subUsingTypeCollectionSelected = value;
                RaisePropertyChangedEvent(nameof(SubUsingTypeCollectionSelected));
            }
        }
        
        //SteelSpreadHeader
        public ObservableCollection<UsingTypeVm> SteelSpreadHeaderCollection
        {
            get => _steelSpreadHeaderCollection;
            set
            {
                _steelSpreadHeaderCollection = value; 
                RaisePropertyChangedEvent(nameof(SteelSpreadHeaderCollection));
            }
        }

        public UsingTypeVm SteelSpreadHeaderCollectionSelected
        {
            get => _steelSpreadHeaderCollectionSelected;
            set
            {
                _steelSpreadHeaderCollectionSelected = value;
                RaisePropertyChangedEvent(nameof(SteelSpreadHeaderCollectionSelected));
            }
        }

        public int SteelSpreadHeaderId
        {
            get => _steelSpreadHeaderId;
            set
            {
                _steelSpreadHeaderId = value; 
                RaisePropertyChangedEvent(nameof(SteelSpreadHeaderId));
            }
        }

        public string SteelSpreadHeaderName
        {
            get => _steelSpreadHeaderName;
            set
            {
                _steelSpreadHeaderName = value; 
                RaisePropertyChangedEvent(nameof(SteelSpreadHeaderName));
            }
        }

        //SteelGroup
        public ObservableCollection<SteelGroupVm> SteelGroup
        {
            get => _steelGroup;
            set
            {
                _steelGroup = value;
                RaisePropertyChangedEvent(nameof(SteelGroup));
            }
        }

        public string GroupMarkPrefix
        {
            get => _groupMarkPrefix;
            set
            {
                _groupMarkPrefix = value;
                RaisePropertyChangedEvent(nameof(GroupMarkPrefix));
            }
        }

        //SteelGroup--Classification
        public ObservableCollection<SteelClassification> SteelClassificationFirstCollection
        {
            get => _steelClassificationFirstCollection;
            set
            {
                _steelClassificationFirstCollection = value;
                RaisePropertyChangedEvent(nameof(SteelClassificationFirstCollection));

                SteelClassificationFirstCollectionSelected = _steelClassificationFirstCollection?.FirstOrDefault();
            }
        }

        public SteelClassification SteelClassificationFirstCollectionSelected
        {
            get => _steelClassificationFirstCollectionSelected;
            set
            {
                _steelClassificationFirstCollectionSelected = value;
                RaisePropertyChangedEvent(nameof(SteelClassificationFirstCollectionSelected));

                SteelClassificationSecondCollection = _steelClassificationFirstCollectionSelected.SubClass != null
                    ? new ObservableCollection<SteelClassification>(
                        _steelClassificationFirstCollectionSelected.SubClass)
                    : null;
            }
        }

        public ObservableCollection<SteelClassification> SteelClassificationSecondCollection
        {
            get => _steelClassificationSecondCollection;
            set
            {
                _steelClassificationSecondCollection = value;
                RaisePropertyChangedEvent(nameof(SteelClassificationSecondCollection));

                SteelClassificationSecondCollectionSelected = 
                    _steelClassificationSecondCollection?.FirstOrDefault();
            }
        }

        public SteelClassification SteelClassificationSecondCollectionSelected
        {
            get => _steelClassificationSecondCollectionSelected;
            set
            {
                _steelClassificationSecondCollectionSelected = value;
                RaisePropertyChangedEvent(nameof(SteelClassificationSecondCollectionSelected));

                SteelClassificationThirdCollection = _steelClassificationSecondCollectionSelected.SubClass != null
                    ? new ObservableCollection<SteelClassification>(
                        _steelClassificationSecondCollectionSelected.SubClass)
                    : null;
            }
        }

        public ObservableCollection<SteelClassification> SteelClassificationThirdCollection
        {
            get => _steelClassificationThirdCollection;
            set
            {
                _steelClassificationThirdCollection = value;
                RaisePropertyChangedEvent(nameof(SteelClassificationThirdCollection));

                SteelClassificationThirdCollectionSelected = SteelClassificationThirdCollection?.FirstOrDefault();
            }
        }

        public SteelClassification SteelClassificationThirdCollectionSelected
        {
            get => _steelClassificationThirdCollectionSelected;
            set
            {
                _steelClassificationThirdCollectionSelected = value;
                RaisePropertyChangedEvent(nameof(SteelClassificationThirdCollectionSelected));
            }
        }

        #endregion

        #region Command
        
        //SteelSpread--Add
        private DelegateCommand _steelSpreadAddCommand;

        public DelegateCommand SteelSpreadAddCommand =>
            _steelSpreadAddCommand ?? (_steelSpreadAddCommand = new DelegateCommand(obj =>
            {
                SteelSpreadHeaderCollection.Add(new UsingTypeVm
                {
                    Id = SteelSpreadHeaderId,
                    Name = SteelSpreadHeaderName
                });
                SteelSpreadHeaderId++;
                SteelSpreadHeaderName = "";
            }));

        //SteelSpread--Remove
        private DelegateCommand _steelSpreadRemoveCommand;

        public ICommand SteelSpreadRemoveCommand =>
            _steelSpreadRemoveCommand ?? (_steelSpreadRemoveCommand =
                new DelegateCommand(o => SteelSpreadHeaderCollection.Remove((UsingTypeVm) o)));

        //SteelGroup--Add
        private DelegateCommand _steelGroupAddCommand;

        public ICommand SteelGroupAddCommand =>
            _steelGroupAddCommand ?? (_steelGroupAddCommand = new DelegateCommand(obj =>
            {
                var tmp = new SteelGroupVm
                {
                    ClassType = new SteelClassificationData
                    {
                        FirstId = SteelClassificationFirstCollectionSelected?.Id ?? 0,
                        SecondId = SteelClassificationSecondCollectionSelected?.Id ?? 0,
                        ThirdId = SteelClassificationThirdCollectionSelected?.Id ?? 0,
                        FirstName = SteelClassificationFirstCollectionSelected?.Name ?? "",
                        SecondName = SteelClassificationSecondCollectionSelected?.Name ?? "",
                        ThirdName = SteelClassificationThirdCollectionSelected?.Name ?? "",
                    },
                    MarkPrefix = GroupMarkPrefix,
                };
                if (GeneralUsingTypeCollectionSelected != null)
                    tmp.GeneralUsingType = (UsingTypeVm) GeneralUsingTypeCollectionSelected.Clone();
                if (SubUsingTypeCollectionSelected != null)
                    tmp.SubUsingType = (UsingTypeVm) SubUsingTypeCollectionSelected.Clone();
                if (SteelSpreadHeaderCollectionSelected != null)
                    tmp.SteelSpreadGroup = (UsingTypeVm) SteelSpreadHeaderCollectionSelected.Clone();
                
                if (SteelClassificationThirdCollectionSelected != null)
                {
                    tmp.ClassType.UseClass = SteelClassificationThirdCollectionSelected.UseClass;
                    tmp.ClassType.StressClass = SteelClassificationThirdCollectionSelected.StressClass;
                }
                else if (SteelClassificationSecondCollectionSelected != null)
                {
                    tmp.ClassType.UseClass = SteelClassificationSecondCollectionSelected.UseClass;
                    tmp.ClassType.StressClass = SteelClassificationSecondCollectionSelected.StressClass;
                }
                else if (SteelClassificationFirstCollectionSelected != null)
                {
                    tmp.ClassType.UseClass = SteelClassificationFirstCollectionSelected.UseClass;
                    tmp.ClassType.StressClass = SteelClassificationFirstCollectionSelected.StressClass;
                }

                SteelGroup.Add(tmp);

                GeneralUsingTypeCollectionSelected = null;
                SubUsingTypeCollectionSelected = null;
                SteelSpreadHeaderCollectionSelected = null;
                GroupMarkPrefix = "";
            }));
        
        //SteelGroup--Remove
        private DelegateCommand _steelGroupRemoveCommand;

        public ICommand SteelGroupRemoveCommand =>
            _steelGroupRemoveCommand ?? (_steelGroupRemoveCommand =
                new DelegateCommand(obj => SteelGroup.Remove((SteelGroupVm) obj)));
        
        //SteelProperties--Save
        private DelegateCommand _steelPropertiesSaveCommand;

        public ICommand SteelPropertiesSaveCommand =>
            _steelPropertiesSaveCommand ?? (_steelPropertiesSaveCommand = new DelegateCommand(obj =>
            {
                
                ExternalHandler.RevitCommand = app =>
                    ExternalHandler.ModifySchema.WriteSetting(SteelGroup, SteelSpreadHeaderCollection);
                
                ExternalHandler.ExEvent.Raise();
            }));

        #endregion

        #region Constructors

        public SteelPropertiesViewModel()
        {
            SteelClassificationFirstCollection =
                new ObservableCollection<SteelClassification>(SteelClassificationAxillary.ClassificationList);
        }

        #endregion
    }
}