﻿﻿using Newtonsoft.Json;
 using OrslavTeam.Revit.Axillary.ViewTemplate.Infrastructure;
using OrslavTeam.Revit.Steel.Properties.Model;

namespace OrslavTeam.Revit.Steel.Properties.ViewModel
{
    public class SteelGroupVm : ObservableObject
    {
        #region Fields

        private UsingTypeVm _generalUsingType;
        private UsingTypeVm _subUsingType;
        private UsingTypeVm _steelSpreadGroup;
        private SteelClassificationData _classType;
        private string _markPrefix;

        #endregion

        #region FieldsAccess

        public UsingTypeVm GeneralUsingType
        {
            get => _generalUsingType;
            set
            {
                _generalUsingType = value; 
                RaisePropertyChangedEvent(nameof(GeneralUsingType));
            }
        }

        public UsingTypeVm SubUsingType
        {
            get => _subUsingType;
            set
            {
                _subUsingType = value; 
                RaisePropertyChangedEvent(nameof(SubUsingType));
            }
        }

        public UsingTypeVm SteelSpreadGroup
        {
            get => _steelSpreadGroup;
            set
            {
                _steelSpreadGroup = value;
                RaisePropertyChangedEvent(nameof(SteelSpreadGroup));
            }
        }

        public SteelClassificationData ClassType
        {
            get => _classType;
            set
            {
                _classType = value; 
                RaisePropertyChangedEvent(nameof(ClassType));
                RaisePropertyChangedEvent(nameof(GroupName));
            }
        }

        public string MarkPrefix
        {
            get => _markPrefix;
            set
            {
                _markPrefix = value; 
                RaisePropertyChangedEvent(nameof(MarkPrefix));
            }
        }

        [JsonIgnore]
        public string GroupName => $"{ClassType.UseClass} : {ClassType.StressClass}";

        #endregion
    }
}