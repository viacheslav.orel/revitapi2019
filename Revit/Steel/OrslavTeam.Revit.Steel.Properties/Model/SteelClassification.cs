﻿using Newtonsoft.Json;

namespace OrslavTeam.Revit.Steel.Properties.Model
{
    public class SteelClassification
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public char UseClass { get; set; }
        public int StressClass { get; set; }
        public SteelClassification[] SubClass { get; set; }
    }

    public class SteelClassificationData
    {
        public int FirstId { get; set; } = 0;
        public int SecondId { get; set; } = 0;
        public int ThirdId { get; set; } = 0;

        public string FirstName { get; set; } = "";
        public string SecondName { get; set; } = "";
        public string ThirdName { get; set; } = "";
        
        [JsonIgnore]
        public string Name => $"{FirstName} : {SecondName} : {ThirdName}";
        
        public char UseClass { get; set; }
        public int StressClass { get; set; }
    }

    public static class SteelClassificationAxillary
    {
        public static readonly SteelClassification[] ClassificationList = new[]
        {
            new SteelClassification
            {
                Id = 1,
                Name = "Конструкції кранових колій",
                SubClass = new[]
                {
                    new SteelClassification
                    {
                        Id = 1,
                        Name =
                            "Підкранові балки і ферми (крім ребер жорсткості) і ферм (пояси, елементи решіток, фасонки)",
                        UseClass = 'А',
                        StressClass = 1,
                        SubClass = null
                    },
                    new SteelClassification
                    {
                        Id = 2,
                        Name = "Гальмівні балки і ферми, деталі кріплення до колон, ребра жорсткості",
                        UseClass = 'А',
                        StressClass = 2,
                        SubClass = null
                    },
                    new SteelClassification
                    {
                        Id = 3,
                        Name = "Балки колій підвісного транспорту",
                        SubClass = new[]
                        {
                            new SteelClassification
                            {
                                Id = 1,
                                Name = "Зварні",
                                UseClass = 'А',
                                StressClass = 1,
                                SubClass = null
                            },
                            new SteelClassification
                            {
                                Id = 2,
                                Name =
                                    "Прокатні під технологічні електричні талі і кран-балки або ручні талі і кран-балки",
                                UseClass = 'Б',
                                StressClass = 1,
                                SubClass = null
                            },
                        }
                    },
                    new SteelClassification
                    {
                        Id = 4,
                        Name = "Допоміжні горизонтальні ферми, вертикальні ферми, тупикові упори",
                        UseClass = 'Б',
                        StressClass = 2,
                        SubClass = null
                    },
                    new SteelClassification
                    {
                        Id = 5,
                        Name = "Деталі кріплення рейок",
                        UseClass = 'В',
                        StressClass = 3,
                        SubClass = null
                    },
                }
            },
            new SteelClassification
            {
                Id = 2,
                Name = "Робочі площадки за наявності рухомого транспорту",
                SubClass = new[]
                {
                    new SteelClassification
                    {
                        Id = 1,
                        Name = "Балки при залізничному рухомому складі",
                        UseClass = 'А',
                        StressClass = 1,
                        SubClass = null
                    },
                    new SteelClassification
                    {
                        Id = 2,
                        Name = "Балки при автонавантажувачах та іншому транспорті",
                        UseClass = 'А',
                        StressClass = 2,
                        SubClass = null
                    },
                    new SteelClassification
                    {
                        Id = 3,
                        Name = "Металевий настил, включений у сумісну роботу з балками настилу, ребра жорсткості балок",
                        UseClass = 'Б',
                        StressClass = 1,
                        SubClass = null
                    },
                    new SteelClassification
                    {
                        Id = 4,
                        Name =
                            "Металевий настил, не включений у сумісну роботу з балками настилу, ребра жорсткості балок",
                        UseClass = 'Б',
                        StressClass = 2,
                        SubClass = null
                    },
                }
            },
            new SteelClassification
            {
                Id = 3,
                Name = "Конструкції технологічних площадок і покриттів",
                UseClass = '\0',
                StressClass = 0,
                SubClass = new[]
                {
                    new SteelClassification
                    {
                        Id = 1,
                        Name = "Головні балки і ригелі рам при динамічному навантаженні",
                        UseClass = 'А',
                        StressClass = 1,
                        SubClass = null
                    },
                    new SteelClassification
                    {
                        Id = 2,
                        Name = "Головні балки при статичному навантаженні",
                        UseClass = 'А',
                        StressClass = 3,
                        SubClass = null
                    },
                    new SteelClassification
                    {
                        Id = 3,
                        Name = "Другорядні балки при динамічному навантаженні",
                        UseClass = 'А',
                        StressClass = 2,
                        SubClass = null
                    },
                    new SteelClassification
                    {
                        Id = 4,
                        Name = "Другорядні балки при статичному навантаженні",
                        UseClass = 'А',
                        StressClass = 3,
                        SubClass = null
                    },
                    new SteelClassification
                    {
                        Id = 5,
                        Name =
                            "Металевий настил, включений до сумісної роботи з балками настилу при динамічному навантаженні",
                        UseClass = 'Б',
                        StressClass = 1,
                        SubClass = null
                    },
                    new SteelClassification
                    {
                        Id = 6,
                        Name =
                            "Металевий настил, окрім включеног до сумісної роботи з балками настилу при динамічному навантаженні",
                        UseClass = 'Б',
                        StressClass = 2,
                        SubClass = null
                    },
                    new SteelClassification
                    {
                        Id = 7,
                        Name = "Ребра жорсткості балок",
                        UseClass = 'В',
                        StressClass = 3,
                        SubClass = null
                    },
                }
            },
            new SteelClassification
            {
                Id = 4,
                Name = "Колони виробничих споруд і відкритих кранових естакад, стояки робочих і технологічних площадок",
                SubClass = new[]
                {
                    new SteelClassification
                    {
                        Id = 1,
                        Name =
                            "Основні елементи поперечного перерізу (у тому числі пояси і решітки при наскрізному перерізу), опорні плити, підкранові траверси колон",
                        UseClass = 'А',
                        StressClass = 3,
                        SubClass = null
                    },
                    new SteelClassification
                    {
                        Id = 2,
                        Name = "Вертикальні в'язі між колонами",
                        UseClass = 'А',
                        StressClass = 3,
                        SubClass = null
                    },
                    new SteelClassification
                    {
                        Id = 3,
                        Name =
                            "Ребра жорсткості і діафрагми колон, елементи решіток двоплощинних в'язей, в'язі з напруженням, меншим за 0,4Ry",
                        UseClass = 'В',
                        StressClass = 3,
                        SubClass = null
                    },
                }
            },
            new SteelClassification
            {
                Id = 5,
                Name = "Конструкції покриття",
                SubClass = new[]
                {
                    new SteelClassification
                    {
                        Id = 1,
                        Name =
                            "Ферми, ригелі та інші елементи, що підлягають безпосередній дії динамічних навантажень від технологічного чи транспортного устаткування",
                        UseClass = 'А',
                        StressClass = 1,
                        SubClass = null
                    },
                    new SteelClassification
                    {
                        Id = 2,
                        Name = "Ферми, ригелі та інші елементи, при статичному навантаженні",
                        UseClass = 'А',
                        StressClass = 2,
                        SubClass = null
                    },
                    new SteelClassification
                    {
                        Id = 3,
                        Name = "Вузлові фасонки",
                        UseClass = 'А',
                        StressClass = 2,
                        SubClass = null
                    },
                    new SteelClassification
                    {
                        Id = 4,
                        Name =
                            "ліхтарні панелі, панелі покрівлі, прогони, горизонтальні торцеві в'язі в рівні покрівлі, поздовжні в'язі при кроці колон, що є більшим за крок кроквяних ферм",
                        UseClass = 'Б',
                        StressClass = 2,
                        SubClass = null
                    },
                    new SteelClassification
                    {
                        Id = 5,
                        Name = "Інші в'язі",
                        UseClass = 'В',
                        StressClass = 3,
                        SubClass = null
                    },
                }
            },
            new SteelClassification
            {
                Id = 6,
                Name = "Конструкції фахверка",
                SubClass = new[]
                {
                    new SteelClassification
                    {
                        Id = 1,
                        Name = "Ригелі під цегляні стіни і над воротами",
                        UseClass = 'А',
                        StressClass = 3,
                        SubClass = null
                    },
                    new SteelClassification
                    {
                        Id = 2,
                        Name = "Стояки, торцеві і вітрові ферми",
                        UseClass = 'Б',
                        StressClass = 2,
                        SubClass = null
                    },
                    new SteelClassification
                    {
                        Id = 3,
                        Name = "Ригелі (крім ригелів під цегляні стіни та над воротами)",
                        UseClass = 'Б',
                        StressClass = 2,
                        SubClass = null
                    },
                }
            },
            new SteelClassification
            {
                Id = 7,
                Name = "Допоміжні конструкції виробничих споруд",
                SubClass = new[]
                {
                    new SteelClassification
                    {
                        Id = 1,
                        Name = "Косоури сходів",
                        UseClass = 'А',
                        StressClass = 3,
                        SubClass = null
                    },
                    new SteelClassification
                    {
                        Id = 2,
                        Name =
                            "Сходи, перехідні площадки, огорожі, площадки світильників, посадкові площадки на крани, балки підвісних стель, імпости, віконні і ліхтарні рами",
                        UseClass = 'В',
                        StressClass = 3,
                        SubClass = null
                    },
                }
            },
            new SteelClassification
            {
                Id = 8,
                Name = "Транспортні галереї",
                SubClass = new[]
                {
                    new SteelClassification
                    {
                        Id = 1,
                        Name = "Прогінні споруди галереї, несучі балки під конвеєри, фасонки ферм",
                        UseClass = 'А',
                        StressClass = 1,
                        SubClass = null
                    },
                    new SteelClassification
                    {
                        Id = 2,
                        Name = "Опори, в'язі між колонами, опорні ребра балок",
                        UseClass = 'А',
                        StressClass = 2,
                        SubClass = null
                    },
                    new SteelClassification
                    {
                        Id = 3,
                        Name =
                            "Елементи фахверка, в'язі, прогони і балки покриттів прогінних споруд, ребра жорсткості балок",
                        UseClass = 'Б',
                        StressClass = 2,
                        SubClass = null
                    },
                }
            },
            new SteelClassification
            {
                Id = 9,
                Name = "Опори повітряних ліній електропередавання, конструкцій відкритих розподільних пристроїв",
                SubClass = new[]
                {
                    new SteelClassification
                    {
                        Id = 1,
                        Name = "Зварні спеціальні опори великих переходів заввишки понад 60 м",
                        UseClass = 'А',
                        StressClass = 1,
                        SubClass = null
                    },
                    new SteelClassification
                    {
                        Id = 2,
                        Name =
                            "Опори ПЛ, окрім зварних спеціальних опор великих переходів заввишки понад 60 м, опори під вимикачі і портали ошинуваня ВРП",
                        UseClass = 'А',
                        StressClass = 3,
                        SubClass = null
                    },
                    new SteelClassification
                    {
                        Id = 3,
                        Name = "Опори під устаткування ВРП тощо, крім зазначених вище",
                        UseClass = 'В',
                        StressClass = 2,
                        SubClass = null
                    },
                }
            },
            new SteelClassification
            {
                Id = 10,
                Name = "Антенні споруди зв'язку заввишки до 500 м",
                SubClass = new[]
                {
                    new SteelClassification
                    {
                        Id = 1,
                        Name = "Стовбури щогл і башт, решітки, елементи обпирання на фундаменти",
                        UseClass = 'А',
                        StressClass = 2,
                        SubClass = null
                    },
                    new SteelClassification
                    {
                        Id = 2,
                        Name =
                            "Елементи (механічні деталі) відтяжок щогл і антенних полотен, деталі кріплення відтяжок до фундаментіві до стовбурів сталевих опор",
                        UseClass = 'А',
                        StressClass = 1,
                        SubClass = null
                    },
                    new SteelClassification
                    {
                        Id = 3,
                        Name = "Діафрагми баштових опор, хідники, перехідні площадки",
                        UseClass = 'Б',
                        StressClass = 2,
                        SubClass = null
                    },
                }
            },
            new SteelClassification
            {
                Id = 11,
                Name = "Витяжні башти",
                SubClass = new[]
                {
                    new SteelClassification
                    {
                        Id = 1,
                        Name = "Пояси башт, вузлові фасонки",
                        UseClass = 'А',
                        StressClass = 2,
                        SubClass = null
                    },
                    new SteelClassification
                    {
                        Id = 2,
                        Name =
                            "Газовідвідний стовбур, елементи решітки, балки і площадки діафрагм, що безпосередньо сприймають вагу стовбура",
                        UseClass = 'Б',
                        StressClass = 2,
                        SubClass = null
                    },
                    new SteelClassification
                    {
                        Id = 3,
                        Name =
                            "Опорні плити, хідники, огорожі, настил площадок, балок і площадки діафрагм, що не сприймають вагу стовбура",
                        UseClass = 'В',
                        StressClass = 3,
                        SubClass = null
                    },
                }
            },
            new SteelClassification
            {
                Id = 12,
                Name = "Димові труби",
                SubClass = new[]
                {
                    new SteelClassification
                    {
                        Id = 1,
                        Name = "Сталева оболонка і ребра жорсткості труби",
                        UseClass = 'А',
                        StressClass = 2,
                        SubClass = null
                    },
                    new SteelClassification
                    {
                        Id = 2,
                        Name = "Площадки, опірні кільця, хідники та огорожі",
                        UseClass = 'Б',
                        StressClass = 2,
                        SubClass = null
                    },
                }
            },
            new SteelClassification
            {
                Id = 13,
                Name = "Градирні баштові і вентиляторні, водонапірні башти",
                SubClass = new[]
                {
                    new SteelClassification
                    {
                        Id = 1,
                        Name = "Пояси решітчастих башт, кільця жорсткості, решітки",
                        UseClass = 'А',
                        StressClass = 3,
                        SubClass = null
                    },
                    new SteelClassification
                    {
                        Id = 2,
                        Name = "Вузлові фасонки",
                        UseClass = 'А',
                        StressClass = 2,
                        SubClass = null
                    },
                    new SteelClassification
                    {
                        Id = 3,
                        Name = "Фахверки, допоміжні площадки, обшивки градирень",
                        UseClass = 'В',
                        StressClass = 3,
                        SubClass = null
                    },
                }
            },
            new SteelClassification
            {
                Id = 14,
                Name = "Бункери",
                SubClass = new[]
                {
                    new SteelClassification
                    {
                        Id = 1,
                        Name = "Бункерні балки, оболонки параболіних бункерів",
                        UseClass = 'А',
                        StressClass = 1,
                        SubClass = null
                    },
                    new SteelClassification
                    {
                        Id = 2,
                        Name = "Стінки інших бункерів, ребра жорсткості бункерів",
                        UseClass = 'А',
                        StressClass = 3,
                        SubClass = null
                    },
                }
            },
            new SteelClassification
            {
                Id = 15,
                Name = "Резервуари і газгольдери",
                SubClass = new[]
                {
                    new SteelClassification
                    {
                        Id = 1,
                        Name = "Стінки та кромки днищ резервуарів місткістю 10 тис. м3 і більше, фасонки покриттів",
                        UseClass = 'А',
                        StressClass = 1,
                        SubClass = null
                    },
                    new SteelClassification
                    {
                        Id = 2,
                        Name = "Стінки та кромки днищ резервуарів місткістю менше 10 тис. м3",
                        UseClass = 'А',
                        StressClass = 2,
                        SubClass = null
                    },
                    new SteelClassification
                    {
                        Id = 3,
                        Name =
                            "Центральні частини днищ, опорні кільця покриття, кільця жорсткості, плавучі покрівлі і понтони покриття",
                        UseClass = 'А',
                        StressClass = 3,
                        SubClass = null
                    },
                    new SteelClassification
                    {
                        Id = 4,
                        Name = "Внутрішні корпуси ізотермічних резервуарів за температури зберігання не вище -50",
                        UseClass = 'А',
                        StressClass = 1,
                        SubClass = null
                    },
                }
            },
            new SteelClassification
            {
                Id = 16,
                Name = "Конструкції контактної мережі траспорту",
                SubClass = new[]
                {
                    new SteelClassification
                    {
                        Id = 1,
                        Name = "Конструкції та елементи, пов'язані з натягом проводі (тяги, штанги, хомути)",
                        UseClass = 'А',
                        StressClass = 2,
                        SubClass = null
                    },
                    new SteelClassification
                    {
                        Id = 2,
                        Name =
                            "Конструкції та елементи несучих, підтримувальних і фіксувальних пристроїв (опори, ригелі жорсткості поперечин, прожекторні щогли, фіксатори)",
                        UseClass = 'Б',
                        StressClass = 2,
                        SubClass = null
                    },
                    new SteelClassification
                    {
                        Id = 3,
                        Name = "Допоміжні конструкції",
                        UseClass = 'В',
                        StressClass = 3,
                        SubClass = null
                    },
                }
            },
            new SteelClassification
            {
                Id = 17,
                Name = "Силоси (оболонка, ребра жорсткості)",
                UseClass = 'А',
                StressClass = 3,
                SubClass = null
            },
            new SteelClassification
            {
                Id = 18,
                Name =
                    "Громадські споруди (театри, кінотеатри, цирки, спортивні споруди, криті ринки, навчальні заклади, дошкільні навчальні заклади, лікарні, пологові будинки, музії, державні архіви тощо, споруди заввишки понад 75 м",
                SubClass = new[]
                {
                    new SteelClassification
                    {
                        Id = 1,
                        Name = "Перекриття і покриття, косоури сходин",
                        UseClass = 'А',
                        StressClass = 2,
                        SubClass = null
                    },
                    new SteelClassification
                    {
                        Id = 2,
                        Name = "Колони",
                        UseClass = 'А',
                        StressClass = 2,
                        SubClass = null
                    },
                }
            },
        };
    }
}