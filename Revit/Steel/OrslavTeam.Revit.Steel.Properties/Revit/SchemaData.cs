﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using Autodesk.Revit.DB;
using Autodesk.Revit.DB.ExtensibleStorage;
using Newtonsoft.Json;
using OrslavTeam.Revit.Steel.Properties.ViewModel;

namespace OrslavTeam.Revit.Steel.Properties.Revit
{
    public class SchemaData
    {
        private static readonly Guid _schemaId = new Guid("5EC64A47-A65A-4444-A46B-B5719AB32880");
        private const string SteelGroupField = "SteelGroup";
        private const string SteelSpreadHeadersFields = "SteelSpreadHeaders";

        private readonly Document _doc;

        private static ObservableCollection<UsingTypeVm> _steelSpreadCollection = new ObservableCollection<UsingTypeVm>();
        private static ObservableCollection<SteelGroupVm> _steelGroups = new ObservableCollection<SteelGroupVm>();

        public SchemaData(Document doc)
        {
            _doc = doc;
        }

        private static Schema GetSchema()
        {
            Schema schema = Schema.Lookup(_schemaId);
            if (schema != null) return schema;

            var schemaBuilder = new SchemaBuilder(_schemaId);
            schemaBuilder.SetSchemaName("SteelProperties");

            schemaBuilder.SetReadAccessLevel(AccessLevel.Public);
            schemaBuilder.SetWriteAccessLevel(AccessLevel.Vendor);
            schemaBuilder.SetVendorId("OrslavTeam");

            schemaBuilder.AddSimpleField(SteelGroupField, typeof(string));
            schemaBuilder.AddSimpleField(SteelSpreadHeadersFields, typeof(string));


            return schemaBuilder.Finish();
        }

        private DataStorage GetDataStorage(bool createIfNotExists = false)
        {
            IEnumerable<DataStorage> dataStorage = new FilteredElementCollector(_doc)
                .OfClass(typeof(DataStorage))
                .OfType<DataStorage>();

            foreach (DataStorage storage in dataStorage)
            {
                Entity entity = storage.GetEntity(GetSchema());
                if (entity != null && entity.IsValid()) return storage;
            }

            if (!createIfNotExists) return null;

            DataStorage newDataStorage = DataStorage.Create(_doc);
            return newDataStorage;
        }

        private void WriteSteelProperties()
        {
            using (var tr = new Transaction(_doc, "CreateDataStorage"))
            {
                tr.Start();
                try
                {
                    DataStorage dataStorage = GetDataStorage(true);
                    var entity = new Entity(GetSchema());

                    entity.Set(SteelGroupField, JsonConvert.SerializeObject(_steelGroups, Formatting.Indented));
                    entity.Set(SteelGroupField, JsonConvert.SerializeObject(_steelSpreadCollection, Formatting.Indented));

                    dataStorage.SetEntity(entity);
                }
                catch (Exception e)
                {
                    MessageBox.Show($"Message:\n{e.Message}\n\n\nSource:\n{e.Source}");
                }
                

                tr.Commit();
            }
        }

        private void ReadSteelProperties()
        {
            DataStorage dataStorage = GetDataStorage();

            Entity entity = dataStorage?.GetEntity(GetSchema());
            if (entity == null) return;

            _steelGroups =
                JsonConvert.DeserializeObject<ObservableCollection<SteelGroupVm>>(
                    entity.Get<string>(SteelGroupField));

            _steelSpreadCollection =
                JsonConvert.DeserializeObject<ObservableCollection<UsingTypeVm>>(
                    entity.Get<string>(SteelGroupField));
        }

        public void WriteSetting(ObservableCollection<SteelGroupVm> steelGroups,
            ObservableCollection<UsingTypeVm> steelSpreadCollection)
        {
            _steelGroups = steelGroups;
            _steelSpreadCollection = steelSpreadCollection;

            WriteSteelProperties();
        }

        public void ReadSetting(out ObservableCollection<SteelGroupVm> steelGroups,
            out ObservableCollection<UsingTypeVm> steelSpreadCollection)
        {
            ReadSteelProperties();
            steelGroups = _steelGroups;
            steelSpreadCollection = _steelSpreadCollection;
        }
    }
}