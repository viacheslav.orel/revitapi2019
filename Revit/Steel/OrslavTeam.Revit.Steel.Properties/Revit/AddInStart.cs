﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using OrslavTeam.Revit.Steel.Properties.ViewModel;

namespace OrslavTeam.Revit.Steel.Properties.Revit
{
    [Transaction(TransactionMode.Manual)]
    public class AddInStart : IExternalCommand
    {
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            Document doc = commandData.Application.ActiveUIDocument.Document;

            Element generalUsingTypeSchedule = new FilteredElementCollector(doc)
                .OfCategory(BuiltInCategory.OST_Schedules)
                .FirstOrDefault(schedule => schedule.Name == "GUT_structuralFraming");

            var generalUsingType = new ObservableCollection<UsingTypeVm>();
            if (generalUsingTypeSchedule != null)
            {
                IList<Element> generalUsingTypeCollection = new FilteredElementCollector(doc, generalUsingTypeSchedule.Id)
                    .ToElements();
                foreach (Element element in generalUsingTypeCollection)
                {
                    IList<Parameter> paramList = element.GetOrderedParameters();
                    generalUsingType.Add(new UsingTypeVm
                    {
                        Id = (int)paramList[1].AsDouble(),
                        Name = paramList[0].AsString()
                    });
                }
            }
            
            Element subUsingTypeSchedule = new FilteredElementCollector(doc)
                .OfCategory(BuiltInCategory.OST_Schedules)
                .FirstOrDefault(schedule => schedule.Name == "SUT_structuralFraming");
            
            var subUsingType = new ObservableCollection<UsingTypeVm>();
            if (subUsingTypeSchedule != null)
            {
                IList<Element> subUsingTypeCollection = new FilteredElementCollector(doc, subUsingTypeSchedule.Id)
                    .ToElements();
                foreach (Element element in subUsingTypeCollection)
                {
                    IList<Parameter> paramList = element.GetOrderedParameters();
                    subUsingType.Add(new UsingTypeVm
                    {
                        Id = (int)paramList[1].AsDouble(),
                        Name = paramList[0].AsString()
                    });
                }
            }

            var schemaData = new SchemaData(doc);
            
            schemaData.ReadSetting(out ObservableCollection<SteelGroupVm> steelGroups,
                out ObservableCollection<UsingTypeVm> steelSpreadCollection);

            var viewModel = new SteelPropertiesViewModel
            {
                GeneralUsingTypeCollection = generalUsingType,
                SubUsingTypeCollection = subUsingType,
                SteelGroup = steelGroups,
                SteelSpreadHeaderCollection = steelSpreadCollection
            };
            
            ExternalHandler.Handler = new ExternalHandler(viewModel);
            ExternalHandler.ExEvent = ExternalEvent.Create(ExternalHandler.Handler);
            ExternalHandler.ModifySchema = schemaData;
            ExternalHandler.ShowDialog();
            
            return Result.Succeeded;
        }
    }
}