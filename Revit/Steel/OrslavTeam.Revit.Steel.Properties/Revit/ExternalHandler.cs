﻿using Autodesk.Revit.UI;
using OrslavTeam.Revit.Steel.Properties.View;
using OrslavTeam.Revit.Steel.Properties.ViewModel;

namespace OrslavTeam.Revit.Steel.Properties.Revit
{
    public class ExternalHandler : IExternalEventHandler
    {
        #region Fields

        public static ExternalHandler Handler;
        public static ExternalEvent ExEvent;
        
        public delegate void RevitCommandDelegate(UIApplication uiApp);
        public static RevitCommandDelegate RevitCommand { get; set; }

        public static SchemaData ModifySchema;

        private static SteelPropertiesView _steelPropertiesView;
        private static SteelPropertiesViewModel _vieModel;

        #endregion

        public ExternalHandler(SteelPropertiesViewModel viewModel)
        {
            _vieModel = viewModel;
        }

        public static void ShowDialog()
        {
            _steelPropertiesView = new SteelPropertiesView(_vieModel);
            _steelPropertiesView.Show();
        }
        
        public void Execute(UIApplication app)
        {
            RevitCommand(app);
            ExEvent.Dispose();
            ExEvent = null;
            Handler = null;
            
            _steelPropertiesView.Close();
            _steelPropertiesView = null;
            _vieModel = null;
        }

        public string GetName()
        {
            return nameof(ExternalHandler);
        }
    }
}