﻿using System.Windows;
using System.Windows.Controls;
using OrslavTeam.Revit.Steel.Properties.ViewModel;

namespace OrslavTeam.Revit.Steel.Properties.View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class SteelPropertiesView
    {
        private readonly SteelPropertiesViewModel _viewModel;
        public SteelPropertiesView()
        {
            InitializeComponent();
            _viewModel = (SteelPropertiesViewModel) DataContext;
        }

        public SteelPropertiesView(SteelPropertiesViewModel viewModel)
        {
            InitializeComponent();
            _viewModel = viewModel;
            DataContext = viewModel;
        }

        private void SteelGroupRemove_ButtonClick(object sender, RoutedEventArgs e)
        {
            _viewModel.SteelGroupRemoveCommand.Execute(((Button) sender).DataContext);
        }

        private void SteelSpreadHeaderRemove_ButtonClick(object sender, RoutedEventArgs e)
        {
            _viewModel.SteelSpreadRemoveCommand.Execute(((Button) sender).DataContext);
        }
    }
}