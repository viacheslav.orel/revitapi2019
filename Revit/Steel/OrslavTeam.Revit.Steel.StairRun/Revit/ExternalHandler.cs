﻿using Autodesk.Revit.UI;
using OrslavTeam.Revit.Steel.Axillary.Repository.ComponentComplex;
using OrslavTeam.Revit.Steel.Axillary.Repository.ComponentComplex.StairRun;
using OrslavTeam.Revit.Steel.StairRun.View;

namespace OrslavTeam.Revit.Steel.StairRun.Revit
{
    public class ExternalHandler : IExternalEventHandler
    {
        #region Fields

        public static ExternalHandler Handler;
        public static ExternalEvent ExEvent;

        private static MainWindow _stairRunParameterView;

        public static StairRunComponent StairRunComponent;

        public delegate void RevitCommandDelegate(UIApplication uiApp);

        public static RevitCommandDelegate RevitCommand { get; set; }

        #endregion

        public static void ShowDialog()
        {
            _stairRunParameterView = new MainWindow();
            _stairRunParameterView.Show();
        }

        #region Implementation of IExternalEventHandler

        public void Execute(UIApplication app)
        {
            RevitCommand(app);
            ExEvent.Dispose();
            ExEvent = null;
            Handler = null;

            _stairRunParameterView.Close();
            _stairRunParameterView = null;
        }

        public string GetName()
        {
            return nameof(ExternalHandler);
        }

        #endregion
    }
}