﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using Autodesk.Revit.UI.Selection;
using Newtonsoft.Json;
using OrslavTeam.Revit.Axillary.PublicInformation;
using OrslavTeam.Revit.Steel.Axillary.Repository.ComponentComplex;
using OrslavTeam.Revit.Steel.Axillary.Repository.ComponentComplex.StairRun;
using OrslavTeam.Revit.Steel.Axillary.SelectionFilters;

namespace OrslavTeam.Revit.Steel.StairRun.Revit
{
    [Transaction(TransactionMode.Manual)]
    public class AddInStart : IExternalCommand
    {
        #region Implementation of IExternalCommand

        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            UIApplication uiApp = commandData.Application;
            Document doc = uiApp.ActiveUIDocument.Document;
            Selection sel = uiApp.ActiveUIDocument.Selection;

            var isStairRun = new IsStairRun();
            ICollection<ElementId> stairRunCollection = sel.GetElementIds();

            Element selectedSymbol;

            if (stairRunCollection.Count > 0)
            {
                Element selectedElement = doc.GetElement(stairRunCollection.FirstOrDefault());
                selectedSymbol = (selectedElement as FamilyInstance)?.Symbol ?? selectedElement;

                if (!isStairRun.AllowElement(selectedElement))
                    return Result.Failed;
            }
            else
            {
                Reference pickedStairRun;
                do
                {
                    try
                    {
                        pickedStairRun = sel.PickObject(ObjectType.Element, isStairRun, "Оберіть сходовий марш");
                    }
                    catch (Exception )
                    {
                        return Result.Cancelled;
                    }
                } while (pickedStairRun == null);

                selectedSymbol = ((FamilyInstance)doc.GetElement(pickedStairRun)).Symbol;
            }

            string getIt = PublicParameter.GetIt(selectedSymbol);
            if (getIt != "")
            {
                try
                {
                    ExternalHandler.StairRunComponent = JsonConvert.DeserializeObject<StairRunComponent>(getIt);
                }
                catch (Exception)
                {
                    //
                }
            }

            if (ExternalHandler.StairRunComponent == null)
                ExternalHandler.StairRunComponent = new StairRunComponent(true);
            ExternalHandler.StairRunComponent.RvtFamily = selectedSymbol;

            ExternalHandler.Handler = new ExternalHandler();
            ExternalHandler.ExEvent = ExternalEvent.Create(ExternalHandler.Handler);
            ExternalHandler.ShowDialog();

            return Result.Succeeded;
        }

        #endregion
    }
}