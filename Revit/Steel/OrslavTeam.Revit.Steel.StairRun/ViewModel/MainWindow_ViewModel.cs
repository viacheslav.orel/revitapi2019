﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Autodesk.Revit.DB;
using OrslavTeam.Revit.Axillary.ViewTemplate.Infrastructure;
using OrslavTeam.Revit.Steel.Axillary.Repository.ComponentSimple;
using OrslavTeam.Revit.Steel.Axillary.Repository.Primitive;
using OrslavTeam.Revit.Steel.StairRun.Revit;
using OrslavTeam.Revit.Steel.StairRun.UserControls.BottomSection;
using OrslavTeam.Revit.Steel.StairRun.UserControls.StepsSection;
using OrslavTeam.Revit.Steel.StairRun.UserControls.TopSection;
using HorizontalControl = OrslavTeam.Revit.Steel.StairRun.UserControls.TopSection.HorizontalControl;
using SectionType = OrslavTeam.Revit.Steel.Axillary.Repository.ComponentComplex.StairRun.SectionType;
using EH = OrslavTeam.Revit.Steel.StairRun.Revit.ExternalHandler;

namespace OrslavTeam.Revit.Steel.StairRun.ViewModel
{
    // ReSharper disable once InconsistentNaming
    public class MainWindow_ViewModel : ObservableObject
    {
        #region Fields

        //Stair--Section--Top
        private string _sectionTopName;
        private SimpleComponent _sectionTopCurrentComponent;
        private FrameworkElement _sectionTopControl;

        //Stair--Section--Bottom
        private string _sectionBottomName;
        private SimpleComponent _sectionBottomCurrentComponent;
        private FrameworkElement _sectionBottomControl;

        //Stair--Section--Steps
        private string _sectionStepsName;
        private SimpleComponent _sectionStepsCurrentComponent;

        private FrameworkElement _sectionStepsControl;
        private FrameworkElement _sectionStepsComplexComponent;

        //Railing
        private string _railingName;
        private SimpleComponent _railingCurrentComponent;

        #endregion

        #region FieldsAcces

        /* -------------------------------------------------------------------------------------------------------------
         * Stair
         * -----------------------------------------------------------------------------------------------------------*/
        
        // Base
        // -------------------------------------------------------------------------------------------------------------
        
        public int RunAngle
        {
            get => EH.StairRunComponent.Angle;
            set
            {
                EH.StairRunComponent.Angle = value;
                RaisePropertyChangedEvent(nameof(RunAngle));
            }
        }

        public int StairWidth
        {
            get => EH.StairRunComponent.Width;
            set
            {
                EH.StairRunComponent.Width = value;
                RaisePropertyChangedEvent(nameof(StairWidth));
            }
        }

        public string StairRunSupportName =>
            $"{EH.StairRunComponent.GetRunSupportComponent.Element.Name}\n" +
            $"({EH.StairRunComponent.GetRunSupportComponent.Material.Name})";

        // Step
        // -------------------------------------------------------------------------------------------------------------
        public int StepsStep
        {
            get => EH.StairRunComponent.StepsStep;
            set
            {
                EH.StairRunComponent.StepsStep = value;
                RaisePropertyChangedEvent(nameof(StepsStep));
            }
        }

        public bool StepsSimple
        {
            get => EH.StairRunComponent.StepsSectionType == SectionType.StepSimple;
            set
            {
                if (value)
                {
                    EH.StairRunComponent.StepsSectionType = SectionType.StepSimple;
                    SectionStepsControl = new Simple_General_Control();
                }
                RaisePropertyChangedEvent(nameof(StepsSimple));
            }
        }

        public bool StepsComplex
        {
            get => EH.StairRunComponent.StepsSectionType == SectionType.StepComplexFirst
                   || EH.StairRunComponent.StepsSectionType == SectionType.StepComplexSecond
                   || EH.StairRunComponent.StepsSectionType == SectionType.StepComplexThird
                   || EH.StairRunComponent.StepsSectionType == SectionType.StepComplexFourth;
            set
            {
                if (value)
                {
                    EH.StairRunComponent.StepsSectionType = SectionType.StepComplexFirst;
                    SectionStepsControl = new Complex_General();
                }
                RaisePropertyChangedEvent(nameof(StepsComplex));
            }
        }

        public bool StepsSingle
        {
            get => EH.StairRunComponent.StepsSectionType == SectionType.StepPurchase;
            set
            {
                if (value)
                {
                    EH.StairRunComponent.StepsSectionType = SectionType.StepPurchase;
                    SectionStepsControl = new Single_General_Control();
                }
                RaisePropertyChangedEvent(nameof(StepsSingle));
            }
        }

        public int SectionStepsDepth
        {
            get => EH.StairRunComponent.GetStepComponent.Depth;
            set
            {
                EH.StairRunComponent.GetStepComponent.Depth = value;
                RaisePropertyChangedEvent(nameof(SectionStepsDepth));
            }
        }

        public int SectionStepsHeightFirst
        {
            get => EH.StairRunComponent.GetStepComponent.HeightFirst;
            set
            {
                EH.StairRunComponent.GetStepComponent.HeightFirst = value;
                RaisePropertyChangedEvent(nameof(SectionStepsHeightFirst));
            }
        }

        public int SectionStepsHeightSecond
        {
            get => EH.StairRunComponent.GetStepComponent.HeightSecond;
            set
            {
                EH.StairRunComponent.GetStepComponent.HeightSecond = value;
                RaisePropertyChangedEvent(nameof(SectionStepsHeightSecond));
            }
        }

        public string SectionStepBaseName =>
            $"{EH.StairRunComponent.GetStepBaseComponent.Element.Name}\n" +
            $"({EH.StairRunComponent.GetStepBaseComponent.Material.Name})";

        public bool SectionStepsSupportNeed
        {
            get => EH.StairRunComponent.StepsComponent.SupportNeed;
            set
            {
                EH.StairRunComponent.GetStepComponent.SupportNeed = value;
                RaisePropertyChangedEvent(nameof(SectionStepsSupportNeed));
            }
        }

        public string SectionStepsSupportName =>
            $"{EH.StairRunComponent.GetStepSupportComponent.Element.Name}\n" +
            $"({EH.StairRunComponent.GetStepSupportComponent.Material.Name})";

        public bool SectionStepsAxillaryNeed
        {
            get => EH.StairRunComponent.GetStepComponent.AxillaryNeed;
            set
            {
                EH.StairRunComponent.GetStepComponent.AxillaryNeed = value;
                RaisePropertyChangedEvent(nameof(SectionStepsAxillaryNeed));
            }
        }

        public string SectionStepsAxillaryName =>
            $"{EH.StairRunComponent.GetStepAxillaryComponent.Element.Name}\n" +
            $"({EH.StairRunComponent.GetStepAxillaryComponent.Material.Name})";

       
        public bool ComplexFirstType
        {
            get => EH.StairRunComponent.StepsSectionType == SectionType.StepComplexFirst;
            set
            {
                if (value)
                {
                    EH.StairRunComponent.StepsSectionType = SectionType.StepComplexFirst;
                    SectionStepsComplexComponent = new Complex_V1_Control();
                }
                RaisePropertyChangedEvent(nameof(ComplexFirstType));
            }
        }

        public bool ComplexSecondType
        {
            get => EH.StairRunComponent.StepsSectionType == SectionType.StepComplexSecond;
            set
            {
                if (value)
                {
                    EH.StairRunComponent.StepsSectionType = SectionType.StepComplexSecond;
                    SectionStepsComplexComponent = new Complex_V2_Control();
                }
                RaisePropertyChangedEvent(nameof(ComplexSecondType));
            }
        }

        public bool ComplexThirdType
        {
            get => EH.StairRunComponent.StepsSectionType == SectionType.StepComplexThird;
            set
            {
                if (value)
                {
                    EH.StairRunComponent.StepsSectionType = SectionType.StepComplexThird;
                    SectionStepsComplexComponent = new Complex_V3_Control();
                }
                RaisePropertyChangedEvent(nameof(ComplexThirdType));
            }
        }

        public bool ComplexForthType
        {
            get => EH.StairRunComponent.StepsSectionType == SectionType.StepComplexFourth;
            set
            {
                if (value)
                {
                    EH.StairRunComponent.StepsSectionType = SectionType.StepComplexFourth;
                    SectionStepsComplexComponent = new Complex_V4_Control();
                }
                RaisePropertyChangedEvent(nameof(ComplexForthType));
            }
        }

        public FrameworkElement SectionStepsControl
        {
            get => _sectionStepsControl;
            set
            {
                _sectionStepsControl = value;
                RaisePropertyChangedEvent(nameof(SectionStepsControl));
            }
        }

        public FrameworkElement SectionStepsComplexComponent
        {
            get => _sectionStepsComplexComponent;
            set
            {
                _sectionStepsComplexComponent = value;
                RaisePropertyChangedEvent(nameof(SectionStepsComplexComponent));
            }
        }

        // Top section
        // -------------------------------------------------------------------------------------------------------------
        // Support
        
        public bool TopSupportAngle
        {
            get => EH.StairRunComponent.TopSectionType == SectionType.TopSupport;
            set
            {
                if (value)
                {
                    EH.StairRunComponent.TopSectionType = SectionType.TopSupport;
                    SectionTopControl = new AngleControl();
                }
                RaisePropertyChangedEvent(nameof(TopSupportAngle));
            }
        }

        public bool TopSupportHorizontal
        {
            get => EH.StairRunComponent.TopSectionType == SectionType.TopLanding;
            set
            {
                if (value)
                {
                    EH.StairRunComponent.TopSectionType = SectionType.TopLanding;
                    SectionTopControl = new HorizontalControl();
                }
                RaisePropertyChangedEvent(nameof(TopSupportHorizontal));
            }
        }

        public FrameworkElement SectionTopControl
        {
            get => _sectionTopControl;
            set
            {
                _sectionTopControl = value;
                RaisePropertyChangedEvent(nameof(SectionTopControl));
            }
        }

        public bool SectionTopSupportNeed
        {
            get => EH.StairRunComponent.SupportTopNeed;
            set
            {
                EH.StairRunComponent.SupportTopNeed = value;
                RaisePropertyChangedEvent(nameof(SectionTopSupportNeed));
            }
        }

        public string TopSupportProfileName =>
            $"{EH.StairRunComponent.GetSupportTopComponent.Element.Name}\n" +
            $"{EH.StairRunComponent.GetSupportTopComponent.Material.Name}";

        // -------------------------------------------------------------------------------------------------------------
        // Landing
        public int SectionTopLandingBaseBinding
        {
            get => EH.StairRunComponent.LandingTopBaseBindingHorizontal;
            set
            {
                EH.StairRunComponent.LandingTopBaseBindingHorizontal = value;
                RaisePropertyChangedEvent(nameof(SectionTopLandingBaseBinding));
            }
        }

        public int SectionTopLandingBaseVerticalBinding
        {
            get => EH.StairRunComponent.LandingTopBaseBindingVertical;
            set
            {
                EH.StairRunComponent.LandingTopBaseBindingVertical = value;
                RaisePropertyChangedEvent(nameof(SectionTopLandingBaseVerticalBinding));
            }
        }

        public string SectionTopLandingBaseName =>
            $"{EH.StairRunComponent.GetTopLandingBaseComponent.Element.Name}\n" +
            $"({EH.StairRunComponent.GetTopLandingBaseComponent.Material.Name})";

        public bool SectionTopLandingSupportNeed
        {
            get => EH.StairRunComponent.LandingTopSupportNeed;
            set
            {
                EH.StairRunComponent.LandingTopSupportNeed = value;
                RaisePropertyChangedEvent(nameof(SectionTopLandingSupportNeed));
            }
        }

        public string SectionTopLandingSupportName =>
            $"{EH.StairRunComponent.GetTopLandingSupportComponent.Element.Name}\n" +
            $"({EH.StairRunComponent.GetTopLandingSupportComponent.Material.Name})";

        public bool SectionTopLandingAxillaryFrontNeed
        {
            get => EH.StairRunComponent.LandingTopAxillaryFrontNeed;
            set
            {
                EH.StairRunComponent.LandingTopAxillaryFrontNeed = value;
                if (!value) SectionTopLandingAxillaryFrontBinding = 0;

                RaisePropertyChangedEvent(nameof(SectionTopLandingAxillaryFrontNeed));
            }
        }

        public int SectionTopLandingAxillaryFrontBinding
        {
            get => EH.StairRunComponent.LandingTopBindingAxillaryFront;
            set
            {
                EH.StairRunComponent.LandingTopBindingAxillaryFront = value;
                RaisePropertyChangedEvent(nameof(SectionTopLandingAxillaryFrontBinding));
            }
        }

        public string SectionTopLandingAxillaryFrontName =>
            $"{EH.StairRunComponent.GetTopLandingAxillaryFrontComponent.Element.Name}\n" +
            $"({EH.StairRunComponent.GetTopLandingAxillaryFrontComponent.Material.Name})";

        public bool SectionTopLandingAxillaryBackNeed
        {
            get => EH.StairRunComponent.LandingTopAxillaryBackNeed;
            set
            {
                EH.StairRunComponent.LandingTopAxillaryBackNeed = value;
                RaisePropertyChangedEvent(nameof(SectionTopLandingAxillaryBackNeed));
            }
        }

        public int SectionTopLandingAxillaryBackBinding
        {
            get => EH.StairRunComponent.LandingTopBindingAxillaryBack;
            set
            {
                EH.StairRunComponent.LandingTopBindingAxillaryBack = value;
                RaisePropertyChangedEvent(nameof(SectionTopLandingAxillaryBackBinding));
            }
        }

        public string SectionTopLandingAxillaryBackName =>
            $"{EH.StairRunComponent.GetTopLandingAxillaryBackComponent.Element.Name}\n" +
            $"({EH.StairRunComponent.GetTopLandingAxillaryBackComponent.Material.Name})";

        // Bottom section
        // -------------------------------------------------------------------------------------------------------------
        //Base
        
        public bool BottomSupportAngleByPlate
        {
            get => EH.StairRunComponent.BottomSectionType == SectionType.BottomSupportPlate;
            set
            {
                if (value)
                {
                    EH.StairRunComponent.BottomSectionType = SectionType.BottomSupportPlate;
                    SectionBottomControl = new AngleByPlateControl();
                }
                RaisePropertyChangedEvent(nameof(BottomSupportAngleByPlate));
            }
        }

        public bool BottomSupportVerticalByPlate
        {
            get => EH.StairRunComponent.BottomSectionType == SectionType.BottomSupportVertical;
            set
            {
                if (value)
                {
                    EH.StairRunComponent.BottomSectionType = SectionType.BottomSupportVertical;
                    SectionBottomControl = new VerticalControl();
                }
                RaisePropertyChangedEvent(nameof(BottomSupportVerticalByPlate));
            }
        }

        public bool BottomSupportAngle
        {
            get => EH.StairRunComponent.BottomSectionType == SectionType.BottomSupportSlice;
            set
            {
                if (value)
                {
                    EH.StairRunComponent.BottomSectionType = SectionType.BottomSupportSlice;
                    SectionBottomControl = new SliceComponent();
                }
                RaisePropertyChangedEvent(nameof(BottomSupportAngle));
            }
        }

        public bool BottomSupportHorizontal
        {
            get => EH.StairRunComponent.BottomSectionType == SectionType.BottomLanding;
            set
            {
                if (value)
                {
                    EH.StairRunComponent.BottomSectionType = SectionType.BottomLanding;
                    SectionBottomControl = new UserControls.BottomSection.HorizontalControl();
                }
                RaisePropertyChangedEvent(nameof(BottomSupportHorizontal));
            }
        }

        public FrameworkElement SectionBottomControl
        {
            get => _sectionBottomControl;
            set
            {
                _sectionBottomControl = value;
                RaisePropertyChangedEvent(nameof(SectionBottomControl));
            }
        }
        
        public bool SectionBottomSupportNeed
        {
            get => EH.StairRunComponent.SupportBottomNeed;
            set
            {
                EH.StairRunComponent.SupportBottomNeed = value;
                RaisePropertyChangedEvent(nameof(SectionBottomSupportNeed));
            }
        }

        public bool SectionBottomSupportAnchorNeed
        {
            get => EH.StairRunComponent.SupportBottomAnchorNeed;
            set
            {
                EH.StairRunComponent.SupportBottomAnchorNeed = value;
                RaisePropertyChangedEvent(nameof(SectionBottomSupportAnchorNeed));
            }
        }

        public int SectionBottomSupportAnchorDepthBinding
        {
            get => EH.StairRunComponent.SupportBottomAnchorBindingFromFront;
            set
            {
                EH.StairRunComponent.SupportBottomAnchorBindingFromFront = value;
                RaisePropertyChangedEvent(nameof(SectionBottomSupportAnchorDepthBinding));
            }
        }

        public int SectionBottomSupportAnchorWidthBinding
        {
            get => EH.StairRunComponent.SupportBottomAnchorBindingFromInner;
            set
            {
                EH.StairRunComponent.SupportBottomAnchorBindingFromInner = value;
                RaisePropertyChangedEvent(nameof(SectionBottomSupportAnchorWidthBinding));
            }
        }

        public string SectionBottomSupportName =>
            $"{EH.StairRunComponent.GetSupportBottomComponent.Element.Name}\n" +
            $"({EH.StairRunComponent.GetSupportBottomComponent.Material.Name})";

        // -------------------------------------------------------------------------------------------------------------
        // Landing
        
        public int SectionBottomLandingBaseBindingHorizontal
        {
            get => EH.StairRunComponent.LandingBottomBaseBindingHorizontal;
            set
            {
                EH.StairRunComponent.LandingBottomBaseBindingHorizontal = value;
                RaisePropertyChangedEvent(nameof(SectionBottomLandingBaseBindingHorizontal));
            }
        }

        public int SectionBottomLandingBaseBindingVertical
        {
            get => EH.StairRunComponent.LandingBottomBaseBindingVertical;
            set
            {
                EH.StairRunComponent.LandingBottomBaseBindingVertical = value;
                RaisePropertyChangedEvent(nameof(SectionBottomLandingBaseBindingVertical));
            }
        }

        public string SectionBottomLandingBaseName =>
            $"{EH.StairRunComponent.GetBottomLandingBaseComponent.Element.Name}\n" +
            $"({EH.StairRunComponent.GetBottomLandingBaseComponent.Material.Name})";

        public bool SectionBottomLandingSupportNeed
        {
            get => EH.StairRunComponent.LandingBottomSupportNeed;
            set
            {
                EH.StairRunComponent.LandingBottomSupportNeed = value;
                RaisePropertyChangedEvent(nameof(SectionBottomLandingSupportNeed));
            }
        }

        public string SectionBottomLandingSupportName =>
            $"{EH.StairRunComponent.GetBottomLandingSupportComponent.Element.Name}\n" +
            $"({EH.StairRunComponent.GetBottomLandingSupportComponent.Material.Name})";

        public bool SectionBottomLandingAxillaryFrontNeed
        {
            get => EH.StairRunComponent.LandingBottomAxillaryFrontNeed;
            set
            {
                EH.StairRunComponent.LandingBottomAxillaryFrontNeed = value;
                RaisePropertyChangedEvent(nameof(SectionBottomLandingAxillaryFrontNeed));
            }
        }

        public int SectionBottomLandingAxillaryFrontBinding
        {
            get => EH.StairRunComponent.LandingBottomBindingAxillaryFront;
            set
            {
                EH.StairRunComponent.LandingBottomBindingAxillaryFront = value;
                RaisePropertyChangedEvent(nameof(SectionBottomLandingAxillaryFrontBinding));
            }
        }

        public string SectionBottomLandingAxillaryFrontName =>
            $"{EH.StairRunComponent.GetBottomLandingAxillaryFrontComponent.Element.Name}\n" +
            $"({EH.StairRunComponent.GetBottomLandingAxillaryFrontComponent.Material.Name})";

        public bool SectionBottomLandingAxillaryBackNeed
        {
            get => EH.StairRunComponent.LandingBottomAxillaryBackNeed;
            set
            {
                EH.StairRunComponent.LandingBottomAxillaryBackNeed = value;
                RaisePropertyChangedEvent(nameof(SectionBottomLandingAxillaryBackNeed));
            }
        }

        public string SectionBottomLandingAxillaryBackName =>
            $"{EH.StairRunComponent.GetBottomLandingAxillaryBackComponent.Element.Name}\n" +
            $"({EH.StairRunComponent.GetBottomLandingAxillaryBackComponent.Material.Name})";

        /* -------------------------------------------------------------------------------------------------------------
         * Railing
         * -----------------------------------------------------------------------------------------------------------*/
        
        public bool RailingLandingTopFrontNeed
        {
            get => EH.StairRunComponent.RailingTopNeed;
            set
            {
                EH.StairRunComponent.RailingTopNeed = value;
                RaisePropertyChangedEvent(nameof(RailingLandingTopFrontNeed));
            }
        }

        public bool RailingLandingTopLeftNeed
        {
            get => EH.StairRunComponent.RailingLeftTopNeed;
            set
            {
                EH.StairRunComponent.RailingLeftTopNeed = value;
                RaisePropertyChangedEvent(nameof(RailingLandingTopLeftNeed));
            }
        }

        public bool RailingLandingTopRightNeed
        {
            get => EH.StairRunComponent.RailingRightTopNeed;
            set
            {
                EH.StairRunComponent.RailingRightTopNeed = value;
                RaisePropertyChangedEvent(nameof(RailingLandingTopRightNeed));
            }
        }

        public bool RailingRunLeftNeed
        {
            get => EH.StairRunComponent.RailingLeftNeed;
            set
            {
                EH.StairRunComponent.RailingLeftNeed = value;
                RaisePropertyChangedEvent(nameof(RailingRunLeftNeed));
                RaisePropertyChangedEvent(nameof(RailingRunNeed));
                RaisePropertyChangedEvent(nameof(RailingBeginVariantNeed));
            }
        }

        public bool RailingRunRightNeed
        {
            get => EH.StairRunComponent.RailingRightRunNeed;
            set
            {
                EH.StairRunComponent.RailingRightRunNeed = value;
                RaisePropertyChangedEvent(nameof(RailingRunRightNeed));
                RaisePropertyChangedEvent(nameof(RailingRunNeed));
                RaisePropertyChangedEvent(nameof(RailingBeginVariantNeed));
            }
        }

        public bool RailingRunNeed => RailingRunLeftNeed
                                      || RailingRunRightNeed;

        public bool RailingLandingBottomLeftNeed
        {
            get => EH.StairRunComponent.RailingLeftBottomNeed;
            set
            {
                EH.StairRunComponent.RailingLeftBottomNeed = value;
                RaisePropertyChangedEvent(nameof(RailingLandingBottomLeftNeed));
                RaisePropertyChangedEvent(nameof(RailingBeginVariantNeed));
            }
        }

        public bool RailingLandingBottomRightNeed
        {
            get => EH.StairRunComponent.RailingRightBottomNeed;
            set
            {
                EH.StairRunComponent.RailingRightBottomNeed = value;
                RaisePropertyChangedEvent(nameof(RailingLandingBottomRightNeed));
                RaisePropertyChangedEvent(nameof(RailingBeginVariantNeed));
            }
        }

        public bool RailingLandingBottomFrontNeed
        {
            get => EH.StairRunComponent.RailingBottomNeed;
            set
            {
                EH.StairRunComponent.RailingBottomNeed = value;
                RaisePropertyChangedEvent(nameof(RailingLandingBottomFrontNeed));
            }
        }

        public bool RailingBeginVariantNeed => RailingRunNeed
                                               && (!RailingLandingBottomLeftNeed
                                                   || !RailingLandingBottomRightNeed);

        public bool RailingBalusterVertical
        {
            get => !EH.StairRunComponent.RailingRunBalusterInclined;
            set
            {
                EH.StairRunComponent.RailingRunBalusterInclined = !value;
                RaisePropertyChangedEvent(nameof(RailingBalusterVertical));
                RaisePropertyChangedEvent(nameof(RailingBalusterInclined));
            }
        }

        public bool RailingBalusterInclined => !RailingBalusterVertical;

        public bool RailingIsFullLength
        {
            get => EH.StairRunComponent.RailingRunFullLength;
            set
            {
                EH.StairRunComponent.RailingRunFullLength = value;
                RaisePropertyChangedEvent(nameof(RailingIsFullLength));
                RaisePropertyChangedEvent(nameof(RailingIsNotFullLength));
            }
        }

        public bool RailingIsNotFullLength => !RailingIsFullLength;

        public int RailingGeometryHeight
        {
            get => EH.StairRunComponent.RailingHandRailBinding;
            set
            {
                EH.StairRunComponent.RailingHandRailBinding = value;
                RaisePropertyChangedEvent(nameof(RailingGeometryHeight));
            }
        }

        public int RailingRailBinding
        {
            get => EH.StairRunComponent.RailingRailBinding;
            set
            {
                EH.StairRunComponent.RailingRailBinding = value;
                RaisePropertyChangedEvent(nameof(RailingRailBinding));
            }
        }

        public int RailingAxillaryBinding
        {
            get => EH.StairRunComponent.RailingAxillaryBinding;
            set
            {
                EH.StairRunComponent.RailingAxillaryBinding = value;
                RaisePropertyChangedEvent(nameof(RailingAxillaryBinding));
            }
        }

        public bool RailingComponentBalusterNeed
        {
            get => EH.StairRunComponent.RailingBalusterNeed;
            set
            {
                EH.StairRunComponent.RailingBalusterNeed = value;
                RaisePropertyChangedEvent(nameof(RailingComponentBalusterNeed));
            }
        }

        public bool RailingComponentRailNeed
        {
            get => EH.StairRunComponent.RailingRailNeed;
            set
            {
                EH.StairRunComponent.RailingRailNeed = value;
                RaisePropertyChangedEvent(nameof(RailingComponentRailNeed));
            }
        }

        public bool RailingComponentAxillaryNeed
        {
            get => EH.StairRunComponent.RailingAxillaryNeed;
            set
            {
                EH.StairRunComponent.RailingAxillaryNeed = value;
                RaisePropertyChangedEvent(nameof(RailingComponentAxillaryNeed));
            }
        }

        public string RailingComponentBalusterName =>
            $"Балясина (1):\n" +
            $"{EH.StairRunComponent.GetRailingBalusterComponent.Element.Name} " +
            $"({EH.StairRunComponent.GetRailingBalusterComponent.Material.Name})";

        public string RailingComponentHandRailingName =>
            $"Поручень (2):\n" +
            $"{EH.StairRunComponent.GetRailingHandRailComponent.Element.Name} " +
            $"({EH.StairRunComponent.GetRailingHandRailComponent.Material.Name})";

        public string RailingComponentRailName =>
            $"Перило (3)\n" +
            $"{EH.StairRunComponent.GetRailingRailComponent.Element.Name} " +
            $"({EH.StairRunComponent.GetRailingRailComponent.Material.Name})";

        public string RailingComponentAxillaryName =>
            $"Відбортовочна полоса (4):\n" +
            $"{EH.StairRunComponent.GetRailingAxillaryComponent.Element.Name} " +
            $"({EH.StairRunComponent.GetRailingAxillaryComponent.Material.Name})";

        public bool RailingBalusterWidthHandmade =>
            RailingComponentBalusterNeed
            && EH.StairRunComponent.GetRailingBalusterComponent.ProductMix.WayType == TypeEnum.TwoWayComponent;

        public int RailingBalusterWidth
        {
            get => EH.StairRunComponent.GetRailingBalusterComponent.Width;
            set
            {
                if (RailingBalusterWidthHandmade)
                    EH.StairRunComponent.GetRailingBalusterComponent.Width = value;

                RaisePropertyChangedEvent(nameof(RailingBalusterWidth));
            }
        }

        public bool RailingRailHandmade =>
            RailingComponentRailNeed
            && EH.StairRunComponent.GetRailingRailComponent.ProductMix.WayType == TypeEnum.TwoWayComponent;

        public int RailingRailWidth
        {
            get => EH.StairRunComponent.GetRailingBalusterComponent.Width;
            set
            {
                if (RailingRailHandmade)
                    EH.StairRunComponent.GetRailingBalusterComponent.Width = value;

                RaisePropertyChangedEvent(nameof(RailingRailWidth));
            }
        }

        public bool RailingAxillaryHandmade =>
            RailingComponentAxillaryNeed
            && EH.StairRunComponent.GetRailingAxillaryComponent.ProductMix.WayType == TypeEnum.TwoWayComponent;

        public int RailingAxillaryWidth
        {
            get => EH.StairRunComponent.GetRailingAxillaryComponent.Width;
            set
            {
                if (RailingAxillaryHandmade)
                    EH.StairRunComponent.GetRailingAxillaryComponent.Width = value;

                RaisePropertyChangedEvent(nameof(RailingAxillaryWidth));
            }
        }

        public int RailingRunBalusterFirstBinding
        {
            get => EH.StairRunComponent.RailingRunBalusterFirstBinding;
            set
            {
                EH.StairRunComponent.RailingRunBalusterFirstBinding = value;
                RaisePropertyChangedEvent(nameof(RailingRunBalusterFirstBinding));
            }
        }

        public int RailingRunBalusterLastBinding
        {
            get => EH.StairRunComponent.RailingRunBalusterLastBinding;
            set
            {
                EH.StairRunComponent.RailingRunBalusterLastBinding = value;
                RaisePropertyChangedEvent(nameof(RailingRunBalusterLastBinding));
            }
        }

        public int RailingRunBalusterStep
        {
            get => EH.StairRunComponent.RailingRunBalusterStep;
            set
            {
                EH.StairRunComponent.RailingRunBalusterStep = value;
                RaisePropertyChangedEvent(nameof(RailingRunBalusterStep));
            }
        }

        public int RailingRunBalusterSupportBinding
        {
            get => EH.StairRunComponent.RailingRunBalusterSupportLength;
            set
            {
                EH.StairRunComponent.RailingRunBalusterSupportLength = value;
                RaisePropertyChangedEvent(nameof(RailingRunBalusterSupportBinding));
            }
        }
        
        // Landing
        // -------------------------------------------------------------------------------------------------------------

        public int RailingLandingBalusterFirstBinding
        {
            get => EH.StairRunComponent.RailingLandingEndBinding;
            set
            {
                EH.StairRunComponent.RailingLandingEndBinding = value;
                RaisePropertyChangedEvent(nameof(RailingLandingBalusterFirstBinding));
            }
        }

        public int RailingLandingBalusterStep
        {
            get => EH.StairRunComponent.RailingLandingBalusterStep;
            set
            {
                EH.StairRunComponent.RailingLandingBalusterStep = value;
                RaisePropertyChangedEvent(nameof(RailingLandingBalusterStep));
            }
        }

        public int RailingLandingBalusterSupport
        {
            get => EH.StairRunComponent.RailingLandingBalusterSupportLength;
            set
            {
                EH.StairRunComponent.RailingLandingBalusterSupportLength = value;
                RaisePropertyChangedEvent(nameof(RailingLandingBalusterSupport));
            }
        }

        #endregion

        #region Collection

        //All
        private ObservableCollection<OtMaterial> _materialCollection;

        // Stair--General
        // -------------------------------------------------------------------------------------------------------------
        
        private ObservableCollection<OtProductMix> _generalProductMixCollection;
        private OtProductMix _generalProductMixCollectionSelectedItem;

        private ObservableCollection<OtElement> _generalElementCollection;
        private OtElement _generalElementCollectionSelectedItem;

        private OtMaterial _generalMaterialCollectionSelectedItem;

        //Stair--Section--Top
        // -------------------------------------------------------------------------------------------------------------
        
        private ObservableCollection<OtProductMix> _topSectionProductMixCollection;
        private OtProductMix _topSectionProductMixCollectionSelectedItem;

        private ObservableCollection<OtElement> _topSectionElementCollection;
        private OtElement _topSectionElementCollectionSelectedItem;

        private OtMaterial _topSectionMaterialCollectionSelectedItem;

        //Stair--Section--Bottom
        // -------------------------------------------------------------------------------------------------------------

        private ObservableCollection<OtProductMix> _sectionBottomProductMixCollection;
        private OtProductMix _sectionBottomProductMixCollectionSelectedItem;

        private ObservableCollection<OtElement> _sectionBottomElementCollection;
        private OtElement _sectionBottomElementCollectionSelectedItem;

        private OtMaterial _sectionBottomMaterialCollectionSelectedItem;

        //Stair--Section--Steps
        // -------------------------------------------------------------------------------------------------------------

        private ObservableCollection<OtProductMix> _sectionStepsProductMixCollection;
        private OtProductMix _sectionStepsProductMixCollectionSelectedItem;

        private ObservableCollection<OtElement> _sectionStepsElementCollection;
        private OtElement _sectionStepsElementCollectionSelectedItem;

        private OtMaterial _sectionStepsMaterialCollectionSelectedItem;

        //Railing
        // -------------------------------------------------------------------------------------------------------------

        private ObservableCollection<OtProductMix> _railingProductMixCollection;
        private OtProductMix _railingProductMixCollectionSelectedItem;

        private ObservableCollection<OtElement> _railingElementCollection;
        private OtElement _railingElementCollectionSelectedItem;

        private OtMaterial _railingMaterialCollectionSelectedItem;

        #endregion

        #region CollectionAccess

        //All
        public ObservableCollection<OtMaterial> MaterialCollection =>
            _materialCollection ?? (_materialCollection = OtRequest.GetMaterialCollection());
        
        /* -------------------------------------------------------------------------------------------------------------
         * Stair
         * -----------------------------------------------------------------------------------------------------------*/

        //Stair--General
        // -------------------------------------------------------------------------------------------------------------
        public ObservableCollection<OtProductMix> GeneralProductMixCollection =>
            _generalProductMixCollection ??
            (_generalProductMixCollection = OtRequest.GetProductMixCollection(401, 450));

        public OtProductMix GeneralProductMixCollectionSelectedItem
        {
            get => _generalProductMixCollectionSelectedItem;
            set
            {
                _generalProductMixCollectionSelectedItem = value;
                RaisePropertyChangedEvent(nameof(GeneralProductMixCollectionSelectedItem));

                GeneralElementCollection = _generalProductMixCollectionSelectedItem != null
                    ? OtRequest.GetElementCollection(_generalProductMixCollectionSelectedItem.Id)
                    : null;

                GeneralElementCollectionSelectedItem = GeneralElementCollection != null
                    ? _generalElementCollection.FirstOrDefault(element =>
                        element.Id == EH.StairRunComponent.GetRunSupportComponent.Element.Id)
                    : null;
            }
        }

        public ObservableCollection<OtElement> GeneralElementCollection
        {
            get => _generalElementCollection;
            set
            {
                _generalElementCollection = value;
                RaisePropertyChangedEvent(nameof(GeneralElementCollection));
            }
        }

        public OtElement GeneralElementCollectionSelectedItem
        {
            get => _generalElementCollectionSelectedItem;
            set
            {
                _generalElementCollectionSelectedItem = value;
                RaisePropertyChangedEvent(nameof(GeneralElementCollectionSelectedItem));
            }
        }

        public OtMaterial GeneralMaterialCollectionSelectedItem
        {
            get => _generalMaterialCollectionSelectedItem;
            set
            {
                _generalMaterialCollectionSelectedItem = value;
                RaisePropertyChangedEvent(nameof(GeneralMaterialCollectionSelectedItem));
            }
        }

        // Stair--Section--Top
        // -------------------------------------------------------------------------------------------------------------
        public ObservableCollection<OtProductMix> TopSectionProductMixCollection
        {
            get => _topSectionProductMixCollection;
            set
            {
                _topSectionProductMixCollection = value;
                RaisePropertyChangedEvent(nameof(TopSectionProductMixCollection));
            }
        }

        public OtProductMix TopSectionProductMixCollectionSelectedItem
        {
            get => _topSectionProductMixCollectionSelectedItem;
            set
            {
                _topSectionProductMixCollectionSelectedItem = value;
                RaisePropertyChangedEvent(nameof(TopSectionProductMixCollectionSelectedItem));

                TopSectionElementCollection =
                    OtRequest.GetElementCollection(TopSectionProductMixCollectionSelectedItem.Id);
                TopSectionElementCollectionSelectedItem = TopSectionElementCollection
                    .FirstOrDefault(el => el.Id == _sectionTopCurrentComponent.Element.Id);
            }
        }

        public ObservableCollection<OtElement> TopSectionElementCollection
        {
            get => _topSectionElementCollection;
            set
            {
                _topSectionElementCollection = value;
                RaisePropertyChangedEvent(nameof(TopSectionElementCollection));
            }
        }

        public OtElement TopSectionElementCollectionSelectedItem
        {
            get => _topSectionElementCollectionSelectedItem;
            set
            {
                _topSectionElementCollectionSelectedItem = value;
                RaisePropertyChangedEvent(nameof(TopSectionElementCollectionSelectedItem));
            }
        }

        public OtMaterial TopSectionMaterialCollectionSelectedItem
        {
            get => _topSectionMaterialCollectionSelectedItem;
            set
            {
                _topSectionMaterialCollectionSelectedItem = value;
                RaisePropertyChangedEvent((nameof(TopSectionMaterialCollectionSelectedItem)));
            }
        }

        //Stair--Section--Bottom
        // -------------------------------------------------------------------------------------------------------------
        public ObservableCollection<OtProductMix> SectionBottomProductMixCollection
        {
            get => _sectionBottomProductMixCollection;
            set
            {
                _sectionBottomProductMixCollection = value;
                RaisePropertyChangedEvent(nameof(SectionBottomProductMixCollection));
            }
        }

        public OtProductMix SectionBottomProductMixCollectionSelectedItem
        {
            get => _sectionBottomProductMixCollectionSelectedItem;
            set
            {
                _sectionBottomProductMixCollectionSelectedItem = value;
                RaisePropertyChangedEvent(nameof(SectionBottomProductMixCollectionSelectedItem));

                SectionBottomElementCollection =
                    OtRequest.GetElementCollection(_sectionBottomProductMixCollectionSelectedItem.Id);
                SectionBottomElementCollectionSelectedItem = _sectionBottomElementCollection
                    .FirstOrDefault(el => el.Id == _sectionBottomCurrentComponent.Element.Id);
            }
        }

        public ObservableCollection<OtElement> SectionBottomElementCollection
        {
            get => _sectionBottomElementCollection;
            set
            {
                _sectionBottomElementCollection = value;
                RaisePropertyChangedEvent(nameof(SectionBottomElementCollection));
            }
        }

        public OtElement SectionBottomElementCollectionSelectedItem
        {
            get => _sectionBottomElementCollectionSelectedItem;
            set
            {
                _sectionBottomElementCollectionSelectedItem = value;
                RaisePropertyChangedEvent(nameof(SectionBottomElementCollectionSelectedItem));
            }
        }

        public OtMaterial SectionBottomMaterialCollectionSelectedItem
        {
            get => _sectionBottomMaterialCollectionSelectedItem;
            set
            {
                _sectionBottomMaterialCollectionSelectedItem = value;
                RaisePropertyChangedEvent(nameof(SectionBottomMaterialCollectionSelectedItem));
            }
        }

        //Stair--Section--Steps
        // -------------------------------------------------------------------------------------------------------------
        public ObservableCollection<OtProductMix> SectionStepsProductMixCollection
        {
            get => _sectionStepsProductMixCollection;
            set
            {
                _sectionStepsProductMixCollection = value;
                RaisePropertyChangedEvent(nameof(SectionStepsProductMixCollection));
            }
        }

        public OtProductMix SectionStepsProductMixCollectionSelectedItem
        {
            get => _sectionStepsProductMixCollectionSelectedItem;
            set
            {
                _sectionStepsProductMixCollectionSelectedItem = value;
                RaisePropertyChangedEvent(nameof(SectionStepsProductMixCollectionSelectedItem));

                SectionStepsElementCollection =
                    OtRequest.GetElementCollection(_sectionStepsProductMixCollectionSelectedItem.Id);
                SectionStepsElementCollectionSelectedItem = _sectionStepsElementCollection
                    .FirstOrDefault(el => el.Id == _sectionStepsCurrentComponent.Element.Id);
            }
        }

        public ObservableCollection<OtElement> SectionStepsElementCollection
        {
            get => _sectionStepsElementCollection;
            set
            {
                _sectionStepsElementCollection = value;
                RaisePropertyChangedEvent(nameof(SectionStepsElementCollection));
            }
        }

        public OtElement SectionStepsElementCollectionSelectedItem
        {
            get => _sectionStepsElementCollectionSelectedItem;
            set
            {
                _sectionStepsElementCollectionSelectedItem = value;
                RaisePropertyChangedEvent(nameof(SectionStepsElementCollectionSelectedItem));
            }
        }

        public OtMaterial SectionStepsMaterialCollectionSelectedItem
        {
            get => _sectionStepsMaterialCollectionSelectedItem;
            set
            {
                _sectionStepsMaterialCollectionSelectedItem = value;
                RaisePropertyChangedEvent(nameof(SectionStepsMaterialCollectionSelectedItem));
            }
        }

        /* -------------------------------------------------------------------------------------------------------------
         * Stair
         * -----------------------------------------------------------------------------------------------------------*/
        
        public ObservableCollection<OtProductMix> RailingProductMixCollection
        {
            get => _railingProductMixCollection;
            set
            {
                _railingProductMixCollection = value;
                RaisePropertyChangedEvent(nameof(RailingProductMixCollection));
            }
        }

        public OtProductMix RailingProductMixCollectionSelectedItem
        {
            get => _railingProductMixCollectionSelectedItem;
            set
            {
                _railingProductMixCollectionSelectedItem = value;
                RaisePropertyChangedEvent(nameof(RailingProductMixCollectionSelectedItem));

                RailingElementCollection =
                    OtRequest.GetElementCollection(_railingProductMixCollectionSelectedItem.Id);
                RailingElementCollectionSelectedItem = RailingElementCollection
                    .FirstOrDefault(el => el.Id == _railingCurrentComponent.Element.Id);
            }
        }

        public ObservableCollection<OtElement> RailingElementCollection
        {
            get => _railingElementCollection;
            set
            {
                _railingElementCollection = value;
                RaisePropertyChangedEvent(nameof(RailingElementCollection));
            }
        }

        public OtElement RailingElementCollectionSelectedItem
        {
            get => _railingElementCollectionSelectedItem;
            set
            {
                _railingElementCollectionSelectedItem = value;
                RaisePropertyChangedEvent(nameof(RailingElementCollectionSelectedItem));
            }
        }

        public OtMaterial RailingMaterialCollectionSelectedItem
        {
            get => _railingMaterialCollectionSelectedItem;
            set
            {
                _railingMaterialCollectionSelectedItem = value;
                RaisePropertyChangedEvent(nameof(RailingMaterialCollectionSelectedItem));
            }
        }

        #endregion

        #region Metods
        
        /* -------------------------------------------------------------------------------------------------------------
         * Base section
         * -----------------------------------------------------------------------------------------------------------*/
        
        private DelegateCommand _generalSaveCommand;

        public ICommand GeneralSaveCommand =>
            _generalSaveCommand ?? (_generalSaveCommand = new DelegateCommand(o =>
            {
                EH.StairRunComponent.GetRunSupportComponent.ProductMix =
                    (OtProductMix) GeneralProductMixCollectionSelectedItem.Clone();
                EH.StairRunComponent.GetRunSupportComponent.Element = OtRequest.GetComponentInfo(
                    GeneralProductMixCollectionSelectedItem.Id,
                    GeneralElementCollectionSelectedItem.Id);
                EH.StairRunComponent.GetRunSupportComponent.Material =
                    (OtMaterial) GeneralMaterialCollectionSelectedItem.Clone();

                RaisePropertyChangedEvent(nameof(StairRunSupportName));
            }));
        
        /* -------------------------------------------------------------------------------------------------------------
         * Top section
         * -----------------------------------------------------------------------------------------------------------*/
        
        private void SectionTopSetCollection(string name, SimpleComponent component, params int[] productMixIds)
        {
            _sectionTopName = name;
            _sectionTopCurrentComponent = component;

            TopSectionProductMixCollection = OtRequest.GetProductMixCollection(productMixIds);
            TopSectionProductMixCollectionSelectedItem = TopSectionProductMixCollection
                .FirstOrDefault(pm => pm.Id == _sectionTopCurrentComponent.ProductMix.Id);

            TopSectionElementCollectionSelectedItem = TopSectionElementCollection
                .FirstOrDefault(el => el.Id == _sectionTopCurrentComponent.Element.Id);

            TopSectionMaterialCollectionSelectedItem = MaterialCollection
                .FirstOrDefault(mat => mat.Id == _sectionTopCurrentComponent.Material.Id);
        }
        
        // Save profile info
        // -------------------------------------------------------------------------------------------------------------
        private DelegateCommand _sectionTopSaveProfileInfoCommand;

        public ICommand SectionTopSaveProfileInfoCommand =>
            _sectionTopSaveProfileInfoCommand ?? (_sectionTopSaveProfileInfoCommand =
                new DelegateCommand(o =>
                {
                    _sectionTopCurrentComponent.ProductMix = (OtProductMix) TopSectionProductMixCollectionSelectedItem.Clone();

                    _sectionTopCurrentComponent.Element = OtRequest.GetComponentInfo(
                        TopSectionProductMixCollectionSelectedItem.Id,
                        TopSectionElementCollectionSelectedItem.Id);

                    _sectionTopCurrentComponent.Material = (OtMaterial) TopSectionMaterialCollectionSelectedItem.Clone();

                    RaisePropertyChangedEvent(_sectionTopName);
                }));
        
        // Top support
        // -------------------------------------------------------------------------------------------------------------
        private DelegateCommand _supportTopGetProfileInfoCommand;

        public ICommand SupportTopGetProfileInfo =>
            _supportTopGetProfileInfoCommand ?? (_supportTopGetProfileInfoCommand =
                new DelegateCommand(o =>
                    SectionTopSetCollection(nameof(TopSupportProfileName), EH.StairRunComponent.GetSupportTopComponent,
                        201, 202, 203)));
        
        // Landing
        // -------------------------------------------------------------------------------------------------------------
        // Landing base
        
        private DelegateCommand _sectionTopLandingBaseGetCommand;

        public ICommand SectionTopLandingBaseGetCommand =>
            _sectionTopLandingBaseGetCommand ?? (_sectionTopLandingBaseGetCommand =
                new DelegateCommand(o =>
                    SectionTopSetCollection(nameof(SectionTopLandingBaseName),
                        EH.StairRunComponent.GetTopLandingBaseComponent, 601, 602, 604, 611, 622)));
        // -------------------------------------------------------------------------------------------------------------
        // Landing support
        
        private DelegateCommand _sectionTopLandingSupportGetCommand;

        public ICommand SectionTopLandingSupportGetCommand =>
            _sectionTopLandingSupportGetCommand ?? (_sectionTopLandingSupportGetCommand =
                new DelegateCommand(o =>
                    SectionTopSetCollection( nameof(SectionTopLandingSupportName),
                        EH.StairRunComponent.GetTopLandingSupportComponent, 201, 202, 203)));
        // -------------------------------------------------------------------------------------------------------------
        // Landing axillary front
        
        private DelegateCommand _sectionTopLandingAxillaryFrontCommand;

        public ICommand SectionTopLandingAxillaryFrontCommand =>
            _sectionTopLandingAxillaryFrontCommand ?? (_sectionTopLandingAxillaryFrontCommand =
                new DelegateCommand(o => SectionTopSetCollection(
                    nameof(SectionTopLandingAxillaryFrontName), EH.StairRunComponent.GetTopLandingAxillaryFrontComponent,
                    201, 202, 203)));
        // -------------------------------------------------------------------------------------------------------------
        // Landing axillary back
        
        private DelegateCommand _sectionTopLandingAxillaryBackCommand;

        public ICommand SectionTopLoadingAxillaryBackCommand =>
            _sectionTopLandingAxillaryBackCommand ?? (_sectionTopLandingAxillaryBackCommand =
                new DelegateCommand(o => SectionTopSetCollection(
                    nameof(SectionTopLandingAxillaryBackName), EH.StairRunComponent.GetTopLandingAxillaryBackComponent,
                    201, 202, 203)));
        
        /* -------------------------------------------------------------------------------------------------------------
         * Bottom section
         * -----------------------------------------------------------------------------------------------------------*/

        private void SectionBottomSetCollection(string name, SimpleComponent component, params int[] productMixIds)
        {
            _sectionBottomName = name;
            _sectionBottomCurrentComponent = component;

            SectionBottomProductMixCollection = OtRequest.GetProductMixCollection(productMixIds);
            try
            {
                SectionBottomProductMixCollectionSelectedItem = SectionBottomProductMixCollection
                    .FirstOrDefault(pm => pm.Id == _sectionBottomCurrentComponent.ProductMix.Id);

                SectionBottomElementCollectionSelectedItem = SectionBottomElementCollection
                    .FirstOrDefault(el => el.Id == _sectionBottomCurrentComponent.Element.Id);
            }
            catch (Exception e)
            {
                SectionBottomProductMixCollectionSelectedItem = SectionBottomProductMixCollection
                    .FirstOrDefault();
                SectionBottomElementCollectionSelectedItem = SectionBottomElementCollection
                    .FirstOrDefault();
            }
            
            SectionBottomMaterialCollectionSelectedItem = MaterialCollection
                .FirstOrDefault(mt => mt.Id == _sectionBottomCurrentComponent.Material.Id);
        }

        // Save profile info
        // -------------------------------------------------------------------------------------------------------------
        private DelegateCommand _sectionBottomSaveProfileInfoCommand;

        public ICommand SectionBottomSaveProfileInfoCommand =>
            _sectionBottomSaveProfileInfoCommand ?? (_sectionBottomSaveProfileInfoCommand = new DelegateCommand(o =>
            {
                _sectionBottomCurrentComponent.ProductMix =
                    (OtProductMix)SectionBottomProductMixCollectionSelectedItem.Clone();

                _sectionBottomCurrentComponent.Element = OtRequest.GetComponentInfo(
                    SectionBottomProductMixCollectionSelectedItem.Id,
                    SectionBottomElementCollectionSelectedItem.Id);

                _sectionBottomCurrentComponent.Material =
                    (OtMaterial)SectionBottomMaterialCollectionSelectedItem.Clone();

                RaisePropertyChangedEvent(_sectionBottomName);
            }));
        
        // Bottom support
        // -------------------------------------------------------------------------------------------------------------
        // Support plate
        
        private DelegateCommand _sectionBottomSupportGetCommand;
        
        public ICommand SectionBottomSupportGetCommand =>
            _sectionBottomSupportGetCommand ?? (_sectionBottomSupportGetCommand =
                new DelegateCommand(o => SectionBottomSetCollection(nameof(SectionBottomSupportName),
                    EH.StairRunComponent.GetSupportBottomComponent, 604, 622)));
        // -------------------------------------------------------------------------------------------------------------
        // Support L-steel
        
        private DelegateCommand _sectionBottomSupportLSteelGetCommand;

        public ICommand SectionBottomSupportLSteelGetCommand =>
            _sectionBottomSupportLSteelGetCommand ?? (_sectionBottomSupportLSteelGetCommand =
                new DelegateCommand(o => SectionBottomSetCollection(nameof(SectionBottomSupportName),
                    EH.StairRunComponent.GetSupportBottomComponent, 201, 202, 203)));
        
        // Landing
        // -------------------------------------------------------------------------------------------------------------
        // Landing base
        
        private DelegateCommand _sectionBottomLandingBaseGetCommand;

        public ICommand SectionBottomLandingBaseGetCommand =>
            _sectionBottomLandingBaseGetCommand ?? (_sectionBottomLandingBaseGetCommand =
                new DelegateCommand(o => SectionBottomSetCollection(nameof(SectionBottomLandingBaseName),
                    EH.StairRunComponent.GetBottomLandingBaseComponent, 601, 602, 604, 611, 622)));
        // -------------------------------------------------------------------------------------------------------------
        // Support
        
        private DelegateCommand _sectionBottomLandingSupportGetCommand;

        public ICommand SectionBottomLandingSupportGetCommand =>
            _sectionBottomLandingSupportGetCommand ?? (_sectionBottomLandingSupportGetCommand =
                new DelegateCommand(o => SectionBottomSetCollection(nameof(SectionBottomLandingSupportName),
                    EH.StairRunComponent.GetBottomLandingSupportComponent, 201, 202, 203)));
        // -------------------------------------------------------------------------------------------------------------
        // Axillary front
        
        private DelegateCommand _sectionBottomLandingAxillaryFrontGetCommand;

        public ICommand SectionBottomLandingAxillaryFrontGetCommand =>
            _sectionBottomLandingAxillaryFrontGetCommand ?? (_sectionBottomLandingAxillaryFrontGetCommand =
                new DelegateCommand(o => SectionBottomSetCollection(nameof(SectionBottomLandingAxillaryFrontName),
                    EH.StairRunComponent.GetBottomLandingAxillaryFrontComponent, 201, 202, 203)));
        // -------------------------------------------------------------------------------------------------------------
        // Axillary back
        
        private DelegateCommand _sectionBottomLandingAxillaryBackGetCommand;

        public ICommand SectionBottomLandingAxillaryBackGetCommand =>
            _sectionBottomLandingAxillaryBackGetCommand ?? (_sectionBottomLandingAxillaryBackGetCommand =
                new DelegateCommand(o => SectionBottomSetCollection(nameof(SectionBottomLandingAxillaryBackName),
                    EH.StairRunComponent.GetBottomLandingAxillaryBackComponent, 201, 202, 203)));
        
        /* -------------------------------------------------------------------------------------------------------------
         * Section step
         * -----------------------------------------------------------------------------------------------------------*/

        private void SectionStepsSetCollection(string name, SimpleComponent component, params int[] productMixIds)
        {
            _sectionStepsName = name;
            _sectionStepsCurrentComponent = component;

            SectionStepsProductMixCollection = OtRequest.GetProductMixCollection(productMixIds);
            SectionStepsProductMixCollectionSelectedItem = SectionStepsProductMixCollection
                .FirstOrDefault(pm => pm.Id == _sectionStepsCurrentComponent.ProductMix.Id);

            SectionStepsElementCollectionSelectedItem = SectionStepsElementCollection
                .FirstOrDefault(el => el.Id == _sectionStepsCurrentComponent.Element.Id);
            SectionStepsMaterialCollectionSelectedItem = MaterialCollection
                .FirstOrDefault(mt => mt.Id == _sectionStepsCurrentComponent.Material.Id);
        }
        
        // Save profile info
        // -------------------------------------------------------------------------------------------------------------
        private DelegateCommand _sectionStepsSaveProfileInfoCommand;

        public ICommand SectionStepsSaveProfileInfoCommand =>
            _sectionStepsSaveProfileInfoCommand ?? (_sectionStepsSaveProfileInfoCommand =
                new DelegateCommand(o =>
                {
                    _sectionStepsCurrentComponent.ProductMix =
                        (OtProductMix) SectionStepsProductMixCollectionSelectedItem.Clone();

                    _sectionStepsCurrentComponent.Element = OtRequest.GetComponentInfo(
                        SectionStepsProductMixCollectionSelectedItem.Id,
                        SectionStepsElementCollectionSelectedItem.Id);

                    _sectionStepsCurrentComponent.Material =
                        (OtMaterial) SectionStepsMaterialCollectionSelectedItem.Clone();

                    RaisePropertyChangedEvent(_sectionStepsName);
                }));
        
        // Step components
        // -------------------------------------------------------------------------------------------------------------
        // Base
        
        private DelegateCommand _sectionStepsBaseGetProfileInfo;

        public ICommand SectionStepsBaseGetProfile =>
            _sectionStepsBaseGetProfileInfo ?? (_sectionStepsBaseGetProfileInfo =
                new DelegateCommand(o => SectionStepsSetCollection(nameof(SectionStepBaseName),
                    EH.StairRunComponent.GetStepBaseComponent, 601, 602, 604, 611, 622)));
        // -------------------------------------------------------------------------------------------------------------
        // Axillary
        
        private DelegateCommand _sectionStepsAxillaryGetProfileInfoCommand;

        public ICommand SectionStepsAxillaryGetProfileInfoCommand =>
            _sectionStepsAxillaryGetProfileInfoCommand ?? (_sectionStepsAxillaryGetProfileInfoCommand =
                new DelegateCommand(o => SectionStepsSetCollection(nameof(SectionStepsAxillaryName),
                    EH.StairRunComponent.GetStepAxillaryComponent, 201, 202, 203)));
        // -------------------------------------------------------------------------------------------------------------
        // Support
        
        private DelegateCommand _sectionStepsSupportGetProfileInfoCommand;

        public ICommand SectionStepsSupportGetProfileInfo =>
            _sectionStepsSupportGetProfileInfoCommand ?? (_sectionStepsSupportGetProfileInfoCommand =
                new DelegateCommand(o => SectionStepsSetCollection(nameof(SectionStepsSupportName),
                    EH.StairRunComponent.GetStepSupportComponent, 201, 202, 203)));
        
        /* -------------------------------------------------------------------------------------------------------------
         * Railing section
         * -----------------------------------------------------------------------------------------------------------*/
        
        private void RailingSetCollection(string name, SimpleComponent component, params int[] productMixIds)
        {
            _railingName = name;
            _railingCurrentComponent = component;

            RailingProductMixCollection = OtRequest.GetProductMixCollection(productMixIds);
            RailingProductMixCollectionSelectedItem = RailingProductMixCollection
                .FirstOrDefault(pm => pm.Id == _railingCurrentComponent.ProductMix.Id);

            RailingElementCollectionSelectedItem = RailingElementCollection
                .FirstOrDefault(el => el.Id == _railingCurrentComponent.Element.Id);
            RailingMaterialCollectionSelectedItem = MaterialCollection
                .FirstOrDefault(mt => mt.Id == _railingCurrentComponent.Material.Id);
        }
        
        // Save profile info
        // -------------------------------------------------------------------------------------------------------------
        private DelegateCommand _railingSaveProfileInfoCommand;

        public ICommand RailingSaveProfileInfoCommand =>
            _railingSaveProfileInfoCommand ?? (_railingSaveProfileInfoCommand =
                new DelegateCommand(o =>
                {
                    _railingCurrentComponent.ProductMix =
                        (OtProductMix) RailingProductMixCollectionSelectedItem.Clone();

                    _railingCurrentComponent.Element = OtRequest.GetComponentInfo(
                        RailingProductMixCollectionSelectedItem.Id,
                        RailingElementCollectionSelectedItem.Id);

                    _railingCurrentComponent.Material =
                        (OtMaterial) RailingMaterialCollectionSelectedItem.Clone();

                    RaisePropertyChangedEvent(_railingName);
                    RaisePropertyChangedEvent(nameof(RailingBalusterWidthHandmade));
                    RaisePropertyChangedEvent(nameof(RailingRailHandmade));
                    RaisePropertyChangedEvent(nameof(RailingAxillaryHandmade));
                }));
        
        // Baluster
        // -------------------------------------------------------------------------------------------------------------
        private DelegateCommand _balusterGetProfileInfoCommand;

        public ICommand BalusterGetProfileInfoCommand =>
            _balusterGetProfileInfoCommand ?? (_balusterGetProfileInfoCommand =
                new DelegateCommand(o => RailingSetCollection(nameof(RailingComponentBalusterName),
                    EH.StairRunComponent.GetRailingBalusterComponent,
                    201, 202, 203, 301, 501, 502, 550, 604, 605, 622)));
        
        // Hand-railing
        // -------------------------------------------------------------------------------------------------------------
        private DelegateCommand _handRailingGetProfileInfoCommand;

        public ICommand HandRailingGetProfileInfoCommand =>
            _handRailingGetProfileInfoCommand ?? (_handRailingGetProfileInfoCommand =
                new DelegateCommand(o => RailingSetCollection(nameof(RailingComponentHandRailingName),
                    EH.StairRunComponent.GetRailingHandRailComponent,
                    201, 202, 203, 301, 501, 502, 550)));
        
        // Rail
        // -------------------------------------------------------------------------------------------------------------
        private DelegateCommand _railGetProfileInfoCommand;

        public ICommand RailGetProfileInfoCommand =>
            _railGetProfileInfoCommand ?? (_railGetProfileInfoCommand =
                new DelegateCommand(o => RailingSetCollection(nameof(RailingComponentRailName),
                    EH.StairRunComponent.GetRailingRailComponent,
                    201, 202, 203, 310, 312, 501, 502, 550, 604, 605, 622)));
        
        // Axillary
        // -------------------------------------------------------------------------------------------------------------
        private DelegateCommand _railingAxillaryGetProfileInfoCommand;

        public ICommand RailingAxillaryGetProfileInfoCommand =>
            _railingAxillaryGetProfileInfoCommand ?? (_railingAxillaryGetProfileInfoCommand =
                new DelegateCommand(o => RailingSetCollection(nameof(RailingComponentAxillaryName),
                    EH.StairRunComponent.GetRailingAxillaryComponent, 604, 605, 622)));
        
        /* -------------------------------------------------------------------------------------------------------------
         * RvtElement
         * -----------------------------------------------------------------------------------------------------------*/
        
        private DelegateCommand _saveToRvtCommand;

        public ICommand SaveToRvtCommand =>
            _saveToRvtCommand ?? (_saveToRvtCommand =
                new DelegateCommand(o =>
                {
                    ExternalHandler.RevitCommand = app =>
                    {
                        Document doc = app.ActiveUIDocument.Document;

                        using (var tr = new Transaction(doc, "setStairRunParameters"))
                        {
                            tr.Start();

                            EH.StairRunComponent.SetValueToRvtElement(
                                new FilteredElementCollector(doc).OfCategory(BuiltInCategory.OST_Materials));

                            tr.Commit();
                        }
                    };

                    ExternalHandler.ExEvent.Raise();
                }));

        #endregion

        #region Constructor

        public MainWindow_ViewModel()
        {
            switch (EH.StairRunComponent.TopSectionType)
            {
                case SectionType.TopSupport:
                    SectionTopControl = new AngleControl();
                    break;
                case SectionType.TopLanding:
                    SectionTopControl = new HorizontalControl();
                    break;
            }

            switch (EH.StairRunComponent.BottomSectionType)
            {
                case SectionType.BottomSupportPlate:
                    SectionBottomControl = new AngleByPlateControl();
                    break;
                case SectionType.BottomLanding:
                    SectionBottomControl = new UserControls.BottomSection.HorizontalControl();
                    break;
                case SectionType.BottomSupportSlice:
                    SectionBottomControl = new SliceComponent();
                    break;
                case SectionType.BottomSupportVertical:
                    SectionBottomControl = new VerticalControl();
                    break;
            }

            switch (EH.StairRunComponent.StepsSectionType)
            {
                case SectionType.StepSimple:
                    SectionStepsControl = new Simple_General_Control();
                    break;
                case SectionType.StepComplexFirst:
                    SectionStepsControl = new Complex_General();
                    SectionStepsComplexComponent = new Complex_V1_Control();
                    break;
                case SectionType.StepComplexSecond:
                    SectionStepsControl = new Complex_General();
                    SectionStepsComplexComponent = new Complex_V2_Control();
                    break;
                case SectionType.StepComplexThird:
                    SectionStepsControl = new Complex_General();
                    SectionStepsComplexComponent = new Complex_V3_Control();
                    break;
                case SectionType.StepComplexFourth:
                    SectionStepsControl = new Complex_General();
                    SectionStepsComplexComponent = new Complex_V4_Control();
                    break;
                case SectionType.StepPurchase:
                    SectionStepsControl = new Single_General_Control();
                    break;
            }
        }

        #endregion
    }
}