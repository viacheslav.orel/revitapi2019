﻿using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using OrslavTeam.Revit.Axillary.PublicInformation;
using OrslavTeam.Revit.Steel.SteelSpreadScheduleViewer.View;

namespace OrslavTeam.Revit.Steel.SteelSpreadScheduleViewer.Revit
{
    [Transaction(TransactionMode.Manual)]
    public class AddInStart : IExternalCommand
    {
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            Document doc = commandData.Application.ActiveUIDocument.Document;

            string filePath = PublicParameter.DocumentPath(doc) + "\\res\\";

            var viewModel = new SteelSpreadScheduleViewModel(filePath);
            var view = new SteelSpreadView(viewModel);
            view.Show();

            return Result.Succeeded;
        }
    }
}