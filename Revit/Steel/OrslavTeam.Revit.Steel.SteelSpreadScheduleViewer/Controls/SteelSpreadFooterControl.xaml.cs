﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using OrslavTeam.Revit.Axillary.PublicInformation;

namespace OrslavTeam.Revit.Steel.SteelSpreadScheduleViewer.Controls
{
    /// <summary>
    /// Interaction logic for SteelSpreadFooterControl.xaml
    /// </summary>
    public partial class SteelSpreadFooterControl
    {
        #region dimensions fields --------------------------------------------------------------------------------------

        private static readonly double _borderThicknessLight = 0.03 * (96 / 2.54);
        private static readonly double _borderThicknessBold = 0.06 * (96 / 2.54);
        private static readonly double _columnWidth = 1.5 * (96 / 2.54);

        public int RowHeight { get; set; }
        private readonly int _columnQuantity;

        #endregion


        #region data fields --------------------------------------------------------------------------------------------

        private Dictionary<string, double[]> _massByMaterial;
        private Dictionary<string, SteelSpreadRow> _materialTotalRows;

        private TextBlock[] _totalMassByColumnsCell;
        private double[] _totalMassByColumn;

        private double _totalMass;

        #endregion


        #region constructors -------------------------------------------------------------------------------------------

        public SteelSpreadFooterControl()
        {
            InitializeComponent();
        }

        public SteelSpreadFooterControl(string allSteelMass, string massByMaterialTitle, int columnQuantity)
        {
            InitializeComponent();

            _columnQuantity = columnQuantity;
            _materialTotalRows = new Dictionary<string, SteelSpreadRow>();
            _totalMassByColumnsCell = new TextBlock[_columnQuantity];
            ClearData();


            this.AllSteelMass.Text = allSteelMass;
            this.MassByMaterialTitle.Text = massByMaterialTitle;
        }

        #endregion


        #region methods ------------------------------------------------------------------------------------------------

        public void ClearData()
        {
            _massByMaterial = new Dictionary<string, double[]>();

            _totalMassByColumn = new double[_columnQuantity];

            _totalMass = 0;
        }

        public void AddMass(string materialName,  double[] value)
        {
            if (!_massByMaterial.ContainsKey(materialName))
            {
                _massByMaterial.Add(materialName, value);

                for (int i = 0; i < value.Length; i++)
                {
                    _totalMassByColumn[i] += value[i];
                    _totalMass += value[i];
                }
            }
            else
            {
                for (int i = 0; i < value.Length; i++)
                {
                    _massByMaterial[materialName][i] += value[i];
                    _totalMassByColumn[i] += value[i];
                    _totalMass += value[i];
                }
            }
        }

        public void CalculateTotal(int startRowNumber)
        {
            this.AllSteelMassNumber.Text = startRowNumber.ToString();
            startRowNumber++;

            RowHeight = 16;

            foreach (KeyValuePair<string, double[]> pair in _massByMaterial.OrderBy(p => p.Key))
            {
                var row = new SteelSpreadRow(pair.Key, startRowNumber++, pair.Value);
                _materialTotalRows.Add(pair.Key, row);

                this.MassByMaterialRows.Children.Add(row);
                RowHeight += 8;
            }

            for (int i = 0; i < _columnQuantity; i++)
            {
                var cell = new TextBlock { Text = _totalMassByColumn[i] < 0.001 ? "" : _totalMassByColumn[i].ToString(PublicParameter.OtCulture) };
                _totalMassByColumnsCell[i] = cell;

                var border = new Border
                {
                    BorderBrush = Brushes.Black,
                    BorderThickness = new Thickness(_borderThicknessLight, 0, _borderThicknessLight, _borderThicknessBold),
                    Child = cell
                };

                this.AllSteelMassData.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(_columnWidth) });
                Grid.SetColumn(border, i);
                Grid.SetRow(border, 0);
                this.AllSteelMassData.Children.Add(border);

                var emptyBorder = new Border
                {
                    BorderBrush = Brushes.Black,
                    BorderThickness = new Thickness(_borderThicknessLight, 0, _borderThicknessLight, _borderThicknessLight)
                };
                Grid.SetRow(emptyBorder, 1);
                Grid.SetColumn(emptyBorder, i);
                this.AllSteelMassData.Children.Add(emptyBorder);
            }

            this.AllSteelMassTotal.Text = _totalMass.ToString(PublicParameter.OtCulture);
        }


        public void RecalculateTotal()
        {
            foreach (KeyValuePair<string, double[]> pair in _massByMaterial.OrderBy(p => p.Key))
            {
                _materialTotalRows[pair.Key].SetNewUnit(pair.Value);
            }

            for (int i = 0; i < _columnQuantity; i++)
            {
                _totalMassByColumnsCell[i].Text = _totalMassByColumn[i] < 0.001
                    ? ""
                    : _totalMassByColumn[i].ToString(PublicParameter.OtCulture);
            }

            this.AllSteelMassTotal.Text = _totalMass.ToString(PublicParameter.OtCulture);
        }

        #endregion
    }
}
