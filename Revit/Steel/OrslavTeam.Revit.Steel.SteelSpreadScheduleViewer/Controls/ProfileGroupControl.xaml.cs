﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using OrslavTeam.Revit.Axillary.PublicInformation;

namespace OrslavTeam.Revit.Steel.SteelSpreadScheduleViewer.Controls
{
    /// <summary>
    /// Interaction logic for ProfileGroupControl.xaml
    /// </summary>
    public partial class ProfileGroupControl
    {
        #region dimensions fields --------------------------------------------------------------------------------------

        public static string Units = "кг";
        public static double RoundingAccuracy = 1;
        private static readonly double _columnWidth = 1.5 * (96 / 2.54);
        private static readonly double _borderThicknessLight = 0.03 * (96 / 2.54);

        #endregion


        #region data fields --------------------------------------------------------------------------------------------

        private readonly double[] _massList;
        private readonly List<TextBlock> _massContainer;
        private double _totalMassValue;

        #endregion


        #region CallBacks ----------------------------------------------------------------------------------------------

        public delegate void MaterialCallBack(int columnNumber, double value);

        #endregion


        #region constructors -------------------------------------------------------------------------------------------

        public ProfileGroupControl()
        {
            InitializeComponent();
        }

        public ProfileGroupControl(string profileName, double[] masses, int rowNumber, MaterialCallBack materialCount)
        {
            InitializeComponent();

            _massList = masses;
            _massContainer = new List<TextBlock>();

            this.RowNumber.Text = rowNumber.ToString();
            this.ProfileName.Text = profileName;

            for (int i = 0; i < masses.Length; i++)
            {
                double currentMass = Units == "кг"
                    ? Math.Round(masses[i] / RoundingAccuracy, MidpointRounding.AwayFromZero) * RoundingAccuracy
                    : Math.Round(masses[i] / (RoundingAccuracy * 1000), MidpointRounding.AwayFromZero) * RoundingAccuracy;

                _totalMassValue += currentMass;
                materialCount(i, currentMass);

                var cell = new TextBlock
                {
                    TextAlignment = TextAlignment.Center,
                    Text = currentMass < 0.001 ? "" : currentMass.ToString(PublicParameter.OtCulture)
                };

                var border = new Border
                {
                    BorderBrush = Brushes.Black,
                    BorderThickness = new Thickness(_borderThicknessLight, 0, _borderThicknessLight, _borderThicknessLight),
                    Child = cell
                };
                Grid.SetRow(border, 0);
                Grid.SetColumn(border, i);

                var gridColumn = new ColumnDefinition { Width = new GridLength(_columnWidth, GridUnitType.Pixel) };
                this.Masses.ColumnDefinitions.Add(gridColumn);
                this.Masses.Children.Add(border);

                _massContainer.Add(cell);
            }

            this.TotalMass.Text = _totalMassValue.ToString(PublicParameter.OtCulture);
        }

        #endregion


        #region methods ------------------------------------------------------------------------------------------------

        public void Recalculate(MaterialCallBack materialCount)
        {
            _totalMassValue = 0;

            for (int i = 0; i < _massList.Length; i++)
            {
                double currentMass = Units == "кг"
                    ? Math.Round(_massList[i] / RoundingAccuracy, MidpointRounding.AwayFromZero) * RoundingAccuracy
                    : Math.Round(_massList[i] / (RoundingAccuracy * 1000), MidpointRounding.AwayFromZero) * RoundingAccuracy;

                _totalMassValue += currentMass;
                materialCount(i, currentMass);
                _massContainer[i].Text = currentMass < 0.001 ? "" : currentMass.ToString(PublicParameter.OtCulture);
            }

            this.TotalMass.Text = _totalMassValue.ToString(PublicParameter.OtCulture);
        }

        #endregion
    }
}
