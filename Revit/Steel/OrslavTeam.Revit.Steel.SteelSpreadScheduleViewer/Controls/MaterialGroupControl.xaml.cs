﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using OrslavTeam.Revit.Axillary.PublicInformation;

namespace OrslavTeam.Revit.Steel.SteelSpreadScheduleViewer.Controls
{
    /// <summary>
    /// Interaction logic for MaterialGroupControl.xaml
    /// </summary>
    public partial class MaterialGroupControl
    {
        #region dimensions fields --------------------------------------------------------------------------------------

        private static readonly double _rowHeight = 0.8 * (96 / 2.54);
        private static readonly double _columnWidth = 1.5 * (96 / 2.54);
        private static readonly double _borderThicknessLight = 0.03 * (96 / 2.54);

        private readonly int _columnQuantity;

        public int RowHeight = 8;

        #endregion


        #region data field ---------------------------------------------------------------------------------------------

        private List<ProfileGroupControl> _profiles;

        private double[] _totalMassByColumnField;
        private readonly List<TextBlock> _totalMassContainers;

        private double _totalMass;

        #endregion


        #region CallBacks ----------------------------------------------------------------------------------------------

        public delegate void ProductMixCallBack(int columnNumber, double value);

        private readonly ProductMixCallBack _productMixCallBack;


        public delegate void TotalMassCallBack(string materialName, double[] values);

        private readonly TotalMassCallBack _totalMassCallBack;

        #endregion


        #region constructors -------------------------------------------------------------------------------------------

        public MaterialGroupControl()
        {
            InitializeComponent();
        }

        public MaterialGroupControl(string name, string totalMassName, int columnQuantity, ProductMixCallBack pmCounter, TotalMassCallBack massCounter)
        {
            InitializeComponent();

            _columnQuantity = columnQuantity;
            _productMixCallBack = pmCounter;
            _totalMassCallBack = massCounter;

            this.MaterialName.Text = name;
            this.TotalMassName.Text = totalMassName;

            ClearData();

            _profiles = new List<ProfileGroupControl>();
            _totalMassContainers = new List<TextBlock>();


            for (int i = 0; i < _columnQuantity; i++)
            {
                var column = new ColumnDefinition { Width = new GridLength(_columnWidth) };
                this.TotalMassByColumn.ColumnDefinitions.Add(column);

                var cell = new TextBlock { TextAlignment = TextAlignment.Center };
                _totalMassContainers.Add(cell);

                var border = new Border
                {
                    Child = cell,
                    BorderBrush = Brushes.Black,
                    BorderThickness = new Thickness(_borderThicknessLight, 0, _borderThicknessLight, _borderThicknessLight)
                };
                Grid.SetColumn(border, i);
                this.TotalMassByColumn.Children.Add(border);
            }
        }

        #endregion


        #region methods ------------------------------------------------------------------------------------------------

        public void ClearData()
        {
            _totalMassByColumnField = new double[_columnQuantity];
            _totalMass = 0;
        }

        public void AddRow(ProfileGroupControl profileRow)
        {
            this.Profiles.RowDefinitions.Add(new RowDefinition ());

            Grid.SetRow(profileRow, _profiles.Count);
            _profiles.Add(profileRow);
            this.Profiles.Children.Add(profileRow);

            RowHeight += 8;
        }

        public void AddMass(int columnNumber, double value)
        {
            _totalMassByColumnField[columnNumber] += value;
            _totalMass += value;

            _productMixCallBack(columnNumber, value);
        }

        public void CalculateTotal(int rowNumber)
        {
            this.RowNumber.Text = rowNumber.ToString();

            for (int i = 0; i < _columnQuantity; i++)
            {
                _totalMassContainers[i].Text = _totalMassByColumnField[i] < 0.001
                ? "" : _totalMassByColumnField[i].ToString(PublicParameter.OtCulture);
            }

            this.TotalMass.Text = _totalMass.ToString(PublicParameter.OtCulture);
            _totalMassCallBack(this.MaterialName.Text, _totalMassByColumnField);
        }


        public void RecalculateTotal()
        {
            ClearData();

            foreach (ProfileGroupControl profileRow in _profiles)
            {
                profileRow.Recalculate(AddMass);
            }

            for (int i = 0; i < _columnQuantity; i++)
            {
                _totalMassContainers[i].Text = _totalMassByColumnField[i] < 0.001
                    ? "" : _totalMassByColumnField[i].ToString(PublicParameter.OtCulture);
            }

            this.TotalMass.Text = _totalMass.ToString(PublicParameter.OtCulture);
            _totalMassCallBack(this.MaterialName.Text, _totalMassByColumnField);
        }

        #endregion
    }
}
