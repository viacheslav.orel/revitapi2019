﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OrslavTeam.Revit.Steel.SteelSpreadScheduleViewer.Controls
{
    /// <summary>
    /// Interaction logic for SteelSpreadHeaderFull.xaml
    /// </summary>
    public partial class SteelSpreadHeaderFull : UserControl
    {
        private static readonly double _columnWidth = 1.5 * (96 / 2.54);
        private static readonly double _borderThicknessLight = 0.03 * (96 / 2.54);
        private static readonly double _borderThicknessBold = 0.06 * (96 / 2.54);

        public SteelSpreadHeaderFull()
        {
            InitializeComponent();
        }

        public SteelSpreadHeaderFull(string[] columnNames)
        {
            InitializeComponent();

            for (int i = 0; i < columnNames.Length; i++)
            {
                var column = new ColumnDefinition {Width = new GridLength(_columnWidth) };
                this.MassColumn.ColumnDefinitions.Add(column);

                var textBlock = new TextBlock
                {
                    Text = columnNames[i],
                    TextAlignment = TextAlignment.Center,
                    LayoutTransform = new RotateTransform(-90)
                };

                var border = new Border
                {
                    Child = textBlock,
                    BorderBrush = Brushes.Black,
                    BorderThickness = new Thickness(_borderThicknessLight, 0, _borderThicknessLight, _borderThicknessBold)
                };
                Grid.SetColumn(border, i);
                this.MassColumn.Children.Add(border);
            }
        }
    }
}
