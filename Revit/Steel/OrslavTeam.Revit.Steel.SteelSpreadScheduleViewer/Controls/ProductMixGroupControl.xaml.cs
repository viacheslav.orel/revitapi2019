﻿using System.Collections.Generic;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using OrslavTeam.Revit.Axillary.PublicInformation;

namespace OrslavTeam.Revit.Steel.SteelSpreadScheduleViewer.Controls
{
    /// <summary>
    /// Interaction logic for ProductMixGroupControl.xaml
    /// </summary>
    public partial class ProductMixGroupControl
    {
        #region dimensions fields --------------------------------------------------------------------------------------

        private static readonly double _columnWidth = 1.5 * (96 / 2.54);
        private static readonly double _borderThicknessLight = 0.03 * (96 / 2.54);
        private static readonly double _borderThicknessBold = 0.06 * (96 / 2.54);

        private readonly int _columnQuantity;

        public int RowHeight = 8;

        #endregion


        #region data fields --------------------------------------------------------------------------------------------

        private double[] _massByColumn;
        private readonly TextBlock[] _massContainer;

        private double _totalMassValue;
        
        private readonly List<MaterialGroupControl> _materials;

        #endregion


        #region contructors --------------------------------------------------------------------------------------------

        public ProductMixGroupControl()
        {
            InitializeComponent();
        }

        public ProductMixGroupControl(string name, string totalMassName, int columnQuantity)
        {
            InitializeComponent();

            _columnQuantity = columnQuantity;
            _materials = new List<MaterialGroupControl>();
            _massContainer = new TextBlock[columnQuantity];
            ClearData();


            this.ProductMixName.Text = name;
            this.TotalMassName.Text = totalMassName;


            for (int i = 0; i < columnQuantity; i++)
            {
                this.TotalMassByColumn.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(_columnWidth) });

                var cell = new TextBlock { TextAlignment = TextAlignment.Center };
                _massContainer[i] = cell;

                var border = new Border
                {
                    Child = cell,
                    BorderBrush = Brushes.Black,
                    BorderThickness = new Thickness(_borderThicknessLight, 0, _borderThicknessLight, _borderThicknessBold)
                };

                Grid.SetColumn(border, i);
                this.TotalMassByColumn.Children.Add(border);
            }
        }

        #endregion


        #region methods ------------------------------------------------------------------------------------------------

        public void ClearData()
        {
            _massByColumn = new double[_columnQuantity];
            _totalMassValue = 0;
        }

        public void AddRow(MaterialGroupControl row)
        {
            this.Materials.RowDefinitions.Add(new RowDefinition ());
            Grid.SetRow(row, _materials.Count);
            this.Materials.Children.Add(row);

            _materials.Add(row);

            RowHeight += row.RowHeight;
        }

        public void AddMass(int columnNumber, double value)
        {
            _massByColumn[columnNumber] += value;
            _totalMassValue += value;
        }

        public void CalculateTotal(int rowNumber)
        {
            this.RowNumber.Text = rowNumber.ToString();

            for (int i = 0; i < _columnQuantity; i++)
            {
                _massContainer[i].Text = _massByColumn[i] < 0.001
                ? "" :_massByColumn[i].ToString(PublicParameter.OtCulture);
            }

            this.TotalMass.Text = _totalMassValue.ToString(PublicParameter.OtCulture);
        }


        public void RecalculateTotal()
        {
            ClearData();

            foreach (MaterialGroupControl materialRow in _materials)
            {
                materialRow.RecalculateTotal();
            }

            for (int i = 0; i < _columnQuantity; i++)
            {
                _massContainer[i].Text = _massByColumn[i] < 0.001
                    ? "" : _massByColumn[i].ToString(PublicParameter.OtCulture);
            }

            this.TotalMass.Text = _totalMassValue.ToString(PublicParameter.OtCulture);
        }

        #endregion
    }
}
