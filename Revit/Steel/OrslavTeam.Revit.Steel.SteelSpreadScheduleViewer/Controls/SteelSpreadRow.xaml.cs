﻿using System.Collections.Generic;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace OrslavTeam.Revit.Steel.SteelSpreadScheduleViewer.Controls
{
    /// <summary>
    /// Interaction logic for SteelSpreadRow.xaml
    /// </summary>
    public partial class SteelSpreadRow : UserControl
    {
        private static readonly double _borderThicknessLight = 0.03 * (96 / 2.54);

        private readonly List<TextBlock> _massByColumnControls = new List<TextBlock>();

        public SteelSpreadRow()
        {
            InitializeComponent();
        }

        public SteelSpreadRow(string title, int rowNumber, IEnumerable<double> massByColumn)
        {
            InitializeComponent();

            this.Tittle.Text = title;
            this.RowNumber.Text = rowNumber.ToString();

            double totalMass = 0;

            foreach (double mass in massByColumn)
            {
                totalMass += mass;

                var cell = new TextBlock { Text = mass < 0.001 ? "" : mass.ToString(CultureInfo.CurrentCulture) };
                _massByColumnControls.Add(cell);

                var border = new Border
                {
                    BorderBrush = Brushes.Black,
                    BorderThickness = new Thickness(_borderThicknessLight, 0, _borderThicknessLight, 0),
                    Child = cell
                };

                this.Values.Children.Add(border);
            }

            this.TotalMass.Text = totalMass.ToString(CultureInfo.CurrentCulture);
        }

        public void SetNewUnit(double[] values)
        {
            for (int i = 0; i < values.Length; i++)
            {
                _massByColumnControls[i].Text = values[i] < 0.001 ? "" : values[i].ToString(CultureInfo.CurrentCulture);
            }
        }
    }
}
