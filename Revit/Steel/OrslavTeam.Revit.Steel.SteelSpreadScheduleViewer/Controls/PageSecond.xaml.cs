﻿using OrslavTeam.Revit.Steel.Axillary.Settings.SteelSpread;

namespace OrslavTeam.Revit.Steel.SteelSpreadScheduleViewer.Controls
{
    /// <summary>
    /// Interaction logic for PageSecond.xaml
    /// </summary>
    public partial class PageSecond
    {
        #region dimensions fields --------------------------------------------------------------------------------------

        public int ContentHeight;

        #endregion


        #region constructors -------------------------------------------------------------------------------------------

        public PageSecond()
        {
            InitializeComponent();
        }

        public PageSecond(SteelSpreadHeaderFull header, SteelSpreadObj dataContext, int pageNumber)
        {
            InitializeComponent();

            this.DataContext = dataContext;
            ContentHeight = dataContext.GetHeight - 85;

            this.Content.Children.Add(header);
            this.CurrentPageNumber.Text = pageNumber.ToString();
        }

        #endregion


        #region methods ------------------------------------------------------------------------------------------------

        public bool AddRow(ProductMixGroupControl row)
        {
            if (row.RowHeight > ContentHeight) return false;

            this.Content.Children.Add(row);
            ContentHeight -= row.RowHeight;

            return true;
        }

        public bool AddRow(SteelSpreadFooterControl row)
        {
            if (row.RowHeight > ContentHeight) return false;

            this.Content.Children.Add(row);
            ContentHeight -= row.RowHeight;

            return true;
        }

        #endregion
    }
}
