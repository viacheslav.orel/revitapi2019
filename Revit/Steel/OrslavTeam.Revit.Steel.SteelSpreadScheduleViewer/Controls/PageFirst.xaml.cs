﻿using System.Windows.Controls;
using OrslavTeam.Revit.Steel.Axillary.Settings.SteelSpread;

namespace OrslavTeam.Revit.Steel.SteelSpreadScheduleViewer.Controls
{
    /// <summary>
    /// Interaction logic for PageFirst.xaml
    /// </summary>
    public partial class PageFirst
    {
        #region dimensions fields --------------------------------------------------------------------------------------

        public int ContentHeight;

        #endregion


        #region constructors -------------------------------------------------------------------------------------------

        public PageFirst()
        {
            InitializeComponent();
        }

        public PageFirst(UserControl header, SteelSpreadObj dataContext)
        {
            InitializeComponent();

            this.DataContext = dataContext;
            ContentHeight = dataContext.GetHeight - 125;
            this.SheetContent.Children.Add(header);
        }

        #endregion


        #region methods ------------------------------------------------------------------------------------------------

        public bool AddRow(ProductMixGroupControl row)
        {
            if (row.RowHeight > ContentHeight) return false;

            this.SheetContent.Children.Add(row);
            ContentHeight -= row.RowHeight;

            return true;
        }
        
        public bool AddRow(SteelSpreadFooterControl row)
        {
            if (row.RowHeight > ContentHeight) return false;

            this.SheetContent.Children.Add(row);
            ContentHeight -= row.RowHeight;

            return true;
        }

        #endregion
    }
}
