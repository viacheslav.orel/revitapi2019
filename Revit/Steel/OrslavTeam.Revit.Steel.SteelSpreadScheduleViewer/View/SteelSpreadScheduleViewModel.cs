﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using Newtonsoft.Json;
using OrslavTeam.Revit.Axillary.ViewTemplate.Infrastructure;
using OrslavTeam.Revit.Steel.Axillary.Settings.SteelSpread;
using OrslavTeam.Revit.Steel.Axillary.Settings.SteelSpreadSchedule;
using OrslavTeam.Revit.Steel.SteelSpreadScheduleViewer.Controls;

namespace OrslavTeam.Revit.Steel.SteelSpreadScheduleViewer.View
{
    public class SteelSpreadScheduleViewModel : ObservableObject
    {
        #region scheduleDataCollection ---------------------------------------------------------------------------------

        private ObservableCollection<KeyValuePair<string, string>> _scheduleDataCollection;

        public ObservableCollection<KeyValuePair<string, string>> ScheduleDataCollection
        {
            get => _scheduleDataCollection;
            set
            {
                _scheduleDataCollection = value;
                RaisePropertyChangedEvent("ScheduleDataCollection");
            }
        }


        private KeyValuePair<string, string> _scheduleDataCollectionSelectedItem;

        public KeyValuePair<string, string> ScheduleDataCollectionSelectedItem
        {
            get => _scheduleDataCollectionSelectedItem;
            set
            {
                _scheduleDataCollectionSelectedItem = value;
                RaisePropertyChangedEvent("ScheduleDataCollectionSelectedItem");
                GenerateSchedule(value.Value);
            }
        }


        private ObservableCollection<UserControl> _pages;

        public ObservableCollection<UserControl> Pages
        {
            get => _pages ?? (_pages = new ObservableCollection<UserControl>());
            set
            {
                _pages = value;
                RaisePropertyChangedEvent("Pages");
            }
        }


        private UserControl _currentPage;

        public UserControl CurrentPage
        {
            get => _currentPage;
            set
            {
                _currentPage = value;
                RaisePropertyChangedEvent("CurrentPage");
            }
        }


        private ObservableCollection<int> _pageNumbers;

        public ObservableCollection<int> PageNumbers
        {
            get => _pageNumbers ?? (_pageNumbers = new ObservableCollection<int>());
            set
            {
                _pageNumbers = value;
                RaisePropertyChangedEvent("PageNumbers");
            }
        }


        private FixedDocument _schedule;

        public FixedDocument Schedule
        {
            get => _schedule;
            set
            {
                _schedule = value;
                RaisePropertyChangedEvent("Schedule");
            }
        }


        private int _currentPageNumber;

        public int CurrentPageNumber
        {
            get => _currentPageNumber;
            set
            {
                _currentPageNumber = value;
                RaisePropertyChangedEvent("CurrentPageNumber");

                CurrentPage = Pages[value - 1];
            }
        }


        private string[] _unitCollection = { "кг", "т" };

        public string[] UnitCollection
        {
            get => _unitCollection;
            set
            {
                _unitCollection = value;
                RaisePropertyChangedEvent("UnitCollection");
            }
        }


        private string _unitCurrent;

        public string UnitCurrent
        {
            get => _unitCurrent ?? (_unitCurrent = _unitCollection[0]);
            set
            {
                _unitCurrent = value;
                RaisePropertyChangedEvent(UnitCurrent);

                ProfileGroupControl.Units = UnitCurrent;
                SteelSpreadDataContext.UnitType = value;
            }
        }


        private double _round = 1;

        public double Rounding
        {
            get => _round;
            set
            {
                _round = value;
                ProfileGroupControl.RoundingAccuracy = value;
                RaisePropertyChangedEvent("Rounding");
            }
        }


        #endregion


        #region shceduleRows -------------------------------------------------------------------------------------------

        public static SteelSpreadObj SteelSpreadDataContext = new SteelSpreadObj();
        public int ColumnQuantity;

        public List<ProductMixGroupControl> ProductMixRows;

        public SteelSpreadFooterControl FooterControl;

        #endregion


        #region contructors --------------------------------------------------------------------------------------------

        public SteelSpreadScheduleViewModel()
        {
        }

        public SteelSpreadScheduleViewModel(string schedulePath)
        {
            try
            {
                IEnumerable<KeyValuePair<string, string>> dataFiles = Directory.EnumerateFiles(schedulePath, "SteelSpreadSchedule*.json")
                    .Select(file => new KeyValuePair<string, string>(Regex.Replace(file.Substring(schedulePath.Length + 1), ".json", ""), file));

                ScheduleDataCollection = new ObservableCollection<KeyValuePair<string, string>>(dataFiles);
            }
            catch (Exception)
            {
                MessageBox.Show("Даних не виявлено");
            }
        }

        #endregion


        #region methods ------------------------------------------------------------------------------------------------

        private SshTable ReadData(string path)
        {
            SshTable steelSpreadData = null;

            try
            {
                using (var sr = new StreamReader(path))
                {
                    steelSpreadData = JsonConvert.DeserializeObject<SshTable>(sr.ReadToEnd());
                }
                ColumnQuantity = steelSpreadData.Columns.Length;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }

            

            return steelSpreadData;
        }


        private void GenerateSchedule(string path)
        {
            ClearAllData();

            ProfileGroupControl.Units = UnitCurrent;
            ProfileGroupControl.RoundingAccuracy = Rounding;

            SshTable schedule = ReadData(path);
            if (schedule == null) return;

            SteelSpreadDataContext = schedule.Header;

            SetCurrentFormat(125 + 15 * schedule.Columns.Length);

            IEnumerable<SshProductMix> orderedCollection = schedule.ProductMixCollection
                .OrderBy(productMix => productMix.Key)
                .Select(keyValuePair => keyValuePair.Value);

            GeneratePages(orderedCollection);

            CurrentPageNumber = _pageNumbers.FirstOrDefault();

            var fixedDocument = new FixedDocument();
            fixedDocument.DocumentPaginator.PageSize = new Size(SteelSpreadDataContext.FormatWidth, SteelSpreadDataContext.FormatHeight);

            foreach (UserControl page in Pages)
            {
                fixedDocument.Pages.Add(CreatePage(page));
            }

            Schedule = fixedDocument;
        }

        private void ClearAllData()
        {
            Pages = new ObservableCollection<UserControl>();
            PageNumbers = new ObservableCollection<int>();

            ProductMixRows = new List<ProductMixGroupControl>();
        }

        private static void SetCurrentFormat(int scheduleWidth)
        {
            if (scheduleWidth > 569)
            {
                SteelSpreadDataContext.FormatName = "А1";
                SteelSpreadDataContext.FormatWidth = 841;
                SteelSpreadDataContext.FormatHeight = 594;
            }
            else if (scheduleWidth > 395)
            {
                SteelSpreadDataContext.FormatName = "А2";
                SteelSpreadDataContext.FormatWidth = 594;
                SteelSpreadDataContext.FormatHeight = 420;
            }
            else if (scheduleWidth > 185)
            {
                SteelSpreadDataContext.FormatName = "А3";
                SteelSpreadDataContext.FormatWidth = 420;
                SteelSpreadDataContext.FormatHeight = 297;
            }
            else
            {
                SteelSpreadDataContext.FormatName = "А4";
                SteelSpreadDataContext.FormatWidth = 210;
                SteelSpreadDataContext.FormatHeight = 297;
            }
        }

        private void GeneratePages(IEnumerable<SshProductMix> sshProductMixes)
        {
            string[] columnsName = SteelSpreadDataContext.ColumnsInfo.Select(c => c.Header).ToArray();
            var header = new SteelSpreadHeaderFull(columnsName) {DataContext = SteelSpreadDataContext};

            FooterControl = new SteelSpreadFooterControl(SteelSpreadDataContext.SteelSpreadTotalMass, 
                SteelSpreadDataContext.SteelSpreadTotalMassBySteelName, ColumnQuantity);

            UserControl currentPage = new PageFirst(header, SteelSpreadDataContext);
            Pages.Add(currentPage);

            var pageNumber = 1;
            PageNumbers.Add(pageNumber);

            var rowNumber = 1;

            foreach (SshProductMix sshProductMix in sshProductMixes)
            {
                ProductMixGroupControl productMixRow = GenerateProductMix(sshProductMix, ref rowNumber);
                ProductMixRows.Add(productMixRow);

                switch (currentPage)
                {
                    case PageFirst firstPage when !firstPage.AddRow(productMixRow):
                        pageNumber++;
                        header = new SteelSpreadHeaderFull(columnsName) { DataContext = SteelSpreadDataContext };
                        currentPage = new PageSecond(header, SteelSpreadDataContext, pageNumber);

                        Pages.Add(currentPage);
                        PageNumbers.Add(pageNumber);

                        ((PageSecond) currentPage).AddRow(productMixRow);
                        break;
                    case PageSecond otherPage when !otherPage.AddRow(productMixRow):
                        pageNumber++;
                        header = new SteelSpreadHeaderFull(columnsName) { DataContext = SteelSpreadDataContext };
                        currentPage = new PageSecond(header, SteelSpreadDataContext, pageNumber);

                        Pages.Add(currentPage);
                        PageNumbers.Add(pageNumber);

                        ((PageSecond)currentPage).AddRow(productMixRow);
                        break;
                }
            }

            FooterControl.CalculateTotal(rowNumber);

            switch (currentPage)
            {
                case PageFirst firstPage when !firstPage.AddRow(FooterControl):
                    pageNumber++;
                    header = new SteelSpreadHeaderFull(columnsName) { DataContext = SteelSpreadDataContext };
                    currentPage = new PageSecond(header, SteelSpreadDataContext, pageNumber);

                    Pages.Add(currentPage);
                    PageNumbers.Add(pageNumber);

                    ((PageSecond)currentPage).AddRow(FooterControl);
                    break;
                case PageSecond otherPage when !otherPage.AddRow(FooterControl):
                    pageNumber++;
                    header = new SteelSpreadHeaderFull(columnsName) { DataContext = SteelSpreadDataContext };
                    currentPage = new PageSecond(header, SteelSpreadDataContext, pageNumber);

                    Pages.Add(currentPage);
                    PageNumbers.Add(pageNumber);

                    ((PageSecond)currentPage).AddRow(FooterControl);
                    break;
            }
        }


        private ProductMixGroupControl GenerateProductMix(SshProductMix sshProductMix, ref int rowNumber)
        {
            var productMixRow = new ProductMixGroupControl(sshProductMix.ProductMixName,
                SteelSpreadDataContext.SteelSpreadTotalMassByProductMix, ColumnQuantity);

            MaterialGroupControl.ProductMixCallBack pmCallBack = productMixRow.AddMass;
            MaterialGroupControl.TotalMassCallBack footerCallBack = FooterControl.AddMass;

            foreach (SshMaterial sshMaterial in sshProductMix.MaterialCollection.OrderBy(m => m.Key).Select(m => m.Value))
            {
                productMixRow.AddRow(GenerateMaterialRow(sshMaterial, pmCallBack, footerCallBack, ref rowNumber));
            }

            productMixRow.CalculateTotal(rowNumber++);

            return productMixRow;
        }

        private MaterialGroupControl GenerateMaterialRow(SshMaterial sshMaterial, 
            MaterialGroupControl.ProductMixCallBack pmCallBack, 
            MaterialGroupControl.TotalMassCallBack tmCallBack, ref int rowNumber)
        {
            var materialRow = new MaterialGroupControl(sshMaterial.MaterialName, 
                SteelSpreadDataContext.SteelSpreadTotalMassByMaterial, ColumnQuantity, pmCallBack, tmCallBack);

            ProfileGroupControl.MaterialCallBack materialCallBack = materialRow.AddMass;

            foreach (SshProfile sshProfile in sshMaterial.ProfileRows.OrderBy(p => p.Key).Select(p => p.Value))
            {
                materialRow.AddRow(GenerateProfileRow(sshProfile, materialCallBack, ref rowNumber));
            }

            materialRow.CalculateTotal(rowNumber++);

            return materialRow;
        }

        private static ProfileGroupControl GenerateProfileRow(SshProfile sshProfile,
            ProfileGroupControl.MaterialCallBack materialCallBack, ref int rowNumber)
        {
            var profileRow = new ProfileGroupControl(sshProfile.ProfileName, sshProfile.MassByColumn, rowNumber++, materialCallBack);

            return profileRow;
        }


        private void RecalculatedMass(object obj)
        {
            FooterControl.ClearData();

            foreach (ProductMixGroupControl pmRow in ProductMixRows)
            {
                pmRow.RecalculateTotal();
            }

            FooterControl.RecalculateTotal();
        }



        private PageContent CreatePage(UIElement content)
        {
            var pageContent = new PageContent();
            var fixedPage = new FixedPage
            {
                Width = SteelSpreadDataContext.FormatWidth,
                Height = SteelSpreadDataContext.FormatHeight
            };

            fixedPage.Children.Add(content);

            ((IAddChild) pageContent).AddChild(fixedPage);

            return pageContent;
        }

        #endregion


        #region command ------------------------------------------------------------------------------------------------

        private DelegateCommand _recalculate;

        public ICommand Recalculate => _recalculate ?? (_recalculate = new DelegateCommand(RecalculatedMass));


        private DelegateCommand _print;

        public ICommand PrintCommand => _print ?? (_print = new DelegateCommand(Print));

        private void Print(object obj)
        {
            var fixedDocument = new FixedDocument();
            fixedDocument.DocumentPaginator.PageSize = new Size(SteelSpreadDataContext.FormatWidth, SteelSpreadDataContext.FormatHeight);

            foreach (UserControl page in Pages)
            {
                fixedDocument.Pages.Add(CreatePage(page));
            }
        }

        #endregion
    }
}