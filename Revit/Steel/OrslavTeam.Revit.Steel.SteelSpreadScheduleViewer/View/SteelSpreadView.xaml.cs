﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;

namespace OrslavTeam.Revit.Steel.SteelSpreadScheduleViewer.View
{
    /// <summary>
    /// Interaction logic for SteelSpreadView.xaml
    /// </summary>
    public partial class SteelSpreadView : Window
    {
        public SteelSpreadView()
        {
            InitializeComponent();

            var FBD = new FolderBrowserDialog();
            if (FBD.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                DataContext = new SteelSpreadScheduleViewModel(FBD.SelectedPath);
            }
        }

        public SteelSpreadView(SteelSpreadScheduleViewModel viewModel = null)
        {
            InitializeComponent();
            DataContext = viewModel ?? new SteelSpreadScheduleViewModel();
        }
    }
}
