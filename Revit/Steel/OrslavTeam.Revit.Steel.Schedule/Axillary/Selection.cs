﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Structure;
using Newtonsoft.Json;
using OrslavTeam.Revit.Axillary.PublicInformation;
using OrslavTeam.Revit.Steel.Axillary.DbElement;
using OrslavTeam.Revit.Steel.Axillary.Repository;
using OrslavTeam.Revit.Steel.Axillary.Settings.SteelSpread;
using OrslavTeam.Revit.Steel.Axillary.Settings.UsingType;

namespace OrslavTeam.Revit.Steel.Schedule.Axillary
{
    public static class Selection
    {
        private static readonly Type _familyInstance = typeof(FamilyInstance);
        private static readonly Type _floorType = typeof(Floor);
        private static readonly Type _structuralTruss = typeof(Truss);
        private static readonly Type _wall = typeof(Wall);


        // -------------------------------------------------------------------------------------------------------------

        public static List<SshElement> GetFloorComponents(Document doc, ElementId phaseId,
            SteelSpreadColumn[] steelSpreadColumns, IReadOnlyList<FullUsingType> fullUsingTypes, ref List<string> report)
        {
            var result = new List<SshElement>();

            IEnumerable<Element> floors = new FilteredElementCollector(doc)
                .OfCategory(BuiltInCategory.OST_Floors)
                .OfClass(_floorType);

            foreach (Element floor in floors)
            {
                if (!IsCurrentFloor((Floor)floor)) continue;

                ElementId gutElementField = PublicParameter.GeneralUsingType(floor);
                ElementId subElementField = PublicParameter.SubUsingType(floor);
                UsingType gut;
                UsingType sut;

                if (gutElementField == ElementId.InvalidElementId)
                {
                    gut = new UsingType(1500, "15 Перекриття");
                    sut = new UsingType(0, "");
                }
                else
                {
                    gut = new UsingType(doc.GetElement(gutElementField));
                    sut = subElementField == ElementId.InvalidElementId
                        ? new UsingType(0, "")
                        : new UsingType(doc.GetElement(subElementField));
                }

                if (gut.Id == 2000) continue;

                FullUsingType fullUsingType = fullUsingTypes
                    .FirstOrDefault(fut => fut.GeneralUsingType.Id == gut.Id && fut.SubUsingType.Id == sut.Id);

                if (fullUsingType == null)
                {
                    report.Add(floor.Id.ToString());
                    continue;
                }
                
                int columnNumber = steelSpreadColumns
                                       .FirstOrDefault(column => column.Id == fullUsingType.SteelSpreadColumnId)
                                       ?.Point ?? -1;

                if (columnNumber == -1)
                {
                    report.Add(floor.Id.ToString());
                    continue;
                }

                Element floorType = ((Floor) floor).FloorType;

                var currentSshElement = new SshElement
                {
                    ProductMixId = PublicParameter.ProductMixKey(floorType),
                    ProfileId = PublicParameter.ElementKey(floorType),
                    MaterialId =  GetMaterialId(doc, floor),
                    Units = UnitUtils.ConvertFromInternalUnits(
                        floor.get_Parameter(BuiltInParameter.HOST_AREA_COMPUTED).AsDouble(),
                        DisplayUnitType.DUT_SQUARE_METERS),
                    Quantity = 1,
                    ColumnNumber = columnNumber
                };

                result.Add(currentSshElement);
            }

            return result;

            // ---------------------------------------------------------------------------------------------------------
            bool IsCurrentFloor(Floor floor)
            {
                if (floor.GetPhaseStatus(phaseId) != ElementOnPhaseStatus.New) return false;
                return PublicParameter.GetUniformatKode(floor.FloorType).StartsWith("B.02");
            }
        }

        public static List<SshElement> GetGenericModels(Document doc, ElementId phaseId,
            SteelSpreadColumn[] steelSpreadColumns, IReadOnlyList<FullUsingType> fullUsingTypes, ref List<string> report)
        {
            var result = new List<SshElement>();

            IEnumerable<Element> genericModels = new FilteredElementCollector(doc)
                .OfCategory(BuiltInCategory.OST_GenericModel)
                .OfClass(_familyInstance);

            foreach (Element genericModel in genericModels)
            {
                if (!IsCorrectFamilyInstance(genericModel, phaseId)) continue;

                Element genericModelFamily = ((FamilyInstance) genericModel).Symbol;


                ElementId gutElementField = PublicParameter.GeneralUsingType(genericModel);
                ElementId sutElementField = PublicParameter.SubUsingType(genericModel);
                UsingType gut;
                UsingType sut;

                if (gutElementField == ElementId.InvalidElementId)
                {
                    string uniformatKod = PublicParameter.GetUniformatKode(genericModelFamily);

                    switch (uniformatKod)
                    {
                        case "B.02.02.03.01":
                        case "B.02.02.03.02":
                        case "B.02.02.03":
                            gut = new UsingType(1000, "10 Сходи");
                            break;
                        case "B.02.03":
                            gut = new UsingType(1100, "11 Огорожа");
                            break;
                        default:
                            gut = new UsingType(0, "");
                            break;
                    }

                    sut = new UsingType();
                }
                else
                {
                    gut = new UsingType(doc.GetElement(gutElementField));
                    sut = sutElementField == ElementId.InvalidElementId
                        ? new UsingType()
                        : new UsingType(doc.GetElement(sutElementField));
                }

                if (gut.Id == 2000) continue;


                // Full using type -------------------------------------------------------------------------------------
                FullUsingType fullUsingType = fullUsingTypes
                    .FirstOrDefault(fut => fut.GeneralUsingType.Id == gut.Id && fut.SubUsingType.Id == sut.Id);

                if (fullUsingType == null)
                {
                    report.Add(genericModel.Id.ToString());
                    continue;
                }
                int currentColumnNumber = steelSpreadColumns
                                              .FirstOrDefault(column => column.Id == fullUsingType.SteelSpreadColumnId)
                                              ?.Point ?? -1;

                if (currentColumnNumber == -1)
                {
                    report.Add(genericModel.Id.ToString());
                    continue;
                }

                // Sub component
                int scheduleFilterPoint = PublicParameter.SpecificationFilter(genericModelFamily);
                if (scheduleFilterPoint == 0)
                {
                    ICollection<ElementId> subComponentIds = ((FamilyInstance) genericModel).GetSubComponentIds();
                    if (!subComponentIds.Any()) continue;

                    GetSshFromSubComponent(subComponentIds, currentColumnNumber, doc, ref result);

                    continue;
                }
                
                var currentSshElement = new SshElement
                {
                    ProductMixId = PublicParameter.ProductMixKey(genericModel),
                    ProfileId = PublicParameter.ElementKey(genericModel),
                    MaterialId = GetMaterialId(doc, genericModel),
                    ColumnNumber = currentColumnNumber,
                    Units = GetElementUnit(genericModel, genericModelFamily),
                    Quantity = PublicParameter.ElementQuantity(genericModelFamily)
                };

                result.Add(currentSshElement);
            }

            return result;
        }

        public static List<SshElement> GetStructuralColumns(Document doc, ElementId phaseId,
            SteelSpreadColumn[] steelSpreadColumns, IReadOnlyList<FullUsingType> fullUsingTypes, ref List<string> report)
        {
            var result = new List<SshElement>();

            IEnumerable<Element> structuralColumns = new FilteredElementCollector(doc)
                .OfCategory(BuiltInCategory.OST_StructuralColumns)
                .OfClass(_familyInstance);

            foreach (Element structuralColumn in structuralColumns)
            {
                if (!IsCorrectFamilyInstance(structuralColumn, phaseId)) continue;

                var element = (FamilyInstance) structuralColumn;
                FamilySymbol family = element.Symbol;

                ElementId gutElementField = PublicParameter.GeneralUsingType(element);
                UsingType gut;
                UsingType sut;

                if (gutElementField == ElementId.InvalidElementId)
                {
                    gut = new UsingType(100, "01 Каркас");
                    sut = new UsingType(100, "01 Колона");
                }
                else
                {
                    gut = new UsingType(doc.GetElement(gutElementField));
                    if (gut.Id == 2000) continue;

                    ElementId sutElementField = PublicParameter.SubUsingType(element);
                    sut = sutElementField == ElementId.InvalidElementId
                        ? new UsingType(0, "")
                        : new UsingType(doc.GetElement(sutElementField));
                }

                FullUsingType fullUsingType = fullUsingTypes
                    .FirstOrDefault(fut => fut.GeneralUsingType.Id == gut.Id && fut.SubUsingType.Id == sut.Id);

                if (fullUsingType == null)
                {
                    report.Add(structuralColumn.Id.ToString());
                    continue;
                }

                int currentColumnNumber = steelSpreadColumns
                                              .First(column => column.Id == fullUsingType.SteelSpreadColumnId)
                                              .Point;

                if (currentColumnNumber == -1)
                {
                    report.Add(element.Id.ToString());
                    continue;
                }

                int materialId = GetMaterialId(doc, element);
                double unit = PublicParameter.DimensionLength(element) / 1000.0;
                int quantity = PublicParameter.ElementQuantity(family);

                if (PublicParameter.ElementGuid(family) == OtIBeam.Guid)
                {
                    GetSshElementFromComplexIBeam(family, materialId, currentColumnNumber, unit, quantity, ref result);
                    continue;
                }

                ICollection<ElementId> subElementIds = element.GetSubComponentIds();
                if (subElementIds.Any())
                {
                    GetSshFromSubComponent(subElementIds, currentColumnNumber, doc, ref result);
                }

                int scheduleFilterPoint = PublicParameter.SpecificationFilter(family);
                if (scheduleFilterPoint == 0) continue;

                var currentElement = new SshElement
                {
                    ProductMixId = PublicParameter.ProductMixKey(family),
                    ProfileId = PublicParameter.ElementKey(family),
                    MaterialId = materialId,
                    ColumnNumber = currentColumnNumber,
                    Units = unit,
                    Quantity = quantity
                };

                result.Add(currentElement);
            }


            return result;
        }

        public static List<SshElement> GetStructuralFraming(Document doc, ElementId phaseId,
            SteelSpreadColumn[] steelSpreadColumns, IReadOnlyList<FullUsingType> fullUsingTypes, ref List<string> report)
        {
            var result = new List<SshElement>();

            IEnumerable<Element> structuralFramingCollection = new FilteredElementCollector(doc)
                .OfCategory(BuiltInCategory.OST_StructuralFraming)
                .OfClass(_familyInstance);

            foreach (Element structuralFraming in structuralFramingCollection)
            {
                if (!IsCorrectFamilyInstance(structuralFraming, phaseId)) continue;

                var element = (FamilyInstance) structuralFraming;

                StructuralInstanceUsage structuralUsage = element.StructuralUsage;
                if (structuralUsage == StructuralInstanceUsage.TrussChord
                    || structuralUsage == StructuralInstanceUsage.TrussWeb) continue;

                FamilySymbol family = element.Symbol;

                ElementId gutElementField = PublicParameter.GeneralUsingType(element);
                UsingType gut;
                UsingType sut;

                if (gutElementField == ElementId.InvalidElementId)
                {
                    // ReSharper disable once SwitchStatementMissingSomeCases
                    switch (structuralUsage)
                    {
                        case StructuralInstanceUsage.Brace:
                            gut = new UsingType(100, "01 Каркас");
                            sut = new UsingType(400, "04 В'язь.Вертикальна");
                            break;
                        case StructuralInstanceUsage.HorizontalBracing:
                            gut = new UsingType(200, "02 Майданчик");
                            sut = new UsingType(300, "03 В'язь.Горизонтальна");
                            break;
                        default:
                        {
                            if (PublicParameter.GetUniformatKode(family) == "B.02.02.02.03")
                            {
                                gut = new UsingType(500, "05 Вантажне");
                                sut = new UsingType(0, "");
                            }
                            else
                            {
                                gut = new UsingType(200, "02 Майданчик");
                                sut = new UsingType(200, "02 Балка");
                            }

                            break;
                        }
                    }
                }
                else
                {
                    gut = new UsingType(doc.GetElement(gutElementField));

                    ElementId sutElementField = PublicParameter.SubUsingType(element);
                    sut = sutElementField == gutElementField
                        ? new UsingType(0, "")
                        : new UsingType(doc.GetElement(sutElementField));
                }

                FullUsingType fullUsingType = fullUsingTypes
                    .FirstOrDefault(fut => fut.GeneralUsingType.Id == gut.Id && fut.SubUsingType.Id == sut.Id);

                if (fullUsingType == null)
                {
                    report.Add(structuralFraming.Id.ToString());
                    continue;
                }

                int currentColumnNumber = steelSpreadColumns
                                              .FirstOrDefault(column => column.Id == fullUsingType.SteelSpreadColumnId)
                                              ?.Point ?? -1;

                if (currentColumnNumber == -1)
                {
                    report.Add(element.Id.ToString());
                    continue;
                }

                int materialId = GetMaterialId(doc, element);
                double unit = PublicParameter.DimensionLength(element) / 1000.0;
                int quantity = PublicParameter.ElementQuantity(family);

                if (PublicParameter.ElementGuid(family) == OtIBeam.Guid)
                {
                    GetSshElementFromComplexIBeam(family, materialId, currentColumnNumber, unit, quantity, ref result);
                    continue;
                }

                ICollection<ElementId> subElementIds = element.GetSubComponentIds();
                if (subElementIds.Any())
                {
                    GetSshFromSubComponent(subElementIds, currentColumnNumber, doc, ref result);
                }

                int scheduleFilterPoint = PublicParameter.SpecificationFilter(family);
                if (scheduleFilterPoint == 0) continue;

                var currentElement = new SshElement
                {
                    ProductMixId = PublicParameter.ProductMixKey(family),
                    ProfileId = PublicParameter.ElementKey(family),
                    MaterialId = materialId,
                    ColumnNumber = currentColumnNumber,
                    Units = unit,
                    Quantity = quantity
                };

                result.Add(currentElement);
            }

            return result;
        }

        public static List<SshElement> GetStructuralTrusses(Document doc, ElementId phaseId,
            SteelSpreadColumn[] steelSpreadColumns, IReadOnlyList<FullUsingType> fullUsingTypes, ref List<string> report)
        {
            var result = new List<SshElement>();

            IEnumerable<Element> structuralTrusses = new FilteredElementCollector(doc)
                .OfCategory(BuiltInCategory.OST_StructuralTruss)
                .OfClass(_structuralTruss);

            foreach (Element structuralTruss in structuralTrusses)
            {
                if (structuralTruss.GetPhaseStatus(phaseId) != ElementOnPhaseStatus.New) continue;

                var element = (Truss) structuralTruss;
                TrussType family = element.TrussType;

                if (PublicParameter.GetUniformatKode(family).StartsWith("B.02")) continue;

                ElementId gutElementField = PublicParameter.GeneralUsingType(element);
                UsingType gut;
                UsingType sut;

                if (gutElementField == ElementId.InvalidElementId)
                {
                    gut = new UsingType(100, "01 Каркас");
                    sut = new UsingType(500, "05 Ферма");
                }
                else
                {
                    gut = new UsingType(doc.GetElement(gutElementField));

                    ElementId sutElementField = PublicParameter.SubUsingType(element);
                    sut = sutElementField == ElementId.InvalidElementId
                        ? new UsingType(0, "")
                        : new UsingType(doc.GetElement(sutElementField));
                }

                FullUsingType fullUsingType = fullUsingTypes
                    .FirstOrDefault(fut => fut.GeneralUsingType.Id == gut.Id && fut.SubUsingType.Id == sut.Id);

                if (fullUsingType == null)
                {
                    report.Add(structuralTruss.Id.ToString());
                    continue;
                }

                int currentColumnNumber = steelSpreadColumns
                                              .First(column => column.Id == fullUsingType.SteelSpreadColumnId)
                                              ?.Point ?? -1;

                if (currentColumnNumber == -1)
                {
                    report.Add(element.Id.ToString());
                    continue;
                }

                ICollection<ElementId> membersIds = element.Members;

                GetSshFromSubComponent(membersIds, currentColumnNumber, doc, ref result);
            }

            return result;
        }

        public static List<SshElement> GetWalls(Document doc, ElementId phaseId,
            SteelSpreadColumn[] steelSpreadColumns, IReadOnlyList<FullUsingType> fullUsingTypes, ref List<string> report)
        {
            var result = new List<SshElement>();

            IEnumerable<Element> walls = new FilteredElementCollector(doc)
                .OfCategory(BuiltInCategory.OST_Walls)
                .OfClass(_wall);

            foreach (Element wall in walls)
            {
                if (wall.GetPhaseStatus(phaseId) != ElementOnPhaseStatus.New) continue;

                var element = (Wall) wall;
                WallType family = element.WallType;

                if (!PublicParameter.GetUniformatKode(family).StartsWith("B.02")) continue;

                ElementId gutElementField = PublicParameter.GeneralUsingType(element);
                UsingType gut;
                UsingType sut;

                if (gutElementField == ElementId.InvalidElementId)
                {
                    gut = new UsingType(1600, "16 Стіни");
                    sut = new UsingType(0, "");
                }
                else
                {
                    gut = new UsingType(doc.GetElement(gutElementField));

                    ElementId sutElementField = PublicParameter.SubUsingType(element);
                    sut = sutElementField == ElementId.InvalidElementId
                        ? new UsingType(0, "")
                        : new UsingType(doc.GetElement(sutElementField));
                }

                FullUsingType fullUsingType = fullUsingTypes
                    .FirstOrDefault(fut => fut.GeneralUsingType.Id == gut.Id && fut.SubUsingType.Id == sut.Id);

                if (fullUsingType == null)
                {
                    report.Add(wall.Id.ToString());
                    continue;
                }

                int currentColumnNumber = steelSpreadColumns
                                              .FirstOrDefault(column => column.Id == fullUsingType.SteelSpreadColumnId)
                                              ?.Point ?? -1;

                if (currentColumnNumber == -1)
                {
                    report.Add(element.Id.ToString());
                    continue;
                }

                var currentComponent = new SshElement
                {
                    ProductMixId = PublicParameter.ProductMixKey(family),
                    ProfileId = PublicParameter.ElementKey(family),
                    MaterialId = GetMaterialId(doc, element),
                    ColumnNumber = currentColumnNumber,
                    Units = UnitUtils.ConvertFromInternalUnits(
                        element.get_Parameter(BuiltInParameter.HOST_AREA_COMPUTED).AsDouble(),
                        DisplayUnitType.DUT_SQUARE_METERS),
                    Quantity = 1
                };

                result.Add(currentComponent);
            }

            return result;
        }

        public static List<SshElement> GetStructuralConnection(Document doc, ElementId phaseId,
            SteelSpreadColumn[] steelSpreadColumns, IReadOnlyList<FullUsingType> fullUsingTypes, ref List<string> report)
        {
            var result = new List<SshElement>();

            IEnumerable<Element> structuralConnections = new FilteredElementCollector(doc)
                .OfCategory(BuiltInCategory.OST_StructConnections)
                .OfClass(_familyInstance);

            foreach (Element structuralConnection in structuralConnections)
            {
                if (!IsCorrectFamilyInstance(structuralConnection, phaseId)) continue;

                ElementId gutElementId = PublicParameter.GeneralUsingType(structuralConnection);
                ElementId sutElementId = PublicParameter.SubUsingType(structuralConnection);

                if (gutElementId == ElementId.InvalidElementId) continue;

                var gut = new UsingType(doc.GetElement(gutElementId));
                UsingType sut = sutElementId == ElementId.InvalidElementId
                    ? new UsingType(0, "")
                    : new UsingType(doc.GetElement(sutElementId));

                FullUsingType fullUsingType = fullUsingTypes
                    .FirstOrDefault(fut => fut.GeneralUsingType.Id == gut.Id && fut.SubUsingType.Id == sut.Id);

                if (fullUsingType == null)
                {
                    report.Add(structuralConnection.Id.ToString());
                    continue;
                }

                int currentColumnNumber = steelSpreadColumns
                                              .FirstOrDefault(column => column.Id == fullUsingType.SteelSpreadColumnId)
                                              ?.Point ?? -1;

                if (currentColumnNumber == -1)
                {
                    report.Add(structuralConnection.Id.ToString());
                    continue;
                }

                var element = (FamilyInstance) structuralConnection;
                FamilySymbol family = element.Symbol;

                ICollection<ElementId> subElementIds = element.GetSubComponentIds();
                if (subElementIds.Any())
                {
                    GetSshFromSubComponent(subElementIds, currentColumnNumber, doc, ref result);
                }

                int scheduleFilterPoint = PublicParameter.SpecificationFilter(family);
                if (scheduleFilterPoint == 0) continue;

                var currentUsingType = new SshElement
                {
                    ProductMixId = PublicParameter.ProductMixKey(family),
                    ProfileId = PublicParameter.ElementKey(family),
                    MaterialId = GetMaterialId(doc, element),
                    ColumnNumber = currentColumnNumber,
                    Units = GetElementUnit(element, family),
                    Quantity = PublicParameter.ElementQuantity(family)
                };

                result.Add(currentUsingType);
            }

            return result;
        }


        // -------------------------------------------------------------------------------------------------------------

        private static bool IsCorrectFamilyInstance(Element element, ElementId phaseId)
        {
            if (element.GetPhaseStatus(phaseId) != ElementOnPhaseStatus.New) return false;
            return PublicParameter.GetUniformatKode(((FamilyInstance)element).Symbol).StartsWith("B.02");
        }

        private static int GetMaterialId(Document doc, Element element)
        {
            ICollection<ElementId> materialIds = element.GetMaterialIds(false);

            var materialKod = 0;

            foreach (ElementId materialId in materialIds)
            {
                Element material = doc.GetElement(materialId);

                materialKod = PublicParameter.MaterialKod(material);
                
                if (materialKod > 0) break;
            }

            return materialKod;
        }

        private static double GetElementUnit(Element element, Element family)
        {
            int unitType = PublicParameter.MassCountingType(family);

            switch (unitType)
            {
                case 1:
                    return 1;
                case 2:
                    return PublicParameter.ElementValue(element);
                case 3:
                    return PublicParameter.SectionArea(element);
                case 4:
                    return PublicParameter.DimensionLength(element) / 1000.0;
                default:
                    return 666666666;
            }
        }

        private static void GetSshFromSubComponent(ICollection<ElementId> elementIds, int currentColumnNumber, 
            Document doc, ref List<SshElement> result)
        {
            foreach (ElementId elementId in elementIds)
            {
                var element = (FamilyInstance)doc.GetElement(elementId);
                FamilySymbol family = element.Symbol;

                int materialId = GetMaterialId(doc, element);
                double unit = GetElementUnit(element, family);
                int quantity = PublicParameter.ElementQuantity(element);

                if (PublicParameter.ElementGuid(family) == OtIBeam.Guid)
                {
                    GetSshElementFromComplexIBeam(family, materialId, currentColumnNumber, unit, quantity, ref result);
                    continue;
                }

                int scheduleFilterPoint = PublicParameter.SpecificationFilter(family);
                if (scheduleFilterPoint == 0)
                {
                    ICollection<ElementId> subComponentIds = element.GetSubComponentIds();
                    if (!subComponentIds.Any()) continue;

                    GetSshFromSubComponent(subComponentIds, currentColumnNumber, doc, ref result);
                    continue;
                }

                var currentComponent = new SshElement
                {
                    ProductMixId = PublicParameter.ProductMixKey(element),
                    ProfileId = PublicParameter.ElementKey(element),
                    MaterialId = materialId,
                    ColumnNumber = currentColumnNumber,
                    Units = unit,
                    Quantity = quantity
                };

                result.Add(currentComponent);
            }
        }

        private static void GetSshElementFromComplexIBeam(FamilySymbol family, int materialId, int column, double unit,
            int quantity, ref List<SshElement> result)
        {
            string getItString = PublicParameter.GetIt(family);
            var beamComponent = JsonConvert.DeserializeObject<OtIBeam>(getItString);

            result.Add(new SshElement
            {
                ProductMixId = beamComponent.BeamWeb.ProductMix.Id,
                ProfileId = beamComponent.BeamWeb.Profile.Id,
                MaterialId = materialId,
                ColumnNumber = column,
                Units = unit,
                Quantity = quantity
            });

            result.Add(new SshElement
            {
                ProductMixId = beamComponent.BeamChord.ProductMix.Id,
                ProfileId = beamComponent.BeamChord.Profile.Id,
                MaterialId = materialId,
                Units = unit,
                ColumnNumber = column,
                Quantity = quantity * 2
            });
        }
    }
}