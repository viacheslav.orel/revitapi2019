﻿using System.Collections.Generic;
using System.Globalization;
using Autodesk.Revit.DB;

namespace OrslavTeam.Revit.Steel.Schedule.Repository
{
    public static class SshHelper
    {
        #region ConstantFields -----------------------------------------------------------------------------------------

        public const string SCHEDULE_VIEW_NAME = "Специфікація металопрокату";
        public const string SCHEDULE_TYPE_NAME = "99_Специфікація";

        #endregion

        #region DocumentFields -----------------------------------------------------------------------------------------

        public static Document Doc;
        public static ViewDrafting View;
        public static FamilySymbol CellDetail;
        public static int ColumnQuantity;

        public static Dictionary<string, double[]> TotalMassByMaterialByColumn;

        #endregion

        #region StaticProperties ---------------------------------------------------------------------------------------

        public static readonly double BaseWidth =
            UnitUtils.ConvertToInternalUnits(15, DisplayUnitType.DUT_MILLIMETERS);

        public static readonly double BaseHeight =
            UnitUtils.ConvertToInternalUnits(8, DisplayUnitType.DUT_MILLIMETERS);

        #endregion

        #region RenderMethods ------------------------------------------------------------------------------------------

        public static (double, double, int) RenderCell((double, double, int) startPoint, double content)
        {
            var startPosition = new XYZ(startPoint.Item1, startPoint.Item2, 0);
            string cellContent = content < 0.001 ? "" : content.ToString(CultureInfo.InvariantCulture);

            CreateCell(startPosition, View, CellDetail, cellContent, BaseWidth, BaseHeight);

            startPoint.Item1 += BaseWidth;

            return startPoint;
        }

        public static (double, double, int) RenderCell((double, double, int) startPoint, double content, int width, double height = 0)
        {
            var startPosition = new XYZ(startPoint.Item1, startPoint.Item2, 0);
            string cellContent = content < 0.001 ? "" : content.ToString(CultureInfo.InvariantCulture);
            double cellWidth = UnitUtils.ConvertToInternalUnits(width, DisplayUnitType.DUT_MILLIMETERS);
            double cellHeight = height < 1 ? BaseHeight : height;

            CreateCell(startPosition, View, CellDetail, cellContent, cellWidth, cellHeight);

            startPoint.Item1 += BaseWidth;

            return startPoint;
        }

        public static (double, double, int) RenderCell((double, double, int) startPoint, string content, int width, double height = 0)
        {
            var startPosition = new XYZ(startPoint.Item1, startPoint.Item2, 0);
            double cellWidth = UnitUtils.ConvertToInternalUnits(width, DisplayUnitType.DUT_MILLIMETERS);
            double cellHeight = height < 1 ? BaseHeight : height;

            CreateCell(startPosition, View, CellDetail, content, cellWidth, cellHeight);

            startPoint.Item1 += BaseWidth;

            return startPoint;
        }

        private static void CreateCell(
            XYZ position, ViewDrafting view, FamilySymbol cell, string content, double width, double height)
        {
            FamilyInstance elementCell = Doc.Create.NewFamilyInstance(position, cell, view);
            elementCell.LookupParameter("Content").Set(content);
            elementCell.LookupParameter("Dimensions.Height").Set(height);
            elementCell.LookupParameter("Dimesions.Width").Set(width);
        }

        #endregion
    }
}