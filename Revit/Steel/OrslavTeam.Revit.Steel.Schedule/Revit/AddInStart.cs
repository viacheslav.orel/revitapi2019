﻿using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using OrslavTeam.Revit.Steel.Schedule.View;

namespace OrslavTeam.Revit.Steel.Schedule.Revit
{
    [Transaction(TransactionMode.Manual)]
    public class AddInStart : IExternalCommand
    {
        public Result Execute(ExternalCommandData commandData, ref string message, Autodesk.Revit.DB.ElementSet elements)
        {
            Document doc = commandData.Application.ActiveUIDocument.Document;

            var dataContext = new MainWindowDataContext(doc);
            var view = new MainWindow { DataContext = dataContext };
            view.ShowDialog();

            return Result.Succeeded;
        }
    }
}
