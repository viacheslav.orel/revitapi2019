﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Autodesk.Revit.DB;
using Newtonsoft.Json;
using OrslavTeam.Revit.Axillary.PublicInformation;
using OrslavTeam.Revit.Axillary.ViewTemplate.Infrastructure;
using OrslavTeam.Revit.Axillary.ViewTemplate.ReportWindow;
using OrslavTeam.Revit.Steel.Axillary.Settings.SteelSpread;
using OrslavTeam.Revit.Steel.Axillary.Settings.SteelSpreadSchedule;
using OrslavTeam.Revit.Steel.Axillary.Settings.SteelStorage;
using OrslavTeam.Revit.Steel.Schedule.Axillary;

namespace OrslavTeam.Revit.Steel.Schedule.View
{
    public class MainWindowDataContext : ObservableObject
    {
        #region Constructors -------------------------------------------------------------------------------------------

        public MainWindowDataContext(Document doc)
        {
            _doc = doc;

            GetStorage();
            Phases = GetPhaseFromModel();
        }

        #endregion


        #region BaseDependency -----------------------------------------------------------------------------------------

        private readonly Document _doc;
        private static List<string> _report = new List<string>{"Наступним елементам не призначене використання:"};

        #endregion


        #region settings -----------------------------------------------------------------------------------------------

        // fields
        private SteelSettingsStorage _steelSettingsStorage;
        private SteelSpreadObj _settings;


        // fields access
        public SteelSpreadObj Settings
        {
            get => _settings ?? (_settings = new SteelSpreadObj());
            set
            {
                _settings = value;
                RaisePropertyChangedEvent("Settings");
            }
        }


        // methods
        private void GetStorage()
        {
            using (var transaction = new Transaction(_doc, "GetStorage"))
            {
                transaction.Start();

                _steelSettingsStorage = new SteelSettingsStorage(_doc);

                transaction.Commit();
            }
        }


        // command
        private DelegateCommand _saveSettings;

        public ICommand SaveSettings => _saveSettings ?? (_saveSettings = new DelegateCommand(SaveSettingsCommand));

        private void SaveSettingsCommand(object obj)
        {
            SetFullUsingType();

            using (var transaction = new Transaction(_doc, "SaveSteelProperties"))
            {
                transaction.Start();

                _steelSettingsStorage.SetSteelSettings(CurrentPhase.Id.IntegerValue, _settings);

                transaction.Commit();
            }
        }

        #endregion


        #region Phases -------------------------------------------------------------------------------------------------

        // fields
        private List<Phase> _phases;
        private Phase _currentPhase;


        // fields access
        public List<Phase> Phases
        {
            get => _phases;
            set
            {
                _phases = value;
                RaisePropertyChangedEvent("Phases");
                CurrentPhase = _phases.FirstOrDefault();
            }
        }

        public Phase CurrentPhase
        {
            get => _currentPhase;
            set
            {
                _currentPhase = value;
                RaisePropertyChangedEvent("CurrentPhase");
            }
        }


        // methods
        private List<Phase> GetPhaseFromModel()
        {
            var result = new List<Phase>();

            PhaseArray phaseArray = _doc.Phases;

            foreach (object obj in phaseArray)
            {
                result.Add(obj as Phase);
            }

            return result;
        }


        // command
        private DelegateCommand _setPhase;

        public ICommand SetPhase => _setPhase ?? (_setPhase = new DelegateCommand(SetPhaseCommand));

        private void SetPhaseCommand(object obj)
        {
            int phaseId = CurrentPhase.Id.IntegerValue;

            Settings = _steelSettingsStorage.GetSteelSettings(phaseId);

            if (_settings == null)
            {
                MessageBox.Show("Ви не виконали маркування на обраній стадії");
                return;
            }

            SteelSpreadColumnCollection = _settings.ColumnsInfo != null
                ? new ObservableCollection<SteelSpreadColumn>(_settings.ColumnsInfo)
                : new ObservableCollection<SteelSpreadColumn>();

            if (_settings.ColumnsInfo != null && _settings.ColumnsInfo.Length > 0)
            {
                SettingRow.Columns = _settings.ColumnsInfo;
            }

            SettingRows = 
                new ObservableCollection<SettingRow>(_settings.FullUsingTypes.Select(item => new SettingRow(item)));
        }

        #endregion


        #region FullUsingType ------------------------------------------------------------------------------------------

        // methods

        private void SetFullUsingType()
        {
            _settings.FullUsingTypes = SettingRows.Select(row => row.GetFullUsingType()).ToArray();
        }

        #endregion


        #region SteelColumn --------------------------------------------------------------------------------------------

        // fields

        private ObservableCollection<SteelSpreadColumn> _steelSpreadColumnCollection;


        // field access

        public ObservableCollection<SteelSpreadColumn> SteelSpreadColumnCollection
        {
            get => _steelSpreadColumnCollection;
            set
            {
                _steelSpreadColumnCollection = value;
                RaisePropertyChangedEvent("SteelSpreadColumnCollection");
            }
        }


        // command

        private DelegateCommand _applySteelSpreadColumns;

        public ICommand ApplySteelSpreadColumns =>
            _applySteelSpreadColumns ?? (_applySteelSpreadColumns = new DelegateCommand(ApplySteelSpreadColumnsCommand));

        private void ApplySteelSpreadColumnsCommand(object obj)
        {
            _settings.ColumnsInfo = SteelSpreadColumnCollection.OrderBy(column => column.Point).ToArray();

            if (SettingRows == null) return;

            SettingRow.Columns = _settings.ColumnsInfo;

            foreach (SettingRow row in SettingRows)
            {
                row.RaiseColumnsChanged();
            }
        }

        #endregion


        #region SettingRows --------------------------------------------------------------------------------------------

        // fields

        private ObservableCollection<SettingRow> _settingRows;


        // fields access

        public ObservableCollection<SettingRow> SettingRows
        {
            get => _settingRows;
            set
            {
                _settingRows = value;
                RaisePropertyChangedEvent("SettingRows");
            }
        }

        #endregion


        #region RenderSteelSpreadSchedule ------------------------------------------------------------------------------

        // command

        private DelegateCommand _renderSteelSpreadSchedule;

        public ICommand RenderSteelSpreadSchedule => _renderSteelSpreadSchedule ??
            (_renderSteelSpreadSchedule = new DelegateCommand(RenderSteelSpreadScheduleCommand));

        private void RenderSteelSpreadScheduleCommand(object obj)
        {
            var steelSpreadSchedule = new SshTable( _settings);

            steelSpreadSchedule.AddElements(Selection.GetFloorComponents(_doc, CurrentPhase.Id, _settings.ColumnsInfo, _settings.FullUsingTypes, ref _report));
            steelSpreadSchedule.AddElements(Selection.GetGenericModels(_doc, CurrentPhase.Id, _settings.ColumnsInfo, _settings.FullUsingTypes, ref _report));
            steelSpreadSchedule.AddElements(Selection.GetStructuralColumns(_doc, CurrentPhase.Id, _settings.ColumnsInfo, _settings.FullUsingTypes, ref _report));
            steelSpreadSchedule.AddElements(Selection.GetStructuralConnection(_doc, CurrentPhase.Id, _settings.ColumnsInfo, _settings.FullUsingTypes, ref _report));
            steelSpreadSchedule.AddElements(Selection.GetStructuralFraming(_doc, CurrentPhase.Id, _settings.ColumnsInfo, _settings.FullUsingTypes, ref _report));
            steelSpreadSchedule.AddElements(Selection.GetStructuralTrusses(_doc, CurrentPhase.Id, _settings.ColumnsInfo, _settings.FullUsingTypes, ref _report));

            string path = PublicParameter.DocumentPath(_doc) + "\\res";
            string fileName = $"SteelSpreadSchedule_{CurrentPhase.Name}_{DateTime.Now:yyyy-MM-dd}.json";
            string fileContent = JsonConvert.SerializeObject(steelSpreadSchedule, Formatting.Indented);

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            using (StreamWriter sw = File.CreateText($"{path}\\{fileName}"))
            {
                sw.Write(fileContent);
            }

            var reportView = new MessageView(string.Join("\n", _report));
            reportView.Show();
        }

        #endregion
    }
}
