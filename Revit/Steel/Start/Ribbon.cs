﻿using System;
using System.Windows.Media.Imaging;
using Autodesk.Revit.UI;
using OrslavTeam.Revit.Axillary.PublicInformation;

namespace OrslavTeam.Revit.Steel.Start
{
    public class Ribbon : IExternalApplication
    {
        public Result OnStartup(UIControlledApplication application)
        {
            if (System.Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties().DomainName.ToLower() ==
                "adept-group.biz") return Result.Succeeded;

            //Create ribbon tab
            const string steelTabName = "otSteel";
            application.CreateRibbonTab(steelTabName);

            RibbonPanel modelingPanel = application.CreateRibbonPanel(steelTabName, "Моделювання");
            RibbonPanel typePropertiesPanel = application.CreateRibbonPanel(steelTabName, "Налаштування типів");
            RibbonPanel steelFinishedPanel = application.CreateRibbonPanel(steelTabName, "Відомості");
            
            //Create button for modeling panel
            
            #region Swithc geometry from bracing

            //ButtonData
            var switchGeometryFromBracing = new PushButtonData("switchGeometryFromBracing",
                "Вкл/викл. умовне\nпозначення в'язів",
                PublicParameter.FilePath + "OrslavTeam.Revit.Steel.BracingGeometry.dll",
                "OrslavTeam.Revit.Steel.BracingGeometry.BracingGeometrySwitcher")
            {
                LargeImage = new BitmapImage(new Uri(PublicParameter.FilePath + "res\\Steel\\btn_ico\\geometryBracing.png")),
                ToolTip = "Ввімкнути або вимкнути відображення \"В'язі\""
            };

            modelingPanel.AddItem(switchGeometryFromBracing);

            #endregion

            //Create button for type property panel

            #region ComplexIBeam

            var complexIBeamData = new PushButtonData(
                "complexIBeamData",
                "Двотавр\n(3 листа)",
                $"{PublicParameter.FilePath}OrslavTeam.Revit.Steel.IBeam.exe",
                "OrslavTeam.Revit.Steel.IBeam.Revit.AddInStart")
            {
                LargeImage =
                    new BitmapImage(new Uri($"{PublicParameter.FilePath}res\\Steel\\btn_ico\\iBeam3Sheets.png")),
                ToolTip = "Налаштування параметрів\nдвотавра з трьох листів",
            };

            typePropertiesPanel.AddItem(complexIBeamData);

            #endregion

            //Create button for schedules

            #region MarkCommand

            var steelMarkData = new PushButtonData(
                "steelMarkData",
                "Маркування",
                $"{PublicParameter.FilePath}OrslavTeam.Revit.Steel.Mark.exe",
                "OrslavTeam.Revit.Steel.Mark.Revit.AddInStart")
            {
                LargeImage =
                    new BitmapImage(new Uri($"{PublicParameter.FilePath}res\\Steel\\btn_ico\\steelMark.png")),
                ToolTip = "Виконати маркування"
            };

            steelFinishedPanel.AddItem(steelMarkData);

            #endregion

            #region CreateSchedule

            var createSchedule = new PushButtonData(
                "createSchedule",
                "Створити\nспецифікацію",
                $"{PublicParameter.FilePath}OrslavTeam.Revit.Steel.Schedule.exe",
                "OrslavTeam.Revit.Steel.Schedule.Revit.AddInStart")
            {
                LargeImage =
                    new BitmapImage(new Uri($"{PublicParameter.FilePath}res\\Steel\\btn_ico\\ico_schedule.png")),
                ToolTip = "Виконується єкспорт данних для специфікацій"
            };

            steelFinishedPanel.AddItem(createSchedule);

            #endregion

            #region RunSteelSpreadViewer

            var cheduleViewver = new PushButtonData(
                "scheduleViewer",
                "Переглянути\nспецифікацію",
                $"{PublicParameter.FilePath}OrslavTeam.Revit.Steel.SteelSpreadScheduleViewer.exe",
                "OrslavTeam.Revit.Steel.SteelSpreadScheduleViewer.Revit.AddInStart")
            {
                LargeImage =
                    new BitmapImage(new Uri($"{PublicParameter.FilePath}res\\Steel\\btn_ico\\scheduleView.png")),
                ToolTip = "Перегляд та друк специфікації металопрокату"
            };

            steelFinishedPanel.AddItem(cheduleViewver);

            #endregion

            return Result.Succeeded;
        }

        public Result OnShutdown(UIControlledApplication application)
        {
            return Result.Succeeded;
        }
    }
}